package de.boy132.minecraftcontentexpansion.config;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.loading.FMLPaths;

import java.io.File;
import java.nio.file.Path;

@Mod.EventBusSubscriber
public class ModConfigValues
{
	private static final ForgeConfigSpec.Builder COMMON_BUILDER = new ForgeConfigSpec.Builder();
	private static final ForgeConfigSpec.Builder CLIENT_BUILDER = new ForgeConfigSpec.Builder();
	
	public static ForgeConfigSpec commonConfig;
	public static ForgeConfigSpec clientConfig;
	
	public static ForgeConfigSpec.BooleanValue general_rightClickCropsToHarvest;
	public static ForgeConfigSpec.BooleanValue general_showUpdateMessage;
	
	public static ForgeConfigSpec.BooleanValue modules_moreCrafting;
	
	public static ForgeConfigSpec.BooleanValue modules_stoneAge;
	public static ForgeConfigSpec.BooleanValue stoneAge_removeWoodenToolRecipes;
	public static ForgeConfigSpec.EnumValue<CraftingMode> stoneAge_toolCraftingMode;
	public static ForgeConfigSpec.EnumValue<CraftingMode> stoneAge_leatherCraftingMode;
	public static ForgeConfigSpec.BooleanValue stoneAge_stoneDropsRocks;
	public static ForgeConfigSpec.EnumValue<CraftingMode> stoneAge_woodCraftingMode;
	
	public static ForgeConfigSpec.BooleanValue modules_metalAge;
	public static ForgeConfigSpec.EnumValue<CraftingMode> metalAge_armorCraftingMode;
	
	public static ForgeConfigSpec.BooleanValue modules_industrialAge;
	
	static
	{
		setupClientGeneral();
		
		setupCommonGeneral();
		setupModules();
		
		commonConfig = COMMON_BUILDER.build();
		clientConfig = CLIENT_BUILDER.build();
	}
	
	public static void loadConfigs()
	{
		loadConfig(ModConfigValues.clientConfig, FMLPaths.CONFIGDIR.get().resolve(MinecraftContentExpansion.MODID + "/client.toml"));
		loadConfig(ModConfigValues.commonConfig, FMLPaths.CONFIGDIR.get().resolve(MinecraftContentExpansion.MODID + "/common.toml"));
	}
	
	private static void loadConfig(ForgeConfigSpec spec, Path path)
	{
		File configDir = new File(FMLPaths.CONFIGDIR.get().toFile(), MinecraftContentExpansion.MODID);
		if(!configDir.exists())
			configDir.mkdirs();
		
		CommentedFileConfig configData = CommentedFileConfig.builder(path).sync().autosave().writingMode(WritingMode.REPLACE).build();
		configData.load();
		
		spec.setConfig(configData);
	}
	
	private static void setupClientGeneral()
	{
		CLIENT_BUILDER.comment("General").push("general");
		
		general_showUpdateMessage = CLIENT_BUILDER.comment("Show the update message?")
				.define("showUpdateMessage", true);
		
		CLIENT_BUILDER.pop();
	}
	
	private static void setupCommonGeneral()
	{
		COMMON_BUILDER.comment("General").push("general");
		
		general_rightClickCropsToHarvest = COMMON_BUILDER.comment("Right click crops to harvest them?")
				.define("rightClickCropsToHarvest", true);
		
		COMMON_BUILDER.pop();
	}
	
	private static void setupModules()
	{
		COMMON_BUILDER.comment("Modules").push("modules");
		
		modules_moreCrafting = COMMON_BUILDER.comment("Enable 'More Crafting' Module? (Currently not used)")
				.define("moduleMoreCrafting", true);
		
		setupStoneAgeSettings();
		setupMetalAgeSettings();
		setupIndustrialAgeSettings();
		
		COMMON_BUILDER.pop();
	}
	
	private static void setupStoneAgeSettings()
	{
		COMMON_BUILDER.comment("Stone Age").push("stoneAge");
		
		modules_stoneAge = COMMON_BUILDER.comment("Enable 'Stone Age' Module?")
				.define("moduleEnabled", true);
		
		stoneAge_removeWoodenToolRecipes = COMMON_BUILDER.comment("Remove recipes for wooden tools?")
				.define("removeWoodenToolRecipes", true);
		
		stoneAge_toolCraftingMode = COMMON_BUILDER.comment("Set to ADVANCED for tool crafting with heads and to VANILLA for normal recipes.")
				.defineEnum("toolCraftingMode", CraftingMode.ADVANCED);
		
		stoneAge_leatherCraftingMode = COMMON_BUILDER.comment("Set to ADVANCED for leather crafting with drying racks and to VANILLA for normal 'crafting'.")
				.defineEnum("leatherCraftingMode", CraftingMode.ADVANCED);
		
		stoneAge_stoneDropsRocks = COMMON_BUILDER.comment("Should stones (stone, andesite, diorite, granite) drop rocks instead of the block?")
				.define("stoneDropsRocks", true);
		
		stoneAge_woodCraftingMode = COMMON_BUILDER.comment("Set to ADVANCED for wood crafting with chopping blocks and to VANILLA for normal crafting.")
				.defineEnum("woodCraftingMode", CraftingMode.ADVANCED);
		
		COMMON_BUILDER.pop();
	}
	
	private static void setupMetalAgeSettings()
	{
		COMMON_BUILDER.comment("Metal Age").push("metalAge");
		
		modules_metalAge = COMMON_BUILDER.comment("Enable 'Metal Age' Module?")
				.define("moduleEnabled", true);
		
		metalAge_armorCraftingMode = COMMON_BUILDER.comment("Set to ADVANCED for armor crafting with plates and to VANILLA for normal recipes.")
				.defineEnum("armorCraftingMode", CraftingMode.ADVANCED);
		
		COMMON_BUILDER.pop();
	}
	
	private static void setupIndustrialAgeSettings()
	{
		COMMON_BUILDER.comment("Industrial Age").push("industrialAge");
		
		modules_industrialAge = COMMON_BUILDER.comment("Enable 'Industrial Age' Module?")
				.define("moduleEnabled", true);
		
		COMMON_BUILDER.pop();
	}
}
