package de.boy132.minecraftcontentexpansion.event;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.ElectricGreenhouseBlockEntityRenderer;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.liquidtank.LiquidTankBlockEntityRenderer;
import de.boy132.minecraftcontentexpansion.block.entity.choppingblock.ChoppingBlockEntityRenderer;
import de.boy132.minecraftcontentexpansion.block.entity.dryingrack.DryingRackBlockEntityRenderer;
import de.boy132.minecraftcontentexpansion.entity.ModEntityTypes;
import de.boy132.minecraftcontentexpansion.fluid.ModFluids;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.keybinds.ModKeyMappings;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.backpack.BackpackScreen;
import de.boy132.minecraftcontentexpansion.screen.battery.BatteryScreen;
import de.boy132.minecraftcontentexpansion.screen.coalgenerator.CoalGeneratorScreen;
import de.boy132.minecraftcontentexpansion.screen.electricbrewery.ElectricBreweryScreen;
import de.boy132.minecraftcontentexpansion.screen.electricgreenhouse.ElectricGreenhouseScreen;
import de.boy132.minecraftcontentexpansion.screen.electricsmelter.ElectricSmelterScreen;
import de.boy132.minecraftcontentexpansion.screen.hydraulicpress.HydraulicPressScreen;
import de.boy132.minecraftcontentexpansion.screen.kiln.KilnScreen;
import de.boy132.minecraftcontentexpansion.screen.liquidtank.LiquidTankScreen;
import de.boy132.minecraftcontentexpansion.screen.millstone.MillstoneScreen;
import de.boy132.minecraftcontentexpansion.screen.smelter.SmelterScreen;
import de.boy132.minecraftcontentexpansion.screen.solarpanel.SolarPanelScreen;
import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.client.renderer.BiomeColors;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.ThrownItemRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.GrassColor;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.client.event.RegisterColorHandlersEvent;
import net.minecraftforge.client.event.RegisterKeyMappingsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.registries.ForgeRegistries;

@Mod.EventBusSubscriber(modid = MinecraftContentExpansion.MODID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientModEvents
{
	@SubscribeEvent
	public static void clientSetup(FMLClientSetupEvent event)
	{
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.TOMATOES.get(), RenderType.cutout());
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.GRAPES.get(), RenderType.cutout());
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.STRAWBERRIES.get(), RenderType.cutout());
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.CORNS.get(), RenderType.cutout());
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.LETTUCES.get(), RenderType.cutout());
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.ONIONS.get(), RenderType.cutout());
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.RICE.get(), RenderType.cutout());
		
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.DIRT_PATH_SLAB.get(), RenderType.cutout());
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.GRASS_BLOCK_SLAB.get(), RenderType.cutoutMipped());
		
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.GLASS_SLAB.get(), RenderType.cutout());
		
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.STONE_DOOR.get(), RenderType.cutout());
		
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.GLASS_DOOR.get(), RenderType.cutout());
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.GLASS_TRAPDOOR.get(), RenderType.cutout());
		
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.WOODEN_GLASS_FRAME.get(), RenderType.cutout());
		
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.LIQUID_TANK.get(), RenderType.cutout());
		
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.ELECTRIC_GREENHOUSE.get(), RenderType.cutout());
		
		ItemBlockRenderTypes.setRenderLayer(ModBlocks.COPPER_CABLE.get(), RenderType.cutout());
		
		ItemBlockRenderTypes.setRenderLayer(ModFluids.OIL.get(), RenderType.translucent());
		ItemBlockRenderTypes.setRenderLayer(ModFluids.FLOWING_OIL.get(), RenderType.translucent());
		
		MenuScreens.register(ModMenuTypes.KILN.get(), KilnScreen::new);
		MenuScreens.register(ModMenuTypes.SMELTER.get(), SmelterScreen::new);
		MenuScreens.register(ModMenuTypes.MILLSTONE.get(), MillstoneScreen::new);
		
		MenuScreens.register(ModMenuTypes.COAL_GENERATOR.get(), CoalGeneratorScreen::new);
		MenuScreens.register(ModMenuTypes.BATTERY.get(), BatteryScreen::new);
		MenuScreens.register(ModMenuTypes.SOLAR_PANEL.get(), SolarPanelScreen::new);
		
		MenuScreens.register(ModMenuTypes.LIQUID_TANK.get(), LiquidTankScreen::new);
		
		MenuScreens.register(ModMenuTypes.ELECTRIC_BREWERY.get(), ElectricBreweryScreen::new);
		MenuScreens.register(ModMenuTypes.ELECTRIC_SMELTER.get(), ElectricSmelterScreen::new);
		MenuScreens.register(ModMenuTypes.ELECTRIC_GREENHOUSE.get(), ElectricGreenhouseScreen::new);
		
		MenuScreens.register(ModMenuTypes.HYDRAULIC_PRESS.get(), HydraulicPressScreen::new);
		
		MenuScreens.register(ModMenuTypes.BACKPACK.get(), BackpackScreen::new);
	}
	
	@SubscribeEvent
	public static void registerEntityRenderers(EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(ModEntityTypes.ROCK.get(), getEntityRendererProvider(ModItems.ROCK.get()));
		event.registerEntityRenderer(ModEntityTypes.ANDESITE_ROCK.get(), getEntityRendererProvider(ModItems.ANDESITE_ROCK.get()));
		event.registerEntityRenderer(ModEntityTypes.DIORITE_ROCK.get(), getEntityRendererProvider(ModItems.DIORITE_ROCK.get()));
		event.registerEntityRenderer(ModEntityTypes.GRANITE_ROCK.get(), getEntityRendererProvider(ModItems.GRANITE_ROCK.get()));
		
		event.registerBlockEntityRenderer(ModBlockEntities.CHOPPING_BLOCK.get(), ChoppingBlockEntityRenderer::new);
		
		event.registerBlockEntityRenderer(ModBlockEntities.DRYING_RACK.get(), DryingRackBlockEntityRenderer::new);
		
		event.registerBlockEntityRenderer(ModBlockEntities.LIQUID_TANK.get(), LiquidTankBlockEntityRenderer::new);
		
		event.registerBlockEntityRenderer(ModBlockEntities.ELECTRIC_GREENHOUSE.get(), ElectricGreenhouseBlockEntityRenderer::new);
	}
	
	private static EntityRendererProvider getEntityRendererProvider(Item item)
	{
		return (EntityRendererProvider<Entity>) context -> new ThrownItemRenderer(context)
		{
			@Override
			public ResourceLocation getTextureLocation(Entity entity)
			{
				return ForgeRegistries.ITEMS.getKey(item);
			}
		};
	}
	
	@SubscribeEvent
	public static void registerKeyMappings(RegisterKeyMappingsEvent event)
	{
		event.register(ModKeyMappings.OPEN_BACKPACK);
	}
	
	@SubscribeEvent
	public static void registerBlockColorHandlersEvent(RegisterColorHandlersEvent.Block event)
	{
		event.register((state, blockAndTintGetter, pos, index) -> blockAndTintGetter != null && pos != null ? BiomeColors.getAverageGrassColor(blockAndTintGetter, pos) : GrassColor.get(0.5D, 1.0D), ModBlocks.GRASS_BLOCK_SLAB.get());
	}
	
	@SubscribeEvent
	public static void registerItemColorHandlersEvent(RegisterColorHandlersEvent.Item event)
	{
		event.register((stack, index) -> GrassColor.get(0.5D, 1.0D), ModBlocks.GRASS_BLOCK_SLAB.get());
	}
}
