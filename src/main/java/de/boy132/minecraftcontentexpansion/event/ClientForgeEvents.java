package de.boy132.minecraftcontentexpansion.event;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.config.ModConfigValues;
import de.boy132.minecraftcontentexpansion.item.backpack.BackpackItem;
import de.boy132.minecraftcontentexpansion.item.backpack.EnderBackpackItem;
import de.boy132.minecraftcontentexpansion.keybinds.ModKeyMappings;
import de.boy132.minecraftcontentexpansion.networking.ModNetworkMessages;
import de.boy132.minecraftcontentexpansion.networking.packet.ServerboundOpenBackpackPacket;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.VersionChecker;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.loading.FMLConfig;
import net.minecraftforge.forgespi.language.IModInfo;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotResult;

import java.util.Optional;

@Mod.EventBusSubscriber(modid = MinecraftContentExpansion.MODID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class ClientForgeEvents
{
	@SubscribeEvent
	public static void onClientTick(TickEvent.ClientTickEvent event)
	{
		if(!ModList.get().isLoaded("curios"))
			return;
		
		if(event.phase == TickEvent.Phase.END)
		{
			Player player = Minecraft.getInstance().player;
			if(player != null && ModKeyMappings.OPEN_BACKPACK.consumeClick())
			{
				Optional<SlotResult> backpackCurio = CuriosApi.getCuriosHelper().findFirstCurio(player, (stack) -> stack.getItem() instanceof BackpackItem || stack.getItem() instanceof EnderBackpackItem);
				backpackCurio.ifPresent(slotResult ->
				{
					ItemStack stack = slotResult.stack();
					if(!stack.isEmpty() && (stack.getItem() instanceof BackpackItem || stack.getItem() instanceof EnderBackpackItem))
						ModNetworkMessages.sendToServer(new ServerboundOpenBackpackPacket());
				});
			}
		}
	}
	
	@SubscribeEvent
	public static void onPlayerLoggedIn(PlayerEvent.PlayerLoggedInEvent event)
	{
		if(ModConfigValues.general_showUpdateMessage.get() && FMLConfig.getBoolConfigValue(FMLConfig.ConfigValue.VERSION_CHECK))
		{
			Player player = event.getEntity();
			
			IModInfo modInfo = MinecraftContentExpansion.CONTAINER.getModInfo();
			VersionChecker.CheckResult result = VersionChecker.getResult(modInfo);
			if(result.status() == VersionChecker.Status.UP_TO_DATE)
				player.sendSystemMessage(Component.translatable("updater." + MinecraftContentExpansion.MODID + ".uptodate"));
			else if(result.status() == VersionChecker.Status.BETA || result.status() == VersionChecker.Status.AHEAD)
				player.sendSystemMessage(Component.translatable("updater." + MinecraftContentExpansion.MODID + ".beta"));
			else if(result.status() == VersionChecker.Status.OUTDATED || result.status() == VersionChecker.Status.BETA_OUTDATED)
				player.sendSystemMessage(Component.translatable("updater." + MinecraftContentExpansion.MODID + ".outdated", modInfo.getVersion().toString(), result.target().toString()));
			else
				player.sendSystemMessage(Component.translatable("updater." + MinecraftContentExpansion.MODID + ".failed"));
		}
	}
}
