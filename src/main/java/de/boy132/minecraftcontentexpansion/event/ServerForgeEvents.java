package de.boy132.minecraftcontentexpansion.event;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.crop.DoubleCropBlock;
import de.boy132.minecraftcontentexpansion.block.entity.choppingblock.ChoppingBlock;
import de.boy132.minecraftcontentexpansion.config.ModConfigValues;
import de.boy132.minecraftcontentexpansion.item.BarkItem;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.core.BlockPos;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TieredItem;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.TierSortingRegistry;
import net.minecraftforge.common.ToolActions;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.level.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = MinecraftContentExpansion.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class ServerForgeEvents
{
	@SubscribeEvent
	public static void onLeftClickBlock(PlayerInteractEvent.LeftClickBlock event)
	{
		if(event.getSide() == LogicalSide.CLIENT || event.getHand() == InteractionHand.OFF_HAND)
			return;
		
		BlockPos pos = event.getPos();
		BlockState state = event.getLevel().getBlockState(pos);
		
		if(state.getBlock() instanceof ChoppingBlock choppingBlock)
		{
			if(choppingBlock.handleWoodWorking(event.getEntity(), pos))
			{
				event.setCanceled(true);
				event.setCancellationResult(InteractionResult.SUCCESS);
			}
		}
	}
	
	@SubscribeEvent
	public static void onRightClickBlock(PlayerInteractEvent.RightClickBlock event)
	{
		if(event.getSide() == LogicalSide.CLIENT || event.getHand() == InteractionHand.OFF_HAND)
			return;
		
		if(ModConfigValues.general_rightClickCropsToHarvest.get())
		{
			Level level = event.getLevel();
			BlockPos pos = event.getPos();
			BlockState state = level.getBlockState(pos);
			
			if(state.getBlock() instanceof CropBlock crop)
			{
				boolean isDoubleCrop = state.getBlock() instanceof DoubleCropBlock;
				if(isDoubleCrop && state.getValue(DoubleCropBlock.HALF) == DoubleBlockHalf.UPPER)
					return;
				
				Player player = event.getEntity();
				if(crop.isMaxAge(state) && !player.isShiftKeyDown())
				{
					state.getBlock().playerDestroy(level, player, pos, state, null, player.getItemInHand(InteractionHand.MAIN_HAND));
					
					if(isDoubleCrop)
						DoubleCropBlock.placeAt(level, crop.defaultBlockState(), pos, 3);
					else
						level.setBlock(pos, crop.defaultBlockState(), 3);
					
					event.setCanceled(true);
					event.setCancellationResult(InteractionResult.SUCCESS);
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onBlockModified(BlockEvent.BlockToolModificationEvent event)
	{
		if(event.isSimulated())
			return;
		
		if(event.getToolAction() == ToolActions.AXE_STRIP && BarkItem.isValidLog(event.getState().getBlock()))
		{
			LevelAccessor levelAccessor = event.getLevel();
			BlockPos pos = event.getPos();
			levelAccessor.addFreshEntity(new ItemEntity((Level) levelAccessor, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, new ItemStack(ModItems.TREE_BARK.get())));
		}
	}
	
	@SubscribeEvent
	public static void onHarvestCheck(PlayerEvent.HarvestCheck event)
	{
		BlockState targetBlock = event.getTargetBlock();
		if(targetBlock.is(BlockTags.LOGS))
		{
			boolean canHarvest = false;
			
			ItemStack mainHand = event.getEntity().getMainHandItem();
			if(mainHand.getItem() instanceof TieredItem tieredItem)
				canHarvest = TierSortingRegistry.isCorrectTierForDrops(tieredItem.getTier(), targetBlock);
			
			event.setCanHarvest(canHarvest && mainHand.canPerformAction(ToolActions.AXE_DIG));
		}
	}
	
	@SubscribeEvent
	public static void onBlockBreak(BlockEvent.BreakEvent event)
	{
		BlockState state = event.getState();
		if(state.is(BlockTags.LOGS))
		{
			Player player = event.getPlayer();
			if(!ForgeHooks.isCorrectToolForDrops(state, player))
				player.hurt(((Level) event.getLevel()).damageSources().generic(), 2f);
		}
	}
}
