package de.boy132.minecraftcontentexpansion.loot.condition;

import com.google.common.base.Suppliers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.registries.ForgeRegistries;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

public class ReplaceLootModifier extends LootModifier
{
	public static final Supplier<Codec<ReplaceLootModifier>> CODEC = Suppliers.memoize(() -> RecordCodecBuilder.create(inst -> codecStart(inst).and(inst.group(ForgeRegistries.ITEMS.getCodec().fieldOf("olditem").forGetter(m -> m.oldItem), ForgeRegistries.ITEMS.getCodec().fieldOf("newitem").forGetter(m -> m.newItem), Codec.INT.fieldOf("minamount").forGetter(m -> m.minAmount), Codec.INT.fieldOf("maxamount").forGetter(m -> m.maxAmount), Codec.BOOL.fieldOf("ignoresilktouch").forGetter(m -> m.ignoreSilkTouch))).apply(inst, ReplaceLootModifier::new)));
	
	private final Item oldItem;
	private final Item newItem;
	
	private final int minAmount;
	private final int maxAmount;
	
	private final boolean ignoreSilkTouch;
	
	public ReplaceLootModifier(LootItemCondition[] conditions, Item oldItem, Item newItem, int minAmount, int maxAmount, boolean ignoreSilkTouch)
	{
		super(conditions);
		
		this.oldItem = oldItem;
		this.newItem = newItem;
		
		this.minAmount = minAmount;
		this.maxAmount = maxAmount;
		
		this.ignoreSilkTouch = ignoreSilkTouch;
	}
	
	@Override
	protected @NotNull ObjectArrayList<ItemStack> doApply(ObjectArrayList<ItemStack> generatedLoot, LootContext context)
	{
		if(!ignoreSilkTouch)
		{
			ItemStack tool = context.getParamOrNull(LootContextParams.TOOL);
			if(tool != null && EnchantmentHelper.getEnchantments(tool).containsKey(Enchantments.SILK_TOUCH))
				return generatedLoot;
		}
		
		if(generatedLoot.removeIf((itemStack) -> itemStack.getItem() == oldItem))
			generatedLoot.add(new ItemStack(newItem, minAmount + context.getRandom().nextInt(maxAmount)));
		
		return generatedLoot;
	}
	
	@Override
	public Codec<? extends ReplaceLootModifier> codec()
	{
		return CODEC.get();
	}
}
