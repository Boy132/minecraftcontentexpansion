package de.boy132.minecraftcontentexpansion.loot.condition;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.level.storage.loot.predicates.LootItemConditionType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

public class ModLootItemConditionTypes
{
	private static final DeferredRegister<LootItemConditionType> LOOT_ITEM_CONDITION_TYPES = DeferredRegister.create(BuiltInRegistries.LOOT_CONDITION_TYPE.key(), MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<LootItemConditionType> MODULE_SETTING = LOOT_ITEM_CONDITION_TYPES.register("module_setting", () -> new LootItemConditionType(ModuleSettingLootItemCondition.CODEC));
	
	public static void register(IEventBus eventBus)
	{
		LOOT_ITEM_CONDITION_TYPES.register(eventBus);
	}
}
