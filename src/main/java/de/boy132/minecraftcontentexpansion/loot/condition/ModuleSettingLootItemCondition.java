package de.boy132.minecraftcontentexpansion.loot.condition;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.config.CraftingMode;
import de.boy132.minecraftcontentexpansion.config.ModConfigValues;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemConditionType;

public class ModuleSettingLootItemCondition implements LootItemCondition
{
	public static final ModuleSettingLootItemCondition STONE_AGE_LEATHER_CRAFTING_MODE_VANILLA = new ModuleSettingLootItemCondition("stoneAge", "leatherCraftingModeVanilla");
	public static final ModuleSettingLootItemCondition STONE_AGE_LEATHER_CRAFTING_MODE_ADVANCED = new ModuleSettingLootItemCondition("stoneAge", "leatherCraftingModeAdvanced");
	
	public static final ModuleSettingLootItemCondition STONE_AGE_STONE_DROPS_ROCKS = new ModuleSettingLootItemCondition("stoneAge", "stoneDropsRocks");
	
	public static final Codec<ModuleSettingLootItemCondition> CODEC = RecordCodecBuilder.create(instance -> instance.group(Codec.STRING.fieldOf("module").forGetter(condition -> condition.module), Codec.STRING.fieldOf("setting").forGetter(condition -> condition.setting)).apply(instance, ModuleSettingLootItemCondition::new));
	
	protected final String module;
	protected final String setting;
	
	public ModuleSettingLootItemCondition(String module, String setting)
	{
		this.module = module;
		this.setting = setting;
	}
	
	@Override
	public LootItemConditionType getType()
	{
		return ModLootItemConditionTypes.MODULE_SETTING.get();
	}
	
	@Override
	public boolean test(LootContext lootContext)
	{
		if(module.equalsIgnoreCase("stoneAge"))
		{
			if(ModConfigValues.modules_stoneAge.get())
			{
				if(setting.equalsIgnoreCase("leatherCraftingModeVanilla"))
					return ModConfigValues.stoneAge_leatherCraftingMode.get() == CraftingMode.VANILLA;
				
				if(setting.equalsIgnoreCase("leatherCraftingModeAdvanced"))
					return ModConfigValues.stoneAge_leatherCraftingMode.get() == CraftingMode.ADVANCED;
				
				if(setting.equalsIgnoreCase("stoneDropsRocks"))
					return ModConfigValues.stoneAge_stoneDropsRocks.get();
			}
		}
		
		if(module.equalsIgnoreCase("metalAge"))
			return ModConfigValues.modules_metalAge.get();
		
		if(module.equalsIgnoreCase("industrialAge"))
			return ModConfigValues.modules_industrialAge.get();
		
		return false;
	}
}
