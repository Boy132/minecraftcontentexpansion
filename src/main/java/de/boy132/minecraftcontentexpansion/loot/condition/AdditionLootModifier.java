package de.boy132.minecraftcontentexpansion.loot.condition;

import com.google.common.base.Suppliers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.registries.ForgeRegistries;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

public class AdditionLootModifier extends LootModifier
{
	public static final Supplier<Codec<AdditionLootModifier>> CODEC = Suppliers.memoize(() -> RecordCodecBuilder.create(inst -> codecStart(inst).and(inst.group(ForgeRegistries.ITEMS.getCodec().fieldOf("addition").forGetter(m -> m.addition), Codec.FLOAT.fieldOf("chance").forGetter(m -> m.chance), Codec.INT.fieldOf("minamount").forGetter(m -> m.minAmount), Codec.INT.fieldOf("maxamount").forGetter(m -> m.maxAmount), Codec.BOOL.fieldOf("ignoresilktouch").forGetter(m -> m.ignoreSilkTouch))).apply(inst, AdditionLootModifier::new)));
	
	private final Item addition;
	
	private final float chance;
	private final int minAmount;
	private final int maxAmount;
	
	private final boolean ignoreSilkTouch;
	
	public AdditionLootModifier(LootItemCondition[] conditions, Item addition, float chance, int minAmount, int maxAmount, boolean ignoreSilkTouch)
	{
		super(conditions);
		this.addition = addition;
		
		this.chance = chance;
		this.minAmount = maxAmount;
		this.maxAmount = maxAmount;
		
		this.ignoreSilkTouch = ignoreSilkTouch;
	}
	
	@Override
	protected @NotNull ObjectArrayList<ItemStack> doApply(ObjectArrayList<ItemStack> generatedLoot, LootContext context)
	{
		if(!ignoreSilkTouch)
		{
			ItemStack tool = context.getParamOrNull(LootContextParams.TOOL);
			if(tool != null && EnchantmentHelper.getEnchantments(tool).containsKey(Enchantments.SILK_TOUCH))
				return generatedLoot;
		}
		
		RandomSource random = context.getRandom();
		
		if(random.nextFloat() < chance)
			generatedLoot.add(new ItemStack(addition, minAmount + random.nextInt(maxAmount)));
		
		return generatedLoot;
	}
	
	@Override
	public Codec<? extends AdditionLootModifier> codec()
	{
		return CODEC.get();
	}
}
