package de.boy132.minecraftcontentexpansion.loot;

import com.mojang.serialization.Codec;
import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.loot.condition.AdditionLootModifier;
import de.boy132.minecraftcontentexpansion.loot.condition.ReplaceLootModifier;
import net.minecraftforge.common.loot.IGlobalLootModifier;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModGlobalLootModifierSerializers
{
	private static final DeferredRegister<Codec<? extends IGlobalLootModifier>> GLOBAL_LOOT_MODIFIER_SERIALIZERS = DeferredRegister.create(ForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<Codec<AdditionLootModifier>> ADDITION_SERIALIZER = GLOBAL_LOOT_MODIFIER_SERIALIZERS.register("addition", AdditionLootModifier.CODEC);
	public static final RegistryObject<Codec<ReplaceLootModifier>> REPLACE_SERIALIZER = GLOBAL_LOOT_MODIFIER_SERIALIZERS.register("replace", ReplaceLootModifier.CODEC);
	
	public static void register(IEventBus eventBus)
	{
		GLOBAL_LOOT_MODIFIER_SERIALIZERS.register(eventBus);
	}
}
