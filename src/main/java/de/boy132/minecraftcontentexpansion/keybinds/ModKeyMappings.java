package de.boy132.minecraftcontentexpansion.keybinds;

import com.mojang.blaze3d.platform.InputConstants;
import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.client.KeyMapping;
import net.minecraftforge.client.settings.KeyConflictContext;
import org.lwjgl.glfw.GLFW;

public class ModKeyMappings
{
	public static final String KEY_CATEGORY = "key.categories." + MinecraftContentExpansion.MODID;
	
	public static final KeyMapping OPEN_BACKPACK = new KeyMapping("key." + MinecraftContentExpansion.MODID + ".open_backpack", KeyConflictContext.IN_GAME, InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_B, KEY_CATEGORY);
}
