package de.boy132.minecraftcontentexpansion.screen.liquidtank;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.liquidtank.LiquidTankBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotFluidBucket;
import de.boy132.minecraftcontentexpansion.screen.SlotOutput;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;

public class LiquidTankMenu extends BaseContainerMenu
{
	public LiquidTankMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()));
	}
	
	public LiquidTankMenu(int id, Inventory inventory, BlockEntity blockEntity)
	{
		super(ModMenuTypes.LIQUID_TANK.get(), id, blockEntity, null);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory);
		addPlayerHotbar(inventory);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotFluidBucket((AdvancedBlockEntity) blockEntity, itemHandler, LiquidTankBlockEntity.INPUT_SLOT, 60, 29));
			addSlot(new SlotOutput(itemHandler, LiquidTankBlockEntity.OUTPUT_SLOT, 100, 29));
		});
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.LIQUID_TANK.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return 2;
	}
}
