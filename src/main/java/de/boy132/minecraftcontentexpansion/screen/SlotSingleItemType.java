package de.boy132.minecraftcontentexpansion.screen;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotSingleItemType extends SlotItemHandler
{
	private final ItemStack itemStack;
	
	public SlotSingleItemType(ItemLike itemType, IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		super(itemHandler, index, xPosition, yPosition);
		this.itemStack = new ItemStack(itemType);
	}
	
	@Override
	public boolean mayPlace(ItemStack stack)
	{
		return ItemStack.isSameItem(stack, itemStack);
	}
}

