package de.boy132.minecraftcontentexpansion.screen.solarpanel;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.solarpanel.SolarPanelBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.level.block.entity.BlockEntity;

public class SolarPanelMenu extends BaseContainerMenu
{
	public SolarPanelMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(1));
	}
	
	public SolarPanelMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.SOLAR_PANEL.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory, 56);
		addPlayerHotbar(inventory, 56);
		
		addDataSlots(data);
	}
	
	public boolean canSeeSky()
	{
		return ((SolarPanelBlockEntity) blockEntity).canSeeSky();
	}
	
	public boolean isDayTime()
	{
		return ((SolarPanelBlockEntity) blockEntity).isDayTime();
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.SOLAR_PANEL.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return 0;
	}
}
