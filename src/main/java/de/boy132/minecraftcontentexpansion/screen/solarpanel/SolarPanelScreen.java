package de.boy132.minecraftcontentexpansion.screen.solarpanel;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerScreen;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class SolarPanelScreen extends BaseContainerScreen<SolarPanelMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/solar_panel.png");
	
	public SolarPanelScreen(SolarPanelMenu solarPanelMenu, Inventory inventory, Component component)
	{
		super(solarPanelMenu, inventory, component);
		imageHeight -= 28;
		inventoryLabelY -= 28;
	}
	
	@Override
	protected void renderLabels(GuiGraphics guiGraphics, int mouseX, int mouseY)
	{
		super.renderLabels(guiGraphics, mouseX, mouseY);
		renderAreaTooltip(guiGraphics, mouseX, mouseY, menu.getEnergyTooltips(), 9, 19, 139, 18);
		
		if(!menu.canSeeSky())
			renderAreaTooltip(guiGraphics, mouseX, mouseY, Component.translatable("inventory." + MinecraftContentExpansion.MODID + ".solar_panel.cantseesky"), 152, 20, 16, 16);
		
		if(!menu.isDayTime())
			renderAreaTooltip(guiGraphics, mouseX, mouseY, Component.translatable("inventory." + MinecraftContentExpansion.MODID + ".solar_panel.nighttime"), 152, 20, 16, 16);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(guiGraphics, partialTicks, mouseX, mouseY);
		renderEnergyBarHorizontal(guiGraphics, 137, 10, 20, 0, 138);
		
		if(!menu.canSeeSky())
			guiGraphics.blit(getBackgroundTexture(), leftPos + 152, topPos + 20, 176, 0, 16, 16);
		else if(!menu.isDayTime())
			guiGraphics.blit(getBackgroundTexture(), leftPos + 152, topPos + 20, 176, 16, 16, 16);
		else
			guiGraphics.blit(getBackgroundTexture(), leftPos + 152, topPos + 20, 176, 32, 16, 16);
	}
	
	@Override
	protected ResourceLocation getBackgroundTexture()
	{
		return BACKGROUND;
	}
}
