package de.boy132.minecraftcontentexpansion.screen;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseContainerMenu extends AbstractContainerMenu
{
	protected final BlockEntity blockEntity;
	protected final Level level;
	protected final ContainerData data;
	
	protected BaseContainerMenu(MenuType<?> menuType, int id, BlockEntity blockEntity, ContainerData data)
	{
		super(menuType, id);
		this.blockEntity = blockEntity;
		this.level = blockEntity.getLevel();
		this.data = data;
	}
	
	protected void addPlayerInventory(Inventory playerInventory, int yOffset)
	{
		for(int i = 0; i < 3; ++i)
			for(int l = 0; l < 9; ++l)
				addSlot(new Slot(playerInventory, l + i * 9 + 9, 8 + l * 18, yOffset + i * 18));
	}
	
	protected void addPlayerInventory(Inventory playerInventory)
	{
		addPlayerInventory(playerInventory, 84);
	}
	
	protected void addPlayerHotbar(Inventory playerInventory, int yOffset)
	{
		for(int i = 0; i < 9; ++i)
			addSlot(new Slot(playerInventory, i, 8 + i * 18, yOffset + 58));
	}
	
	protected void addPlayerHotbar(Inventory playerInventory)
	{
		addPlayerHotbar(playerInventory, 84);
	}
	
	public int getEnergyStoredScaled(int pixels)
	{
		IEnergyStorage energyStorage = blockEntity.getCapability(ForgeCapabilities.ENERGY).orElse(null);
		if(energyStorage != null)
		{
			int energyStored = energyStorage.getEnergyStored();
			int maxEnergy = energyStorage.getMaxEnergyStored();
			
			return energyStored != 0 && maxEnergy != 0 ? energyStored * pixels / maxEnergy : 0;
		}
		
		return 0;
	}
	
	public List<Component> getEnergyTooltips()
	{
		List<Component> toolTips = new ArrayList<>();
		
		blockEntity.getCapability(ForgeCapabilities.ENERGY).ifPresent(energyStorage ->
		{
			NumberFormat numberFormat = NumberFormat.getIntegerInstance();
			toolTips.add(Component.literal(numberFormat.format(energyStorage.getEnergyStored()) + " FE"));
			
			int energyIO = data.get(0);
			if(energyIO > 0)
				toolTips.add(Component.literal(ChatFormatting.GREEN + "+" + numberFormat.format(energyIO) + " FE/ Tick"));
			else if(energyIO < 0)
				toolTips.add(Component.literal(ChatFormatting.RED + numberFormat.format(energyIO) + " FE/ Tick"));
		});
		
		return toolTips;
	}
	
	public FluidStack getFluidStack()
	{
		IFluidHandler fluidHandler = blockEntity.getCapability(ForgeCapabilities.FLUID_HANDLER).orElse(null);
		if(fluidHandler != null)
			return fluidHandler.getFluidInTank(0);
		
		return FluidStack.EMPTY;
	}
	
	public int getFluidStoredScaled(int pixels)
	{
		IFluidHandler fluidHandler = blockEntity.getCapability(ForgeCapabilities.FLUID_HANDLER).orElse(null);
		if(fluidHandler != null)
		{
			int fluidStored = fluidHandler.getFluidInTank(0).getAmount();
			int maxFluid = fluidHandler.getTankCapacity(0);
			
			return fluidStored != 0 && maxFluid != 0 ? fluidStored * pixels / maxFluid : 0;
		}
		
		return 0;
	}
	
	public List<Component> getFluidTooltips()
	{
		List<Component> toolTips = new ArrayList<>();
		
		blockEntity.getCapability(ForgeCapabilities.FLUID_HANDLER).ifPresent(fluidHandler ->
		{
			FluidStack fluidStack = fluidHandler.getFluidInTank(0);
			toolTips.add(fluidStack.isEmpty() ? Component.literal("Empty") : fluidStack.getFluid().getFluidType().getDescription());
			
			NumberFormat numberFormat = NumberFormat.getIntegerInstance();
			toolTips.add(Component.literal(numberFormat.format(fluidStack.getAmount()) + " mB"));
		});
		
		return toolTips;
	}
	
	// CREDIT GOES TO: diesieben07 | https://github.com/diesieben07/SevenCommons
	// must assign a slot number to each of the slots used by the GUI.
	// For this container, we can see both the tile inventory's slots as well as the player inventory slots and the hotbar.
	// Each time we add a Slot to the container, it automatically increases the slotIndex, which means
	//  0 - 8 = hotbar slots (which will map to the InventoryPlayer slot numbers 0 - 8)
	//  9 - 35 = player inventory slots (which map to the InventoryPlayer slot numbers 9 - 35)
	//  36 - 44 = TileInventory slots, which map to our TileEntity slot numbers 0 - 8)
	private static final int HOTBAR_SLOT_COUNT = 9;
	private static final int PLAYER_INVENTORY_ROW_COUNT = 3;
	private static final int PLAYER_INVENTORY_COLUMN_COUNT = 9;
	private static final int PLAYER_INVENTORY_SLOT_COUNT = PLAYER_INVENTORY_COLUMN_COUNT * PLAYER_INVENTORY_ROW_COUNT;
	private static final int VANILLA_SLOT_COUNT = HOTBAR_SLOT_COUNT + PLAYER_INVENTORY_SLOT_COUNT;
	private static final int VANILLA_FIRST_SLOT_INDEX = 0;
	private static final int BE_INVENTORY_FIRST_SLOT_INDEX = VANILLA_FIRST_SLOT_INDEX + VANILLA_SLOT_COUNT;
	
	protected abstract int getInventorySlotCount();
	
	@Override
	public ItemStack quickMoveStack(Player player, int index)
	{
		int BE_INVENTORY_SLOT_COUNT = getInventorySlotCount();
		
		Slot sourceSlot = slots.get(index);
		if(sourceSlot == null || !sourceSlot.hasItem())
			return ItemStack.EMPTY;
		
		ItemStack sourceStack = sourceSlot.getItem();
		ItemStack copyOfSourceStack = sourceStack.copy();
		
		// Check if the slot clicked is one of the vanilla container slots
		if(index < VANILLA_FIRST_SLOT_INDEX + VANILLA_SLOT_COUNT)
		{
			// This is a vanilla container slot so merge the stack into the tile inventory
			if(!moveItemStackTo(sourceStack, BE_INVENTORY_FIRST_SLOT_INDEX, BE_INVENTORY_FIRST_SLOT_INDEX + BE_INVENTORY_SLOT_COUNT, false))
				return ItemStack.EMPTY;
		} else if(index < BE_INVENTORY_FIRST_SLOT_INDEX + BE_INVENTORY_SLOT_COUNT)
		{
			// This is a BE slot so merge the stack into the players inventory
			if(!moveItemStackTo(sourceStack, VANILLA_FIRST_SLOT_INDEX, VANILLA_FIRST_SLOT_INDEX + VANILLA_SLOT_COUNT, false))
				return ItemStack.EMPTY;
		} else
		{
			System.out.println("Invalid slotIndex:" + index);
			return ItemStack.EMPTY;
		}
		
		// If stack size == 0 (the entire stack was moved) set slot contents to null
		if(sourceStack.getCount() == 0)
			sourceSlot.set(ItemStack.EMPTY);
		else
			sourceSlot.setChanged();
		
		sourceSlot.onTake(player, sourceStack);
		return copyOfSourceStack;
	}
}
