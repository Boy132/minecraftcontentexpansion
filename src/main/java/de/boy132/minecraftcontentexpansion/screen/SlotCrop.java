package de.boy132.minecraftcontentexpansion.screen;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.common.Tags;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import org.jetbrains.annotations.NotNull;

public class SlotCrop extends SlotItemHandler
{
	public SlotCrop(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		super(itemHandler, index, xPosition, yPosition);
	}
	
	@Override
	public int getMaxStackSize()
	{
		return 1;
	}
	
	@Override
	public int getMaxStackSize(@NotNull ItemStack stack)
	{
		return 1;
	}
	
	@Override
	public boolean mayPlace(ItemStack stack)
	{
		return getItem().isEmpty() && (stack.is(Tags.Items.SEEDS) || stack.is(Tags.Items.CROPS) || stack.getItem() == Items.SWEET_BERRIES);
	}
}
