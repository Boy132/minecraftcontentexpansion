package de.boy132.minecraftcontentexpansion.screen;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.screen.backpack.BackpackMenu;
import de.boy132.minecraftcontentexpansion.screen.battery.BatteryMenu;
import de.boy132.minecraftcontentexpansion.screen.coalgenerator.CoalGeneratorMenu;
import de.boy132.minecraftcontentexpansion.screen.electricbrewery.ElectricBreweryMenu;
import de.boy132.minecraftcontentexpansion.screen.electricgreenhouse.ElectricGreenhouseMenu;
import de.boy132.minecraftcontentexpansion.screen.electricsmelter.ElectricSmelterMenu;
import de.boy132.minecraftcontentexpansion.screen.hydraulicpress.HydraulicPressMenu;
import de.boy132.minecraftcontentexpansion.screen.kiln.KilnMenu;
import de.boy132.minecraftcontentexpansion.screen.liquidtank.LiquidTankMenu;
import de.boy132.minecraftcontentexpansion.screen.millstone.MillstoneMenu;
import de.boy132.minecraftcontentexpansion.screen.smelter.SmelterMenu;
import de.boy132.minecraftcontentexpansion.screen.solarpanel.SolarPanelMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.common.extensions.IForgeMenuType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModMenuTypes
{
	private static final DeferredRegister<MenuType<?>> MENU_TYPES = DeferredRegister.create(ForgeRegistries.MENU_TYPES, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<MenuType<KilnMenu>> KILN = MENU_TYPES.register("kiln", () -> IForgeMenuType.create(KilnMenu::new));
	public static final RegistryObject<MenuType<SmelterMenu>> SMELTER = MENU_TYPES.register("smelter", () -> IForgeMenuType.create(SmelterMenu::new));
	public static final RegistryObject<MenuType<MillstoneMenu>> MILLSTONE = MENU_TYPES.register("millstone", () -> IForgeMenuType.create(MillstoneMenu::new));
	
	public static final RegistryObject<MenuType<CoalGeneratorMenu>> COAL_GENERATOR = MENU_TYPES.register("coal_generator", () -> IForgeMenuType.create(CoalGeneratorMenu::new));
	public static final RegistryObject<MenuType<BatteryMenu>> BATTERY = MENU_TYPES.register("battery", () -> IForgeMenuType.create(BatteryMenu::new));
	public static final RegistryObject<MenuType<SolarPanelMenu>> SOLAR_PANEL = MENU_TYPES.register("solar_panel", () -> IForgeMenuType.create(SolarPanelMenu::new));
	
	public static final RegistryObject<MenuType<LiquidTankMenu>> LIQUID_TANK = MENU_TYPES.register("liquid_tank", () -> IForgeMenuType.create(LiquidTankMenu::new));
	
	public static final RegistryObject<MenuType<ElectricBreweryMenu>> ELECTRIC_BREWERY = MENU_TYPES.register("electric_brewery", () -> IForgeMenuType.create(ElectricBreweryMenu::new));
	public static final RegistryObject<MenuType<ElectricSmelterMenu>> ELECTRIC_SMELTER = MENU_TYPES.register("electric_smelter", () -> IForgeMenuType.create(ElectricSmelterMenu::new));
	public static final RegistryObject<MenuType<ElectricGreenhouseMenu>> ELECTRIC_GREENHOUSE = MENU_TYPES.register("electric_greenhouse", () -> IForgeMenuType.create(ElectricGreenhouseMenu::new));
	
	public static final RegistryObject<MenuType<HydraulicPressMenu>> HYDRAULIC_PRESS = MENU_TYPES.register("hydraulic_press", () -> IForgeMenuType.create(HydraulicPressMenu::new));
	
	public static final RegistryObject<MenuType<BackpackMenu>> BACKPACK = MENU_TYPES.register("backpack", () -> IForgeMenuType.create(BackpackMenu::new));
	
	public static void register(IEventBus eventBus)
	{
		MENU_TYPES.register(eventBus);
	}
}
