package de.boy132.minecraftcontentexpansion.screen.electricsmelter;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricsmelter.ElectricSmelterBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotOutput;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.SlotItemHandler;

public class ElectricSmelterMenu extends BaseContainerMenu
{
	public ElectricSmelterMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(5));
	}
	
	public ElectricSmelterMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.ELECTRIC_SMELTER.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory);
		addPlayerHotbar(inventory);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotOutput(itemHandler, ElectricSmelterBlockEntity.OUTPUT_SLOT, 125, 35));
			
			for(int i = 0; i < ElectricSmelterBlockEntity.INPUT_SLOT_COUNT / 2; i++)
				addSlot(new SlotItemHandler(itemHandler, ElectricSmelterBlockEntity.INPUT_SLOT_START + i, 44 + 18 * i, 25));
			
			for(int i = ElectricSmelterBlockEntity.INPUT_SLOT_COUNT / 2; i < ElectricSmelterBlockEntity.INPUT_SLOT_COUNT; i++)
				addSlot(new SlotItemHandler(itemHandler, ElectricSmelterBlockEntity.INPUT_SLOT_START + i, 44 + 18 * (i - ElectricSmelterBlockEntity.INPUT_SLOT_COUNT / 2), 43));
			
		});
		
		addDataSlots(data);
	}
	
	public int getSmeltProgressScaled(int pixels)
	{
		int smeltTime = data.get(1);
		int neededSmeltTime = data.get(2);
		
		return smeltTime > 0 && neededSmeltTime > 0 ? smeltTime * pixels / neededSmeltTime : 0;
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.ELECTRIC_SMELTER.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return 3;
	}
}
