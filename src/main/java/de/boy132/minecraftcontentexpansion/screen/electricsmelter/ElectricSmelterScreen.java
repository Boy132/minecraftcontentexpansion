package de.boy132.minecraftcontentexpansion.screen.electricsmelter;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerScreen;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class ElectricSmelterScreen extends BaseContainerScreen<ElectricSmelterMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/electric_smelter.png");
	
	public ElectricSmelterScreen(ElectricSmelterMenu electricSmelterMenu, Inventory inventory, Component component)
	{
		super(electricSmelterMenu, inventory, component);
		titleLabelX += 20;
		inventoryLabelX += 20;
	}
	
	@Override
	protected void renderLabels(GuiGraphics guiGraphics, int mouseX, int mouseY)
	{
		super.renderLabels(guiGraphics, mouseX, mouseY);
		renderAreaTooltip(guiGraphics, mouseX, mouseY, menu.getEnergyTooltips(), 7, 5, 18, 75);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(guiGraphics, partialTicks, mouseX, mouseY);
		
		int smeltProgress = menu.getSmeltProgressScaled(24);
		if(smeltProgress > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 88, topPos + 34, 176, 0, smeltProgress + 1, 16);
		
		renderEnergyBarVertical(guiGraphics, 73, 8, 6);
	}
	
	@Override
	protected ResourceLocation getBackgroundTexture()
	{
		return BACKGROUND;
	}
}