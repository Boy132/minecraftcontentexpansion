package de.boy132.minecraftcontentexpansion.screen;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotPotion extends SlotItemHandler
{
	public SlotPotion(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		super(itemHandler, index, xPosition, yPosition);
	}
	
	@Override
	public boolean mayPlace(ItemStack stack)
	{
		Item stackItem = stack.getItem();
		return stackItem == Items.GLASS_BOTTLE || stackItem == Items.POTION || stackItem == Items.LINGERING_POTION || stackItem == Items.SPLASH_POTION;
	}
}
