package de.boy132.minecraftcontentexpansion.screen;

import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import java.util.function.Predicate;

public class SlotFuel extends SlotItemHandler
{
	protected Predicate<ItemStack> fuelTest;
	
	public SlotFuel(IItemHandler itemHandler, int index, int xPosition, int yPosition, Predicate<ItemStack> fuelTest)
	{
		super(itemHandler, index, xPosition, yPosition);
		this.fuelTest = fuelTest;
	}
	
	@Override
	public boolean mayPlace(ItemStack stack)
	{
		return fuelTest.test(stack);
	}
}
