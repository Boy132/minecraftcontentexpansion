package de.boy132.minecraftcontentexpansion.screen.battery;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.battery.BatteryBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotEnergyItem;
import de.boy132.minecraftcontentexpansion.screen.SlotOutput;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;

public class BatteryMenu extends BaseContainerMenu
{
	public BatteryMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(3));
	}
	
	public BatteryMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.BATTERY.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory, 76);
		addPlayerHotbar(inventory, 76);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotEnergyItem(itemHandler, BatteryBlockEntity.INPUT_SLOT, 26, 40));
			addSlot(new SlotOutput(itemHandler, BatteryBlockEntity.OUTPUT_SLOT, 44, 40));
			
		});
		
		addDataSlots(data);
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.BATTERY.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return 2;
	}
}
