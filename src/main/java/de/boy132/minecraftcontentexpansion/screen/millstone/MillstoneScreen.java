package de.boy132.minecraftcontentexpansion.screen.millstone;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerScreen;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class MillstoneScreen extends BaseContainerScreen<MillstoneMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/millstone.png");
	
	public MillstoneScreen(MillstoneMenu millstoneMenu, Inventory inventory, Component component)
	{
		super(millstoneMenu, inventory, component);
	}
	
	@Override
	protected void renderLabels(GuiGraphics guiGraphics, int mouseX, int mouseY)
	{
		super.renderLabels(guiGraphics, mouseX, mouseY);
		
		if(menu.getSurroundingWater() <= 0)
			renderAreaTooltip(guiGraphics, mouseX, mouseY, Component.translatable("inventory." + MinecraftContentExpansion.MODID + ".millstone.nowater"), 152, 59, 16, 16);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(guiGraphics, partialTicks, mouseX, mouseY);
		
		int millProgress = menu.getMillProgressScaled(24);
		if(millProgress > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 67, topPos + 34, 176, 16, millProgress + 1, 16);
		
		if(menu.getSurroundingWater() <= 0)
		{
			guiGraphics.blit(getBackgroundTexture(), leftPos + 152, topPos + 59, 176, 0, 16, 16);
			guiGraphics.blit(getBackgroundTexture(), leftPos + 67, topPos + 35, 176, 33, 24, 16);
		}
	}
	
	@Override
	protected ResourceLocation getBackgroundTexture()
	{
		return BACKGROUND;
	}
}
