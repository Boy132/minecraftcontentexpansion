package de.boy132.minecraftcontentexpansion.screen.millstone;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.millstone.MillstoneBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotOutput;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.SlotItemHandler;

public class MillstoneMenu extends BaseContainerMenu
{
	public MillstoneMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(2));
	}
	
	public MillstoneMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.MILLSTONE.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory);
		addPlayerHotbar(inventory);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotItemHandler(itemHandler, MillstoneBlockEntity.INPUT_SLOT, 44, 34));
			addSlot(new SlotOutput(itemHandler, MillstoneBlockEntity.OUTPUT_SLOT, 102, 35));
		});
		
		addDataSlots(data);
	}
	
	public int getMillProgressScaled(int pixels)
	{
		int millTime = data.get(0);
		int neededMillTime = data.get(1);
		
		return neededMillTime != 0 && millTime != 0 ? millTime * pixels / neededMillTime : 0;
	}
	
	public int getSurroundingWater()
	{
		return ((MillstoneBlockEntity) blockEntity).getSurroundingWater();
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.MILLSTONE.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return 2;
	}
}
