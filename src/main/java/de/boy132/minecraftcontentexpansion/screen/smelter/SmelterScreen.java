package de.boy132.minecraftcontentexpansion.screen.smelter;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerScreen;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class SmelterScreen extends BaseContainerScreen<SmelterMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/smelter.png");
	
	public SmelterScreen(SmelterMenu smelterMenu, Inventory inventory, Component component)
	{
		super(smelterMenu, inventory, component);
		titleLabelX += 20;
		inventoryLabelX += 20;
	}
	
	@Override
	protected void renderLabels(GuiGraphics guiGraphics, int mouseX, int mouseY)
	{
		super.renderLabels(guiGraphics, mouseX, mouseY);
		renderAreaTooltip(guiGraphics, mouseX, mouseY, Component.literal(menu.getHeatString()), 7, 5, 18, 75);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(guiGraphics, partialTicks, mouseX, mouseY);
		
		int burnLeft = menu.getBurnLeftScaled(14);
		if(burnLeft > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 36, topPos + 53 + 13 - burnLeft, 176, 13 - burnLeft, 13, burnLeft + 1);
		
		int smeltProgress = menu.getSmeltProgressScaled(24);
		if(smeltProgress > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 107, topPos + 34, 176, 14, smeltProgress + 1, 16);
		
		int heat = menu.getHeatScaled(73);
		if(heat > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 8, topPos + 6 + (73 - heat), 176, 31 + (73 - heat), 16, heat);
		
		int minHeat = menu.getMinHeatPos(73);
		if(minHeat > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 8, topPos + 78 - minHeat, 8, 5, 16, 1);
	}
	
	@Override
	protected ResourceLocation getBackgroundTexture()
	{
		return BACKGROUND;
	}
}
