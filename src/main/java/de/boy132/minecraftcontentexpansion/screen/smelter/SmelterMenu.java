package de.boy132.minecraftcontentexpansion.screen.smelter;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.smelter.SmelterBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotFuel;
import de.boy132.minecraftcontentexpansion.screen.SlotOutput;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.SlotItemHandler;

public class SmelterMenu extends BaseContainerMenu
{
	public SmelterMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(6));
	}
	
	public SmelterMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.SMELTER.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory);
		addPlayerHotbar(inventory);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotFuel(itemHandler, SmelterBlockEntity.FUEL_SLOT, 36, 34, ((SmelterBlockEntity) this.blockEntity)::isItemFuel));
			addSlot(new SlotItemHandler(itemHandler, SmelterBlockEntity.INPUT_SLOT_1, 65, 25));
			addSlot(new SlotItemHandler(itemHandler, SmelterBlockEntity.INPUT_SLOT_2, 83, 25));
			addSlot(new SlotItemHandler(itemHandler, SmelterBlockEntity.INPUT_SLOT_3, 65, 43));
			addSlot(new SlotItemHandler(itemHandler, SmelterBlockEntity.INPUT_SLOT_4, 83, 43));
			addSlot(new SlotOutput(itemHandler, SmelterBlockEntity.OUTPUT_SLOT, 141, 35));
		});
		
		addDataSlots(data);
	}
	
	public int getHeatScaled(int pixels)
	{
		int heat = data.get(0);
		return heat > 0 ? heat * pixels / SmelterBlockEntity.MAX_HEAT : 0;
	}
	
	public int getMinHeatPos(int pixels)
	{
		int minHeat = data.get(1);
		return minHeat > 0 ? minHeat * pixels / SmelterBlockEntity.MAX_HEAT : 0;
	}
	
	public String getHeatString()
	{
		int heat = data.get(0);
		return heat > 0 ? Math.round(((double) heat / SmelterBlockEntity.MAX_HEAT) * 100) + " %" : "0 %";
	}
	
	public int getBurnLeftScaled(int pixels)
	{
		int burnTime = data.get(2);
		int maxBurnTime = data.get(3);
		
		return burnTime > 0 ? burnTime * pixels / maxBurnTime : 0;
	}
	
	public int getSmeltProgressScaled(int pixels)
	{
		int smeltTime = data.get(4);
		int neededSmeltTime = data.get(5);
		
		return neededSmeltTime != 0 && smeltTime != 0 ? smeltTime * pixels / neededSmeltTime : 0;
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.SMELTER.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return 6;
	}
}
