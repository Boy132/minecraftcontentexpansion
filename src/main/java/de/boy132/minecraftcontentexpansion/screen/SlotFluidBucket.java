package de.boy132.minecraftcontentexpansion.screen;

import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotFluidBucket extends SlotItemHandler
{
	private final AdvancedBlockEntity advancedBlockEntity;
	
	public SlotFluidBucket(AdvancedBlockEntity advancedBlockEntity, IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		super(itemHandler, index, xPosition, yPosition);
		this.advancedBlockEntity = advancedBlockEntity;
	}
	
	@Override
	public boolean mayPlace(ItemStack stack)
	{
		return AdvancedBlockEntity.isItemValidForFluid((FluidTank) advancedBlockEntity.getCapability(ForgeCapabilities.FLUID_HANDLER).orElse(null), stack);
	}
}
