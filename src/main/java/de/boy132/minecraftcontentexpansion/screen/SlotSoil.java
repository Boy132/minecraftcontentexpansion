package de.boy132.minecraftcontentexpansion.screen;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import org.jetbrains.annotations.NotNull;

public class SlotSoil extends SlotItemHandler
{
	public SlotSoil(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		super(itemHandler, index, xPosition, yPosition);
	}
	
	@Override
	public int getMaxStackSize()
	{
		return 1;
	}
	
	@Override
	public int getMaxStackSize(@NotNull ItemStack stack)
	{
		return 1;
	}
	
	@Override
	public boolean mayPlace(ItemStack stack)
	{
		Item stackItem = stack.getItem();
		return stackItem == Items.DIRT || stackItem == Items.FARMLAND || stackItem == Items.SAND || stackItem == Items.SOUL_SAND;
	}
}
