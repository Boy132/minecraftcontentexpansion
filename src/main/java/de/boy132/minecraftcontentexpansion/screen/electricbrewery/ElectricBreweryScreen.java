package de.boy132.minecraftcontentexpansion.screen.electricbrewery;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerScreen;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class ElectricBreweryScreen extends BaseContainerScreen<ElectricBreweryMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/electric_brewery.png");
	
	public ElectricBreweryScreen(ElectricBreweryMenu electricBreweryMenu, Inventory inventory, Component component)
	{
		super(electricBreweryMenu, inventory, component);
		titleLabelX += 20;
		inventoryLabelX += 20;
	}
	
	@Override
	protected void renderLabels(GuiGraphics guiGraphics, int mouseX, int mouseY)
	{
		super.renderLabels(guiGraphics, mouseX, mouseY);
		renderAreaTooltip(guiGraphics, mouseX, mouseY, menu.getEnergyTooltips(), 8, 7, 16, 74);
		renderAreaTooltip(guiGraphics, mouseX, mouseY, menu.getFluidTooltips(), 152, 7, 16, 73);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(guiGraphics, partialTicks, mouseX, mouseY);
		
		int brewTime = menu.getBrewTimeScaled(24);
		if(brewTime > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 57, topPos + 31, 176, 73, brewTime, 7);
		
		renderEnergyBarVertical(guiGraphics, 73, 8, 7);
		renderFluidBar(guiGraphics, 73, 152, 7);
	}
	
	@Override
	protected ResourceLocation getBackgroundTexture()
	{
		return BACKGROUND;
	}
}
