package de.boy132.minecraftcontentexpansion.screen.electricbrewery;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricbrewery.ElectricBreweryBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotPotion;
import de.boy132.minecraftcontentexpansion.screen.SlotSingleItemType;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.SlotItemHandler;

public class ElectricBreweryMenu extends BaseContainerMenu
{
	public ElectricBreweryMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(3));
	}
	
	public ElectricBreweryMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.ELECTRIC_BREWERY.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory);
		addPlayerHotbar(inventory);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotItemHandler(itemHandler, ElectricBreweryBlockEntity.INPUT_SLOT, 35, 27));
			addSlot(new SlotSingleItemType(Items.GLASS_BOTTLE, itemHandler, ElectricBreweryBlockEntity.BOTTLE_SLOT, 35, 45));
			
			for(int i = 0; i < ElectricBreweryBlockEntity.PROCESS_SLOTS_COUNT / 2; i++)
				addSlot(new SlotPotion(itemHandler, ElectricBreweryBlockEntity.PROCESS_SLOTS_START + i, 89 + 18 * i, 27));
			
			for(int i = ElectricBreweryBlockEntity.PROCESS_SLOTS_COUNT / 2; i < ElectricBreweryBlockEntity.PROCESS_SLOTS_COUNT; i++)
				addSlot(new SlotPotion(itemHandler, ElectricBreweryBlockEntity.PROCESS_SLOTS_START + i, 89 + 18 * (i - ElectricBreweryBlockEntity.PROCESS_SLOTS_COUNT / 2), 45));
		});
		
		addDataSlots(data);
	}
	
	public int getBrewTimeScaled(int pixels)
	{
		int brewTime = data.get(1);
		int neededBrewTime = data.get(2);
		
		return brewTime > 0 ? brewTime * pixels / neededBrewTime : 0;
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.ELECTRIC_BREWERY.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return 2 + ElectricBreweryBlockEntity.PROCESS_SLOTS_COUNT;
	}
}
