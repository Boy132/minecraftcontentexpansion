package de.boy132.minecraftcontentexpansion.screen.hydraulicpress;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerScreen;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class HydraulicPressScreen extends BaseContainerScreen<HydraulicPressMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/hydraulic_press.png");
	
	public HydraulicPressScreen(HydraulicPressMenu hydraulicPressMenu, Inventory inventory, Component component)
	{
		super(hydraulicPressMenu, inventory, component);
		titleLabelX += 20;
		inventoryLabelX += 20;
	}
	
	@Override
	protected void renderLabels(GuiGraphics guiGraphics, int mouseX, int mouseY)
	{
		super.renderLabels(guiGraphics, mouseX, mouseY);
		renderAreaTooltip(guiGraphics, mouseX, mouseY, menu.getEnergyTooltips(), 8, 7, 16, 74);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(guiGraphics, partialTicks, mouseX, mouseY);
		
		int pressTime = menu.getPressTimeScaled(17);
		if(pressTime > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 52, topPos + 15, 176, 73, 15, pressTime);
		
		renderEnergyBarVertical(guiGraphics, 73, 8, 7);
	}
	
	@Override
	protected ResourceLocation getBackgroundTexture()
	{
		return BACKGROUND;
	}
}
