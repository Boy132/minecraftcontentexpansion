package de.boy132.minecraftcontentexpansion.screen.hydraulicpress;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.hydraulicpress.HydraulicPressBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotOutput;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.SlotItemHandler;

public class HydraulicPressMenu extends BaseContainerMenu
{
	public HydraulicPressMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(3));
	}
	
	public HydraulicPressMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.HYDRAULIC_PRESS.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory);
		addPlayerHotbar(inventory);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotItemHandler(itemHandler, HydraulicPressBlockEntity.PRIMARY_INPUT_SLOT, 53, 50));
			addSlot(new SlotItemHandler(itemHandler, HydraulicPressBlockEntity.SECONDARY_INPUT_SLOT, 53, 32));
			
			for(int i = HydraulicPressBlockEntity.OUTPUT_SLOT_START; i < HydraulicPressBlockEntity.OUTPUT_SLOT_START + HydraulicPressBlockEntity.OUTPUT_SLOT_COUNT; i++)
				addSlot(new SlotOutput(itemHandler, i, 98 + (i - HydraulicPressBlockEntity.OUTPUT_SLOT_START) * 18, 50));
		});
		
		addDataSlots(data);
	}
	
	public int getPressTimeScaled(int pixels)
	{
		int pressTime = data.get(1);
		int neededPressTime = data.get(2);
		
		return pressTime > 0 ? pressTime * pixels / neededPressTime : 0;
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.HYDRAULIC_PRESS.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return HydraulicPressBlockEntity.OUTPUT_SLOT_START + HydraulicPressBlockEntity.OUTPUT_SLOT_COUNT;
	}
}
