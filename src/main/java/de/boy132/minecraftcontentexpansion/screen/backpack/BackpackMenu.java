package de.boy132.minecraftcontentexpansion.screen.backpack;

import de.boy132.minecraftcontentexpansion.item.backpack.BackpackItem;
import de.boy132.minecraftcontentexpansion.item.backpack.EnderBackpackItem;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ClickType;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class BackpackMenu extends AbstractContainerMenu
{
	private final ItemStack ITEM;
	private final IItemHandler ITEM_HANDLER;
	
	private int slotOfThisBackpack = -1;
	
	public BackpackMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, extraData.readItem());
	}
	
	public BackpackMenu(int id, Inventory inventory)
	{
		this(id, inventory, getHeldItem(inventory.player));
	}
	
	public BackpackMenu(int id, Inventory inventory, ItemStack backpackStack)
	{
		super(ModMenuTypes.BACKPACK.get(), id);
		this.ITEM = backpackStack;
		this.ITEM_HANDLER = ((BackpackItem) ITEM.getItem()).getInventory(ITEM);
		
		checkContainerSize(inventory, ITEM_HANDLER.getSlots());
		
		for(int i = 0; i < ITEM_HANDLER.getSlots(); ++i)
		{
			int x = 8 + 18 * (i % 9);
			int y = 18 + 18 * (i / 9);
			addSlot(new SlotItemHandler(ITEM_HANDLER, i, x, y));
		}
		
		addPlayerInventory(inventory);
	}
	
	protected void addPlayerInventory(Inventory inventory)
	{
		int yOffset = (getRowCount() - 4) * 18 + 19;
		
		for(int y = 0; y < 3; y++)
			for(int x = 0; x < 9; x++)
				addSlot(new Slot(inventory, x + y * 9 + 9, 8 + x * 18, 84 + y * 18 + yOffset));
		
		for(int x = 0; x < 9; x++)
		{
			Slot slot = addSlot(new Slot(inventory, x, 8 + x * 18, 142 + yOffset)
			{
				@Override
				public boolean mayPickup(Player player)
				{
					return index != slotOfThisBackpack;
				}
			});
			
			if(x == inventory.selected && ItemStack.matches(inventory.getSelected(), ITEM))
				slotOfThisBackpack = slot.index;
		}
	}
	
	@Override
	public boolean stillValid(Player p_38874_)
	{
		return true;
	}
	
	// CREDIT GOES TO: diesieben07 | https://github.com/diesieben07/SevenCommons
	@Override
	public ItemStack quickMoveStack(Player player, int index)
	{
		int HOTBAR_SLOT_COUNT = 9;
		int PLAYER_INVENTORY_ROW_COUNT = 3;
		int PLAYER_INVENTORY_COLUMN_COUNT = 9;
		int PLAYER_INVENTORY_SLOT_COUNT = PLAYER_INVENTORY_COLUMN_COUNT * PLAYER_INVENTORY_ROW_COUNT;
		int VANILLA_SLOT_COUNT = HOTBAR_SLOT_COUNT + PLAYER_INVENTORY_SLOT_COUNT;
		int INVENTORY_FIRST_SLOT_INDEX = 0;
		int INVENTORY_SLOT_COUNT = ITEM_HANDLER.getSlots();
		int VANILLA_FIRST_SLOT_INDEX = INVENTORY_FIRST_SLOT_INDEX + INVENTORY_SLOT_COUNT;
		
		Slot sourceSlot = slots.get(index);
		if(sourceSlot == null || !sourceSlot.hasItem())
			return ItemStack.EMPTY;
		
		ItemStack sourceStack = sourceSlot.getItem();
		ItemStack copyOfSourceStack = sourceStack.copy();
		
		// Check if the slot clicked is one of the backpack container slots
		if(index < INVENTORY_FIRST_SLOT_INDEX + INVENTORY_SLOT_COUNT)
		{
			// This is a backpack slot so merge the stack into the players inventory
			if(!moveItemStackTo(sourceStack, VANILLA_FIRST_SLOT_INDEX, VANILLA_FIRST_SLOT_INDEX + VANILLA_SLOT_COUNT, false))
				return ItemStack.EMPTY;
		} else if(index < VANILLA_FIRST_SLOT_INDEX + VANILLA_SLOT_COUNT)
		{
			// This is a vanilla container slot so merge the stack into the tile inventory
			if(!moveItemStackTo(sourceStack, INVENTORY_FIRST_SLOT_INDEX, INVENTORY_FIRST_SLOT_INDEX + INVENTORY_SLOT_COUNT, false))
				return ItemStack.EMPTY;
		} else
		{
			System.out.println("Invalid slotIndex:" + index);
			return ItemStack.EMPTY;
		}
		
		// If stack size == 0 (the entire stack was moved) set slot contents to null
		if(sourceStack.getCount() == 0)
			sourceSlot.set(ItemStack.EMPTY);
		else
			sourceSlot.setChanged();
		
		sourceSlot.onTake(player, sourceStack);
		return copyOfSourceStack;
	}
	
	@Override
	public void removed(Player player)
	{
		super.removed(player);
		((BackpackItem) ITEM.getItem()).saveInventory(ITEM, ITEM_HANDLER);
	}
	
	@Override
	public void clicked(int slotId, int dragType, ClickType clickType, Player player)
	{
		if(slotId < 0 || slotId > slots.size())
		{
			super.clicked(slotId, dragType, clickType, player);
			return;
		}
		
		if(canTake(slotId, dragType, clickType))
			super.clicked(slotId, dragType, clickType, player);
	}
	
	public int getRowCount()
	{
		return ((BackpackItem) ITEM.getItem()).getRowCount();
	}
	
	private boolean canTake(int slotId, int button, ClickType clickType)
	{
		Slot slot = slots.get(slotId);
		
		if(slotId == slotOfThisBackpack || isBackpack(slot.getItem())) // FIXME: this allows moving backpacks inside the player inv but not inside the backpack inv - however, you can still put a backpack in a backpack //(slotId <= ITEM_HANDLER.getSlots() - 1 && isBackpack(slot.getItem())))
			return false;
		
		if(clickType == ClickType.SWAP)
		{
			int hotbarId = ITEM_HANDLER.getSlots() + 27 + button;
			
			if(slotOfThisBackpack == hotbarId)
				return false;
			
			if(slotId <= ITEM_HANDLER.getSlots() - 1)
				return !isBackpack(slot.getItem()) && !isBackpack(getSlot(hotbarId).getItem());
		}
		
		return true;
	}
	
	private static ItemStack getHeldItem(Player player)
	{
		if(isBackpack(player.getMainHandItem()))
			return player.getMainHandItem();
		
		if(isBackpack(player.getOffhandItem()))
			return player.getOffhandItem();
		
		return ItemStack.EMPTY;
	}
	
	private static boolean isBackpack(ItemStack stack)
	{
		return stack.getItem() instanceof BackpackItem || stack.getItem() instanceof EnderBackpackItem;
	}
}
