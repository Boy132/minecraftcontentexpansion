package de.boy132.minecraftcontentexpansion.screen.backpack;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class BackpackScreen extends AbstractContainerScreen<BackpackMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.parse("textures/gui/container/generic_54.png");
	
	public BackpackScreen(BackpackMenu backpackMenu, Inventory inventory, Component component)
	{
		super(backpackMenu, inventory, component);
		inventoryLabelY = menu.getRowCount() * 18 + 20;
	}
	
	@Override
	public void render(GuiGraphics guiGraphics, int mouseX, int mouseY, float partialTicks)
	{
		renderTransparentBackground(guiGraphics);
		super.render(guiGraphics, mouseX, mouseY, partialTicks);
		renderTooltip(guiGraphics, mouseX, mouseY);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		renderTransparentBackground(guiGraphics);
		
		guiGraphics.blit(BACKGROUND, leftPos, topPos, 0, 0, imageWidth, menu.getRowCount() * 18 + 17);
		guiGraphics.blit(BACKGROUND, leftPos, topPos + menu.getRowCount() * 18 + 17, 0, 126, imageWidth, 96);
		
	}
}
