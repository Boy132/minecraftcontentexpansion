package de.boy132.minecraftcontentexpansion.screen.electricgreenhouse;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.ElectricGreenhouseBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerScreen;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class ElectricGreenhouseScreen extends BaseContainerScreen<ElectricGreenhouseMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/electric_greenhouse.png");
	
	public ElectricGreenhouseScreen(ElectricGreenhouseMenu electricGreenhouseMenu, Inventory inventory, Component component)
	{
		super(electricGreenhouseMenu, inventory, component);
		titleLabelX += 20;
		inventoryLabelX += 20;
	}
	
	@Override
	protected void renderLabels(GuiGraphics guiGraphics, int mouseX, int mouseY)
	{
		super.renderLabels(guiGraphics, mouseX, mouseY);
		renderAreaTooltip(guiGraphics, mouseX, mouseY, menu.getEnergyTooltips(), 8, 7, 16, 74);
		renderAreaTooltip(guiGraphics, mouseX, mouseY, menu.getFluidTooltips(), 152, 7, 16, 73);
		
		guiGraphics.pose().pushPose();
		guiGraphics.pose().scale(0.8f, 0.8f, 0.8f);
		for(int i = 0; i < ElectricGreenhouseBlockEntity.INPUT_SLOTS_COUNT; i++)
		{
			if(menu.isSoilValid(i))
			{
				int growTime = menu.getGrowTime(i);
				if(growTime > 0)
					guiGraphics.drawString(font, growTime + "%", 90 + (23 * i), 62, 4210752, false);
			} else
				guiGraphics.drawString(font, "X", 90 + (23 * i), 62, 4210752, false);
		}
		guiGraphics.pose().scale(1f, 1f, 1f);
		guiGraphics.pose().popPose();
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(guiGraphics, partialTicks, mouseX, mouseY);
		renderEnergyBarVertical(guiGraphics, 73, 8, 7);
		renderFluidBar(guiGraphics, 73, 152, 7);
	}
	
	@Override
	protected ResourceLocation getBackgroundTexture()
	{
		return BACKGROUND;
	}
}
