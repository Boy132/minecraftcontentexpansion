package de.boy132.minecraftcontentexpansion.screen.electricgreenhouse;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.ElectricGreenhouseBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotCrop;
import de.boy132.minecraftcontentexpansion.screen.SlotSoil;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;

public class ElectricGreenhouseMenu extends BaseContainerMenu
{
	public ElectricGreenhouseMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(ElectricGreenhouseBlockEntity.INPUT_SLOTS_START + ElectricGreenhouseBlockEntity.INPUT_SLOTS_COUNT));
	}
	
	public ElectricGreenhouseMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.ELECTRIC_GREENHOUSE.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory);
		addPlayerHotbar(inventory);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotSoil(itemHandler, ElectricGreenhouseBlockEntity.SOIL_SLOT, 35, 31));
			
			for(int i = ElectricGreenhouseBlockEntity.INPUT_SLOTS_START; i < ElectricGreenhouseBlockEntity.INPUT_SLOTS_START + ElectricGreenhouseBlockEntity.INPUT_SLOTS_COUNT; i++)
				addSlot(new SlotCrop(itemHandler, i, 71 + (18 * (i - ElectricGreenhouseBlockEntity.INPUT_SLOTS_START)), 31));
		});
		
		addDataSlots(data);
	}
	
	public boolean isSoilValid(int slot)
	{
		return ((ElectricGreenhouseBlockEntity) blockEntity).isSoilValid(slot);
	}
	
	public int getGrowTime(int slot)
	{
		int growTime = data.get(ElectricGreenhouseBlockEntity.INPUT_SLOTS_START + slot);
		int neededGrowTime = ElectricGreenhouseBlockEntity.NEEDED_GROW_TIME;
		
		return growTime > 0 ? Math.round(((float) growTime / (float) neededGrowTime) * 100) : 0;
	}
	
	public int getGrowTimeScaled(int slot, int pixels)
	{
		int growTime = data.get(ElectricGreenhouseBlockEntity.INPUT_SLOTS_START + slot);
		int neededGrowTime = ElectricGreenhouseBlockEntity.NEEDED_GROW_TIME;
		
		return growTime > 0 ? growTime * pixels / neededGrowTime : 0;
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.ELECTRIC_GREENHOUSE.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return ElectricGreenhouseBlockEntity.INPUT_SLOTS_START + ElectricGreenhouseBlockEntity.INPUT_SLOTS_COUNT;
	}
}
