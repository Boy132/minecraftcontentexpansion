package de.boy132.minecraftcontentexpansion.screen;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.InventoryMenu;
import net.minecraftforge.client.extensions.common.IClientFluidTypeExtensions;
import net.minecraftforge.fluids.FluidStack;

import java.util.List;

public abstract class BaseContainerScreen<T extends BaseContainerMenu> extends AbstractContainerScreen<T>
{
	public BaseContainerScreen(T menu, Inventory inventory, Component component)
	{
		super(menu, inventory, component);
	}
	
	protected abstract ResourceLocation getBackgroundTexture();
	
	@Override
	public void render(GuiGraphics guiGraphics, int mouseX, int mouseY, float partialTicks)
	{
		renderTransparentBackground(guiGraphics);
		super.render(guiGraphics, mouseX, mouseY, partialTicks);
		renderTooltip(guiGraphics, mouseX, mouseY);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
		guiGraphics.blit(getBackgroundTexture(), leftPos, topPos, 0, 0, imageWidth, imageHeight);
	}
	
	protected void renderAreaTooltip(GuiGraphics guiGraphics, int mouseX, int mouseY, List<Component> toolTips, int x, int y, int width, int height)
	{
		if(mouseX >= (leftPos + x) && mouseY >= (topPos + y) && mouseX < (leftPos + x + width) && mouseY < (topPos + y + height))
			guiGraphics.renderComponentTooltip(font, toolTips, mouseX - leftPos, mouseY - topPos);
	}
	
	protected void renderAreaTooltip(GuiGraphics guiGraphics, int mouseX, int mouseY, Component toolTip, int x, int y, int width, int height)
	{
		renderAreaTooltip(guiGraphics, mouseX, mouseY, List.of(toolTip), x, y, width, height);
	}
	
	protected void renderEnergyBarVertical(GuiGraphics guiGraphics, int height, int x, int y)
	{
		int energyStored = menu.getEnergyStoredScaled(height);
		if(energyStored > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + x, topPos + y + (height - energyStored), 176, (height - energyStored), 16, energyStored);
	}
	
	protected void renderEnergyBarHorizontal(GuiGraphics guiGraphics, int width, int x, int y, int textureX, int textureY)
	{
		int energyStored = menu.getEnergyStoredScaled(width);
		if(energyStored > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + x, topPos + y, textureX, textureY, energyStored, 16);
	}
	
	protected void renderEnergyBarHorizontal(GuiGraphics guiGraphics, int width, int x, int y)
	{
		renderEnergyBarHorizontal(guiGraphics, width, x, y, 0, 156);
	}
	
	protected void renderFluidBar(GuiGraphics guiGraphics, int height, int x, int y)
	{
		FluidStack fluidStack = menu.getFluidStack();
		if(!fluidStack.isEmpty())
		{
			IClientFluidTypeExtensions fluidTypeExtensions = IClientFluidTypeExtensions.of(fluidStack.getFluid());
			
			int color = fluidTypeExtensions.getTintColor(fluidStack);
			RenderSystem.setShaderColor(((color >> 16) & 0xFF) / 255f, ((color >> 8) & 0xFF) / 255f, ((color) & 0xFF) / 255f, ((color >> 24) & 0xFF) / 255f);
			
			ResourceLocation stillLocation = fluidTypeExtensions.getStillTexture(fluidStack);
			TextureAtlasSprite sprite = Minecraft.getInstance().getTextureAtlas(InventoryMenu.BLOCK_ATLAS).apply(stillLocation);
			
			int fluidStored = menu.getFluidStoredScaled(height);
			guiGraphics.blit(leftPos + x, topPos + y + (height - fluidStored), 0, 16, fluidStored, sprite);
			
			RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}
}
