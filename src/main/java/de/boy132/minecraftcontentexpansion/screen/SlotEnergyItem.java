package de.boy132.minecraftcontentexpansion.screen;

import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotEnergyItem extends SlotItemHandler
{
	public SlotEnergyItem(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		super(itemHandler, index, xPosition, yPosition);
	}
	
	@Override
	public boolean mayPlace(ItemStack stack)
	{
		return stack.getCapability(ForgeCapabilities.ENERGY).isPresent();
	}
}
