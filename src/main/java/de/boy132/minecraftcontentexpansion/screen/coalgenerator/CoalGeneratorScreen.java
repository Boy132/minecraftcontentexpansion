package de.boy132.minecraftcontentexpansion.screen.coalgenerator;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerScreen;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class CoalGeneratorScreen extends BaseContainerScreen<CoalGeneratorMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/coal_generator.png");
	
	public CoalGeneratorScreen(CoalGeneratorMenu coalGeneratorMenu, Inventory inventory, Component component)
	{
		super(coalGeneratorMenu, inventory, component);
		imageHeight -= 10;
		inventoryLabelY -= 10;
	}
	
	@Override
	protected void renderLabels(GuiGraphics guiGraphics, int mouseX, int mouseY)
	{
		super.renderLabels(guiGraphics, mouseX, mouseY);
		renderAreaTooltip(guiGraphics, mouseX, mouseY, menu.getEnergyTooltips(), 30, 19, 139, 18);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(guiGraphics, partialTicks, mouseX, mouseY);
		
		int burnLeft = menu.getBurnLeftScaled(14);
		if(burnLeft > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 10, topPos + 39 + 13 - burnLeft, 176, 13 - burnLeft, 14, burnLeft + 1);
		
		renderEnergyBarHorizontal(guiGraphics, 137, 31, 20);
	}
	
	@Override
	protected ResourceLocation getBackgroundTexture()
	{
		return BACKGROUND;
	}
}
