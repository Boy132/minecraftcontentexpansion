package de.boy132.minecraftcontentexpansion.screen.coalgenerator;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.coalgenerator.CoalGeneratorBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotFuel;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;

public class CoalGeneratorMenu extends BaseContainerMenu
{
	public CoalGeneratorMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(3));
	}
	
	public CoalGeneratorMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.COAL_GENERATOR.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory, 74);
		addPlayerHotbar(inventory, 74);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotFuel(itemHandler, CoalGeneratorBlockEntity.FUEL_SLOT, 10, 20, ((CoalGeneratorBlockEntity) this.blockEntity)::isItemFuel));
		});
		
		addDataSlots(data);
	}
	
	public int getBurnLeftScaled(int pixels)
	{
		int burnTime = data.get(1);
		int maxBurnTime = data.get(2);
		
		return burnTime > 0 ? burnTime * pixels / maxBurnTime : 0;
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.COAL_GENERATOR.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return 1;
	}
}
