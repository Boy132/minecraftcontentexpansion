package de.boy132.minecraftcontentexpansion.screen.kiln;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerScreen;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class KilnScreen extends BaseContainerScreen<KilnMenu>
{
	private final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/kiln.png");
	
	public KilnScreen(KilnMenu kilnMenu, Inventory inventory, Component component)
	{
		super(kilnMenu, inventory, component);
	}
	
	@Override
	protected void renderBg(GuiGraphics guiGraphics, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(guiGraphics, partialTicks, mouseX, mouseY);
		
		int burnLeft = menu.getBurnLeftScaled(14);
		if(burnLeft > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 19, topPos + 51 + 13 - burnLeft, 176, 13 - burnLeft, 13, burnLeft + 1);
		
		int cookProgress = menu.getCookProgressScaled(24);
		if(cookProgress > 0)
			guiGraphics.blit(getBackgroundTexture(), leftPos + 81, topPos + 32, 176, 14, cookProgress + 1, 16);
	}
	
	@Override
	protected ResourceLocation getBackgroundTexture()
	{
		return BACKGROUND;
	}
}
