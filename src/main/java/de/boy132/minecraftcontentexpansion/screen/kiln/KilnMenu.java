package de.boy132.minecraftcontentexpansion.screen.kiln;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.kiln.KilnBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.BaseContainerMenu;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.SlotFuel;
import de.boy132.minecraftcontentexpansion.screen.SlotOutput;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.SimpleContainerData;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.SlotItemHandler;

public class KilnMenu extends BaseContainerMenu
{
	public KilnMenu(int id, Inventory inventory, FriendlyByteBuf extraData)
	{
		this(id, inventory, inventory.player.level().getBlockEntity(extraData.readBlockPos()), new SimpleContainerData(4));
	}
	
	public KilnMenu(int id, Inventory inventory, BlockEntity blockEntity, ContainerData data)
	{
		super(ModMenuTypes.KILN.get(), id, blockEntity, data);
		checkContainerSize(inventory, getInventorySlotCount());
		
		addPlayerInventory(inventory);
		addPlayerHotbar(inventory);
		
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent((itemHandler) ->
		{
			addSlot(new SlotFuel(itemHandler, KilnBlockEntity.FUEL_SLOT, 19, 32, ((KilnBlockEntity) this.blockEntity)::isItemFuel));
			addSlot(new SlotItemHandler(itemHandler, KilnBlockEntity.INPUT_SLOT, 58, 32));
			addSlot(new SlotOutput(itemHandler, KilnBlockEntity.OUTPUT_SLOT, 116, 33));
		});
		
		addDataSlots(data);
	}
	
	public int getBurnLeftScaled(int pixels)
	{
		int burnTime = data.get(0);
		int maxBurnTime = data.get(1);
		
		return burnTime > 0 ? burnTime * pixels / maxBurnTime : 0;
	}
	
	public int getCookProgressScaled(int pixels)
	{
		int cookTime = data.get(2);
		int neededCookTime = data.get(3);
		
		return neededCookTime != 0 && cookTime != 0 ? cookTime * pixels / neededCookTime : 0;
	}
	
	@Override
	public boolean stillValid(Player player)
	{
		return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()), player, ModBlocks.KILN.get());
	}
	
	@Override
	protected int getInventorySlotCount()
	{
		return 3;
	}
}
