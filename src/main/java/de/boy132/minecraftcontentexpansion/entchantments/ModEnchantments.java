package de.boy132.minecraftcontentexpansion.entchantments;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModEnchantments
{
	private static final DeferredRegister<Enchantment> ENCHANTMENTS = DeferredRegister.create(ForgeRegistries.ENCHANTMENTS, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<Enchantment> POWER_SAVING = ENCHANTMENTS.register("power_saving", PowerSavingEnchantment::new);
	
	public static void register(IEventBus eventBus)
	{
		ENCHANTMENTS.register(eventBus);
	}
}
