package de.boy132.minecraftcontentexpansion.entchantments;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.enchantment.Enchantment;

public class PowerSavingEnchantment extends Enchantment
{
	protected PowerSavingEnchantment()
	{
		super(Rarity.RARE, ModEnchantmentCategories.DRILL, new EquipmentSlot[] {EquipmentSlot.MAINHAND});
	}
}
