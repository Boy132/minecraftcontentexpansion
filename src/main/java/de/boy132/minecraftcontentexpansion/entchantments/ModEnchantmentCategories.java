package de.boy132.minecraftcontentexpansion.entchantments;

import de.boy132.minecraftcontentexpansion.item.energy.DrillItem;
import net.minecraft.world.item.enchantment.EnchantmentCategory;

public class ModEnchantmentCategories
{
	public static EnchantmentCategory DRILL = EnchantmentCategory.create("drill", (item) -> item instanceof DrillItem);
}
