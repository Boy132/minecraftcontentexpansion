package de.boy132.minecraftcontentexpansion.entity;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.entity.rock.AndesiteRockEntity;
import de.boy132.minecraftcontentexpansion.entity.rock.DioriteRockEntity;
import de.boy132.minecraftcontentexpansion.entity.rock.GraniteRockEntity;
import de.boy132.minecraftcontentexpansion.entity.rock.RockEntity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModEntityTypes
{
	private static final DeferredRegister<EntityType<?>> ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.ENTITY_TYPES, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<EntityType<RockEntity>> ROCK = ENTITY_TYPES.register("rock", () -> EntityType.Builder.<RockEntity>of(RockEntity::new, MobCategory.MISC).setTrackingRange(4).setUpdateInterval(10).setShouldReceiveVelocityUpdates(true).setCustomClientFactory(RockEntity::new).build(MinecraftContentExpansion.MODID + ":rock"));
	public static final RegistryObject<EntityType<AndesiteRockEntity>> ANDESITE_ROCK = ENTITY_TYPES.register("andesite_rock", () -> EntityType.Builder.<AndesiteRockEntity>of(AndesiteRockEntity::new, MobCategory.MISC).setTrackingRange(4).setUpdateInterval(10).setShouldReceiveVelocityUpdates(true).setCustomClientFactory(AndesiteRockEntity::new).build(MinecraftContentExpansion.MODID + ":andesite_rock"));
	public static final RegistryObject<EntityType<DioriteRockEntity>> DIORITE_ROCK = ENTITY_TYPES.register("diorite_rock", () -> EntityType.Builder.<DioriteRockEntity>of(DioriteRockEntity::new, MobCategory.MISC).setTrackingRange(4).setUpdateInterval(10).setShouldReceiveVelocityUpdates(true).setCustomClientFactory(DioriteRockEntity::new).build(MinecraftContentExpansion.MODID + ":diorite_rock"));
	public static final RegistryObject<EntityType<GraniteRockEntity>> GRANITE_ROCK = ENTITY_TYPES.register("granite_rock", () -> EntityType.Builder.<GraniteRockEntity>of(GraniteRockEntity::new, MobCategory.MISC).setTrackingRange(4).setUpdateInterval(10).setShouldReceiveVelocityUpdates(true).setCustomClientFactory(GraniteRockEntity::new).build(MinecraftContentExpansion.MODID + ":granite_rock"));
	
	public static void register(IEventBus eventBus)
	{
		ENTITY_TYPES.register(eventBus);
	}
}
