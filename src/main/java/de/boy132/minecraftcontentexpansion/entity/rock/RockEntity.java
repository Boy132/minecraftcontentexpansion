package de.boy132.minecraftcontentexpansion.entity.rock;

import de.boy132.minecraftcontentexpansion.entity.ModEntityTypes;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.packets.SpawnEntity;

public class RockEntity extends BaseRockEntity
{
	public RockEntity(EntityType<? extends ThrowableItemProjectile> type, Level level)
	{
		super(type, level);
	}
	
	public RockEntity(Level level, LivingEntity livingEntity)
	{
		super(ModEntityTypes.ROCK.get(), level, livingEntity);
	}
	
	public RockEntity(Level level, double x, double y, double z)
	{
		super(ModEntityTypes.ROCK.get(), level, x, y, z);
	}
	
	public RockEntity(SpawnEntity spawnEntity, Level level)
	{
		super(ModEntityTypes.ROCK.get(), level);
	}
	
	@Override
	protected Item getDefaultItem()
	{
		return ModItems.ROCK.get();
	}
}

