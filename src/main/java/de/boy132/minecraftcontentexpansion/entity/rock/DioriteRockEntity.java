package de.boy132.minecraftcontentexpansion.entity.rock;

import de.boy132.minecraftcontentexpansion.entity.ModEntityTypes;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.packets.SpawnEntity;

public class DioriteRockEntity extends BaseRockEntity
{
	public DioriteRockEntity(EntityType<? extends ThrowableItemProjectile> type, Level level)
	{
		super(type, level);
	}
	
	public DioriteRockEntity(Level world, LivingEntity livingEntity)
	{
		super(ModEntityTypes.DIORITE_ROCK.get(), world, livingEntity);
	}
	
	public DioriteRockEntity(Level level, double x, double y, double z)
	{
		super(ModEntityTypes.DIORITE_ROCK.get(), level, x, y, z);
	}
	
	public DioriteRockEntity(SpawnEntity spawnEntity, Level level)
	{
		super(ModEntityTypes.DIORITE_ROCK.get(), level);
	}
	
	@Override
	protected Item getDefaultItem()
	{
		return ModItems.DIORITE_ROCK.get();
	}
}

