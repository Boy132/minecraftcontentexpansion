package de.boy132.minecraftcontentexpansion.entity.rock;

import de.boy132.minecraftcontentexpansion.entity.ModEntityTypes;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.packets.SpawnEntity;

public class AndesiteRockEntity extends BaseRockEntity
{
	public AndesiteRockEntity(EntityType<? extends ThrowableItemProjectile> type, Level level)
	{
		super(type, level);
	}
	
	public AndesiteRockEntity(Level world, LivingEntity livingEntity)
	{
		super(ModEntityTypes.ANDESITE_ROCK.get(), world, livingEntity);
	}
	
	public AndesiteRockEntity(Level level, double x, double y, double z)
	{
		super(ModEntityTypes.ANDESITE_ROCK.get(), level, x, y, z);
	}
	
	public AndesiteRockEntity(SpawnEntity spawnEntity, Level level)
	{
		super(ModEntityTypes.ANDESITE_ROCK.get(), level);
	}
	
	@Override
	protected Item getDefaultItem()
	{
		return ModItems.ANDESITE_ROCK.get();
	}
}

