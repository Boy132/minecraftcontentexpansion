package de.boy132.minecraftcontentexpansion.entity.rock;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;


public abstract class BaseRockEntity extends ThrowableItemProjectile
{
	public BaseRockEntity(EntityType<? extends ThrowableItemProjectile> type, Level level)
	{
		super(type, level);
	}
	
	public BaseRockEntity(EntityType<? extends ThrowableItemProjectile> type, Level level, LivingEntity livingEntity)
	{
		super(type, livingEntity, level);
	}
	
	public BaseRockEntity(EntityType<? extends ThrowableItemProjectile> type, Level level, double x, double y, double z)
	{
		super(type, x, y, z, level);
	}
	
	@Override
	protected abstract Item getDefaultItem();
	
	@Override
	protected void onHitEntity(EntityHitResult result)
	{
		super.onHitEntity(result);
		
		Entity entity = result.getEntity();
		entity.hurt(damageSources().thrown(this, getOwner()), 3f);
	}
	
	@Override
	protected void onHit(HitResult result)
	{
		super.onHit(result);
		
		Level level = level();
		if(!level.isClientSide)
		{
			level.broadcastEntityEvent(this, (byte) 3);
			discard();
			
			if(level.random.nextInt(2) == 0)
				level.addFreshEntity(new ItemEntity(level, getX(), getY(), getZ(), getItem()));
		}
	}
	
}
