package de.boy132.minecraftcontentexpansion.world.feature;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.placement.*;

import java.util.List;

public class ModPlacedFeatures
{
	public static final ResourceKey<PlacedFeature> ORE_UNIM = createKey("ore_unim");
	public static final ResourceKey<PlacedFeature> ORE_RUBY = createKey("ore_ruby");
	public static final ResourceKey<PlacedFeature> ORE_ASCABIT = createKey("ore_ascabit");
	public static final ResourceKey<PlacedFeature> ORE_TIN = createKey("ore_tin");
	public static final ResourceKey<PlacedFeature> ORE_TITANIUM = createKey("ore_titanium");
	public static final ResourceKey<PlacedFeature> ORE_ZINC = createKey("ore_zinc");
	
	public static final ResourceKey<PlacedFeature> ORE_LIMESTONE_UPPER = createKey("ore_limestone_upper");
	public static final ResourceKey<PlacedFeature> ORE_LIMESTONE_LOWER = createKey("ore_limestone_lower");
	
	public static final ResourceKey<PlacedFeature> ORE_MARBLE = createKey("ore_marble");
	
	public static void bootstrap(BootstapContext<PlacedFeature> context)
	{
		HolderGetter<ConfiguredFeature<?, ?>> configuredFeatures = context.lookup(Registries.CONFIGURED_FEATURE);
		
		register(context, ORE_UNIM, configuredFeatures.getOrThrow(ModConfiguredFeatures.ORE_UNIM), commonOrePlacement(8, HeightRangePlacement.triangle(VerticalAnchor.absolute(-32), VerticalAnchor.absolute(64))));
		register(context, ORE_RUBY, configuredFeatures.getOrThrow(ModConfiguredFeatures.ORE_RUBY), commonOrePlacement(100, HeightRangePlacement.uniform(VerticalAnchor.absolute(-16), VerticalAnchor.absolute(128))));
		register(context, ORE_ASCABIT, configuredFeatures.getOrThrow(ModConfiguredFeatures.ORE_ASCABIT), commonOrePlacement(9, HeightRangePlacement.triangle(VerticalAnchor.absolute(-32), VerticalAnchor.absolute(64))));
		register(context, ORE_TIN, configuredFeatures.getOrThrow(ModConfiguredFeatures.ORE_TIN), commonOrePlacement(16, HeightRangePlacement.triangle(VerticalAnchor.absolute(-16), VerticalAnchor.absolute(128))));
		register(context, ORE_TITANIUM, configuredFeatures.getOrThrow(ModConfiguredFeatures.ORE_TITANIUM), commonOrePlacement(7, HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-80), VerticalAnchor.aboveBottom(80))));
		register(context, ORE_ZINC, configuredFeatures.getOrThrow(ModConfiguredFeatures.ORE_ZINC), commonOrePlacement(14, HeightRangePlacement.triangle(VerticalAnchor.absolute(-16), VerticalAnchor.absolute(128))));
		
		register(context, ORE_LIMESTONE_UPPER, configuredFeatures.getOrThrow(ModConfiguredFeatures.ORE_LIMESTONE), rareOrePlacement(6, HeightRangePlacement.uniform(VerticalAnchor.absolute(64), VerticalAnchor.absolute(128))));
		register(context, ORE_LIMESTONE_LOWER, configuredFeatures.getOrThrow(ModConfiguredFeatures.ORE_LIMESTONE), commonOrePlacement(2, HeightRangePlacement.uniform(VerticalAnchor.absolute(0), VerticalAnchor.absolute(60))));
		
		register(context, ORE_MARBLE, configuredFeatures.getOrThrow(ModConfiguredFeatures.ORE_MARBLE), commonOrePlacement(3, HeightRangePlacement.uniform(VerticalAnchor.absolute(0), VerticalAnchor.absolute(60))));
	}
	
	private static List<PlacementModifier> orePlacement(PlacementModifier placementModifier1, PlacementModifier placementModifier2)
	{
		return List.of(placementModifier1, InSquarePlacement.spread(), placementModifier2, BiomeFilter.biome());
	}
	
	private static List<PlacementModifier> commonOrePlacement(int veinsPerChunk, PlacementModifier placementModifier)
	{
		return orePlacement(CountPlacement.of(veinsPerChunk), placementModifier);
	}
	
	private static List<PlacementModifier> rareOrePlacement(int avg, PlacementModifier placementModifier)
	{
		return orePlacement(RarityFilter.onAverageOnceEvery(avg), placementModifier);
	}
	
	private static ResourceKey<PlacedFeature> createKey(String name)
	{
		return ResourceKey.create(Registries.PLACED_FEATURE, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, name));
	}
	
	private static void register(BootstapContext<PlacedFeature> context, ResourceKey<PlacedFeature> key, Holder<ConfiguredFeature<?, ?>> configuration, List<PlacementModifier> modifiers)
	{
		context.register(key, new PlacedFeature(configuration, List.copyOf(modifiers)));
	}
	
	private static void register(BootstapContext<PlacedFeature> context, ResourceKey<PlacedFeature> key, Holder<ConfiguredFeature<?, ?>> configuration, PlacementModifier... modifiers)
	{
		register(context, key, configuration, List.of(modifiers));
	}
}
