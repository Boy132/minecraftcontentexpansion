package de.boy132.minecraftcontentexpansion.networking.packet;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.item.backpack.BackpackItem;
import de.boy132.minecraftcontentexpansion.item.backpack.EnderBackpackItem;
import de.boy132.minecraftcontentexpansion.screen.backpack.BackpackMenu;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.inventory.ChestMenu;
import net.minecraft.world.inventory.PlayerEnderChestContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.network.CustomPayloadEvent;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotResult;

import java.util.Optional;

public class ServerboundOpenBackpackPacket
{
	public ServerboundOpenBackpackPacket()
	{
	}
	
	public ServerboundOpenBackpackPacket(FriendlyByteBuf friendlyByteBuf)
	{
	}
	
	public void encode(FriendlyByteBuf friendlyByteBuf)
	{
	}
	
	public void handle(CustomPayloadEvent.Context context)
	{
		ServerPlayer player = context.getSender();
		
		Optional<SlotResult> backpackCurio = CuriosApi.getCuriosHelper().findFirstCurio(player, (stack) -> stack.getItem() instanceof BackpackItem || stack.getItem() instanceof EnderBackpackItem);
		backpackCurio.ifPresent(slotResult ->
		{
			ItemStack stack = slotResult.stack();
			if(!stack.isEmpty())
			{
				if(stack.getItem() instanceof EnderBackpackItem)
				{
					PlayerEnderChestContainer enderChestInventory = player.getEnderChestInventory();
					player.openMenu(new SimpleMenuProvider((id, inventory, target) -> ChestMenu.threeRows(id, inventory, enderChestInventory), Component.translatable("item." + MinecraftContentExpansion.MODID + ".ender_backpack")));
				} else if(stack.getItem() instanceof BackpackItem)
					player.openMenu(new SimpleMenuProvider((id, inventory, target) -> new BackpackMenu(id, inventory, stack), stack.getHoverName()), (friendlyByteBuf) -> friendlyByteBuf.writeItemStack(stack, false));
				
			}
		});
	}
}
