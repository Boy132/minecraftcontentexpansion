package de.boy132.minecraftcontentexpansion.networking;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.networking.packet.ServerboundOpenBackpackPacket;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.network.ChannelBuilder;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.SimpleChannel;

public class ModNetworkMessages
{
	private static final SimpleChannel CHANNEL = ChannelBuilder.named(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "main"))
			.networkProtocolVersion(1)
			.clientAcceptedVersions((status, version) -> true)
			.serverAcceptedVersions((status, version) -> true).simpleChannel();
	
	public static void register()
	{
		CHANNEL.messageBuilder(ServerboundOpenBackpackPacket.class, NetworkDirection.PLAY_TO_SERVER)
				.encoder(ServerboundOpenBackpackPacket::encode)
				.decoder(ServerboundOpenBackpackPacket::new)
				.consumerMainThread(ServerboundOpenBackpackPacket::handle).add();
	}
	
	public static void sendToServer(Object msg)
	{
		CHANNEL.send(msg, PacketDistributor.SERVER.noArg());
	}
	
	public static void sendToPlayer(Object msg, ServerPlayer player)
	{
		CHANNEL.send(msg, PacketDistributor.PLAYER.with(player));
	}
	
	public static void sendToAll(Object msg)
	{
		CHANNEL.send(msg, PacketDistributor.ALL.noArg());
	}
}
