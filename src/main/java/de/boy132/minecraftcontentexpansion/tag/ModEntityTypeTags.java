package de.boy132.minecraftcontentexpansion.tag;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraftforge.registries.ForgeRegistries;

public class ModEntityTypeTags
{
	public static final TagKey<EntityType<?>> ROCKS = mceTag("rocks");
	
	private static TagKey<EntityType<?>> mceTag(String name)
	{
		return TagKey.create(ForgeRegistries.ENTITY_TYPES.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, name));
	}
	
	private static TagKey<EntityType<?>> forgeTag(String name)
	{
		return TagKey.create(ForgeRegistries.ENTITY_TYPES.getRegistryKey(), ResourceLocation.fromNamespaceAndPath("forge", name));
	}
}
