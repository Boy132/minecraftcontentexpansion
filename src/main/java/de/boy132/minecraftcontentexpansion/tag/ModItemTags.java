package de.boy132.minecraftcontentexpansion.tag;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;

public class ModItemTags
{
	public static final TagKey<Item> ORES_UNIM = forgeTag("ores/unim");
	public static final TagKey<Item> STORAGE_BLOCKS_UNIM = forgeTag("storage_blocks/unim");
	
	public static final TagKey<Item> ORES_RUBY = forgeTag("ores/ruby");
	public static final TagKey<Item> STORAGE_BLOCKS_RUBY = forgeTag("storage_blocks/ruby");
	
	public static final TagKey<Item> ORES_ASCABIT = forgeTag("ores/ascabit");
	public static final TagKey<Item> STORAGE_BLOCKS_ASCABIT = forgeTag("storage_blocks/ascabit");
	public static final TagKey<Item> STORAGE_BLOCKS_RAW_ASCABIT = forgeTag("storage_blocks/raw_ascabit");
	
	public static final TagKey<Item> ORES_TIN = forgeTag("ores/tin");
	public static final TagKey<Item> STORAGE_BLOCKS_TIN = forgeTag("storage_blocks/tin");
	public static final TagKey<Item> STORAGE_BLOCKS_RAW_TIN = forgeTag("storage_blocks/raw_tin");
	
	public static final TagKey<Item> ORES_TITANIUM = forgeTag("ores/titanium");
	public static final TagKey<Item> STORAGE_BLOCKS_TITANIUM = forgeTag("storage_blocks/titanium");
	public static final TagKey<Item> STORAGE_BLOCKS_RAW_TITANIUM = forgeTag("storage_blocks/raw_titanium");
	
	public static final TagKey<Item> ORES_ZINC = forgeTag("ores/zinc");
	public static final TagKey<Item> STORAGE_BLOCKS_ZINC = forgeTag("storage_blocks/zinc");
	public static final TagKey<Item> STORAGE_BLOCKS_RAW_ZINC = forgeTag("storage_blocks/raw_zinc");
	
	public static final TagKey<Item> STORAGE_BLOCKS_STEEL = forgeTag("storage_blocks/steel");
	
	public static final TagKey<Item> STORAGE_BLOCKS_BRONZE = forgeTag("storage_blocks/bronze");
	
	public static final TagKey<Item> STORAGE_BLOCKS_BRASS = forgeTag("storage_blocks/brass");
	
	public static final TagKey<Item> INGOTS_UNIM = forgeTag("ingots/unim");
	public static final TagKey<Item> DUSTS_UNIM = forgeTag("dusts/unim");
	
	public static final TagKey<Item> GEMS_RUBY = forgeTag("gems/ruby");
	
	public static final TagKey<Item> INGOTS_ASCABIT = forgeTag("ingots/ascabit");
	public static final TagKey<Item> RAW_MATERIALS_ASCABIT = forgeTag("raw_materials/ascabit");
	
	public static final TagKey<Item> INGOTS_TIN = forgeTag("ingots/tin");
	public static final TagKey<Item> RAW_MATERIALS_TIN = forgeTag("raw_materials/tin");
	
	public static final TagKey<Item> INGOTS_TITANIUM = forgeTag("ingots/titanium");
	public static final TagKey<Item> RAW_MATERIALS_TITANIUM = forgeTag("raw_materials/titanium");
	
	public static final TagKey<Item> INGOTS_ZINC = forgeTag("ingots/zinc");
	public static final TagKey<Item> RAW_MATERIALS_ZINC = forgeTag("raw_materials/zinc");
	
	public static final TagKey<Item> INGOTS_STEEL = forgeTag("ingots/steel");
	
	public static final TagKey<Item> INGOTS_BRONZE = forgeTag("ingots/bronze");
	
	public static final TagKey<Item> INGOTS_BRASS = forgeTag("ingots/brass");
	
	public static final TagKey<Item> DUSTS_COAL = forgeTag("dusts/coal");
	
	public static final TagKey<Item> TOOLS_HAMMERS = forgeTag("tools/hammers");
	public static final TagKey<Item> TOOLS_KNIVES = forgeTag("tools/knives");
	
	public static final TagKey<Item> CONTAINERS_WATER = forgeTag("containers/water");
	
	public static final TagKey<Item> ROCKS = forgeTag("rocks");
	
	public static final TagKey<Item> CHOPPING_BLOCKS = forgeTag("chopping_blocks");
	
	public static final TagKey<Item> DRYING_RACKS = forgeTag("drying_racks");
	
	private static TagKey<Item> mceTag(String name)
	{
		return ItemTags.create(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, name));
	}
	
	private static TagKey<Item> forgeTag(String name)
	{
		return ItemTags.create(ResourceLocation.fromNamespaceAndPath("forge", name));
	}
}
