package de.boy132.minecraftcontentexpansion.tag;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.block.Block;

public class ModBlockTags
{
	public static final TagKey<Block> ORES_UNIM = forgeTag("ores/unim");
	public static final TagKey<Block> STORAGE_BLOCKS_UNIM = forgeTag("storage_blocks/unim");
	
	public static final TagKey<Block> ORES_RUBY = forgeTag("ores/ruby");
	public static final TagKey<Block> STORAGE_BLOCKS_RUBY = forgeTag("storage_blocks/ruby");
	
	public static final TagKey<Block> ORES_ASCABIT = forgeTag("ores/ascabit");
	public static final TagKey<Block> STORAGE_BLOCKS_ASCABIT = forgeTag("storage_blocks/ascabit");
	public static final TagKey<Block> STORAGE_BLOCKS_RAW_ASCABIT = forgeTag("storage_blocks/raw_ascabit");
	
	public static final TagKey<Block> ORES_TIN = forgeTag("ores/tin");
	public static final TagKey<Block> STORAGE_BLOCKS_TIN = forgeTag("storage_blocks/tin");
	public static final TagKey<Block> STORAGE_BLOCKS_RAW_TIN = forgeTag("storage_blocks/raw_tin");
	
	public static final TagKey<Block> ORES_TITANIUM = forgeTag("ores/titanium");
	public static final TagKey<Block> STORAGE_BLOCKS_TITANIUM = forgeTag("storage_blocks/titanium");
	public static final TagKey<Block> STORAGE_BLOCKS_RAW_TITANIUM = forgeTag("storage_blocks/raw_titanium");
	
	public static final TagKey<Block> ORES_ZINC = forgeTag("ores/zinc");
	public static final TagKey<Block> STORAGE_BLOCKS_ZINC = forgeTag("storage_blocks/zinc");
	public static final TagKey<Block> STORAGE_BLOCKS_RAW_ZINC = forgeTag("storage_blocks/raw_zinc");
	
	public static final TagKey<Block> STORAGE_BLOCKS_STEEL = forgeTag("storage_blocks/steel");
	
	public static final TagKey<Block> STORAGE_BLOCKS_BRONZE = forgeTag("storage_blocks/bronze");
	
	public static final TagKey<Block> STORAGE_BLOCKS_BRASS = forgeTag("storage_blocks/brass");
	
	private static TagKey<Block> mceTag(String name)
	{
		return BlockTags.create(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, name));
	}
	
	private static TagKey<Block> forgeTag(String name)
	{
		return BlockTags.create(ResourceLocation.fromNamespaceAndPath("forge", name));
	}
}
