package de.boy132.minecraftcontentexpansion.recipe.dryingrack;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.block.entity.dryingrack.DryingRackBlockEntity;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.registries.ForgeRegistries;

public class DryingRackRecipeSerializer implements RecipeSerializer<DryingRackRecipe>
{
	private final Codec<DryingRackRecipe> codec;
	
	public DryingRackRecipeSerializer()
	{
		codec = RecordCodecBuilder.create((instance) ->
		{
			return instance.group(Ingredient.CODEC_NONEMPTY.fieldOf("ingredient").forGetter((recipe) ->
			{
				return recipe.ingredient;
			}), ForgeRegistries.ITEMS.getCodec().fieldOf("result").forGetter((recipe) ->
			{
				return recipe.result.asItem();
			}), Codec.INT.fieldOf("dryingtime").orElse(DryingRackBlockEntity.STANDARD_DRYING_TIME).forGetter((recipe) ->
			{
				return recipe.dryingTime;
			})).apply(instance, DryingRackRecipe::new);
		});
	}
	
	@Override
	public Codec<DryingRackRecipe> codec()
	{
		return codec;
	}
	
	@Override
	public DryingRackRecipe fromNetwork(FriendlyByteBuf buffer)
	{
		Ingredient ingredient = Ingredient.fromNetwork(buffer);
		ItemLike result = buffer.readItem().getItem();
		int dryingTime = buffer.readVarInt();
		return new DryingRackRecipe(ingredient, result, dryingTime);
	}
	
	@Override
	public void toNetwork(FriendlyByteBuf buffer, DryingRackRecipe recipe)
	{
		recipe.ingredient.toNetwork(buffer);
		buffer.writeItem(new ItemStack(recipe.result));
		buffer.writeVarInt(recipe.dryingTime);
	}
}
