package de.boy132.minecraftcontentexpansion.recipe.smelter;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.smelter.SmelterBlockEntity;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeSerializers;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import net.minecraft.core.NonNullList;
import net.minecraft.core.RegistryAccess;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;

public class SmelterRecipe implements Recipe<Container>
{
	protected final NonNullList<Ingredient> ingredients;
	protected final ItemStack result;
	protected final float minHeat;
	protected final int smeltTime;
	
	public SmelterRecipe(NonNullList<Ingredient> ingredients, ItemStack result, float minHeat, int smeltTime)
	{
		if(ingredients.size() != 4)
			throw new IllegalArgumentException("ingredients list size is not equal to 4!");
		
		this.ingredients = ingredients;
		this.result = result;
		this.minHeat = minHeat;
		this.smeltTime = smeltTime;
	}
	
	@Override
	public ItemStack getToastSymbol()
	{
		return new ItemStack(ModBlocks.SMELTER.get());
	}
	
	@Override
	public boolean matches(Container container, Level level)
	{
		if(!ingredients.get(0).test(container.getItem(SmelterBlockEntity.INPUT_SLOT_1)))
			return false;
		if(!ingredients.get(1).test(container.getItem(SmelterBlockEntity.INPUT_SLOT_2)))
			return false;
		if(!ingredients.get(2).test(container.getItem(SmelterBlockEntity.INPUT_SLOT_3)))
			return false;
		return ingredients.get(3).test(container.getItem(SmelterBlockEntity.INPUT_SLOT_4));
	}
	
	@Override
	public ItemStack assemble(Container container, RegistryAccess registryAccess)
	{
		return getResultItem(registryAccess).copy();
	}
	
	@Override
	public boolean canCraftInDimensions(int width, int height)
	{
		return false;
	}
	
	@Override
	public NonNullList<Ingredient> getIngredients()
	{
		return ingredients;
	}
	
	@Override
	public ItemStack getResultItem(RegistryAccess registryAccess)
	{
		return result;
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return ModRecipeSerializers.SMELTER.get();
	}
	
	@Override
	public RecipeType<?> getType()
	{
		return ModRecipeTypes.SMELTER.get();
	}
	
	public float getMinHeat()
	{
		return minHeat;
	}
	
	public int getSmeltTime()
	{
		return smeltTime;
	}
}
