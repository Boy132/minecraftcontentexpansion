package de.boy132.minecraftcontentexpansion.recipe.smelter;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.smelter.SmelterBlockEntity;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.RecipeUnlockedTrigger;
import net.minecraft.core.NonNullList;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SmelterRecipeBuilder implements RecipeBuilder
{
	private final Map<String, Criterion<?>> criteria = new LinkedHashMap<>();
	
	private final NonNullList<Ingredient> ingredients;
	private final Item result;
	private final int count;
	private final float minHeat;
	private final int smeltTime;
	
	private String group;
	
	private SmelterRecipeBuilder(Ingredient[] ingredients, Item result, int count, float minHeat, int smeltTime)
	{
		this.ingredients = NonNullList.create();
		this.ingredients.addAll(List.of(ingredients));
		this.result = result;
		this.count = count;
		this.minHeat = minHeat;
		this.smeltTime = smeltTime;
	}
	
	public static SmelterRecipeBuilder smelter(Ingredient[] ingredients, ItemStack result, float minHeat, int smeltTime)
	{
		return new SmelterRecipeBuilder(ingredients, result.getItem(), result.getCount(), minHeat, smeltTime);
	}
	
	public static SmelterRecipeBuilder smelter(Ingredient[] ingredients, ItemStack result, float minHeat)
	{
		return smelter(ingredients, result, minHeat, SmelterBlockEntity.STANDARD_SMELT_TIME);
	}
	
	@Override
	public RecipeBuilder unlockedBy(String name, Criterion<?> criterion)
	{
		criteria.put(name, criterion);
		return this;
	}
	
	@Override
	public RecipeBuilder group(String group)
	{
		this.group = group;
		return this;
	}
	
	@Override
	public Item getResult()
	{
		return result;
	}
	
	@Override
	public void save(RecipeOutput recipeOutput, ResourceLocation id)
	{
		ResourceLocation name = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "smelter/" + id.getPath());
		ensureValid(name);
		
		Advancement.Builder advancementBuilder = recipeOutput.advancement()
				.addCriterion("has_the_recipe", RecipeUnlockedTrigger.unlocked(name))
				.rewards(AdvancementRewards.Builder.recipe(name))
				.requirements(AdvancementRequirements.Strategy.OR);
		criteria.forEach(advancementBuilder::addCriterion);
		
		recipeOutput.accept(name, new SmelterRecipe(ingredients, new ItemStack(result, count), minHeat, smeltTime), advancementBuilder.build(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "recipes/smelter/" + id.getPath())));
	}
	
	private void ensureValid(ResourceLocation id)
	{
		if(ingredients.size() != 4)
			throw new IllegalArgumentException("Ingredients array size is not equal to 4 for recipe " + id);
		
		if(criteria.isEmpty())
			throw new IllegalStateException("No way of obtaining recipe " + id);
	}
}
