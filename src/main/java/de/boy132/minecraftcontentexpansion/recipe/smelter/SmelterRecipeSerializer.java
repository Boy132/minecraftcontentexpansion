package de.boy132.minecraftcontentexpansion.recipe.smelter;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.block.entity.smelter.SmelterBlockEntity;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.util.Mth;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;

public class SmelterRecipeSerializer implements RecipeSerializer<SmelterRecipe>
{
	private final Codec<SmelterRecipe> codec;
	
	public SmelterRecipeSerializer()
	{
		codec = RecordCodecBuilder.create((instance) ->
		{
			return instance.group(Ingredient.CODEC_NONEMPTY.listOf().fieldOf("ingredients").flatXmap((ingredients) ->
			{
				Ingredient[] ingredientArray = ingredients.stream().filter((ingredient) ->
				{
					return !ingredient.isEmpty();
				}).toArray(Ingredient[]::new);
				if(ingredientArray.length == 0)
				{
					return DataResult.error(() -> "No ingredients for electric smelter recipe");
				} else
				{
					return ingredientArray.length > 4 ? DataResult.error(() -> "Too many ingredients for electric smelter recipe") : DataResult.success(NonNullList.of(Ingredient.EMPTY, ingredientArray));
				}
			}, DataResult::success).forGetter((recipe) ->
			{
				return recipe.ingredients;
			}), ItemStack.ITEM_WITH_COUNT_CODEC.fieldOf("result").forGetter((recipe) ->
			{
				return recipe.result;
			}), Codec.FLOAT.fieldOf("minheat").orElse(SmelterBlockEntity.STANDARD_MIN_HEAT).forGetter((recipe) ->
			{
				return recipe.minHeat;
			}), Codec.INT.fieldOf("smelttime").orElse(SmelterBlockEntity.STANDARD_SMELT_TIME).forGetter((recipe) ->
			{
				return recipe.smeltTime;
			})).apply(instance, SmelterRecipe::new);
		});
	}
	
	@Override
	public Codec<SmelterRecipe> codec()
	{
		return codec;
	}
	
	@Override
	public SmelterRecipe fromNetwork(FriendlyByteBuf buffer)
	{
		NonNullList<Ingredient> ingredients = NonNullList.withSize(4, Ingredient.EMPTY);
		for(int i = 0; i < ingredients.size(); i++)
			ingredients.set(i, Ingredient.fromNetwork(buffer));
		
		ItemStack result = buffer.readItem();
		float minHeat = Mth.clamp(buffer.readFloat(), 0f, 1f);
		int smeltTime = buffer.readVarInt();
		return new SmelterRecipe(ingredients, result, minHeat, smeltTime);
	}
	
	@Override
	public void toNetwork(FriendlyByteBuf buffer, SmelterRecipe recipe)
	{
		recipe.ingredients.forEach(ingredient -> ingredient.toNetwork(buffer));
		buffer.writeItem(recipe.result);
		buffer.writeFloat(recipe.minHeat);
		buffer.writeVarInt(recipe.smeltTime);
	}
}
