package de.boy132.minecraftcontentexpansion.recipe.choppingblock;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;

public class ChoppingBlockRecipeSerializer implements RecipeSerializer<ChoppingBlockRecipe>
{
	private final Codec<ChoppingBlockRecipe> codec;
	
	public ChoppingBlockRecipeSerializer()
	{
		codec = RecordCodecBuilder.create((instance) ->
		{
			return instance.group(Ingredient.CODEC_NONEMPTY.fieldOf("ingredient").forGetter((recipe) ->
			{
				return recipe.ingredient;
			}), ItemStack.ITEM_WITH_COUNT_CODEC.fieldOf("result").forGetter((recipe) ->
			{
				return recipe.result;
			})).apply(instance, ChoppingBlockRecipe::new);
		});
	}
	
	@Override
	public Codec<ChoppingBlockRecipe> codec()
	{
		return codec;
	}
	
	@Override
	public ChoppingBlockRecipe fromNetwork(FriendlyByteBuf buffer)
	{
		Ingredient ingredient = Ingredient.fromNetwork(buffer);
		ItemStack result = buffer.readItem();
		return new ChoppingBlockRecipe(ingredient, result);
	}
	
	@Override
	public void toNetwork(FriendlyByteBuf buffer, ChoppingBlockRecipe recipe)
	{
		recipe.ingredient.toNetwork(buffer);
		buffer.writeItem(recipe.result);
	}
}
