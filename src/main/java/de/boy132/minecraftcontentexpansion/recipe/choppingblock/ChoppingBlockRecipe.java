package de.boy132.minecraftcontentexpansion.recipe.choppingblock;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.choppingblock.ChoppingBlockEntity;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeSerializers;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import net.minecraft.core.NonNullList;
import net.minecraft.core.RegistryAccess;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;

public class ChoppingBlockRecipe implements Recipe<Container>
{
	protected final Ingredient ingredient;
	protected final ItemStack result;
	
	public ChoppingBlockRecipe(Ingredient ingredient, ItemStack result)
	{
		this.ingredient = ingredient;
		this.result = result;
	}
	
	@Override
	public ItemStack getToastSymbol()
	{
		return new ItemStack(ModBlocks.OAK_CHOPPING_BLOCK.get());
	}
	
	@Override
	public boolean matches(Container container, Level level)
	{
		return ingredient.test(container.getItem(ChoppingBlockEntity.SLOT));
	}
	
	@Override
	public ItemStack assemble(Container container, RegistryAccess registryAccess)
	{
		return getResultItem(registryAccess).copy();
	}
	
	@Override
	public boolean canCraftInDimensions(int width, int height)
	{
		return false;
	}
	
	@Override
	public NonNullList<Ingredient> getIngredients()
	{
		NonNullList<Ingredient> ingredients = NonNullList.create();
		ingredients.add(ingredient);
		return ingredients;
	}
	
	@Override
	public ItemStack getResultItem(RegistryAccess registryAccess)
	{
		return result;
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return ModRecipeSerializers.CHOPPING_BLOCK.get();
	}
	
	@Override
	public RecipeType<?> getType()
	{
		return ModRecipeTypes.CHOPPING_BLOCK.get();
	}
}
