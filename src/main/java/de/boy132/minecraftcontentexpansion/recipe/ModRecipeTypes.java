package de.boy132.minecraftcontentexpansion.recipe;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.recipe.choppingblock.ChoppingBlockRecipe;
import de.boy132.minecraftcontentexpansion.recipe.dryingrack.DryingRackRecipe;
import de.boy132.minecraftcontentexpansion.recipe.electricsmelter.ElectricSmelterRecipe;
import de.boy132.minecraftcontentexpansion.recipe.greenhouse.GreenhouseRecipe;
import de.boy132.minecraftcontentexpansion.recipe.hydraulicpress.HydraulicPressRecipe;
import de.boy132.minecraftcontentexpansion.recipe.kiln.KilnRecipe;
import de.boy132.minecraftcontentexpansion.recipe.millstone.MillstoneRecipe;
import de.boy132.minecraftcontentexpansion.recipe.smelter.SmelterRecipe;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModRecipeTypes
{
	private static final DeferredRegister<RecipeType<?>> RECIPE_TYPES = DeferredRegister.create(ForgeRegistries.RECIPE_TYPES, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<RecipeType<KilnRecipe>> KILN = RECIPE_TYPES.register("kiln", () -> RecipeType.simple(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "kiln")));
	public static final RegistryObject<RecipeType<SmelterRecipe>> SMELTER = RECIPE_TYPES.register("smelter", () -> RecipeType.simple(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "smelter")));
	public static final RegistryObject<RecipeType<MillstoneRecipe>> MILLSTONE = RECIPE_TYPES.register("millstone", () -> RecipeType.simple(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "millstone")));
	
	public static final RegistryObject<RecipeType<ChoppingBlockRecipe>> CHOPPING_BLOCK = RECIPE_TYPES.register("chopping_block", () -> RecipeType.simple(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "chopping_block")));
	
	public static final RegistryObject<RecipeType<DryingRackRecipe>> DRYING_RACK = RECIPE_TYPES.register("drying_rack", () -> RecipeType.simple(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "drying_rack")));
	
	public static final RegistryObject<RecipeType<ElectricSmelterRecipe>> ELECTRIC_SMELTER = RECIPE_TYPES.register("electric_smelter", () -> RecipeType.simple(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "electric_smelter")));
	
	public static final RegistryObject<RecipeType<GreenhouseRecipe>> GREENHOUSE = RECIPE_TYPES.register("greenhouse", () -> RecipeType.simple(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "greenhouse")));
	
	public static final RegistryObject<RecipeType<HydraulicPressRecipe>> HYDRAULIC_PRESS = RECIPE_TYPES.register("hydraulic_press", () -> RecipeType.simple(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "hydraulic_press")));
	
	public static void register(IEventBus eventBus)
	{
		RECIPE_TYPES.register(eventBus);
	}
}
