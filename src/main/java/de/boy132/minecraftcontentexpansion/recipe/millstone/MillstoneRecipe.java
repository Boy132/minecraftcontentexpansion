package de.boy132.minecraftcontentexpansion.recipe.millstone;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.millstone.MillstoneBlockEntity;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeSerializers;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import net.minecraft.core.NonNullList;
import net.minecraft.core.RegistryAccess;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;

public class MillstoneRecipe implements Recipe<Container>
{
	protected final Ingredient ingredient;
	protected final ItemStack result;
	protected final int millTime;
	
	public MillstoneRecipe(Ingredient ingredient, ItemStack result, int millTime)
	{
		this.ingredient = ingredient;
		this.result = result;
		this.millTime = millTime;
	}
	
	@Override
	public ItemStack getToastSymbol()
	{
		return new ItemStack(ModBlocks.MILLSTONE.get());
	}
	
	@Override
	public boolean matches(Container container, Level world)
	{
		return ingredient.test(container.getItem(MillstoneBlockEntity.INPUT_SLOT));
	}
	
	@Override
	public ItemStack assemble(Container container, RegistryAccess registryAccess)
	{
		return getResultItem(registryAccess).copy();
	}
	
	@Override
	public boolean canCraftInDimensions(int width, int height)
	{
		return false;
	}
	
	@Override
	public NonNullList<Ingredient> getIngredients()
	{
		NonNullList<Ingredient> ingredients = NonNullList.create();
		ingredients.add(ingredient);
		return ingredients;
	}
	
	@Override
	public ItemStack getResultItem(RegistryAccess registryAccess)
	{
		return result;
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return ModRecipeSerializers.MILLSTONE.get();
	}
	
	@Override
	public RecipeType<?> getType()
	{
		return ModRecipeTypes.MILLSTONE.get();
	}
	
	public int getMillTime()
	{
		return millTime;
	}
}
