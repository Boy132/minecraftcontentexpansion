package de.boy132.minecraftcontentexpansion.recipe.millstone;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.millstone.MillstoneBlockEntity;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.RecipeUnlockedTrigger;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;

import java.util.LinkedHashMap;
import java.util.Map;

public class MillstoneRecipeBuilder implements RecipeBuilder
{
	private final Map<String, Criterion<?>> criteria = new LinkedHashMap<>();
	
	private final Ingredient ingredient;
	private final Item result;
	private final int count;
	private final int millTime;
	
	private String group;
	
	private MillstoneRecipeBuilder(Ingredient ingredient, Item result, int count, int millTime)
	{
		this.ingredient = ingredient;
		this.result = result;
		this.count = count;
		this.millTime = millTime;
	}
	
	public static MillstoneRecipeBuilder millstone(Ingredient ingredient, ItemStack result, int millTime)
	{
		return new MillstoneRecipeBuilder(ingredient, result.getItem(), result.getCount(), millTime);
	}
	
	public static MillstoneRecipeBuilder millstone(Ingredient ingredient, ItemStack result)
	{
		return millstone(ingredient, result, MillstoneBlockEntity.STANDARD_MILL_TIME);
	}
	
	@Override
	public RecipeBuilder unlockedBy(String name, Criterion<?> criterion)
	{
		criteria.put(name, criterion);
		return this;
	}
	
	@Override
	public RecipeBuilder group(String group)
	{
		this.group = group;
		return this;
	}
	
	@Override
	public Item getResult()
	{
		return result;
	}
	
	@Override
	public void save(RecipeOutput recipeOutput, ResourceLocation id)
	{
		ResourceLocation name = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "millstone/" + id.getPath());
		ensureValid(name);
		
		Advancement.Builder advancementBuilder = recipeOutput.advancement()
				.addCriterion("has_the_recipe", RecipeUnlockedTrigger.unlocked(name))
				.rewards(AdvancementRewards.Builder.recipe(name))
				.requirements(AdvancementRequirements.Strategy.OR);
		criteria.forEach(advancementBuilder::addCriterion);
		
		recipeOutput.accept(name, new MillstoneRecipe(ingredient, new ItemStack(result, count), millTime), advancementBuilder.build(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "recipes/millstone/" + id.getPath())));
	}
	
	private void ensureValid(ResourceLocation id)
	{
		if(criteria.isEmpty())
			throw new IllegalStateException("No way of obtaining recipe " + id);
	}
}
