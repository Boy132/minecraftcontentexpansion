package de.boy132.minecraftcontentexpansion.recipe.millstone;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.hydraulicpress.HydraulicPressBlockEntity;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;

public class MillstoneRecipeSerializer implements RecipeSerializer<MillstoneRecipe>
{
	private final Codec<MillstoneRecipe> codec;
	
	public MillstoneRecipeSerializer()
	{
		codec = RecordCodecBuilder.create((instance) ->
		{
			return instance.group(Ingredient.CODEC_NONEMPTY.fieldOf("ingredient").forGetter((recipe) ->
			{
				return recipe.ingredient;
			}), ItemStack.ITEM_WITH_COUNT_CODEC.fieldOf("result").forGetter((recipe) ->
			{
				return recipe.result;
			}), Codec.INT.fieldOf("milltime").orElse(HydraulicPressBlockEntity.STANDARD_PRESS_TIME).forGetter((recipe) ->
			{
				return recipe.millTime;
			})).apply(instance, MillstoneRecipe::new);
		});
	}
	
	@Override
	public Codec<MillstoneRecipe> codec()
	{
		return codec;
	}
	
	@Override
	public MillstoneRecipe fromNetwork(FriendlyByteBuf buffer)
	{
		Ingredient ingredient = Ingredient.fromNetwork(buffer);
		ItemStack result = buffer.readItem();
		int millTime = buffer.readVarInt();
		return new MillstoneRecipe(ingredient, result, millTime);
	}
	
	@Override
	public void toNetwork(FriendlyByteBuf buffer, MillstoneRecipe recipe)
	{
		recipe.ingredient.toNetwork(buffer);
		buffer.writeItem(recipe.result);
		buffer.writeVarInt(recipe.millTime);
	}
}
