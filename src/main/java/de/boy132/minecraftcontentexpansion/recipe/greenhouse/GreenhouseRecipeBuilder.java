package de.boy132.minecraftcontentexpansion.recipe.greenhouse;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.SoilType;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.RecipeUnlockedTrigger;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;

import java.util.LinkedHashMap;
import java.util.Map;

public class GreenhouseRecipeBuilder implements RecipeBuilder
{
	private final Map<String, Criterion<?>> criteria = new LinkedHashMap<>();
	
	private final Ingredient ingredient;
	private final Item result;
	private final int count;
	private final SoilType soil;
	private final boolean needsWater;
	
	private String group;
	
	private GreenhouseRecipeBuilder(Ingredient ingredient, Item result, int count, SoilType soil, boolean needsWater)
	{
		this.ingredient = ingredient;
		this.result = result;
		this.count = count;
		this.soil = soil;
		this.needsWater = needsWater;
	}
	
	public static GreenhouseRecipeBuilder greenhouse(Ingredient ingredient, ItemStack result, SoilType soil, boolean needsWater)
	{
		return new GreenhouseRecipeBuilder(ingredient, result.getItem(), result.getCount(), soil, needsWater);
	}
	
	public static GreenhouseRecipeBuilder greenhouse(Ingredient ingredient, ItemStack result, SoilType soil)
	{
		return greenhouse(ingredient, result, soil, false);
	}
	
	@Override
	public RecipeBuilder unlockedBy(String name, Criterion<?> criterion)
	{
		criteria.put(name, criterion);
		return this;
	}
	
	@Override
	public RecipeBuilder group(String group)
	{
		this.group = group;
		return this;
	}
	
	@Override
	public Item getResult()
	{
		return result;
	}
	
	@Override
	public void save(RecipeOutput recipeOutput, ResourceLocation id)
	{
		ResourceLocation name = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "greenhouse/" + id.getPath());
		ensureValid(name);
		
		Advancement.Builder advancementBuilder = recipeOutput.advancement()
				.addCriterion("has_the_recipe", RecipeUnlockedTrigger.unlocked(name))
				.rewards(AdvancementRewards.Builder.recipe(name))
				.requirements(AdvancementRequirements.Strategy.OR);
		criteria.forEach(advancementBuilder::addCriterion);
		
		recipeOutput.accept(name, new GreenhouseRecipe(ingredient, new ItemStack(result, count), soil, needsWater), advancementBuilder.build(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "recipes/greenhouse/" + id.getPath())));
	}
	
	private void ensureValid(ResourceLocation id)
	{
		if(criteria.isEmpty())
			throw new IllegalStateException("No way of obtaining recipe " + id);
	}
}
