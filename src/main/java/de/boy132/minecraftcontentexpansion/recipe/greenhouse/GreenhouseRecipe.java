package de.boy132.minecraftcontentexpansion.recipe.greenhouse;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.SoilType;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeSerializers;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import net.minecraft.core.NonNullList;
import net.minecraft.core.RegistryAccess;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;

public class GreenhouseRecipe implements Recipe<Container>
{
	protected final Ingredient ingredient;
	protected final ItemStack result;
	protected final SoilType soil;
	protected final boolean needsWater;
	
	public GreenhouseRecipe(Ingredient ingredient, ItemStack result, SoilType soil, boolean needsWater)
	{
		this.ingredient = ingredient;
		this.result = result;
		this.soil = soil;
		this.needsWater = needsWater;
	}
	
	protected GreenhouseRecipe(Ingredient ingredient, ItemStack result, String soil, boolean needsWater)
	{
		this(ingredient, result, SoilType.valueOf(soil.toUpperCase()), needsWater);
	}
	
	@Override
	public ItemStack getToastSymbol()
	{
		return new ItemStack(ModBlocks.ELECTRIC_GREENHOUSE.get());
	}
	
	@Override
	public boolean matches(Container container, Level world)
	{
		return ingredient.test(container.getItem(0));
	}
	
	@Override
	public ItemStack assemble(Container container, RegistryAccess registryAccess)
	{
		return getResultItem(registryAccess).copy();
	}
	
	@Override
	public boolean canCraftInDimensions(int width, int height)
	{
		return false;
	}
	
	@Override
	public NonNullList<Ingredient> getIngredients()
	{
		NonNullList<Ingredient> ingredients = NonNullList.create();
		ingredients.add(ingredient);
		return ingredients;
	}
	
	@Override
	public ItemStack getResultItem(RegistryAccess registryAccess)
	{
		return result;
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return ModRecipeSerializers.GREENHOUSE.get();
	}
	
	@Override
	public RecipeType<?> getType()
	{
		return ModRecipeTypes.GREENHOUSE.get();
	}
	
	public SoilType getSoilType()
	{
		return soil;
	}
	
	public boolean needsWater()
	{
		return needsWater;
	}
}
