package de.boy132.minecraftcontentexpansion.recipe.greenhouse;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.SoilType;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;

public class GreenhouseRecipeSerializer implements RecipeSerializer<GreenhouseRecipe>
{
	private final Codec<GreenhouseRecipe> codec;
	
	public GreenhouseRecipeSerializer()
	{
		codec = RecordCodecBuilder.create((instance) ->
		{
			return instance.group(Ingredient.CODEC_NONEMPTY.fieldOf("ingredient").forGetter((recipe) ->
			{
				return recipe.ingredient;
			}), ItemStack.ITEM_WITH_COUNT_CODEC.fieldOf("result").forGetter((recipe) ->
			{
				return recipe.result;
			}), Codec.STRING.fieldOf("soil").forGetter((recipe) ->
			{
				return recipe.soil.getSerializedName();
			}), Codec.BOOL.fieldOf("needswater").orElse(false).forGetter((recipe) ->
			{
				return recipe.needsWater;
			})).apply(instance, GreenhouseRecipe::new);
		});
	}
	
	@Override
	public Codec<GreenhouseRecipe> codec()
	{
		return codec;
	}
	
	@Override
	public GreenhouseRecipe fromNetwork(FriendlyByteBuf buffer)
	{
		Ingredient ingredient = Ingredient.fromNetwork(buffer);
		ItemStack result = buffer.readItem();
		SoilType soil = buffer.readEnum(SoilType.class);
		boolean needsWater = buffer.readBoolean();
		return new GreenhouseRecipe(ingredient, result, soil, needsWater);
	}
	
	@Override
	public void toNetwork(FriendlyByteBuf buffer, GreenhouseRecipe recipe)
	{
		recipe.ingredient.toNetwork(buffer);
		buffer.writeItem(recipe.result);
		buffer.writeEnum(recipe.soil);
		buffer.writeBoolean(recipe.needsWater);
	}
}
