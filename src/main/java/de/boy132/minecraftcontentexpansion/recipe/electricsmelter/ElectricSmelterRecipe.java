package de.boy132.minecraftcontentexpansion.recipe.electricsmelter;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricsmelter.ElectricSmelterBlockEntity;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeSerializers;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import net.minecraft.core.NonNullList;
import net.minecraft.core.RegistryAccess;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;

public class ElectricSmelterRecipe implements Recipe<Container>
{
	protected final NonNullList<Ingredient> ingredients;
	protected final ItemStack result;
	protected final int smeltTime;
	
	public ElectricSmelterRecipe(NonNullList<Ingredient> ingredients, ItemStack result, int smeltTime)
	{
		if(ingredients.size() != 4)
			throw new IllegalArgumentException("ingredients list size is not equal to 4!");
		
		this.ingredients = ingredients;
		this.result = result;
		this.smeltTime = smeltTime;
	}
	
	@Override
	public ItemStack getToastSymbol()
	{
		return new ItemStack(ModBlocks.ELECTRIC_SMELTER.get());
	}
	
	@Override
	public boolean matches(Container container, Level level)
	{
		for(int i = 0; i < ElectricSmelterBlockEntity.INPUT_SLOT_COUNT; i++)
		{
			if(!ingredients.get(i).test(container.getItem(i)))
				return false;
		}
		
		return true;
	}
	
	@Override
	public ItemStack assemble(Container container, RegistryAccess registryAccess)
	{
		return getResultItem(registryAccess).copy();
	}
	
	@Override
	public boolean canCraftInDimensions(int width, int height)
	{
		return false;
	}
	
	@Override
	public NonNullList<Ingredient> getIngredients()
	{
		return ingredients;
	}
	
	@Override
	public ItemStack getResultItem(RegistryAccess registryAccess)
	{
		return result;
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return ModRecipeSerializers.ELECTRIC_SMELTER.get();
	}
	
	@Override
	public RecipeType<?> getType()
	{
		return ModRecipeTypes.ELECTRIC_SMELTER.get();
	}
	
	public int getSmeltTime()
	{
		return smeltTime;
	}
}
