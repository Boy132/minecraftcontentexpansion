package de.boy132.minecraftcontentexpansion.recipe.electricsmelter;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricsmelter.ElectricSmelterBlockEntity;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;

public class ElectricSmelterRecipeSerializer implements RecipeSerializer<ElectricSmelterRecipe>
{
	private final Codec<ElectricSmelterRecipe> codec;
	
	public ElectricSmelterRecipeSerializer()
	{
		codec = RecordCodecBuilder.create((instance) ->
		{
			return instance.group(Ingredient.CODEC_NONEMPTY.listOf().fieldOf("ingredients").flatXmap((ingredients) ->
			{
				Ingredient[] ingredientArray = ingredients.stream().filter((ingredient) ->
				{
					return !ingredient.isEmpty();
				}).toArray(Ingredient[]::new);
				if(ingredientArray.length == 0)
				{
					return DataResult.error(() -> "No ingredients for electric smelter recipe");
				} else
				{
					return ingredientArray.length > 4 ? DataResult.error(() -> "Too many ingredients for electric smelter recipe") : DataResult.success(NonNullList.of(Ingredient.EMPTY, ingredientArray));
				}
			}, DataResult::success).forGetter((recipe) ->
			{
				return recipe.ingredients;
			}), ItemStack.ITEM_WITH_COUNT_CODEC.fieldOf("result").forGetter((recipe) ->
			{
				return recipe.result;
			}), Codec.INT.fieldOf("smelttime").orElse(ElectricSmelterBlockEntity.STANDARD_SMELT_TIME).forGetter((recipe) ->
			{
				return recipe.smeltTime;
			})).apply(instance, ElectricSmelterRecipe::new);
		});
	}
	
	@Override
	public Codec<ElectricSmelterRecipe> codec()
	{
		return codec;
	}
	
	@Override
	public ElectricSmelterRecipe fromNetwork(FriendlyByteBuf buffer)
	{
		NonNullList<Ingredient> ingredients = NonNullList.withSize(4, Ingredient.EMPTY);
		for(int i = 0; i < ingredients.size(); i++)
			ingredients.set(i, Ingredient.fromNetwork(buffer));
		
		ItemStack result = buffer.readItem();
		int smeltTime = buffer.readVarInt();
		return new ElectricSmelterRecipe(ingredients, result, smeltTime);
	}
	
	@Override
	public void toNetwork(FriendlyByteBuf buffer, ElectricSmelterRecipe recipe)
	{
		recipe.ingredients.forEach(ingredient -> ingredient.toNetwork(buffer));
		buffer.writeItem(recipe.result);
		buffer.writeVarInt(recipe.smeltTime);
	}
}
