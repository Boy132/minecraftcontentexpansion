package de.boy132.minecraftcontentexpansion.recipe.kiln;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.block.entity.kiln.KilnBlockEntity;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;

public class KilnRecipeSerializer implements RecipeSerializer<KilnRecipe>
{
	private final Codec<KilnRecipe> codec;
	
	public KilnRecipeSerializer()
	{
		codec = RecordCodecBuilder.create((instance) ->
		{
			return instance.group(Ingredient.CODEC_NONEMPTY.fieldOf("ingredient").forGetter((recipe) ->
			{
				return recipe.ingredient;
			}), ItemStack.ITEM_WITH_COUNT_CODEC.fieldOf("result").forGetter((recipe) ->
			{
				return recipe.result;
			}), Codec.INT.fieldOf("smelttime").orElse(KilnBlockEntity.STANDARD_SMELT_TIME).forGetter((recipe) ->
			{
				return recipe.smeltTime;
			})).apply(instance, KilnRecipe::new);
		});
	}
	
	@Override
	public Codec<KilnRecipe> codec()
	{
		return codec;
	}
	
	@Override
	public KilnRecipe fromNetwork(FriendlyByteBuf buffer)
	{
		Ingredient ingredient = Ingredient.fromNetwork(buffer);
		ItemStack result = buffer.readItem();
		int smeltTime = buffer.readVarInt();
		return new KilnRecipe(ingredient, result, smeltTime);
	}
	
	@Override
	public void toNetwork(FriendlyByteBuf buffer, KilnRecipe recipe)
	{
		recipe.ingredient.toNetwork(buffer);
		buffer.writeItem(recipe.result);
		buffer.writeVarInt(recipe.smeltTime);
	}
}
