package de.boy132.minecraftcontentexpansion.recipe.hydraulicpress;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.hydraulicpress.HydraulicPressBlockEntity;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.RecipeUnlockedTrigger;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;

import java.util.LinkedHashMap;
import java.util.Map;

public class HydraulicPressRecipeBuilder implements RecipeBuilder
{
	private final Map<String, Criterion<?>> criteria = new LinkedHashMap<>();
	
	private final Ingredient primaryIngredient;
	private final Ingredient secondaryIngredient;
	private final Item result;
	private final int count;
	private final int pressTime;
	
	private String group;
	
	private HydraulicPressRecipeBuilder(Ingredient primaryIngredient, Ingredient secondaryIngredient, Item result, int count, int pressTime)
	{
		this.primaryIngredient = primaryIngredient;
		this.secondaryIngredient = secondaryIngredient;
		this.result = result;
		this.count = count;
		this.pressTime = pressTime;
	}
	
	public static HydraulicPressRecipeBuilder press(Ingredient primaryIngredient, Ingredient secondaryIngredient, ItemStack result, int pressTime)
	{
		return new HydraulicPressRecipeBuilder(primaryIngredient, secondaryIngredient, result.getItem(), result.getCount(), pressTime);
	}
	
	public static HydraulicPressRecipeBuilder press(Ingredient primaryIngredient, ItemStack result, int pressTime)
	{
		return new HydraulicPressRecipeBuilder(primaryIngredient, Ingredient.EMPTY, result.getItem(), result.getCount(), pressTime);
	}
	
	public static HydraulicPressRecipeBuilder press(Ingredient primaryIngredient, Ingredient secondaryIngredient, ItemStack result)
	{
		return press(primaryIngredient, secondaryIngredient, result, HydraulicPressBlockEntity.STANDARD_PRESS_TIME);
	}
	
	public static HydraulicPressRecipeBuilder press(Ingredient primaryIngredient, ItemStack result)
	{
		return press(primaryIngredient, Ingredient.EMPTY, result, HydraulicPressBlockEntity.STANDARD_PRESS_TIME);
	}
	
	@Override
	public RecipeBuilder unlockedBy(String name, Criterion<?> criterion)
	{
		criteria.put(name, criterion);
		return this;
	}
	
	@Override
	public RecipeBuilder group(String group)
	{
		this.group = group;
		return this;
	}
	
	@Override
	public Item getResult()
	{
		return result;
	}
	
	@Override
	public void save(RecipeOutput recipeOutput, ResourceLocation id)
	{
		ResourceLocation name = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "hydraulic_press/" + id.getPath());
		ensureValid(name);
		
		Advancement.Builder advancementBuilder = recipeOutput.advancement()
				.addCriterion("has_the_recipe", RecipeUnlockedTrigger.unlocked(name))
				.rewards(AdvancementRewards.Builder.recipe(name))
				.requirements(AdvancementRequirements.Strategy.OR);
		criteria.forEach(advancementBuilder::addCriterion);
		
		recipeOutput.accept(name, new HydraulicPressRecipe(primaryIngredient, secondaryIngredient, new ItemStack(result, count), pressTime), advancementBuilder.build(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "recipes/hydraulic_press/" + id.getPath())));
	}
	
	private void ensureValid(ResourceLocation id)
	{
		if(criteria.isEmpty())
			throw new IllegalStateException("No way of obtaining recipe " + id);
	}
}
