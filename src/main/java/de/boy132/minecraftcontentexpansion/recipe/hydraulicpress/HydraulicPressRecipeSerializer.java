package de.boy132.minecraftcontentexpansion.recipe.hydraulicpress;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.hydraulicpress.HydraulicPressBlockEntity;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;

public class HydraulicPressRecipeSerializer implements RecipeSerializer<HydraulicPressRecipe>
{
	private final Codec<HydraulicPressRecipe> codec;
	
	public HydraulicPressRecipeSerializer()
	{
		codec = RecordCodecBuilder.create((instance) ->
		{
			return instance.group(Ingredient.CODEC_NONEMPTY.fieldOf("primaryingredient").forGetter((recipe) ->
			{
				return recipe.primaryIngredient;
			}), Ingredient.CODEC.fieldOf("secondaryingredient").forGetter((recipe) ->
			{
				return recipe.secondaryIngredient;
			}), ItemStack.ITEM_WITH_COUNT_CODEC.fieldOf("result").forGetter((recipe) ->
			{
				return recipe.result;
			}), Codec.INT.fieldOf("presstime").orElse(HydraulicPressBlockEntity.STANDARD_PRESS_TIME).forGetter((recipe) ->
			{
				return recipe.pressTime;
			})).apply(instance, HydraulicPressRecipe::new);
		});
	}
	
	@Override
	public Codec<HydraulicPressRecipe> codec()
	{
		return codec;
	}
	
	@Override
	public HydraulicPressRecipe fromNetwork(FriendlyByteBuf buffer)
	{
		Ingredient primaryIngredient = Ingredient.fromNetwork(buffer);
		Ingredient secondaryIngredient = Ingredient.fromNetwork(buffer);
		ItemStack result = buffer.readItem();
		int pressTime = buffer.readVarInt();
		return new HydraulicPressRecipe(primaryIngredient, secondaryIngredient, result, pressTime);
	}
	
	@Override
	public void toNetwork(FriendlyByteBuf buffer, HydraulicPressRecipe recipe)
	{
		recipe.primaryIngredient.toNetwork(buffer);
		recipe.secondaryIngredient.toNetwork(buffer);
		buffer.writeItem(recipe.result);
		buffer.writeVarInt(recipe.pressTime);
	}
}
