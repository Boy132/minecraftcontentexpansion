package de.boy132.minecraftcontentexpansion.recipe.hydraulicpress;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeSerializers;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import net.minecraft.core.NonNullList;
import net.minecraft.core.RegistryAccess;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;

public class HydraulicPressRecipe implements Recipe<Container>
{
	protected final Ingredient primaryIngredient;
	protected final Ingredient secondaryIngredient;
	protected final ItemStack result;
	protected final int pressTime;
	
	public HydraulicPressRecipe(Ingredient primaryIngredient, Ingredient secondaryIngredient, ItemStack result, int pressTime)
	{
		this.primaryIngredient = primaryIngredient;
		this.secondaryIngredient = secondaryIngredient;
		this.result = result;
		this.pressTime = pressTime;
	}
	
	@Override
	public ItemStack getToastSymbol()
	{
		return new ItemStack(ModBlocks.HYDRAULIC_PRESS.get());
	}
	
	@Override
	public boolean matches(Container container, Level world)
	{
		return primaryIngredient.test(container.getItem(0)) && secondaryIngredient.test(container.getItem(1));
	}
	
	@Override
	public ItemStack assemble(Container container, RegistryAccess registryAccess)
	{
		return getResultItem(registryAccess).copy();
	}
	
	@Override
	public boolean canCraftInDimensions(int width, int height)
	{
		return false;
	}
	
	@Override
	public NonNullList<Ingredient> getIngredients()
	{
		NonNullList<Ingredient> ingredients = NonNullList.create();
		ingredients.add(primaryIngredient);
		ingredients.add(secondaryIngredient);
		return ingredients;
	}
	
	@Override
	public ItemStack getResultItem(RegistryAccess registryAccess)
	{
		return result;
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return ModRecipeSerializers.HYDRAULIC_PRESS.get();
	}
	
	@Override
	public RecipeType<?> getType()
	{
		return ModRecipeTypes.HYDRAULIC_PRESS.get();
	}
	
	public int getPressTime()
	{
		return pressTime;
	}
}
