package de.boy132.minecraftcontentexpansion.recipe;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.recipe.choppingblock.ChoppingBlockRecipe;
import de.boy132.minecraftcontentexpansion.recipe.choppingblock.ChoppingBlockRecipeSerializer;
import de.boy132.minecraftcontentexpansion.recipe.dryingrack.DryingRackRecipe;
import de.boy132.minecraftcontentexpansion.recipe.dryingrack.DryingRackRecipeSerializer;
import de.boy132.minecraftcontentexpansion.recipe.electricsmelter.ElectricSmelterRecipe;
import de.boy132.minecraftcontentexpansion.recipe.electricsmelter.ElectricSmelterRecipeSerializer;
import de.boy132.minecraftcontentexpansion.recipe.greenhouse.GreenhouseRecipe;
import de.boy132.minecraftcontentexpansion.recipe.greenhouse.GreenhouseRecipeSerializer;
import de.boy132.minecraftcontentexpansion.recipe.hydraulicpress.HydraulicPressRecipe;
import de.boy132.minecraftcontentexpansion.recipe.hydraulicpress.HydraulicPressRecipeSerializer;
import de.boy132.minecraftcontentexpansion.recipe.kiln.KilnRecipe;
import de.boy132.minecraftcontentexpansion.recipe.kiln.KilnRecipeSerializer;
import de.boy132.minecraftcontentexpansion.recipe.millstone.MillstoneRecipe;
import de.boy132.minecraftcontentexpansion.recipe.millstone.MillstoneRecipeSerializer;
import de.boy132.minecraftcontentexpansion.recipe.smelter.SmelterRecipe;
import de.boy132.minecraftcontentexpansion.recipe.smelter.SmelterRecipeSerializer;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModRecipeSerializers
{
	private static final DeferredRegister<RecipeSerializer<?>> RECIPE_SERIALIZERS = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<RecipeSerializer<KilnRecipe>> KILN = RECIPE_SERIALIZERS.register("kiln", KilnRecipeSerializer::new);
	public static final RegistryObject<RecipeSerializer<SmelterRecipe>> SMELTER = RECIPE_SERIALIZERS.register("smelter", SmelterRecipeSerializer::new);
	public static final RegistryObject<RecipeSerializer<MillstoneRecipe>> MILLSTONE = RECIPE_SERIALIZERS.register("millstone", MillstoneRecipeSerializer::new);
	
	public static final RegistryObject<RecipeSerializer<ChoppingBlockRecipe>> CHOPPING_BLOCK = RECIPE_SERIALIZERS.register("chopping_block", ChoppingBlockRecipeSerializer::new);
	
	public static final RegistryObject<RecipeSerializer<DryingRackRecipe>> DRYING_RACK = RECIPE_SERIALIZERS.register("drying_rack", DryingRackRecipeSerializer::new);
	
	public static final RegistryObject<RecipeSerializer<ElectricSmelterRecipe>> ELECTRIC_SMELTER = RECIPE_SERIALIZERS.register("electric_smelter", ElectricSmelterRecipeSerializer::new);
	
	public static final RegistryObject<RecipeSerializer<GreenhouseRecipe>> GREENHOUSE = RECIPE_SERIALIZERS.register("greenhouse", GreenhouseRecipeSerializer::new);
	
	public static final RegistryObject<RecipeSerializer<HydraulicPressRecipe>> HYDRAULIC_PRESS = RECIPE_SERIALIZERS.register("hydraulic_press", HydraulicPressRecipeSerializer::new);
	
	public static void register(IEventBus eventBus)
	{
		RECIPE_SERIALIZERS.register(eventBus);
	}
}
