package de.boy132.minecraftcontentexpansion.datagen.blockstate;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.*;
import net.minecraftforge.client.model.generators.ModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ModBlockStateProvider extends BaseBlockStateProvider
{
	public ModBlockStateProvider(PackOutput packOutput, ExistingFileHelper existingFileHelper)
	{
		super(packOutput, existingFileHelper);
	}
	
	@Override
	protected void registerStatesAndModels()
	{
		simpleBlock(ModBlocks.UNIM_ORE.get());
		simpleBlock(ModBlocks.DEEPSLATE_UNIM_ORE.get());
		simpleBlock(ModBlocks.UNIM_BLOCK.get());
		
		simpleBlock(ModBlocks.RUBY_ORE.get());
		simpleBlock(ModBlocks.DEEPSLATE_RUBY_ORE.get());
		simpleBlock(ModBlocks.RUBY_BLOCK.get());
		
		simpleBlock(ModBlocks.ASCABIT_ORE.get());
		simpleBlock(ModBlocks.DEEPSLATE_ASCABIT_ORE.get());
		simpleBlock(ModBlocks.ASCABIT_BLOCK.get());
		simpleBlock(ModBlocks.RAW_ASCABIT_BLOCK.get());
		
		simpleBlock(ModBlocks.TIN_ORE.get());
		simpleBlock(ModBlocks.DEEPSLATE_TIN_ORE.get());
		simpleBlock(ModBlocks.TIN_BLOCK.get());
		simpleBlock(ModBlocks.RAW_TIN_BLOCK.get());
		
		simpleBlock(ModBlocks.TITANIUM_ORE.get());
		simpleBlock(ModBlocks.DEEPSLATE_TITANIUM_ORE.get());
		simpleBlock(ModBlocks.TITANIUM_BLOCK.get());
		simpleBlock(ModBlocks.RAW_TITANIUM_BLOCK.get());
		
		simpleBlock(ModBlocks.ZINC_ORE.get());
		simpleBlock(ModBlocks.DEEPSLATE_ZINC_ORE.get());
		simpleBlock(ModBlocks.ZINC_BLOCK.get());
		simpleBlock(ModBlocks.RAW_ZINC_BLOCK.get());
		
		simpleBlock(ModBlocks.STEEL_BLOCK.get());
		
		simpleBlock(ModBlocks.BRONZE_BLOCK.get());
		
		simpleBlock(ModBlocks.BRASS_BLOCK.get());
		
		simpleBlock(ModBlocks.CEMENT.get());
		stairsBlock((StairBlock) ModBlocks.CEMENT_STAIRS.get(), blockTexture(ModBlocks.CEMENT.get()));
		slabBlock((SlabBlock) ModBlocks.CEMENT_SLAB.get(), blockTexture(ModBlocks.CEMENT.get()), blockTexture(ModBlocks.CEMENT.get()));
		wallBlock((WallBlock) ModBlocks.CEMENT_WALL.get(), blockTexture(ModBlocks.CEMENT.get()));
		
		simpleBlock(ModBlocks.CEMENT_PLATE.get());
		stairsBlock((StairBlock) ModBlocks.CEMENT_PLATE_STAIRS.get(), blockTexture(ModBlocks.CEMENT_PLATE.get()));
		slabBlock((SlabBlock) ModBlocks.CEMENT_PLATE_SLAB.get(), blockTexture(ModBlocks.CEMENT_PLATE.get()), blockTexture(ModBlocks.CEMENT_PLATE.get()));
		wallBlock((WallBlock) ModBlocks.CEMENT_PLATE_WALL.get(), blockTexture(ModBlocks.CEMENT_PLATE.get()));
		
		simpleBlock(ModBlocks.LIMESTONE.get());
		stairsBlock((StairBlock) ModBlocks.LIMESTONE_STAIRS.get(), blockTexture(ModBlocks.LIMESTONE.get()));
		slabBlock((SlabBlock) ModBlocks.LIMESTONE_SLAB.get(), blockTexture(ModBlocks.LIMESTONE.get()), blockTexture(ModBlocks.LIMESTONE.get()));
		wallBlock((WallBlock) ModBlocks.LIMESTONE_WALL.get(), blockTexture(ModBlocks.LIMESTONE.get()));
		
		simpleBlock(ModBlocks.LIMESTONE_BRICKS.get());
		stairsBlock((StairBlock) ModBlocks.LIMESTONE_BRICK_STAIRS.get(), blockTexture(ModBlocks.LIMESTONE_BRICKS.get()));
		slabBlock((SlabBlock) ModBlocks.LIMESTONE_BRICK_SLAB.get(), blockTexture(ModBlocks.LIMESTONE_BRICKS.get()), blockTexture(ModBlocks.LIMESTONE_BRICKS.get()));
		wallBlock((WallBlock) ModBlocks.LIMESTONE_BRICK_WALL.get(), blockTexture(ModBlocks.LIMESTONE_BRICKS.get()));
		
		simpleBlock(ModBlocks.MARBLE.get());
		stairsBlock((StairBlock) ModBlocks.MARBLE_STAIRS.get(), blockTexture(ModBlocks.MARBLE.get()));
		slabBlock((SlabBlock) ModBlocks.MARBLE_SLAB.get(), blockTexture(ModBlocks.MARBLE.get()), blockTexture(ModBlocks.MARBLE.get()));
		wallBlock((WallBlock) ModBlocks.MARBLE_WALL.get(), blockTexture(ModBlocks.MARBLE.get()));
		
		simpleBlock(ModBlocks.MARBLE_COBBLE.get());
		stairsBlock((StairBlock) ModBlocks.MARBLE_COBBLE_STAIRS.get(), blockTexture(ModBlocks.MARBLE_COBBLE.get()));
		slabBlock((SlabBlock) ModBlocks.MARBLE_COBBLE_SLAB.get(), blockTexture(ModBlocks.MARBLE_COBBLE.get()), blockTexture(ModBlocks.MARBLE_COBBLE.get()));
		wallBlock((WallBlock) ModBlocks.MARBLE_COBBLE_WALL.get(), blockTexture(ModBlocks.MARBLE_COBBLE.get()));
		
		simpleBlock(ModBlocks.MARBLE_BRICKS.get());
		stairsBlock((StairBlock) ModBlocks.MARBLE_BRICK_STAIRS.get(), blockTexture(ModBlocks.MARBLE_BRICKS.get()));
		slabBlock((SlabBlock) ModBlocks.MARBLE_BRICK_SLAB.get(), blockTexture(ModBlocks.MARBLE_BRICKS.get()), blockTexture(ModBlocks.MARBLE_BRICKS.get()));
		wallBlock((WallBlock) ModBlocks.MARBLE_BRICK_WALL.get(), blockTexture(ModBlocks.MARBLE_BRICKS.get()));
		
		simpleBlock(ModBlocks.MARBLE_LAMP.get());
		
		slabBlock((SlabBlock) ModBlocks.DIRT_SLAB.get(), blockTexture(Blocks.DIRT), blockTexture(Blocks.DIRT));
		slabBlock((SlabBlock) ModBlocks.COARSE_DIRT_SLAB.get(), blockTexture(Blocks.COARSE_DIRT), blockTexture(Blocks.COARSE_DIRT));
		//slabBlock((SlabBlock) ModBlocks.DIRT_PATH_SLAB.get(), blockTexture(Blocks.DIRT_PATH), ResourceLocation.fromNamespaceAndPath(ModelProvider.BLOCK_FOLDER + "/dirt_path_side"), ResourceLocation.fromNamespaceAndPath(ModelProvider.BLOCK_FOLDER + "/dirt"), ResourceLocation.fromNamespaceAndPath(ModelProvider.BLOCK_FOLDER + "/dirt_path_top"));
		//slabBlock((SlabBlock) ModBlocks.GRASS_BLOCK_SLAB.get(), blockTexture(Blocks.GRASS_BLOCK), ResourceLocation.fromNamespaceAndPath(ModelProvider.BLOCK_FOLDER + "/grass_block_side"), ResourceLocation.fromNamespaceAndPath(ModelProvider.BLOCK_FOLDER + "/dirt"), ResourceLocation.fromNamespaceAndPath(ModelProvider.BLOCK_FOLDER + "/grass_block_top"));
		
		slabBlock((SlabBlock) ModBlocks.GLASS_SLAB.get(), blockTexture(Blocks.GLASS), blockTexture(Blocks.GLASS));
		
		doorBlock((DoorBlock) ModBlocks.STONE_DOOR.get(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, ModelProvider.BLOCK_FOLDER + "/stone_door_bottom"), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, ModelProvider.BLOCK_FOLDER + "/stone_door_top"));
		trapdoorBlock((TrapDoorBlock) ModBlocks.STONE_TRAPDOOR.get(), blockTexture(Blocks.CHISELED_STONE_BRICKS), true);
		
		doorBlock((DoorBlock) ModBlocks.SANDSTONE_DOOR.get(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, ModelProvider.BLOCK_FOLDER + "/sandstone_door_bottom"), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, ModelProvider.BLOCK_FOLDER + "/sandstone_door_top"));
		trapdoorBlock((TrapDoorBlock) ModBlocks.SANDSTONE_TRAPDOOR.get(), blockTexture(Blocks.CHISELED_SANDSTONE), true);
		
		doorBlock((DoorBlock) ModBlocks.GLASS_DOOR.get(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, ModelProvider.BLOCK_FOLDER + "/glass_door_bottom"), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, ModelProvider.BLOCK_FOLDER + "/glass_door_top"));
		trapdoorBlock((TrapDoorBlock) ModBlocks.GLASS_TRAPDOOR.get(), blockTexture(ModBlocks.GLASS_TRAPDOOR.get()), true);
		
		paneBlock((IronBarsBlock) ModBlocks.WOODEN_GLASS_FRAME.get(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, ModelProvider.BLOCK_FOLDER + "/wooden_glass_frame"), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, ModelProvider.BLOCK_FOLDER + "/wooden_glass_frame_top"));
		
		simpleBlock(ModBlocks.MACHINE_FRAME.get());
		
		crossCropBlock(ModBlocks.TOMATOES.get(), "tomatoes");
		crossCropBlock(ModBlocks.GRAPES.get(), "grapes");
		crossCropBlock(ModBlocks.STRAWBERRIES.get(), "strawberries");
		doubleCropBlock(ModBlocks.CORNS.get(), "corns");
		cropBlock(ModBlocks.LETTUCES.get(), "lettuces");
		cropBlock(ModBlocks.ONIONS.get(), "onions");
		cropBlock(ModBlocks.RICE.get(), "rice");
	}
}
