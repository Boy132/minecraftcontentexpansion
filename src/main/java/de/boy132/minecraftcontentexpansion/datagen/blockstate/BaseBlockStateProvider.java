package de.boy132.minecraftcontentexpansion.datagen.blockstate;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.crop.DoubleCropBlock;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ConfiguredModel;
import net.minecraftforge.common.data.ExistingFileHelper;

import java.util.function.Function;

public abstract class BaseBlockStateProvider extends BlockStateProvider
{
	public BaseBlockStateProvider(PackOutput packOutput, ExistingFileHelper existingFileHelper)
	{
		super(packOutput, MinecraftContentExpansion.MODID, existingFileHelper);
	}
	
	@Override
	public String getName()
	{
		return "Mod Block States";
	}
	
	protected void cropBlock(CropBlock block, String modelName, String textureName)
	{
		Function<BlockState, ConfiguredModel[]> function = state -> cropModels(state, block, modelName, textureName);
		getVariantBuilder(block).forAllStates(function);
	}
	
	protected void cropBlock(CropBlock block, String name)
	{
		cropBlock(block, name, name);
	}
	
	protected void crossCropBlock(CropBlock block, String modelName, String textureName)
	{
		Function<BlockState, ConfiguredModel[]> function = state -> crossCropModels(state, block, modelName, textureName);
		getVariantBuilder(block).forAllStates(function);
	}
	
	protected void crossCropBlock(CropBlock block, String name)
	{
		crossCropBlock(block, name, name);
	}
	
	protected void doubleCropBlock(CropBlock block, String modelName, String textureName)
	{
		Function<BlockState, ConfiguredModel[]> function = state -> doubleCropModels(state, block, modelName, textureName);
		getVariantBuilder(block).forAllStates(function);
	}
	
	protected void doubleCropBlock(CropBlock block, String name)
	{
		doubleCropBlock(block, name, name);
	}
	
	private ConfiguredModel[] cropModels(BlockState state, CropBlock block, String modelName, String textureName)
	{
		int age = block.getAge(state);
		
		ConfiguredModel[] models = new ConfiguredModel[1];
		models[0] = new ConfiguredModel(models().crop(modelName + "_stage" + age, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/" + textureName + "_stage" + age)));
		return models;
	}
	
	private ConfiguredModel[] crossCropModels(BlockState state, CropBlock block, String modelName, String textureName)
	{
		int age = block.getAge(state);
		
		ConfiguredModel[] models = new ConfiguredModel[1];
		models[0] = new ConfiguredModel(models().cross(modelName + "_stage" + age, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/" + textureName + "_stage" + age)));
		return models;
	}
	
	private ConfiguredModel[] doubleCropModels(BlockState state, CropBlock block, String modelName, String textureName)
	{
		int age = block.getAge(state);
		DoubleBlockHalf halfValue = state.getValue(DoubleCropBlock.HALF);
		
		ConfiguredModel[] models = new ConfiguredModel[1];
		models[0] = new ConfiguredModel(models().cross(modelName + "_" + halfValue + "_stage" + age, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/" + textureName + "_" + halfValue + "_stage" + age)));
		return models;
	}
}
