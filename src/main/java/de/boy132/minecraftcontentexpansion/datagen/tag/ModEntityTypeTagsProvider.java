package de.boy132.minecraftcontentexpansion.datagen.tag;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.entity.ModEntityTypes;
import de.boy132.minecraftcontentexpansion.tag.ModEntityTypeTags;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.EntityTypeTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

import java.util.concurrent.CompletableFuture;

public class ModEntityTypeTagsProvider extends EntityTypeTagsProvider
{
	public ModEntityTypeTagsProvider(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookupProvider, ExistingFileHelper existingFileHelper)
	{
		super(packOutput, lookupProvider, MinecraftContentExpansion.MODID, existingFileHelper);
	}
	
	@Override
	protected void addTags(HolderLookup.Provider lookupProvider)
	{
		tag(ModEntityTypeTags.ROCKS).add(ModEntityTypes.ROCK.get(), ModEntityTypes.ANDESITE_ROCK.get(), ModEntityTypes.DIORITE_ROCK.get(), ModEntityTypes.GRANITE_ROCK.get());
	}
	
	@Override
	public String getName()
	{
		return "Mod Entity Type Tags";
	}
}
