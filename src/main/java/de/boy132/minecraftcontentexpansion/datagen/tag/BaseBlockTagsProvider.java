package de.boy132.minecraftcontentexpansion.datagen.tag;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

import java.util.concurrent.CompletableFuture;

public abstract class BaseBlockTagsProvider extends BlockTagsProvider
{
	public BaseBlockTagsProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider, ExistingFileHelper existingFileHelper)
	{
		super(output, lookupProvider, MinecraftContentExpansion.MODID, existingFileHelper);
	}
	
	protected abstract void addBlockTags();
	
	protected abstract void addNeedsToolTags();
	
	protected abstract void addMinableTags();
	
	protected abstract void addToVanillaTags();
	
	@Override
	protected void addTags(HolderLookup.Provider lookupProvider)
	{
		addBlockTags();
		
		addNeedsToolTags();
		addMinableTags();
		
		addToVanillaTags();
	}
	
	@Override
	public String getName()
	{
		return "Mod Block Tags";
	}
}
