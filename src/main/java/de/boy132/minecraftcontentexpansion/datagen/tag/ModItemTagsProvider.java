package de.boy132.minecraftcontentexpansion.datagen.tag;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.tag.ModItemTags;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.concurrent.CompletableFuture;

public class ModItemTagsProvider extends ItemTagsProvider
{
	public ModItemTagsProvider(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookupProvider, ExistingFileHelper existingFileHelper, TagsProvider<Block> blockTagsProvider)
	{
		super(packOutput, lookupProvider, blockTagsProvider.contentsGetter(), MinecraftContentExpansion.MODID, existingFileHelper);
	}
	
	@Override
	protected void addTags(HolderLookup.Provider lookupProvider)
	{
		tag(ModItemTags.ORES_UNIM).add(ModBlocks.UNIM_ORE.get().asItem(), ModBlocks.DEEPSLATE_UNIM_ORE.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_UNIM).add(ModBlocks.UNIM_BLOCK.get().asItem());
		
		tag(ModItemTags.ORES_RUBY).add(ModBlocks.RUBY_ORE.get().asItem(), ModBlocks.DEEPSLATE_RUBY_ORE.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_RUBY).add(ModBlocks.RUBY_BLOCK.get().asItem());
		
		tag(ModItemTags.ORES_ASCABIT).add(ModBlocks.ASCABIT_ORE.get().asItem(), ModBlocks.DEEPSLATE_ASCABIT_ORE.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_ASCABIT).add(ModBlocks.ASCABIT_BLOCK.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_RAW_ASCABIT).add(ModBlocks.RAW_ASCABIT_BLOCK.get().asItem());
		
		tag(ModItemTags.ORES_TIN).add(ModBlocks.TIN_ORE.get().asItem(), ModBlocks.DEEPSLATE_TIN_ORE.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_TIN).add(ModBlocks.TIN_BLOCK.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_RAW_TIN).add(ModBlocks.RAW_TIN_BLOCK.get().asItem());
		
		tag(ModItemTags.ORES_TITANIUM).add(ModBlocks.TITANIUM_ORE.get().asItem(), ModBlocks.DEEPSLATE_TITANIUM_ORE.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_TITANIUM).add(ModBlocks.TITANIUM_BLOCK.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_RAW_TITANIUM).add(ModBlocks.RAW_TITANIUM_BLOCK.get().asItem());
		
		tag(ModItemTags.ORES_ZINC).add(ModBlocks.ZINC_ORE.get().asItem(), ModBlocks.DEEPSLATE_ZINC_ORE.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_ZINC).add(ModBlocks.ZINC_BLOCK.get().asItem());
		tag(ModItemTags.STORAGE_BLOCKS_RAW_ZINC).add(ModBlocks.RAW_ZINC_BLOCK.get().asItem());
		
		tag(ModItemTags.STORAGE_BLOCKS_STEEL).add(ModBlocks.STEEL_BLOCK.get().asItem());
		
		tag(ModItemTags.STORAGE_BLOCKS_BRONZE).add(ModBlocks.BRONZE_BLOCK.get().asItem());
		
		tag(ModItemTags.STORAGE_BLOCKS_BRASS).add(ModBlocks.BRASS_BLOCK.get().asItem());
		
		tag(ModItemTags.INGOTS_UNIM).add(ModItems.UNIM_INGOT.get());
		tag(ModItemTags.DUSTS_UNIM).add(ModItems.UNIM.get());
		
		tag(ModItemTags.GEMS_RUBY).add(ModItems.RUBY.get());
		
		tag(ModItemTags.INGOTS_ASCABIT).add(ModItems.ASCABIT_INGOT.get());
		tag(ModItemTags.RAW_MATERIALS_ASCABIT).add(ModItems.RAW_ASCABIT.get());
		
		tag(ModItemTags.INGOTS_TIN).add(ModItems.TIN_INGOT.get());
		tag(ModItemTags.RAW_MATERIALS_TIN).add(ModItems.RAW_TIN.get());
		
		tag(ModItemTags.INGOTS_TITANIUM).add(ModItems.TITANIUM_INGOT.get());
		tag(ModItemTags.RAW_MATERIALS_TITANIUM).add(ModItems.RAW_TITANIUM.get());
		
		tag(ModItemTags.INGOTS_ZINC).add(ModItems.ZINC_INGOT.get());
		tag(ModItemTags.RAW_MATERIALS_ZINC).add(ModItems.RAW_ZINC.get());
		
		tag(ModItemTags.INGOTS_STEEL).add(ModItems.STEEL_INGOT.get());
		
		tag(ModItemTags.INGOTS_BRONZE).add(ModItems.BRONZE_INGOT.get());
		
		tag(ModItemTags.INGOTS_BRASS).add(ModItems.BRASS_INGOT.get());
		
		tag(ModItemTags.DUSTS_COAL).add(ModItems.PULVERISED_COAL.get());
		
		tag(ModItemTags.TOOLS_HAMMERS).add(ModItems.IRON_HAMMER.getKey(), ModItems.BRONZE_HAMMER.getKey(), ModItems.STEEL_HAMMER.getKey(), ModItems.DIAMOND_HAMMER.getKey());
		tag(ModItemTags.TOOLS_KNIVES).add(ModItems.FLINT_KNIFE.getKey(), ModItems.STONE_KNIFE.getKey(), ModItems.IRON_KNIFE.getKey(), ModItems.UNIM_KNIFE.getKey(), ModItems.DIAMOND_KNIFE.getKey());
		
		tag(ModItemTags.CONTAINERS_WATER).add(ForgeRegistries.ITEMS.getResourceKey(Items.WATER_BUCKET).get(), ModItems.WATER_CLAY_BUCKET.getKey());
		
		tag(ModItemTags.ROCKS).add(ModItems.ROCK.get(), ModItems.ANDESITE_ROCK.get(), ModItems.DIORITE_ROCK.get(), ModItems.GRANITE_ROCK.get());
		
		tag(ModItemTags.CHOPPING_BLOCKS).add(ModBlocks.OAK_CHOPPING_BLOCK.get().asItem(), ModBlocks.SPRUCE_CHOPPING_BLOCK.get().asItem(), ModBlocks.BIRCH_CHOPPING_BLOCK.get().asItem(), ModBlocks.JUNGLE_CHOPPING_BLOCK.get().asItem(), ModBlocks.ACACIA_CHOPPING_BLOCK.get().asItem(), ModBlocks.DARK_OAK_CHOPPING_BLOCK.get().asItem(), ModBlocks.MANGROVE_CHOPPING_BLOCK.get().asItem(), ModBlocks.CHERRY_CHOPPING_BLOCK.get().asItem(), ModBlocks.CRIMSON_CHOPPING_BLOCK.get().asItem(), ModBlocks.WARPED_CHOPPING_BLOCK.get().asItem());
		
		tag(ModItemTags.DRYING_RACKS).add(ModBlocks.OAK_DRYING_RACK.get().asItem(), ModBlocks.SPRUCE_DRYING_RACK.get().asItem(), ModBlocks.BIRCH_DRYING_RACK.get().asItem(), ModBlocks.JUNGLE_DRYING_RACK.get().asItem(), ModBlocks.ACACIA_DRYING_RACK.get().asItem(), ModBlocks.DARK_OAK_DRYING_RACK.get().asItem(), ModBlocks.MANGROVE_DRYING_RACK.get().asItem(), ModBlocks.CHERRY_DRYING_RACK.get().asItem(), ModBlocks.CRIMSON_DRYING_RACK.get().asItem(), ModBlocks.WARPED_DRYING_RACK.get().asItem());
		
		tag(Tags.Items.SEEDS).replace(false).add(ModItems.TOMATO_SEEDS.get(), ModItems.GRAPE_SEEDS.get(), ModItems.STRAWBERRY_SEEDS.get(), ModItems.CORN_SEEDS.get(), ModItems.LETTUCE_SEEDS.get(), ModItems.ONION_SEEDS.get());
		tag(Tags.Items.CROPS).replace(false).add(ModItems.TOMATO.get(), ModItems.GRAPE.get(), ModItems.STRAWBERRY.get(), ModItems.CORN.get(), ModItems.LETTUCE.get(), ModItems.ONION.get(), ModItems.RICE.get());
		
		tag(ItemTags.TRIMMABLE_ARMOR).replace(false).add(ModItems.UNIM_HELMET.getKey(), ModItems.UNIM_CHESTPLATE.getKey(), ModItems.UNIM_LEGGINGS.getKey(), ModItems.UNIM_BOOTS.getKey(), ModItems.RUBY_HELMET.getKey(), ModItems.RUBY_CHESTPLATE.getKey(), ModItems.RUBY_LEGGINGS.getKey(), ModItems.RUBY_BOOTS.getKey(), ModItems.ASCABIT_HELMET.getKey(), ModItems.ASCABIT_CHESTPLATE.getKey(), ModItems.ASCABIT_LEGGINGS.getKey(), ModItems.ASCABIT_BOOTS.getKey(), ModItems.COPPER_HELMET.getKey(), ModItems.COPPER_CHESTPLATE.getKey(), ModItems.COPPER_LEGGINGS.getKey(), ModItems.COPPER_BOOTS.getKey(), ModItems.TIN_HELMET.getKey(), ModItems.TIN_CHESTPLATE.getKey(), ModItems.TIN_LEGGINGS.getKey(), ModItems.TIN_BOOTS.getKey(), ModItems.TITANIUM_HELMET.getKey(), ModItems.TITANIUM_CHESTPLATE.getKey(), ModItems.TITANIUM_LEGGINGS.getKey(), ModItems.TITANIUM_BOOTS.getKey(), ModItems.ZINC_HELMET.getKey(), ModItems.ZINC_CHESTPLATE.getKey(), ModItems.ZINC_LEGGINGS.getKey(), ModItems.ZINC_BOOTS.getKey(), ModItems.STEEL_HELMET.getKey(), ModItems.STEEL_CHESTPLATE.getKey(), ModItems.STEEL_LEGGINGS.getKey(), ModItems.STEEL_BOOTS.getKey(), ModItems.BRONZE_HELMET.getKey(), ModItems.BRONZE_CHESTPLATE.getKey(), ModItems.BRONZE_LEGGINGS.getKey(), ModItems.BRONZE_BOOTS.getKey());
	}
	
	@Override
	public String getName()
	{
		return "Mod Item Tags";
	}
}
