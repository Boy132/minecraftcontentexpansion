package de.boy132.minecraftcontentexpansion.datagen.tag;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.item.tool.ModTiers;
import de.boy132.minecraftcontentexpansion.tag.ModBlockTags;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.data.ExistingFileHelper;

import java.util.concurrent.CompletableFuture;

public class ModBlockTagsProvider extends BaseBlockTagsProvider
{
	public ModBlockTagsProvider(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookupProvider, ExistingFileHelper existingFileHelper)
	{
		super(packOutput, lookupProvider, existingFileHelper);
	}
	
	@Override
	protected void addBlockTags()
	{
		tag(ModBlockTags.ORES_UNIM).add(ModBlocks.UNIM_ORE.get(), ModBlocks.DEEPSLATE_UNIM_ORE.get());
		tag(ModBlockTags.STORAGE_BLOCKS_UNIM).add(ModBlocks.UNIM_BLOCK.get());
		
		tag(ModBlockTags.ORES_RUBY).add(ModBlocks.RUBY_ORE.get(), ModBlocks.DEEPSLATE_RUBY_ORE.get());
		tag(ModBlockTags.STORAGE_BLOCKS_RUBY).add(ModBlocks.RUBY_BLOCK.get());
		
		tag(ModBlockTags.ORES_ASCABIT).add(ModBlocks.ASCABIT_ORE.get(), ModBlocks.DEEPSLATE_ASCABIT_ORE.get());
		tag(ModBlockTags.STORAGE_BLOCKS_ASCABIT).add(ModBlocks.ASCABIT_BLOCK.get());
		tag(ModBlockTags.STORAGE_BLOCKS_RAW_ASCABIT).add(ModBlocks.RAW_ASCABIT_BLOCK.get());
		
		tag(ModBlockTags.ORES_TIN).add(ModBlocks.TIN_ORE.get(), ModBlocks.DEEPSLATE_TIN_ORE.get());
		tag(ModBlockTags.STORAGE_BLOCKS_TIN).add(ModBlocks.TIN_BLOCK.get());
		tag(ModBlockTags.STORAGE_BLOCKS_RAW_TIN).add(ModBlocks.RAW_TIN_BLOCK.get());
		
		tag(ModBlockTags.ORES_TITANIUM).add(ModBlocks.TITANIUM_ORE.get(), ModBlocks.DEEPSLATE_TITANIUM_ORE.get());
		tag(ModBlockTags.STORAGE_BLOCKS_TITANIUM).add(ModBlocks.TITANIUM_BLOCK.get());
		tag(ModBlockTags.STORAGE_BLOCKS_RAW_TITANIUM).add(ModBlocks.RAW_TITANIUM_BLOCK.get());
		
		tag(ModBlockTags.ORES_ZINC).add(ModBlocks.ZINC_ORE.get(), ModBlocks.DEEPSLATE_ZINC_ORE.get());
		tag(ModBlockTags.STORAGE_BLOCKS_ZINC).add(ModBlocks.ZINC_BLOCK.get());
		tag(ModBlockTags.STORAGE_BLOCKS_RAW_ZINC).add(ModBlocks.RAW_ZINC_BLOCK.get());
		
		tag(ModBlockTags.STORAGE_BLOCKS_STEEL).add(ModBlocks.STEEL_BLOCK.get());
		
		tag(ModBlockTags.STORAGE_BLOCKS_BRONZE).add(ModBlocks.BRONZE_BLOCK.get());
		
		tag(ModBlockTags.STORAGE_BLOCKS_BRASS).add(ModBlocks.BRASS_BLOCK.get());
	}
	
	@Override
	protected void addNeedsToolTags()
	{
		tag(BlockTags.NEEDS_STONE_TOOL).add(ModBlocks.UNIM_ORE.getKey(), ModBlocks.DEEPSLATE_UNIM_ORE.getKey(), ModBlocks.UNIM_BLOCK.getKey(), ModBlocks.RUBY_ORE.getKey(), ModBlocks.DEEPSLATE_RUBY_ORE.getKey(), ModBlocks.RUBY_BLOCK.getKey(), ModBlocks.ASCABIT_ORE.getKey(), ModBlocks.DEEPSLATE_ASCABIT_ORE.getKey(), ModBlocks.ASCABIT_BLOCK.getKey(), ModBlocks.RAW_ASCABIT_BLOCK.getKey(), ModBlocks.TIN_ORE.getKey(), ModBlocks.DEEPSLATE_TIN_ORE.getKey(), ModBlocks.TIN_BLOCK.getKey(), ModBlocks.RAW_TIN_BLOCK.getKey(), ModBlocks.ZINC_ORE.getKey(), ModBlocks.DEEPSLATE_ZINC_ORE.getKey(), ModBlocks.ZINC_BLOCK.getKey(), ModBlocks.RAW_ZINC_BLOCK.getKey(), ModBlocks.STEEL_BLOCK.getKey(), ModBlocks.BRONZE_BLOCK.getKey(), ModBlocks.BRASS_BLOCK.getKey());
		tag(BlockTags.NEEDS_IRON_TOOL).add(ModBlocks.TITANIUM_ORE.getKey(), ModBlocks.DEEPSLATE_TITANIUM_ORE.getKey(), ModBlocks.TITANIUM_BLOCK.getKey(), ModBlocks.RAW_TITANIUM_BLOCK.getKey(), ModBlocks.MACHINE_FRAME.getKey(), ModBlocks.COAL_GENERATOR.getKey(), ModBlocks.BATTERY.getKey(), ModBlocks.SOLAR_PANEL.getKey(), ModBlocks.LIQUID_TANK.getKey(), ModBlocks.ELECTRIC_BREWERY.getKey(), ModBlocks.ELECTRIC_SMELTER.getKey(), ModBlocks.ELECTRIC_GREENHOUSE.getKey(), ModBlocks.HYDRAULIC_PRESS.getKey());
		
		tag(ModTiers.NEEDS_FLINT_TOOL).addTag(BlockTags.LOGS);
		tag(ModTiers.NEEDS_BRONZE_TOOL).add(Blocks.IRON_ORE);
	}
	
	@Override
	protected void addMinableTags()
	{
		tag(BlockTags.MINEABLE_WITH_PICKAXE).add(ModBlocks.UNIM_ORE.getKey(), ModBlocks.DEEPSLATE_UNIM_ORE.getKey(), ModBlocks.UNIM_BLOCK.getKey(), ModBlocks.RUBY_ORE.getKey(), ModBlocks.DEEPSLATE_RUBY_ORE.getKey(), ModBlocks.RUBY_BLOCK.getKey(), ModBlocks.ASCABIT_ORE.getKey(), ModBlocks.DEEPSLATE_ASCABIT_ORE.getKey(), ModBlocks.ASCABIT_BLOCK.getKey(), ModBlocks.RAW_ASCABIT_BLOCK.getKey(), ModBlocks.TIN_ORE.getKey(), ModBlocks.DEEPSLATE_TIN_ORE.getKey(), ModBlocks.TIN_BLOCK.getKey(), ModBlocks.RAW_TIN_BLOCK.getKey(), ModBlocks.TITANIUM_ORE.getKey(), ModBlocks.DEEPSLATE_TITANIUM_ORE.getKey(), ModBlocks.TITANIUM_BLOCK.getKey(), ModBlocks.RAW_TITANIUM_BLOCK.getKey(), ModBlocks.ZINC_ORE.getKey(), ModBlocks.DEEPSLATE_ZINC_ORE.getKey(), ModBlocks.ZINC_BLOCK.getKey(), ModBlocks.RAW_ZINC_BLOCK.getKey(), ModBlocks.STEEL_BLOCK.getKey(), ModBlocks.BRONZE_BLOCK.getKey(), ModBlocks.BRASS_BLOCK.getKey(), ModBlocks.CEMENT.getKey(), ModBlocks.CEMENT_STAIRS.getKey(), ModBlocks.CEMENT_SLAB.getKey(), ModBlocks.CEMENT_WALL.getKey(), ModBlocks.CEMENT_PLATE.getKey(), ModBlocks.CEMENT_PLATE_STAIRS.getKey(), ModBlocks.CEMENT_PLATE_SLAB.getKey(), ModBlocks.CEMENT_PLATE_WALL.getKey(), ModBlocks.LIMESTONE.getKey(), ModBlocks.LIMESTONE_STAIRS.getKey(), ModBlocks.LIMESTONE_SLAB.getKey(), ModBlocks.LIMESTONE_WALL.getKey(), ModBlocks.LIMESTONE_BRICKS.getKey(), ModBlocks.LIMESTONE_BRICK_STAIRS.getKey(), ModBlocks.LIMESTONE_BRICK_SLAB.getKey(), ModBlocks.LIMESTONE_BRICK_WALL.getKey(), ModBlocks.MARBLE.getKey(), ModBlocks.MARBLE_STAIRS.getKey(), ModBlocks.MARBLE_SLAB.getKey(), ModBlocks.MARBLE_WALL.getKey(), ModBlocks.MARBLE_COBBLE.getKey(), ModBlocks.MARBLE_COBBLE_STAIRS.getKey(), ModBlocks.MARBLE_COBBLE_SLAB.getKey(), ModBlocks.MARBLE_COBBLE_WALL.getKey(), ModBlocks.MARBLE_BRICKS.getKey(), ModBlocks.MARBLE_BRICK_STAIRS.getKey(), ModBlocks.MARBLE_BRICK_SLAB.getKey(), ModBlocks.MARBLE_BRICK_WALL.getKey(), ModBlocks.MARBLE_LAMP.getKey(), ModBlocks.STONE_DOOR.getKey(), ModBlocks.STONE_TRAPDOOR.getKey(), ModBlocks.SANDSTONE_DOOR.getKey(), ModBlocks.SANDSTONE_TRAPDOOR.getKey(), ModBlocks.GLASS_DOOR.getKey(), ModBlocks.GLASS_TRAPDOOR.getKey(), ModBlocks.KILN.getKey(), ModBlocks.SMELTER.getKey(), ModBlocks.MILLSTONE.getKey(), ModBlocks.MACHINE_FRAME.getKey(), ModBlocks.COAL_GENERATOR.getKey(), ModBlocks.BATTERY.getKey(), ModBlocks.SOLAR_PANEL.getKey(), ModBlocks.LIQUID_TANK.getKey(), ModBlocks.ELECTRIC_BREWERY.getKey(), ModBlocks.ELECTRIC_SMELTER.getKey(), ModBlocks.ELECTRIC_GREENHOUSE.getKey(), ModBlocks.HYDRAULIC_PRESS.getKey(), ModBlocks.COPPER_CABLE.getKey());
		tag(BlockTags.MINEABLE_WITH_AXE).add(ModBlocks.OAK_CHOPPING_BLOCK.getKey(), ModBlocks.SPRUCE_CHOPPING_BLOCK.getKey(), ModBlocks.BIRCH_CHOPPING_BLOCK.getKey(), ModBlocks.JUNGLE_CHOPPING_BLOCK.getKey(), ModBlocks.ACACIA_CHOPPING_BLOCK.getKey(), ModBlocks.DARK_OAK_CHOPPING_BLOCK.getKey(), ModBlocks.MANGROVE_CHOPPING_BLOCK.getKey(), ModBlocks.CHERRY_CHOPPING_BLOCK.getKey(), ModBlocks.CRIMSON_CHOPPING_BLOCK.getKey(), ModBlocks.WARPED_CHOPPING_BLOCK.getKey(), ModBlocks.OAK_DRYING_RACK.getKey(), ModBlocks.SPRUCE_DRYING_RACK.getKey(), ModBlocks.BIRCH_DRYING_RACK.getKey(), ModBlocks.JUNGLE_DRYING_RACK.getKey(), ModBlocks.ACACIA_DRYING_RACK.getKey(), ModBlocks.DARK_OAK_DRYING_RACK.getKey(), ModBlocks.MANGROVE_DRYING_RACK.getKey(), ModBlocks.CHERRY_DRYING_RACK.getKey(), ModBlocks.CRIMSON_DRYING_RACK.getKey(), ModBlocks.WARPED_DRYING_RACK.getKey());
		tag(BlockTags.MINEABLE_WITH_SHOVEL).add(ModBlocks.DIRT_SLAB.getKey(), ModBlocks.COARSE_DIRT_SLAB.getKey(), ModBlocks.DIRT_PATH_SLAB.getKey(), ModBlocks.GRASS_BLOCK_SLAB.getKey());
	}
	
	@Override
	protected void addToVanillaTags()
	{
		tag(BlockTags.WALLS).add(ModBlocks.LIMESTONE_WALL.getKey(), ModBlocks.MARBLE_WALL.getKey(), ModBlocks.MARBLE_COBBLE_WALL.getKey(), ModBlocks.MARBLE_BRICK_WALL.getKey(), ModBlocks.LIMESTONE_BRICK_WALL.getKey(), ModBlocks.CEMENT_WALL.getKey(), ModBlocks.CEMENT_PLATE_WALL.getKey());
	}
}
