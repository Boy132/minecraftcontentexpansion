package de.boy132.minecraftcontentexpansion.datagen.recipe;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.datagen.recipe.condition.ModuleSettingCondition;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.recipe.choppingblock.ChoppingBlockRecipeBuilder;
import de.boy132.minecraftcontentexpansion.recipe.electricsmelter.ElectricSmelterRecipeBuilder;
import de.boy132.minecraftcontentexpansion.recipe.smelter.SmelterRecipeBuilder;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.crafting.ConditionalRecipe;
import net.minecraftforge.common.crafting.conditions.IConditionBuilder;

import javax.annotation.Nullable;
import java.util.List;

public abstract class BaseRecipeProvider extends RecipeProvider implements IConditionBuilder
{
	public BaseRecipeProvider(PackOutput packOutput)
	{
		super(packOutput);
	}
	
	protected abstract void overrideVanillaRecipes(RecipeOutput recipeOutput);
	
	protected abstract void generateCraftingRecipes(RecipeOutput recipeOutput);
	
	protected abstract void generateCookingRecipes(RecipeOutput recipeOutput);
	
	protected abstract void generateCustomRecipes(RecipeOutput recipeOutput);
	
	@Override
	protected void buildRecipes(RecipeOutput recipeOutput)
	{
		overrideVanillaRecipes(recipeOutput);
		
		generateCraftingRecipes(recipeOutput);
		generateCookingRecipes(recipeOutput);
		
		generateCustomRecipes(recipeOutput);
	}
	
	protected static void oreSmelting(RecipeOutput recipeOutput, List<ItemLike> smeltableItems, ItemLike result, float exp, int cookingTime, @Nullable String group)
	{
		for(ItemLike itemlike : smeltableItems)
		{
			SimpleCookingRecipeBuilder.smelting(Ingredient.of(itemlike), RecipeCategory.MISC, result, exp, cookingTime).group(group)
					.unlockedBy(getHasName(itemlike), has(itemlike))
					.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getItemName(result) + "_from_smelting_" + getItemName(itemlike)));
		}
	}
	
	protected static void oreBlasting(RecipeOutput recipeOutput, List<ItemLike> smeltableItems, ItemLike result, float exp, int cookingTime, @Nullable String group)
	{
		for(ItemLike itemlike : smeltableItems)
		{
			SimpleCookingRecipeBuilder.blasting(Ingredient.of(itemlike), RecipeCategory.MISC, result, exp, cookingTime).group(group)
					.unlockedBy(getHasName(itemlike), has(itemlike))
					.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getItemName(result) + "_from_blasting_" + getItemName(itemlike)));
		}
	}
	
	protected static void foodCooking(RecipeOutput recipeOutput, ItemLike input, ItemLike result, float exp, boolean withCampfireRecipe)
	{
		foodCooking(recipeOutput, input, result, exp, withCampfireRecipe, getSimpleRecipeName(result));
	}
	
	protected static void foodCooking(RecipeOutput recipeOutput, ItemLike input, ItemLike result, float exp, boolean withCampfireRecipe, String name)
	{
		SimpleCookingRecipeBuilder.smelting(Ingredient.of(input), RecipeCategory.FOOD, result, exp, 200)
				.unlockedBy(getHasName(input), has(input))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, name));
		
		SimpleCookingRecipeBuilder.smoking(Ingredient.of(input), RecipeCategory.FOOD, result, exp, 100)
				.unlockedBy(getHasName(input), has(input))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, name + "_from_smoking"));
		
		if(withCampfireRecipe)
		{
			SimpleCookingRecipeBuilder.campfireCooking(Ingredient.of(input), RecipeCategory.FOOD, result, exp, 600)
					.unlockedBy(getHasName(input), has(input))
					.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, name + "_from_campfire_cooking"));
		}
	}
	
	protected static void nineBlockStorageRecipes(RecipeOutput recipeOutput, ItemLike rawItem, ItemLike storageItem, String storageItemRecipeName, @Nullable String storageItemGroup, String rawItemRecipeName, @Nullable String rawItemGroup)
	{
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, rawItem, 9)
				.requires(storageItem).group(rawItemGroup)
				.unlockedBy(getHasName(storageItem), has(storageItem))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, rawItemRecipeName));
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, storageItem)
				.define('#', rawItem)
				.pattern("###")
				.pattern("###")
				.pattern("###").group(storageItemGroup)
				.unlockedBy(getHasName(rawItem), has(rawItem))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, storageItemRecipeName));
	}
	
	protected static void stairs(RecipeOutput recipeOutput, ItemLike stairs, ItemLike ingredient)
	{
		stairBuilder(stairs, Ingredient.of(ingredient)).unlockedBy(getHasName(ingredient), has(ingredient))
				.save(recipeOutput);
	}
	
	protected static void stairs(RecipeOutput recipeOutput, ItemLike stairs, TagKey<Item> ingredient, String hasName)
	{
		stairBuilder(stairs, Ingredient.of(ingredient)).unlockedBy(hasName, has(ingredient))
				.save(recipeOutput);
	}
	
	protected static void slab(RecipeOutput recipeOutput, ItemLike slab, ItemLike ingredient)
	{
		slab(recipeOutput, RecipeCategory.BUILDING_BLOCKS, slab, ingredient);
	}
	
	protected static void slab(RecipeOutput recipeOutput, ItemLike slab, TagKey<Item> ingredient, String hasName)
	{
		slabBuilder(RecipeCategory.BUILDING_BLOCKS, slab, Ingredient.of(ingredient)).unlockedBy(hasName, has(ingredient))
				.save(recipeOutput);
	}
	
	protected static void wall(RecipeOutput recipeOutput, ItemLike wall, ItemLike ingredient)
	{
		wall(recipeOutput, RecipeCategory.BUILDING_BLOCKS, wall, ingredient);
	}
	
	protected static void wall(RecipeOutput recipeOutput, ItemLike wall, TagKey<Item> ingredient, String hasName)
	{
		wallBuilder(RecipeCategory.BUILDING_BLOCKS, wall, Ingredient.of(ingredient)).unlockedBy(hasName, has(ingredient))
				.save(recipeOutput);
	}
	
	protected static void door(RecipeOutput recipeOutput, ItemLike door, ItemLike ingredient)
	{
		doorBuilder(door, Ingredient.of(ingredient)).unlockedBy(getHasName(ingredient), has(ingredient))
				.save(recipeOutput);
	}
	
	protected static void door(RecipeOutput recipeOutput, ItemLike door, TagKey<Item> ingredient, String hasName)
	{
		doorBuilder(door, Ingredient.of(ingredient)).unlockedBy(hasName, has(ingredient))
				.save(recipeOutput);
	}
	
	protected static void trapDoor(RecipeOutput recipeOutput, ItemLike trapDoor, ItemLike ingredient)
	{
		trapdoorBuilder(trapDoor, Ingredient.of(ingredient)).unlockedBy(getHasName(ingredient), has(ingredient))
				.save(recipeOutput);
	}
	
	protected static void trapDoor(RecipeOutput recipeOutput, ItemLike trapDoor, TagKey<Item> ingredient, String hasName)
	{
		trapdoorBuilder(trapDoor, Ingredient.of(ingredient)).unlockedBy(hasName, has(ingredient))
				.save(recipeOutput);
	}
	
	protected static void stonecutterResultFromBase(RecipeOutput recipeOutput, ItemLike result, ItemLike base)
	{
		stonecutterResultFromBase(recipeOutput, result, base, 1);
	}
	
	protected static void stonecutterResultFromBase(RecipeOutput recipeOutput, ItemLike result, ItemLike base, int count)
	{
		SingleItemRecipeBuilder.stonecutting(Ingredient.of(base), RecipeCategory.BUILDING_BLOCKS, result, count)
				.unlockedBy(getHasName(base), has(base))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getConversionRecipeName(result, base) + "_stonecutting"));
	}
	
	protected static void oreSmelterRecipes(RecipeOutput recipeOutput, float heat, ItemLike ingot, TagKey<Item> rawItem, TagKey<Item> oreItem)
	{
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(rawItem),
						Ingredient.of(rawItem),
						Ingredient.of(rawItem),
						Ingredient.of(rawItem)
				}, new ItemStack(ingot, 2), heat)
				.unlockedBy("has_raw_item", has(rawItem))
				.save(recipeOutput);
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(oreItem),
						Ingredient.of(oreItem),
						Ingredient.of(oreItem),
						Ingredient.of(oreItem)
				}, new ItemStack(ingot, 2), heat + 0.05f)
				.unlockedBy("has_ore", has(oreItem))
				.save(recipeOutput, getItemName(ingot) + "_from_ore");
	}
	
	protected static void oreElectricSmelterRecipes(RecipeOutput recipeOutput, ItemLike ingot, TagKey<Item> rawItem, TagKey<Item> oreItem)
	{
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(rawItem),
						Ingredient.of(rawItem),
						Ingredient.of(rawItem),
						Ingredient.of(rawItem)
				}, new ItemStack(ingot, 4))
				.unlockedBy("has_raw_item", has(rawItem))
				.save(recipeOutput);
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(oreItem),
						Ingredient.of(oreItem),
						Ingredient.of(oreItem),
						Ingredient.of(oreItem)
				}, new ItemStack(ingot, 4))
				.unlockedBy("has_ore", has(oreItem))
				.save(recipeOutput, getItemName(ingot) + "_from_ore");
	}
	
	protected static void toolHeadRecipes(RecipeOutput recipeOutput, ItemLike material, Item swordBlade, Item pickaxeHead, Item axeHead, Item shovelHead, Item hoeHead)
	{
		ShapedRecipeBuilder swordBladeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, swordBlade)
				.define('#', material)
				.pattern(" # ")
				.pattern(" # ")
				.pattern("###")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(swordBladeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(swordBlade)));
		
		ShapedRecipeBuilder pickaxeHeadRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, pickaxeHead)
				.define('#', material)
				.pattern("###")
				.pattern(" # ")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(pickaxeHeadRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(pickaxeHead)));
		
		ShapedRecipeBuilder axeHeadRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, axeHead)
				.define('#', material)
				.pattern("##")
				.pattern(" #")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(axeHeadRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(axeHead)));
		
		ShapedRecipeBuilder shovelHeadRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, shovelHead)
				.define('#', material)
				.pattern("##")
				.pattern("##")
				.pattern(" #")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(shovelHeadRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(shovelHead)));
		
		ShapedRecipeBuilder hoeHeadRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hoeHead)
				.define('#', material)
				.pattern("###")
				.pattern("  #")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(hoeHeadRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hoeHead)));
	}
	
	protected static void toolHeadRecipes(RecipeOutput recipeOutput, TagKey<Item> material, Item swordBlade, Item pickaxeHead, Item axeHead, Item shovelHead, Item hoeHead)
	{
		ShapedRecipeBuilder swordBladeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, swordBlade)
				.define('#', material)
				.pattern(" # ")
				.pattern(" # ")
				.pattern("###")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(swordBladeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(swordBlade)));
		
		ShapedRecipeBuilder pickaxeHeadRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, pickaxeHead)
				.define('#', material)
				.pattern("###")
				.pattern(" # ")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(pickaxeHeadRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(pickaxeHead)));
		
		ShapedRecipeBuilder axeHeadRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, axeHead)
				.define('#', material)
				.pattern("##")
				.pattern(" #")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(axeHeadRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(axeHead)));
		
		ShapedRecipeBuilder shovelHeadRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, shovelHead)
				.define('#', material)
				.pattern("##")
				.pattern("##")
				.pattern(" #")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(shovelHeadRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(shovelHead)));
		
		ShapedRecipeBuilder hoeHeadRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hoeHead)
				.define('#', material)
				.pattern("###")
				.pattern("  #")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(hoeHeadRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hoeHead)));
	}
	
	protected static void toolVanillaRecipes(RecipeOutput recipeOutput, String namespace, ItemLike material, Item sword, Item pickaxe, Item axe, Item shovel, Item hoe)
	{
		ShapedRecipeBuilder swordRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, sword)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("M")
				.pattern("M")
				.pattern("S")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(swordRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(sword)));
		
		ShapedRecipeBuilder pickaxeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, pickaxe)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("MMM")
				.pattern(" S ")
				.pattern(" S ")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(pickaxeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(pickaxe)));
		
		ShapedRecipeBuilder axeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, axe)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("MM")
				.pattern("SM")
				.pattern("S ")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(axeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(axe)));
		
		ShapedRecipeBuilder shovelRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, shovel)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("M")
				.pattern("S")
				.pattern("S")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(shovelRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(shovel)));
		
		ShapedRecipeBuilder hoeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hoe)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("MM")
				.pattern("S ")
				.pattern("S ")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(hoeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(hoe)));
	}
	
	protected static void toolVanillaRecipes(RecipeOutput recipeOutput, String namespace, TagKey<Item> material, Item sword, Item pickaxe, Item axe, Item shovel, Item hoe)
	{
		ShapedRecipeBuilder swordRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, sword)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("M")
				.pattern("M")
				.pattern("S")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(swordRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(sword)));
		
		ShapedRecipeBuilder pickaxeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, pickaxe)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("MMM")
				.pattern(" S ")
				.pattern(" S ")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(pickaxeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(pickaxe)));
		
		ShapedRecipeBuilder axeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, axe)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("MM")
				.pattern("SM")
				.pattern("S ")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(axeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(axe)));
		
		ShapedRecipeBuilder shovelRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, shovel)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("M")
				.pattern("S")
				.pattern("S")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(shovelRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(shovel)));
		
		ShapedRecipeBuilder hoeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hoe)
				.define('S', Items.STICK)
				.define('M', material)
				.pattern("MM")
				.pattern("S ")
				.pattern("S ")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(hoeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(hoe)));
	}
	
	protected static void toolAdvancedRecipes(RecipeOutput recipeOutput, boolean isPrimitive, Item sword, Item swordBlade, Item pickaxe, Item pickaxeHead, Item axe, Item axeHead, Item shovel, Item shovelHead, Item hoe, Item hoeHead)
	{
		ShapedRecipeBuilder swordRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, sword)
				.define('S', Items.STICK)
				.define('H', swordBlade)
				.define('X', isPrimitive ? ModItems.PLANT_FIBER.get() : ModItems.LEATHER_STRIP.get())
				.pattern("HX")
				.pattern(" S")
				.unlockedBy(getHasName(swordBlade), has(swordBlade));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> swordRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(sword) + "_with_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(sword) + "_with_head"));
		
		ShapedRecipeBuilder pickaxeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, pickaxe)
				.define('S', Items.STICK)
				.define('H', pickaxeHead)
				.define('X', isPrimitive ? ModItems.PLANT_FIBER.get() : ModItems.LEATHER_STRIP.get())
				.pattern("HX")
				.pattern(" S")
				.unlockedBy(getHasName(pickaxeHead), has(pickaxeHead));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> pickaxeRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(pickaxe) + "_with_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(pickaxe) + "_with_head"));
		
		ShapedRecipeBuilder axeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, axe)
				.define('S', Items.STICK)
				.define('H', axeHead)
				.define('X', isPrimitive ? ModItems.PLANT_FIBER.get() : ModItems.LEATHER_STRIP.get())
				.pattern("HX")
				.pattern(" S")
				.unlockedBy(getHasName(axeHead), has(axeHead));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> axeRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(axe) + "_with_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(axe) + "_with_head"));
		
		ShapedRecipeBuilder shovelRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, shovel)
				.define('S', Items.STICK)
				.define('H', shovelHead)
				.define('X', isPrimitive ? ModItems.PLANT_FIBER.get() : ModItems.LEATHER_STRIP.get())
				.pattern("HX")
				.pattern(" S")
				.unlockedBy(getHasName(shovelHead), has(shovelHead));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> shovelRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(shovel) + "_with_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(shovel) + "_with_head"));
		
		ShapedRecipeBuilder hoeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hoe)
				.define('S', Items.STICK)
				.define('H', hoeHead)
				.define('X', isPrimitive ? ModItems.PLANT_FIBER.get() : ModItems.LEATHER_STRIP.get())
				.pattern("HX")
				.pattern(" S")
				.unlockedBy(getHasName(hoeHead), has(hoeHead));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> hoeRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hoe) + "_with_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hoe) + "_with_head"));
	}
	
	protected static void armorVanillaRecipes(RecipeOutput recipeOutput, String namespace, ItemLike material, Item helmet, Item chestplate, Item leggings, Item boots)
	{
		ShapedRecipeBuilder helmetRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, helmet)
				.define('M', material)
				.pattern("MMM")
				.pattern("M M")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.METAL_AGE_ARMOR_CRAFTING_MODE_VANILLA)
				.recipe(helmetRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(helmet)));
		
		ShapedRecipeBuilder chestplateRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, chestplate)
				.define('M', material)
				.pattern("M M")
				.pattern("MMM")
				.pattern("MMM")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.METAL_AGE_ARMOR_CRAFTING_MODE_VANILLA)
				.recipe(chestplateRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(chestplate)));
		
		ShapedRecipeBuilder leggingsRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, leggings)
				.define('M', material)
				.pattern("MMM")
				.pattern("MMM")
				.pattern("M M")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.METAL_AGE_ARMOR_CRAFTING_MODE_VANILLA)
				.recipe(leggingsRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(leggings)));
		
		ShapedRecipeBuilder bootsRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, boots)
				.define('M', material)
				.pattern("M M")
				.pattern("M M")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.METAL_AGE_ARMOR_CRAFTING_MODE_VANILLA)
				.recipe(bootsRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(boots)));
	}
	
	protected static void armorVanillaRecipes(RecipeOutput recipeOutput, String namespace, TagKey<Item> material, Item helmet, Item chestplate, Item leggings, Item boots)
	{
		ShapedRecipeBuilder helmetRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, helmet)
				.define('M', material)
				.pattern("MMM")
				.pattern("M M")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.METAL_AGE_ARMOR_CRAFTING_MODE_VANILLA)
				.recipe(helmetRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(helmet)));
		
		ShapedRecipeBuilder chestplateRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, chestplate)
				.define('M', material)
				.pattern("M M")
				.pattern("MMM")
				.pattern("MMM")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.METAL_AGE_ARMOR_CRAFTING_MODE_VANILLA)
				.recipe(chestplateRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(chestplate)));
		
		ShapedRecipeBuilder leggingsRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, leggings)
				.define('M', material)
				.pattern("MMM")
				.pattern("MMM")
				.pattern("M M")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.METAL_AGE_ARMOR_CRAFTING_MODE_VANILLA)
				.recipe(leggingsRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(leggings)));
		
		ShapedRecipeBuilder bootsRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, boots)
				.define('M', material)
				.pattern("M M")
				.pattern("M M")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.METAL_AGE_ARMOR_CRAFTING_MODE_VANILLA)
				.recipe(bootsRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(namespace, getSimpleRecipeName(boots)));
	}
	
	protected static void armorAdvancedRecipes(RecipeOutput recipeOutput, ItemLike plate, Item helmet, Item chestplate, Item leggings, Item boots)
	{
		ShapedRecipeBuilder helmetRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, helmet)
				.define('M', plate)
				.pattern("MMM")
				.pattern("M M")
				.unlockedBy(getHasName(plate), has(plate));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> helmetRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(helmet) + "_with_plates")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(helmet) + "_with_plates"));
		
		ShapedRecipeBuilder chestplateRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, chestplate)
				.define('M', plate)
				.define('L', ModItems.LEATHER_STRIP.get())
				.pattern("L L")
				.pattern("MMM")
				.pattern("MMM")
				.unlockedBy(getHasName(plate), has(plate));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> chestplateRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(chestplate) + "_with_plates")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(chestplate) + "_with_plates"));
		
		ShapedRecipeBuilder leggingsRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, leggings)
				.define('M', plate)
				.define('L', ModItems.LEATHER_STRIP.get())
				.pattern("LML")
				.pattern("M M")
				.pattern("M M")
				.unlockedBy(getHasName(plate), has(plate));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> leggingsRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(leggings) + "_with_plates")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(leggings) + "_with_plates"));
		
		ShapedRecipeBuilder bootsRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, boots)
				.define('M', plate)
				.define('L', Items.LEATHER)
				.pattern("L L")
				.pattern("M M")
				.unlockedBy(getHasName(plate), has(plate));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> bootsRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(boots) + "_with_plates")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(boots) + "_with_plates"));
	}
	
	protected static void knifeRecipes(RecipeOutput recipeOutput, boolean isPrimitive, ItemLike material, Item knife, Item knifeBlade)
	{
		ShapedRecipeBuilder vanillaKnifeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, knife)
				.define('M', material)
				.define('X', isPrimitive ? ModItems.PLANT_FIBER.get() : ModItems.LEATHER_STRIP.get())
				.define('S', Items.STICK)
				.pattern("MX")
				.pattern(" S")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(vanillaKnifeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(knife)));
		
		ShapedRecipeBuilder bladeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, knifeBlade)
				.define('M', material)
				.pattern(" M")
				.pattern("MM")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(bladeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(knifeBlade)));
		
		ShapedRecipeBuilder advancedKnifeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, knife)
				.define('B', knifeBlade)
				.define('X', isPrimitive ? ModItems.PLANT_FIBER.get() : ModItems.LEATHER_STRIP.get())
				.define('S', Items.STICK)
				.pattern("BX")
				.pattern(" S")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> advancedKnifeRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(knife) + "_with_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(knife) + "_with_head"));
	}
	
	protected static void knifeRecipes(RecipeOutput recipeOutput, boolean isPrimitive, TagKey<Item> material, Item knife, Item knifeBlade)
	{
		ShapedRecipeBuilder vanillaKnifeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, knife)
				.define('M', material)
				.define('X', isPrimitive ? ModItems.PLANT_FIBER.get() : ModItems.LEATHER_STRIP.get())
				.define('S', Items.STICK)
				.pattern("MX")
				.pattern(" S")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(vanillaKnifeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(knife)));
		
		ShapedRecipeBuilder bladeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, knifeBlade)
				.define('M', material)
				.pattern(" M")
				.pattern("MM")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(bladeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(knifeBlade)));
		
		ShapedRecipeBuilder advancedKnifeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, knife)
				.define('B', knifeBlade)
				.define('X', isPrimitive ? ModItems.PLANT_FIBER.get() : ModItems.LEATHER_STRIP.get())
				.define('S', Items.STICK)
				.pattern("BX")
				.pattern(" S")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> advancedKnifeRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(knife) + "_with_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(knife) + "_with_head"));
	}
	
	protected static void hammerRecipes(RecipeOutput recipeOutput, ItemLike material, Item hammer, Item hammerHead)
	{
		ShapedRecipeBuilder vanillaHammerRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hammer)
				.define('S', Items.STICK)
				.define('F', ModItems.LEATHER_STRIP.get())
				.define('M', material)
				.pattern("MFM")
				.pattern(" S ")
				.pattern(" S ")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(vanillaHammerRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hammer)));
		
		ShapedRecipeBuilder HammerHead = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hammerHead)
				.define('M', material)
				.pattern("MMM")
				.pattern("MMM")
				.pattern(" M ")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(HammerHead::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hammerHead)));
		
		ShapedRecipeBuilder advancedHammerRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hammer)
				.define('S', Items.STICK)
				.define('F', ModItems.LEATHER_STRIP.get())
				.define('H', hammerHead)
				.pattern("HF")
				.pattern(" S")
				.unlockedBy(getHasName(material), has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> advancedHammerRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hammer) + "_with_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hammer) + "_with_head"));
	}
	
	protected static void hammerRecipes(RecipeOutput recipeOutput, TagKey<Item> material, Item hammer, Item hammerHead)
	{
		ShapedRecipeBuilder vanillaHammerRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hammer)
				.define('S', Items.STICK)
				.define('F', ModItems.LEATHER_STRIP.get())
				.define('M', material)
				.pattern("MFM")
				.pattern(" S ")
				.pattern(" S ")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(vanillaHammerRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hammer)));
		
		ShapedRecipeBuilder HammerHead = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hammerHead)
				.define('M', material)
				.pattern("MMM")
				.pattern("MMM")
				.pattern(" M ")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(HammerHead::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hammerHead)));
		
		ShapedRecipeBuilder advancedHammerRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, hammer)
				.define('S', Items.STICK)
				.define('F', ModItems.LEATHER_STRIP.get())
				.define('H', hammerHead)
				.pattern("HF")
				.pattern(" S")
				.unlockedBy("has_material", has(material));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(reciperecipeOutput -> advancedHammerRecipe.save(reciperecipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hammer) + "_with_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, getSimpleRecipeName(hammer) + "_with_head"));
	}
	
	protected static void removeVanillaWoodRecipes(RecipeOutput recipeOutput, TagKey<Item> logs, Block planks, Block slab)
	{
		ShapelessRecipeBuilder removePlanksRecipe = ShapelessRecipeBuilder.shapeless(RecipeCategory.BUILDING_BLOCKS, planks, 4)
				.requires(logs)
				.unlockedBy("has_log", has(logs));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_WOOD_CRAFTING_MODE_VANILLA)
				.recipe(removePlanksRecipe::save)
				.save(recipeOutput, ResourceLocation.parse(getSimpleRecipeName(planks)));
		
		ShapedRecipeBuilder removeSlabRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, slab, 6)
				.define('P', planks)
				.pattern("PPP")
				.unlockedBy(getHasName(planks), has(planks));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_WOOD_CRAFTING_MODE_VANILLA)
				.recipe(removeSlabRecipe::save)
				.save(recipeOutput, ResourceLocation.parse(getSimpleRecipeName(slab)));
	}
	
	protected static void woodChoppingBlockRecipes(RecipeOutput recipeOutput, Block choppingBlock, TagKey<Item> logs, Block planks, Block slab)
	{
		ChoppingBlockRecipeBuilder.choppingBlock(Ingredient.of(logs), new ItemStack(planks, 4))
				.unlockedBy("has_log", has(logs))
				.save(recipeOutput);
		
		ChoppingBlockRecipeBuilder.choppingBlock(Ingredient.of(planks), new ItemStack(slab, 2))
				.unlockedBy("has_plank", has(planks))
				.save(recipeOutput);
		
		ChoppingBlockRecipeBuilder.choppingBlock(Ingredient.of(choppingBlock), new ItemStack(planks, 2))
				.unlockedBy("has_chopping_block", has(choppingBlock))
				.save(recipeOutput, ResourceLocation.parse(getSimpleRecipeName(planks) + "_from_chopping_block"));
	}
}
