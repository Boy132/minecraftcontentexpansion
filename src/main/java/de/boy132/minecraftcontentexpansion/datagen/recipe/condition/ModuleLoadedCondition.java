package de.boy132.minecraftcontentexpansion.datagen.recipe.condition;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.config.ModConfigValues;
import net.minecraftforge.common.crafting.conditions.ICondition;

public record ModuleLoadedCondition(String module) implements ICondition
{
	public static final ModuleLoadedCondition MORE_CRAFTING = new ModuleLoadedCondition("moreCrafting");
	public static final ModuleLoadedCondition STONE_AGE = new ModuleLoadedCondition("stoneAge");
	public static final ModuleLoadedCondition METAL_AGE = new ModuleLoadedCondition("metalAge");
	public static final ModuleLoadedCondition INDUSTRIAL_AGE = new ModuleLoadedCondition("industrialAge");
	
	public static final Codec<ModuleLoadedCondition> CODEC = RecordCodecBuilder.create(instance -> instance.group(Codec.STRING.fieldOf("module").forGetter(condition -> condition.module)).apply(instance, ModuleLoadedCondition::new));
	
	@Override
	public Codec<? extends ICondition> codec()
	{
		return CODEC;
	}
	
	@Override
	public boolean test(IContext context)
	{
		if(module.equalsIgnoreCase("moreCrafting"))
			return ModConfigValues.modules_moreCrafting.get();
		
		if(module.equalsIgnoreCase("stoneAge"))
			return ModConfigValues.modules_stoneAge.get();
		
		if(module.equalsIgnoreCase("metalAge"))
			return ModConfigValues.modules_metalAge.get();
		
		if(module.equalsIgnoreCase("industrialAge"))
			return ModConfigValues.modules_industrialAge.get();
		
		return false;
	}
	
	@Override
	public String toString()
	{
		return "module_loaded(\"" + module + "\")";
	}
}