package de.boy132.minecraftcontentexpansion.datagen.recipe.condition;

import com.mojang.serialization.Codec;
import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraftforge.common.crafting.conditions.ICondition;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModConditions
{
	private static final DeferredRegister<Codec<? extends ICondition>> CONDITION_SERIALIZERS = DeferredRegister.create(ForgeRegistries.Keys.CONDITION_SERIALIZERS, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<Codec<ModuleLoadedCondition>> ADDITION_SERIALIZER = CONDITION_SERIALIZERS.register("module_loaded", () -> ModuleLoadedCondition.CODEC);
	public static final RegistryObject<Codec<ModuleSettingCondition>> REPLACE_SERIALIZER = CONDITION_SERIALIZERS.register("module_setting", () -> ModuleSettingCondition.CODEC);
	
	public static void register(IEventBus eventBus)
	{
		CONDITION_SERIALIZERS.register(eventBus);
	}
}
