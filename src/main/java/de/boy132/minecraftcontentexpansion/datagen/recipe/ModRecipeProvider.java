package de.boy132.minecraftcontentexpansion.datagen.recipe;

import com.google.common.collect.ImmutableList;
import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.SoilType;
import de.boy132.minecraftcontentexpansion.datagen.recipe.condition.ModuleLoadedCondition;
import de.boy132.minecraftcontentexpansion.datagen.recipe.condition.ModuleSettingCondition;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.recipe.choppingblock.ChoppingBlockRecipeBuilder;
import de.boy132.minecraftcontentexpansion.recipe.dryingrack.DryingRackRecipeBuilder;
import de.boy132.minecraftcontentexpansion.recipe.electricsmelter.ElectricSmelterRecipeBuilder;
import de.boy132.minecraftcontentexpansion.recipe.greenhouse.GreenhouseRecipeBuilder;
import de.boy132.minecraftcontentexpansion.recipe.hydraulicpress.HydraulicPressRecipeBuilder;
import de.boy132.minecraftcontentexpansion.recipe.kiln.KilnRecipeBuilder;
import de.boy132.minecraftcontentexpansion.recipe.millstone.MillstoneRecipeBuilder;
import de.boy132.minecraftcontentexpansion.recipe.smelter.SmelterRecipeBuilder;
import de.boy132.minecraftcontentexpansion.tag.ModItemTags;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.crafting.ConditionalRecipe;
import net.minecraftforge.common.crafting.conditions.NotCondition;

public class ModRecipeProvider extends BaseRecipeProvider
{
	public ModRecipeProvider(PackOutput packOutput)
	{
		super(packOutput);
	}
	
	@Override
	protected void overrideVanillaRecipes(RecipeOutput recipeOutput)
	{
		ShapedRecipeBuilder oldFurnaceRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Blocks.FURNACE)
				.define('C', Tags.Items.COBBLESTONE)
				.pattern("CCC")
				.pattern("C C")
				.pattern("CCC")
				.unlockedBy("has_cobblestone", has(Tags.Items.COBBLESTONE));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.STONE_AGE))
				.recipe(oldFurnaceRecipe::save)
				.save(recipeOutput, ResourceLocation.parse("furnace"));
		
		ShapedRecipeBuilder newFurnaceRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Blocks.FURNACE)
				.define('C', Tags.Items.COBBLESTONE)
				.define('F', Blocks.CAMPFIRE)
				.define('S', Blocks.SMOOTH_STONE)
				.pattern("CCC")
				.pattern("C C")
				.pattern("SFS")
				.unlockedBy("has_cobblestone", has(Tags.Items.COBBLESTONE))
				.unlockedBy(getHasName(Blocks.CAMPFIRE), has(Blocks.CAMPFIRE))
				.unlockedBy(getHasName(Blocks.SMOOTH_STONE), has(Blocks.SMOOTH_STONE));
		ConditionalRecipe.builder().condition(ModuleLoadedCondition.STONE_AGE)
				.recipe(newFurnaceRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "furnace"));
		
		ShapedRecipeBuilder removeBreadRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Items.BREAD)
				.define('W', Items.WHEAT)
				.pattern("WWW")
				.unlockedBy(getHasName(Items.WHEAT), has(Items.WHEAT));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.STONE_AGE))
				.recipe(removeBreadRecipe::save)
				.save(recipeOutput, ResourceLocation.parse("bread"));
		
		ShapedRecipeBuilder oldCakeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, Items.CAKE)
				.define('M', Items.MILK_BUCKET)
				.define('S', Items.SUGAR)
				.define('W', Items.WHEAT)
				.define('E', Items.EGG)
				.pattern("MMM")
				.pattern("SES")
				.pattern("WWW")
				.unlockedBy(getHasName(Items.EGG), has(Items.EGG));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.STONE_AGE))
				.recipe(oldCakeRecipe::save)
				.save(recipeOutput, ResourceLocation.parse("cake"));
		
		ShapedRecipeBuilder newCakeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, Items.CAKE)
				.define('M', Items.MILK_BUCKET)
				.define('S', Items.SUGAR)
				.define('D', ModItems.DOUGH.get())
				.define('E', Items.EGG)
				.pattern("MMM")
				.pattern("SES")
				.pattern("DDD")
				.unlockedBy(getHasName(Items.EGG), has(Items.EGG));
		ConditionalRecipe.builder().condition(ModuleLoadedCondition.STONE_AGE)
				.recipe(newCakeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "cake"));
		
		ShapedRecipeBuilder oldCookieRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, Items.COOKIE, 8)
				.define('W', Items.WHEAT)
				.define('C', Items.COCOA_BEANS)
				.pattern("WCW")
				.unlockedBy(getHasName(Items.COCOA_BEANS), has(Items.COCOA_BEANS));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.STONE_AGE))
				.recipe(oldCookieRecipe::save)
				.save(recipeOutput, ResourceLocation.parse("cookie"));
		
		ShapedRecipeBuilder newCookieRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, Items.COOKIE, 4)
				.define('D', ModItems.DOUGH.get())
				.define('C', Items.COCOA_BEANS)
				.pattern("DCD")
				.unlockedBy(getHasName(Items.COCOA_BEANS), has(Items.COCOA_BEANS));
		ConditionalRecipe.builder().condition(ModuleLoadedCondition.STONE_AGE)
				.recipe(newCookieRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "cookie"));
		
		SimpleCookingRecipeBuilder removeIronIngotFromIronOreRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.IRON_ORE), RecipeCategory.MISC, Items.IRON_INGOT, 0.7f, 200)
				.unlockedBy(getHasName(Items.IRON_ORE), has(Items.IRON_ORE));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeIronIngotFromIronOreRecipe.save(recipeConsumer, "iron_ingot_from_smelting_iron_ore"))
				.save(recipeOutput, ResourceLocation.parse("iron_ingot_from_smelting_iron_ore"));
		
		SimpleCookingRecipeBuilder removeIronIngotFromDeepslateIronOreRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.DEEPSLATE_IRON_ORE), RecipeCategory.MISC, Items.IRON_INGOT, 0.7f, 200)
				.unlockedBy(getHasName(Items.DEEPSLATE_IRON_ORE), has(Items.DEEPSLATE_IRON_ORE));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeIronIngotFromDeepslateIronOreRecipe.save(recipeConsumer, "iron_ingot_from_smelting_deepslate_iron_ore"))
				.save(recipeOutput, ResourceLocation.parse("iron_ingot_from_smelting_deepslate_iron_ore"));
		
		SimpleCookingRecipeBuilder removeIronIngotFromRawIronRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.RAW_IRON), RecipeCategory.MISC, Items.IRON_INGOT, 0.7f, 200)
				.unlockedBy(getHasName(Items.RAW_IRON), has(Items.RAW_IRON));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeIronIngotFromRawIronRecipe.save(recipeConsumer, "iron_ingot_from_smelting_raw_iron"))
				.save(recipeOutput, ResourceLocation.parse("iron_ingot_from_smelting_raw_iron"));
		
		SimpleCookingRecipeBuilder removeCopperIngotFromCopperOreRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.COPPER_ORE), RecipeCategory.MISC, Items.COPPER_INGOT, 0.7f, 200)
				.unlockedBy(getHasName(Items.COPPER_ORE), has(Items.COPPER_ORE));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeCopperIngotFromCopperOreRecipe.save(recipeConsumer, "copper_ingot_from_smelting_copper_ore"))
				.save(recipeOutput, ResourceLocation.parse("copper_ingot_from_smelting_copper_ore"));
		
		SimpleCookingRecipeBuilder removeCopperIngotFromDeepslateCopperOreRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.DEEPSLATE_COPPER_ORE), RecipeCategory.MISC, Items.COPPER_INGOT, 0.7f, 200)
				.unlockedBy(getHasName(Items.DEEPSLATE_COPPER_ORE), has(Items.DEEPSLATE_COPPER_ORE));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeCopperIngotFromDeepslateCopperOreRecipe.save(recipeConsumer, "copper_ingot_from_smelting_deepslate_copper_ore"))
				.save(recipeOutput, ResourceLocation.parse("copper_ingot_from_smelting_deepslate_copper_ore"));
		
		SimpleCookingRecipeBuilder removeCopperIngotFromRawCopperRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.RAW_COPPER), RecipeCategory.MISC, Items.COPPER_INGOT, 0.7f, 200)
				.unlockedBy(getHasName(Items.RAW_COPPER), has(Items.RAW_COPPER));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeCopperIngotFromRawCopperRecipe.save(recipeConsumer, "copper_ingot_from_smelting_raw_copper"))
				.save(recipeOutput, ResourceLocation.parse("copper_ingot_from_smelting_raw_copper"));
		
		SimpleCookingRecipeBuilder removeGoldIngotFromGoldOreRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.GOLD_ORE), RecipeCategory.MISC, Items.GOLD_INGOT, 1f, 200)
				.unlockedBy(getHasName(Items.GOLD_ORE), has(Items.GOLD_ORE));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeGoldIngotFromGoldOreRecipe.save(recipeConsumer, "gold_ingot_from_smelting_gold_ore"))
				.save(recipeOutput, ResourceLocation.parse("gold_ingot_from_smelting_gold_ore"));
		
		SimpleCookingRecipeBuilder removeGoldIngotFromDeepslateGoldOreRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.DEEPSLATE_GOLD_ORE), RecipeCategory.MISC, Items.GOLD_INGOT, 1f, 200)
				.unlockedBy(getHasName(Items.DEEPSLATE_GOLD_ORE), has(Items.DEEPSLATE_GOLD_ORE));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeGoldIngotFromDeepslateGoldOreRecipe.save(recipeConsumer, "gold_ingot_from_smelting_deepslate_gold_ore"))
				.save(recipeOutput, ResourceLocation.parse("gold_ingot_from_smelting_deepslate_gold_ore"));
		
		SimpleCookingRecipeBuilder removeGoldIngotFromNetherGoldOreRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.NETHER_GOLD_ORE), RecipeCategory.MISC, Items.GOLD_INGOT, 1f, 200)
				.unlockedBy(getHasName(Items.NETHER_GOLD_ORE), has(Items.NETHER_GOLD_ORE));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeGoldIngotFromNetherGoldOreRecipe.save(recipeConsumer, "gold_ingot_from_smelting_nether_gold_ore"))
				.save(recipeOutput, ResourceLocation.parse("gold_ingot_from_smelting_nether_gold_ore"));
		
		SimpleCookingRecipeBuilder removeGoldIngotFromRawGoldRecipe = SimpleCookingRecipeBuilder.smelting(Ingredient.of(Items.RAW_GOLD), RecipeCategory.MISC, Items.GOLD_INGOT, 1f, 200)
				.unlockedBy(getHasName(Items.RAW_GOLD), has(Items.RAW_GOLD));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.METAL_AGE))
				.recipe(recipeConsumer -> removeGoldIngotFromRawGoldRecipe.save(recipeConsumer, "gold_ingot_from_smelting_raw_gold"))
				.save(recipeOutput, ResourceLocation.parse("gold_ingot_from_smelting_raw_gold"));
		
		ShapedRecipeBuilder removeWoodenSwordRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, Items.WOODEN_SWORD)
				.define('X', ItemTags.PLANKS)
				.define('S', Items.STICK)
				.pattern("X")
				.pattern("X")
				.pattern("S")
				.unlockedBy(getHasName(Items.STICK), has(Items.STICK));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleSettingCondition.STONE_AGE_REMOVE_WOODEN_TOOL_RECIPES))
				.recipe(removeWoodenSwordRecipe::save)
				.save(recipeOutput, ResourceLocation.parse(getSimpleRecipeName(Items.WOODEN_SWORD)));
		
		ShapedRecipeBuilder removeWoodenPickaxeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, Items.WOODEN_PICKAXE)
				.define('X', ItemTags.PLANKS)
				.define('S', Items.STICK)
				.pattern("XXX")
				.pattern(" S ")
				.pattern(" S ")
				.unlockedBy(getHasName(Items.STICK), has(Items.STICK));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleSettingCondition.STONE_AGE_REMOVE_WOODEN_TOOL_RECIPES))
				.recipe(removeWoodenPickaxeRecipe::save)
				.save(recipeOutput, ResourceLocation.parse(getSimpleRecipeName(Items.WOODEN_PICKAXE)));
		
		ShapedRecipeBuilder removeWoodenAxeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, Items.WOODEN_AXE)
				.define('X', ItemTags.PLANKS)
				.define('S', Items.STICK)
				.pattern("XXX")
				.pattern(" SX")
				.pattern(" S ")
				.unlockedBy(getHasName(Items.STICK), has(Items.STICK));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleSettingCondition.STONE_AGE_REMOVE_WOODEN_TOOL_RECIPES))
				.recipe(removeWoodenAxeRecipe::save)
				.save(recipeOutput, ResourceLocation.parse(getSimpleRecipeName(Items.WOODEN_AXE)));
		
		ShapedRecipeBuilder removeWoodenShovelRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, Items.WOODEN_SHOVEL)
				.define('X', ItemTags.PLANKS)
				.define('S', Items.STICK)
				.pattern("X")
				.pattern("S")
				.pattern("S")
				.unlockedBy(getHasName(Items.STICK), has(Items.STICK));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleSettingCondition.STONE_AGE_REMOVE_WOODEN_TOOL_RECIPES))
				.recipe(removeWoodenShovelRecipe::save)
				.save(recipeOutput, ResourceLocation.parse(getSimpleRecipeName(Items.WOODEN_SHOVEL)));
		
		ShapedRecipeBuilder removeWoodenHoeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, Items.WOODEN_HOE)
				.define('X', ItemTags.PLANKS)
				.define('S', Items.STICK)
				.pattern("XX")
				.pattern(" S")
				.pattern(" S")
				.unlockedBy(getHasName(Items.STICK), has(Items.STICK));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleSettingCondition.STONE_AGE_REMOVE_WOODEN_TOOL_RECIPES))
				.recipe(removeWoodenHoeRecipe::save)
				.save(recipeOutput, ResourceLocation.parse(getSimpleRecipeName(Items.WOODEN_HOE)));
		
		toolVanillaRecipes(recipeOutput, "minecraft", ItemTags.STONE_CRAFTING_MATERIALS, Items.STONE_SWORD, Items.STONE_PICKAXE, Items.STONE_AXE, Items.STONE_SHOVEL, Items.STONE_HOE);
		toolAdvancedRecipes(recipeOutput, true, Items.STONE_SWORD, ModItems.STONE_SWORD_BLADE.get(), Items.STONE_PICKAXE, ModItems.STONE_PICKAXE_HEAD.get(), Items.STONE_AXE, ModItems.STONE_AXE_HEAD.get(), Items.STONE_SHOVEL, ModItems.STONE_SHOVEL_HEAD.get(), Items.STONE_HOE, ModItems.STONE_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, "minecraft", Tags.Items.INGOTS_IRON, Items.IRON_SWORD, Items.IRON_PICKAXE, Items.IRON_AXE, Items.IRON_SHOVEL, Items.IRON_HOE);
		toolAdvancedRecipes(recipeOutput, false, Items.IRON_SWORD, ModItems.IRON_SWORD_BLADE.get(), Items.IRON_PICKAXE, ModItems.IRON_PICKAXE_HEAD.get(), Items.IRON_AXE, ModItems.IRON_AXE_HEAD.get(), Items.IRON_SHOVEL, ModItems.IRON_SHOVEL_HEAD.get(), Items.IRON_HOE, ModItems.IRON_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, "minecraft", Tags.Items.INGOTS_GOLD, Items.GOLDEN_SWORD, Items.GOLDEN_PICKAXE, Items.GOLDEN_AXE, Items.GOLDEN_SHOVEL, Items.GOLDEN_HOE);
		toolAdvancedRecipes(recipeOutput, false, Items.GOLDEN_SWORD, ModItems.GOLDEN_SWORD_BLADE.get(), Items.GOLDEN_PICKAXE, ModItems.GOLDEN_PICKAXE_HEAD.get(), Items.GOLDEN_AXE, ModItems.GOLDEN_AXE_HEAD.get(), Items.GOLDEN_SHOVEL, ModItems.GOLDEN_SHOVEL_HEAD.get(), Items.GOLDEN_HOE, ModItems.GOLDEN_HOE_HEAD.get());
		
		armorVanillaRecipes(recipeOutput, "minecraft", Tags.Items.INGOTS_IRON, Items.IRON_HELMET, Items.IRON_CHESTPLATE, Items.IRON_LEGGINGS, Items.IRON_BOOTS);
		armorAdvancedRecipes(recipeOutput, ModItems.IRON_PLATE.get(), Items.IRON_HELMET, Items.IRON_CHESTPLATE, Items.IRON_LEGGINGS, Items.IRON_BOOTS);
		
		armorVanillaRecipes(recipeOutput, "minecraft", Tags.Items.INGOTS_GOLD, Items.GOLDEN_HELMET, Items.GOLDEN_CHESTPLATE, Items.GOLDEN_LEGGINGS, Items.GOLDEN_BOOTS);
		armorAdvancedRecipes(recipeOutput, ModItems.GOLD_PLATE.get(), Items.GOLDEN_HELMET, Items.GOLDEN_CHESTPLATE, Items.GOLDEN_LEGGINGS, Items.GOLDEN_BOOTS);
		
		armorVanillaRecipes(recipeOutput, "minecraft", Tags.Items.GEMS_DIAMOND, Items.DIAMOND_HELMET, Items.DIAMOND_CHESTPLATE, Items.DIAMOND_LEGGINGS, Items.DIAMOND_BOOTS);
		armorAdvancedRecipes(recipeOutput, ModItems.DIAMOND_PLATE.get(), Items.DIAMOND_HELMET, Items.DIAMOND_CHESTPLATE, Items.DIAMOND_LEGGINGS, Items.DIAMOND_BOOTS);
		
		ShapedRecipeBuilder oldShearsRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, Items.SHEARS)
				.define('I', Tags.Items.INGOTS_IRON)
				.pattern(" I")
				.pattern("I ")
				.unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(oldShearsRecipe::save)
				.save(recipeOutput, ResourceLocation.parse("shears"));
		
		ShapedRecipeBuilder newShearsRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, Items.SHEARS)
				.define('S', Items.STICK)
				.define('L', ModItems.LEATHER_STRIP.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.pattern("I I")
				.pattern(" L ")
				.pattern("S S")
				.unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(newShearsRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "shears"));
		
		ShapedRecipeBuilder removeStickRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Items.STICK, 4)
				.define('P', ItemTags.PLANKS)
				.pattern("P")
				.pattern("P")
				.unlockedBy("has_plank", has(ItemTags.PLANKS));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_WOOD_CRAFTING_MODE_VANILLA)
				.recipe(removeStickRecipe::save)
				.save(recipeOutput, ResourceLocation.parse(getSimpleRecipeName(Items.STICK)));
		
		removeVanillaWoodRecipes(recipeOutput, ItemTags.OAK_LOGS, Blocks.OAK_PLANKS, Blocks.OAK_SLAB);
		removeVanillaWoodRecipes(recipeOutput, ItemTags.SPRUCE_LOGS, Blocks.SPRUCE_PLANKS, Blocks.SPRUCE_SLAB);
		removeVanillaWoodRecipes(recipeOutput, ItemTags.BIRCH_LOGS, Blocks.BIRCH_PLANKS, Blocks.BIRCH_SLAB);
		removeVanillaWoodRecipes(recipeOutput, ItemTags.JUNGLE_LOGS, Blocks.JUNGLE_PLANKS, Blocks.JUNGLE_SLAB);
		removeVanillaWoodRecipes(recipeOutput, ItemTags.ACACIA_LOGS, Blocks.ACACIA_PLANKS, Blocks.ACACIA_SLAB);
		removeVanillaWoodRecipes(recipeOutput, ItemTags.DARK_OAK_LOGS, Blocks.DARK_OAK_PLANKS, Blocks.DARK_OAK_SLAB);
		removeVanillaWoodRecipes(recipeOutput, ItemTags.MANGROVE_LOGS, Blocks.MANGROVE_PLANKS, Blocks.MANGROVE_SLAB);
		removeVanillaWoodRecipes(recipeOutput, ItemTags.CHERRY_LOGS, Blocks.CHERRY_PLANKS, Blocks.CHERRY_SLAB);
		removeVanillaWoodRecipes(recipeOutput, ItemTags.CRIMSON_STEMS, Blocks.CRIMSON_PLANKS, Blocks.CRIMSON_SLAB);
		removeVanillaWoodRecipes(recipeOutput, ItemTags.WARPED_STEMS, Blocks.WARPED_PLANKS, Blocks.WARPED_SLAB);
	}
	
	@Override
	protected void generateCraftingRecipes(RecipeOutput recipeOutput)
	{
		nineBlockStorageRecipes(recipeOutput, ModItems.UNIM_INGOT.get(), ModBlocks.UNIM_BLOCK.get(), "unim_block", "unim_block", "unim_ingot_from_unim_block", "unim_ingot");
		
		nineBlockStorageRecipes(recipeOutput, ModItems.RUBY.get(), ModBlocks.RUBY_BLOCK.get(), "ruby_block", "ruby_block", "ruby_from_ruby_block", "ruby");
		
		nineBlockStorageRecipes(recipeOutput, ModItems.ASCABIT_INGOT.get(), ModBlocks.ASCABIT_BLOCK.get(), "ascabit_block", "ascabit_block", "ascabit_ingot_from_ascabit_block", "ascabit_ingot");
		nineBlockStorageRecipes(recipeOutput, ModItems.RAW_ASCABIT.get(), ModBlocks.RAW_ASCABIT_BLOCK.get(), "raw_ascabit_block", "raw_ascabit_block", "raw_ascabit_from_raw_ascabit_block", "raw_ascabit");
		
		nineBlockStorageRecipes(recipeOutput, ModItems.TIN_INGOT.get(), ModBlocks.UNIM_BLOCK.get(), "tin_block", "tin_block", "tin_ingot_from_tin_block", "tin_ingot");
		nineBlockStorageRecipes(recipeOutput, ModItems.RAW_TIN.get(), ModBlocks.RAW_TIN_BLOCK.get(), "raw_tin_block", "raw_tin_block", "raw_tin_from_raw_tin_block", "raw_tin");
		
		nineBlockStorageRecipes(recipeOutput, ModItems.TITANIUM_INGOT.get(), ModBlocks.TITANIUM_BLOCK.get(), "titanium_block", "titanium_block", "titanium_ingot_from_titanium_block", "titanium_ingot");
		nineBlockStorageRecipes(recipeOutput, ModItems.RAW_TITANIUM.get(), ModBlocks.RAW_TITANIUM_BLOCK.get(), "raw_titanium_block", "raw_titanium_block", "raw_titanium_from_raw_titanium_block", "raw_titanium");
		
		nineBlockStorageRecipes(recipeOutput, ModItems.ZINC_INGOT.get(), ModBlocks.ZINC_BLOCK.get(), "zinc_block", "zinc_block", "zinc_ingot_from_zinc_block", "zinc_ingot");
		nineBlockStorageRecipes(recipeOutput, ModItems.RAW_ZINC.get(), ModBlocks.RAW_ZINC_BLOCK.get(), "raw_zinc_block", "raw_zinc_block", "raw_zinc_from_raw_zinc_block", "raw_zinc");
		
		nineBlockStorageRecipes(recipeOutput, ModItems.STEEL_INGOT.get(), ModBlocks.STEEL_BLOCK.get(), "steel_block", "steel_block", "steel_ingot_from_steel_block", "steel_ingot");
		
		nineBlockStorageRecipes(recipeOutput, ModItems.BRONZE_INGOT.get(), ModBlocks.BRONZE_BLOCK.get(), "bronze_block", "bronze_block", "bronze_ingot_from_bronze_block", "bronze_ingot");
		
		nineBlockStorageRecipes(recipeOutput, ModItems.BRASS_INGOT.get(), ModBlocks.BRASS_BLOCK.get(), "brass_block", "brass_block", "brass_ingot_from_brass_block", "brass_ingot");
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.UNIM_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(ModItemTags.INGOTS_UNIM)
				.unlockedBy("has_unim_ingot", has(ModItemTags.INGOTS_UNIM))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.RUBY_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(ModItems.IRON_PLATE.get())
				.requires(ModItemTags.GEMS_RUBY)
				.unlockedBy("has_ruby", has(ModItemTags.GEMS_RUBY))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.ASCABIT_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(ModItemTags.INGOTS_ASCABIT)
				.unlockedBy("has_ascabit_ingot", has(ModItemTags.INGOTS_ASCABIT))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.TIN_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(ModItemTags.INGOTS_TIN)
				.unlockedBy("has_tin_ingot", has(ModItemTags.INGOTS_TIN))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.TITANIUM_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(ModItemTags.INGOTS_TITANIUM)
				.unlockedBy("has_titanium_ingot", has(ModItemTags.INGOTS_TITANIUM))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.ZINC_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(ModItemTags.INGOTS_ZINC)
				.unlockedBy("has_zinc_ingot", has(ModItemTags.INGOTS_ZINC))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.STEEL_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(ModItemTags.INGOTS_STEEL)
				.unlockedBy("has_steel_ingot", has(ModItemTags.INGOTS_STEEL))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.BRONZE_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(ModItemTags.INGOTS_BRONZE)
				.unlockedBy("has_bronze_ingot", has(ModItemTags.INGOTS_BRONZE))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.IRON_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(Tags.Items.INGOTS_IRON)
				.unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.COPPER_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(Tags.Items.INGOTS_COPPER)
				.unlockedBy("has_copper_ingot", has(Tags.Items.INGOTS_COPPER))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.GOLD_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(Tags.Items.INGOTS_GOLD)
				.unlockedBy("has_gold_ingot", has(Tags.Items.INGOTS_GOLD))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.DIAMOND_PLATE.get())
				.requires(ModItemTags.TOOLS_HAMMERS)
				.requires(ModItems.IRON_PLATE.get())
				.requires(Tags.Items.GEMS_DIAMOND)
				.unlockedBy("has_diamond", has(Tags.Items.GEMS_DIAMOND))
				.save(recipeOutput);
		
		armorVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItems.UNIM_PLATE.get(), ModItems.UNIM_HELMET.get(), ModItems.UNIM_CHESTPLATE.get(), ModItems.UNIM_LEGGINGS.get(), ModItems.UNIM_BOOTS.get());
		armorAdvancedRecipes(recipeOutput, ModItems.UNIM_PLATE.get(), ModItems.UNIM_HELMET.get(), ModItems.UNIM_CHESTPLATE.get(), ModItems.UNIM_LEGGINGS.get(), ModItems.UNIM_BOOTS.get());
		
		armorVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItems.RUBY_PLATE.get(), ModItems.RUBY_HELMET.get(), ModItems.RUBY_CHESTPLATE.get(), ModItems.RUBY_LEGGINGS.get(), ModItems.RUBY_BOOTS.get());
		armorAdvancedRecipes(recipeOutput, ModItems.RUBY_PLATE.get(), ModItems.RUBY_HELMET.get(), ModItems.RUBY_CHESTPLATE.get(), ModItems.RUBY_LEGGINGS.get(), ModItems.RUBY_BOOTS.get());
		
		armorVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItems.ASCABIT_PLATE.get(), ModItems.ASCABIT_HELMET.get(), ModItems.ASCABIT_CHESTPLATE.get(), ModItems.ASCABIT_LEGGINGS.get(), ModItems.ASCABIT_BOOTS.get());
		armorAdvancedRecipes(recipeOutput, ModItems.ASCABIT_PLATE.get(), ModItems.ASCABIT_HELMET.get(), ModItems.ASCABIT_CHESTPLATE.get(), ModItems.ASCABIT_LEGGINGS.get(), ModItems.ASCABIT_BOOTS.get());
		
		armorVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItems.COPPER_PLATE.get(), ModItems.COPPER_HELMET.get(), ModItems.COPPER_CHESTPLATE.get(), ModItems.COPPER_LEGGINGS.get(), ModItems.COPPER_BOOTS.get());
		armorAdvancedRecipes(recipeOutput, ModItems.COPPER_PLATE.get(), ModItems.COPPER_HELMET.get(), ModItems.COPPER_CHESTPLATE.get(), ModItems.COPPER_LEGGINGS.get(), ModItems.COPPER_BOOTS.get());
		
		armorVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItems.TIN_PLATE.get(), ModItems.TIN_HELMET.get(), ModItems.TIN_CHESTPLATE.get(), ModItems.TIN_LEGGINGS.get(), ModItems.TIN_BOOTS.get());
		armorAdvancedRecipes(recipeOutput, ModItems.TIN_PLATE.get(), ModItems.TIN_HELMET.get(), ModItems.TIN_CHESTPLATE.get(), ModItems.TIN_LEGGINGS.get(), ModItems.TIN_BOOTS.get());
		
		armorVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItems.TITANIUM_PLATE.get(), ModItems.TITANIUM_HELMET.get(), ModItems.TITANIUM_CHESTPLATE.get(), ModItems.TITANIUM_LEGGINGS.get(), ModItems.TITANIUM_BOOTS.get());
		armorAdvancedRecipes(recipeOutput, ModItems.TITANIUM_PLATE.get(), ModItems.TITANIUM_HELMET.get(), ModItems.TITANIUM_CHESTPLATE.get(), ModItems.TITANIUM_LEGGINGS.get(), ModItems.TITANIUM_BOOTS.get());
		
		armorVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItems.ZINC_PLATE.get(), ModItems.ZINC_HELMET.get(), ModItems.ZINC_CHESTPLATE.get(), ModItems.ZINC_LEGGINGS.get(), ModItems.ZINC_BOOTS.get());
		armorAdvancedRecipes(recipeOutput, ModItems.ZINC_PLATE.get(), ModItems.ZINC_HELMET.get(), ModItems.ZINC_CHESTPLATE.get(), ModItems.ZINC_LEGGINGS.get(), ModItems.ZINC_BOOTS.get());
		
		armorVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItems.BRONZE_PLATE.get(), ModItems.BRONZE_HELMET.get(), ModItems.BRONZE_CHESTPLATE.get(), ModItems.BRONZE_LEGGINGS.get(), ModItems.BRONZE_BOOTS.get());
		armorAdvancedRecipes(recipeOutput, ModItems.BRONZE_PLATE.get(), ModItems.BRONZE_HELMET.get(), ModItems.BRONZE_CHESTPLATE.get(), ModItems.BRONZE_LEGGINGS.get(), ModItems.BRONZE_BOOTS.get());
		
		armorVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItems.STEEL_PLATE.get(), ModItems.STEEL_HELMET.get(), ModItems.STEEL_CHESTPLATE.get(), ModItems.STEEL_LEGGINGS.get(), ModItems.STEEL_BOOTS.get());
		armorAdvancedRecipes(recipeOutput, ModItems.STEEL_PLATE.get(), ModItems.STEEL_HELMET.get(), ModItems.STEEL_CHESTPLATE.get(), ModItems.STEEL_LEGGINGS.get(), ModItems.STEEL_BOOTS.get());
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModItems.SMALL_BACKPACK.get())
				.define('L', Items.LEATHER)
				.define('C', Blocks.CHEST)
				.define('S', Items.STRING)
				.pattern("SLS")
				.pattern("LCL")
				.pattern("LLL")
				.unlockedBy(getHasName(Items.LEATHER), has(Items.LEATHER))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModItems.LARGE_BACKPACK.get())
				.define('L', Items.LEATHER)
				.define('B', ModItems.SMALL_BACKPACK.get())
				.define('S', ModItems.LEATHER_STRIP.get())
				.pattern("SLS")
				.pattern("LBL")
				.pattern("LLL")
				.unlockedBy(getHasName(Items.LEATHER), has(Items.LEATHER))
				.unlockedBy(getHasName(ModItems.SMALL_BACKPACK.get()), has(ModItems.SMALL_BACKPACK.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModItems.ENDER_BACKPACK.get())
				.define('L', Items.LEATHER)
				.define('B', ModItems.SMALL_BACKPACK.get())
				.define('S', ModItems.LEATHER_STRIP.get())
				.define('E', Items.ENDER_EYE)
				.pattern("SES")
				.pattern("EBE")
				.pattern("LLL")
				.unlockedBy(getHasName(Items.LEATHER), has(Items.LEATHER))
				.unlockedBy(getHasName(Items.ENDER_EYE), has(Items.ENDER_EYE))
				.unlockedBy(getHasName(ModItems.SMALL_BACKPACK.get()), has(ModItems.SMALL_BACKPACK.get()))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.PREPARED_HIDE.get())
				.requires(ModItemTags.TOOLS_KNIVES)
				.requires(ModItems.ANIMAL_HIDE.get())
				.requires(ModItems.TREE_BARK.get())
				.unlockedBy(getHasName(ModItems.ANIMAL_HIDE.get()), has(ModItems.ANIMAL_HIDE.get()))
				.unlockedBy(getHasName(ModItems.TREE_BARK.get()), has(ModItems.TREE_BARK.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Blocks.TORCH, 2)
				.define('F', ModItems.ANIMAL_FAT.get())
				.define('S', Items.STICK)
				.pattern("F")
				.pattern("S")
				.unlockedBy(getHasName(ModItems.ANIMAL_FAT.get()), has(ModItems.ANIMAL_FAT.get()))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "torch_with_animal_fat"));
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.LEATHER_STRIP.get(), 2)
				.requires(ModItemTags.TOOLS_KNIVES)
				.requires(Items.LEATHER)
				.unlockedBy(getHasName(Items.LEATHER), has(Items.LEATHER))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.DOUGH.get(), 2)
				.requires(Items.WHEAT)
				.requires(Items.WHEAT)
				.requires(ModItemTags.CONTAINERS_WATER)
				.unlockedBy(getHasName(Items.WHEAT), has(Items.WHEAT))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.RAW_BACON.get(), 4)
				.requires(Items.PORKCHOP)
				.requires(ModItemTags.TOOLS_KNIVES)
				.unlockedBy(getHasName(Items.PORKCHOP), has(Items.PORKCHOP))
				.save(recipeOutput);
		
		foodCooking(recipeOutput, ModItems.RAW_BACON.get(), ModItems.COOKED_BACON.get(), 0.35f, true);
		foodCooking(recipeOutput, Items.EGG, ModItems.FRIED_EGG.get(), 0.35f, false);
		foodCooking(recipeOutput, Items.MILK_BUCKET, ModItems.CHEESE.get(), 0.35f, false);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, ModItems.PIZZA.get(), 3)
				.define('C', ModItems.CHEESE.get())
				.define('T', ModItems.TOMATO.get())
				.define('D', ModItems.DOUGH.get())
				.pattern("CTC")
				.pattern("DDD")
				.unlockedBy(getHasName(ModItems.DOUGH.get()), has(ModItems.DOUGH.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, ModItems.CHOCOLATE.get(), 2)
				.define('C', Items.COCOA_BEANS)
				.define('S', Items.SUGAR)
				.pattern("CSC")
				.unlockedBy(getHasName(Items.COCOA_BEANS), has(Items.COCOA_BEANS))
				.save(recipeOutput);
		
		foodCooking(recipeOutput, ModItems.CORN.get(), ModItems.POPCORN.get(), 0.35f, false);
		foodCooking(recipeOutput, ModItems.ONION.get(), ModItems.ONION_RINGS.get(), 0.35f, false);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, ModItems.SAUSAGE.get(), 3)
				.define('P', Items.PORKCHOP)
				.pattern(" P")
				.pattern("P ")
				.pattern(" P")
				.unlockedBy(getHasName(Items.PORKCHOP), has(Items.PORKCHOP))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.HOT_DOG.get())
				.requires(Items.BREAD)
				.requires(ModItems.SAUSAGE.get())
				.unlockedBy(getHasName(ModItems.SAUSAGE.get()), has(ModItems.SAUSAGE.get()))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.GARDEN_SALAD.get())
				.requires(Items.BOWL)
				.requires(ModItems.LETTUCE.get())
				.requires(ModItems.TOMATO.get())
				.requires(Items.CARROT)
				.unlockedBy(getHasName(ModItems.LETTUCE.get()), has(ModItems.LETTUCE.get()))
				.unlockedBy(getHasName(ModItems.TOMATO.get()), has(ModItems.TOMATO.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, ModItems.HAMBURGER.get())
				.define('B', Items.BREAD)
				.define('C', Items.COOKED_BEEF)
				.define('T', ModItems.TOMATO.get())
				.define('L', ModItems.LETTUCE.get())
				.pattern(" B ")
				.pattern("TCL")
				.pattern(" B ")
				.unlockedBy(getHasName(Items.COOKED_BEEF), has(Items.COOKED_BEEF))
				.unlockedBy(getHasName(ModItems.TOMATO.get()), has(ModItems.TOMATO.get()))
				.unlockedBy(getHasName(ModItems.LETTUCE.get()), has(ModItems.LETTUCE.get()))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.CHEESEBURGER.get())
				.requires(ModItems.HAMBURGER.get())
				.requires(ModItems.CHEESE.get())
				.unlockedBy(getHasName(ModItems.HAMBURGER.get()), has(ModItems.HAMBURGER.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, ModItems.SUSHI.get(), 3)
				.define('D', Items.DRIED_KELP)
				.define('R', ModItems.RICE.get())
				.define('F', Items.SALMON)
				.pattern("DDD")
				.pattern("RFR")
				.pattern("DDD")
				.unlockedBy(getHasName(Items.DRIED_KELP), has(Items.DRIED_KELP))
				.unlockedBy(getHasName(ModItems.RICE.get()), has(ModItems.RICE.get()))
				.unlockedBy(getHasName(Items.SALMON), has(Items.SALMON))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.APPLE_PIE.get())
				.requires(Items.APPLE)
				.requires(Items.SUGAR)
				.requires(Items.EGG)
				.unlockedBy(getHasName(Items.APPLE), has(Items.APPLE))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.STRAWBERRY_PIE.get())
				.requires(ModItems.STRAWBERRY.get())
				.requires(Items.SUGAR)
				.requires(Items.EGG)
				.unlockedBy(getHasName(ModItems.STRAWBERRY.get()), has(ModItems.STRAWBERRY.get()))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.VEGETABLE_SOUP.get())
				.requires(Items.BOWL)
				.requires(Items.POTATO)
				.requires(Items.CARROT)
				.unlockedBy(getHasName(Items.BOWL), has(Items.BOWL))
				.unlockedBy(getHasName(Items.POTATO), has(Items.POTATO))
				.unlockedBy(getHasName(Items.CARROT), has(Items.CARROT))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.PUMPKIN_SOUP.get())
				.requires(Items.BOWL)
				.requires(Items.PUMPKIN)
				.unlockedBy(getHasName(Items.BOWL), has(Items.BOWL))
				.unlockedBy(getHasName(Items.PUMPKIN), has(Items.PUMPKIN))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.CACTUS_SOUP.get())
				.requires(Items.BOWL)
				.requires(Items.CACTUS)
				.unlockedBy(getHasName(Items.BOWL), has(Items.BOWL))
				.unlockedBy(getHasName(Items.CACTUS), has(Items.CACTUS))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, ModItems.FISH_SOUP.get())
				.requires(Items.BOWL)
				.requires(ItemTags.FISHES)
				.unlockedBy(getHasName(Items.BOWL), has(Items.BOWL))
				.unlockedBy("has_fishes", has(ItemTags.FISHES))
				.save(recipeOutput);
		
		ShapedRecipeBuilder vanillaChocolateCakeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, ModBlocks.CHOCOLATE_CAKE.get())
				.define('C', ModItems.CHOCOLATE.get())
				.define('S', Items.SUGAR)
				.define('W', Items.WHEAT)
				.define('E', Items.EGG)
				.pattern("CCC")
				.pattern("SES")
				.pattern("WWW")
				.unlockedBy(getHasName(ModItems.CHOCOLATE.get()), has(ModItems.CHOCOLATE.get()));
		ConditionalRecipe.builder().condition(new NotCondition(ModuleLoadedCondition.STONE_AGE))
				.recipe(vanillaChocolateCakeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "chocolate_cake"));
		
		ShapedRecipeBuilder advancedChocolateCakeRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.FOOD, ModBlocks.CHOCOLATE_CAKE.get())
				.define('C', ModItems.CHOCOLATE.get())
				.define('S', Items.SUGAR)
				.define('D', ModItems.DOUGH.get())
				.define('E', Items.EGG)
				.pattern("CCC")
				.pattern("SES")
				.pattern("DDD")
				.unlockedBy(getHasName(ModItems.CHOCOLATE.get()), has(ModItems.CHOCOLATE.get()));
		ConditionalRecipe.builder().condition(ModuleLoadedCondition.STONE_AGE)
				.recipe(advancedChocolateCakeRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "chocolate_cake_with_dough"));
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.CEMENT_MIXTURE.get(), 6)
				.requires(ModBlocks.LIMESTONE.get())
				.requires(ModBlocks.LIMESTONE.get())
				.requires(Blocks.SAND)
				.requires(Items.CLAY_BALL)
				.requires(Items.CLAY_BALL)
				.requires(ModItemTags.CONTAINERS_WATER)
				.requires(Items.BOWL)
				.unlockedBy(getHasName(Items.CLAY_BALL), has(Items.CLAY_BALL))
				.save(recipeOutput);
		
		ShapedRecipeBuilder vanillaFlintHatchetRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ModItems.FLINT_HATCHET.get())
				.define('S', Items.STICK)
				.define('F', Items.FLINT)
				.pattern("FF")
				.pattern("FS")
				.pattern(" S")
				.unlockedBy(getHasName(Items.FLINT), has(Items.FLINT));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_VANILLA)
				.recipe(vanillaFlintHatchetRecipe::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "flint_hatchet"));
		
		ShapedRecipeBuilder flintHatchetHead = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ModItems.FLINT_HATCHET_HEAD.get())
				.define('F', Items.FLINT)
				.pattern("FF")
				.pattern(" F")
				.unlockedBy(getHasName(Items.FLINT), has(Items.FLINT));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(flintHatchetHead::save)
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "flint_hatchet_head"));
		
		ShapedRecipeBuilder advancedFlintHatchetRecipe = ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ModItems.FLINT_HATCHET.get())
				.define('S', Items.STICK)
				.define('F', ModItems.PLANT_FIBER.get())
				.define('H', ModItems.FLINT_HATCHET_HEAD.get())
				.pattern("HF")
				.pattern(" S")
				.unlockedBy(getHasName(Items.FLINT), has(Items.FLINT));
		ConditionalRecipe.builder().condition(ModuleSettingCondition.STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED)
				.recipe(recipeConsumer -> advancedFlintHatchetRecipe.save(recipeConsumer, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "flint_hatchet_with_tool_head")))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "flint_hatchet_with_tool_head"));
		
		toolHeadRecipes(recipeOutput, ModItemTags.ROCKS, ModItems.STONE_SWORD_BLADE.get(), ModItems.STONE_PICKAXE_HEAD.get(), ModItems.STONE_AXE_HEAD.get(), ModItems.STONE_SHOVEL_HEAD.get(), ModItems.STONE_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, Tags.Items.INGOTS_IRON, ModItems.IRON_SWORD_BLADE.get(), ModItems.IRON_PICKAXE_HEAD.get(), ModItems.IRON_AXE_HEAD.get(), ModItems.IRON_SHOVEL_HEAD.get(), ModItems.IRON_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, Tags.Items.INGOTS_GOLD, ModItems.GOLDEN_SWORD_BLADE.get(), ModItems.GOLDEN_PICKAXE_HEAD.get(), ModItems.GOLDEN_AXE_HEAD.get(), ModItems.GOLDEN_SHOVEL_HEAD.get(), ModItems.GOLDEN_HOE_HEAD.get());
		
		toolHeadRecipes(recipeOutput, ModItemTags.INGOTS_UNIM, ModItems.UNIM_SWORD_BLADE.get(), ModItems.UNIM_PICKAXE_HEAD.get(), ModItems.UNIM_AXE_HEAD.get(), ModItems.UNIM_SHOVEL_HEAD.get(), ModItems.UNIM_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, ModItemTags.GEMS_RUBY, ModItems.RUBY_SWORD_BLADE.get(), ModItems.RUBY_PICKAXE_HEAD.get(), ModItems.RUBY_AXE_HEAD.get(), ModItems.RUBY_SHOVEL_HEAD.get(), ModItems.RUBY_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, ModItemTags.INGOTS_ASCABIT, ModItems.ASCABIT_SWORD_BLADE.get(), ModItems.ASCABIT_PICKAXE_HEAD.get(), ModItems.ASCABIT_AXE_HEAD.get(), ModItems.ASCABIT_SHOVEL_HEAD.get(), ModItems.ASCABIT_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, Tags.Items.INGOTS_COPPER, ModItems.COPPER_SWORD_BLADE.get(), ModItems.COPPER_PICKAXE_HEAD.get(), ModItems.COPPER_AXE_HEAD.get(), ModItems.COPPER_SHOVEL_HEAD.get(), ModItems.COPPER_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, ModItemTags.INGOTS_TIN, ModItems.TIN_SWORD_BLADE.get(), ModItems.TIN_PICKAXE_HEAD.get(), ModItems.TIN_AXE_HEAD.get(), ModItems.TIN_SHOVEL_HEAD.get(), ModItems.TIN_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, ModItemTags.INGOTS_TITANIUM, ModItems.TITANIUM_SWORD_BLADE.get(), ModItems.TITANIUM_PICKAXE_HEAD.get(), ModItems.TITANIUM_AXE_HEAD.get(), ModItems.TITANIUM_SHOVEL_HEAD.get(), ModItems.TITANIUM_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, ModItemTags.INGOTS_ZINC, ModItems.ZINC_SWORD_BLADE.get(), ModItems.ZINC_PICKAXE_HEAD.get(), ModItems.ZINC_AXE_HEAD.get(), ModItems.ZINC_SHOVEL_HEAD.get(), ModItems.ZINC_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, ModItemTags.INGOTS_STEEL, ModItems.STEEL_SWORD_BLADE.get(), ModItems.STEEL_PICKAXE_HEAD.get(), ModItems.STEEL_AXE_HEAD.get(), ModItems.STEEL_SHOVEL_HEAD.get(), ModItems.STEEL_HOE_HEAD.get());
		toolHeadRecipes(recipeOutput, ModItemTags.INGOTS_BRONZE, ModItems.BRONZE_SWORD_BLADE.get(), ModItems.BRONZE_PICKAXE_HEAD.get(), ModItems.BRONZE_AXE_HEAD.get(), ModItems.BRONZE_SHOVEL_HEAD.get(), ModItems.BRONZE_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItemTags.INGOTS_UNIM, ModItems.UNIM_SWORD.get(), ModItems.UNIM_PICKAXE.get(), ModItems.UNIM_AXE.get(), ModItems.UNIM_SHOVEL.get(), ModItems.UNIM_HOE.get());
		toolAdvancedRecipes(recipeOutput, false, ModItems.UNIM_SWORD.get(), ModItems.UNIM_SWORD_BLADE.get(), ModItems.UNIM_PICKAXE.get(), ModItems.UNIM_PICKAXE_HEAD.get(), ModItems.UNIM_AXE.get(), ModItems.UNIM_AXE_HEAD.get(), ModItems.UNIM_SHOVEL.get(), ModItems.UNIM_SHOVEL_HEAD.get(), ModItems.UNIM_HOE.get(), ModItems.UNIM_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItemTags.GEMS_RUBY, ModItems.RUBY_SWORD.get(), ModItems.RUBY_PICKAXE.get(), ModItems.RUBY_AXE.get(), ModItems.RUBY_SHOVEL.get(), ModItems.RUBY_HOE.get());
		toolAdvancedRecipes(recipeOutput, false, ModItems.RUBY_SWORD.get(), ModItems.RUBY_SWORD_BLADE.get(), ModItems.RUBY_PICKAXE.get(), ModItems.RUBY_PICKAXE_HEAD.get(), ModItems.RUBY_AXE.get(), ModItems.RUBY_AXE_HEAD.get(), ModItems.RUBY_SHOVEL.get(), ModItems.RUBY_SHOVEL_HEAD.get(), ModItems.RUBY_HOE.get(), ModItems.RUBY_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItemTags.INGOTS_ASCABIT, ModItems.ASCABIT_SWORD.get(), ModItems.ASCABIT_PICKAXE.get(), ModItems.ASCABIT_AXE.get(), ModItems.ASCABIT_SHOVEL.get(), ModItems.ASCABIT_HOE.get());
		toolAdvancedRecipes(recipeOutput, false, ModItems.ASCABIT_SWORD.get(), ModItems.ASCABIT_SWORD_BLADE.get(), ModItems.ASCABIT_PICKAXE.get(), ModItems.ASCABIT_PICKAXE_HEAD.get(), ModItems.ASCABIT_AXE.get(), ModItems.ASCABIT_AXE_HEAD.get(), ModItems.ASCABIT_SHOVEL.get(), ModItems.ASCABIT_SHOVEL_HEAD.get(), ModItems.ASCABIT_HOE.get(), ModItems.ASCABIT_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, Tags.Items.INGOTS_COPPER, ModItems.COPPER_SWORD.get(), ModItems.COPPER_PICKAXE.get(), ModItems.COPPER_AXE.get(), ModItems.COPPER_SHOVEL.get(), ModItems.COPPER_HOE.get());
		toolAdvancedRecipes(recipeOutput, false, ModItems.COPPER_SWORD.get(), ModItems.COPPER_SWORD_BLADE.get(), ModItems.COPPER_PICKAXE.get(), ModItems.COPPER_PICKAXE_HEAD.get(), ModItems.COPPER_AXE.get(), ModItems.COPPER_AXE_HEAD.get(), ModItems.COPPER_SHOVEL.get(), ModItems.COPPER_SHOVEL_HEAD.get(), ModItems.COPPER_HOE.get(), ModItems.COPPER_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItemTags.INGOTS_TIN, ModItems.TIN_SWORD.get(), ModItems.TIN_PICKAXE.get(), ModItems.TIN_AXE.get(), ModItems.TIN_SHOVEL.get(), ModItems.TIN_HOE.get());
		toolAdvancedRecipes(recipeOutput, false, ModItems.TIN_SWORD.get(), ModItems.TIN_SWORD_BLADE.get(), ModItems.TIN_PICKAXE.get(), ModItems.TIN_PICKAXE_HEAD.get(), ModItems.TIN_AXE.get(), ModItems.TIN_AXE_HEAD.get(), ModItems.TIN_SHOVEL.get(), ModItems.TIN_SHOVEL_HEAD.get(), ModItems.TIN_HOE.get(), ModItems.TIN_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItemTags.INGOTS_TITANIUM, ModItems.TITANIUM_SWORD.get(), ModItems.TITANIUM_PICKAXE.get(), ModItems.TITANIUM_AXE.get(), ModItems.TITANIUM_SHOVEL.get(), ModItems.TITANIUM_HOE.get());
		toolAdvancedRecipes(recipeOutput, false, ModItems.TITANIUM_SWORD.get(), ModItems.TITANIUM_SWORD_BLADE.get(), ModItems.TITANIUM_PICKAXE.get(), ModItems.TITANIUM_PICKAXE_HEAD.get(), ModItems.TITANIUM_AXE.get(), ModItems.TITANIUM_AXE_HEAD.get(), ModItems.TITANIUM_SHOVEL.get(), ModItems.TITANIUM_SHOVEL_HEAD.get(), ModItems.TITANIUM_HOE.get(), ModItems.TITANIUM_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItemTags.INGOTS_ZINC, ModItems.ZINC_SWORD.get(), ModItems.ZINC_PICKAXE.get(), ModItems.ZINC_AXE.get(), ModItems.ZINC_SHOVEL.get(), ModItems.ZINC_HOE.get());
		toolAdvancedRecipes(recipeOutput, false, ModItems.ZINC_SWORD.get(), ModItems.ZINC_SWORD_BLADE.get(), ModItems.ZINC_PICKAXE.get(), ModItems.ZINC_PICKAXE_HEAD.get(), ModItems.ZINC_AXE.get(), ModItems.ZINC_AXE_HEAD.get(), ModItems.ZINC_SHOVEL.get(), ModItems.ZINC_SHOVEL_HEAD.get(), ModItems.ZINC_HOE.get(), ModItems.ZINC_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItemTags.INGOTS_STEEL, ModItems.STEEL_SWORD.get(), ModItems.STEEL_PICKAXE.get(), ModItems.STEEL_AXE.get(), ModItems.STEEL_SHOVEL.get(), ModItems.STEEL_HOE.get());
		toolAdvancedRecipes(recipeOutput, false, ModItems.STEEL_SWORD.get(), ModItems.STEEL_SWORD_BLADE.get(), ModItems.STEEL_PICKAXE.get(), ModItems.STEEL_PICKAXE_HEAD.get(), ModItems.STEEL_AXE.get(), ModItems.STEEL_AXE_HEAD.get(), ModItems.STEEL_SHOVEL.get(), ModItems.STEEL_SHOVEL_HEAD.get(), ModItems.STEEL_HOE.get(), ModItems.STEEL_HOE_HEAD.get());
		
		toolVanillaRecipes(recipeOutput, MinecraftContentExpansion.MODID, ModItemTags.INGOTS_BRONZE, ModItems.BRONZE_SWORD.get(), ModItems.BRONZE_PICKAXE.get(), ModItems.BRONZE_AXE.get(), ModItems.BRONZE_SHOVEL.get(), ModItems.BRONZE_HOE.get());
		toolAdvancedRecipes(recipeOutput, false, ModItems.BRONZE_SWORD.get(), ModItems.BRONZE_SWORD_BLADE.get(), ModItems.BRONZE_PICKAXE.get(), ModItems.BRONZE_PICKAXE_HEAD.get(), ModItems.BRONZE_AXE.get(), ModItems.BRONZE_AXE_HEAD.get(), ModItems.BRONZE_SHOVEL.get(), ModItems.BRONZE_SHOVEL_HEAD.get(), ModItems.BRONZE_HOE.get(), ModItems.BRONZE_HOE_HEAD.get());
		
		ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ModItems.FLINT_SHEARS.get())
				.define('S', Items.STICK)
				.define('P', ModItems.PLANT_FIBER.get())
				.define('F', Items.FLINT)
				.pattern("F F")
				.pattern(" P ")
				.pattern("S S")
				.unlockedBy(getHasName(Items.FLINT), has(Items.FLINT))
				.save(recipeOutput);
		
		knifeRecipes(recipeOutput, true, Items.FLINT, ModItems.FLINT_KNIFE.get(), ModItems.FLINT_KNIFE_BLADE.get());
		knifeRecipes(recipeOutput, true, ModItemTags.ROCKS, ModItems.STONE_KNIFE.get(), ModItems.STONE_KNIFE_BLADE.get());
		knifeRecipes(recipeOutput, false, Tags.Items.INGOTS_IRON, ModItems.IRON_KNIFE.get(), ModItems.IRON_KNIFE_BLADE.get());
		knifeRecipes(recipeOutput, false, ModItemTags.INGOTS_UNIM, ModItems.UNIM_KNIFE.get(), ModItems.UNIM_KNIFE_BLADE.get());
		knifeRecipes(recipeOutput, false, Tags.Items.GEMS_DIAMOND, ModItems.DIAMOND_KNIFE.get(), ModItems.DIAMOND_KNIFE_BLADE.get());
		
		hammerRecipes(recipeOutput, Tags.Items.INGOTS_IRON, ModItems.IRON_HAMMER.get(), ModItems.IRON_HAMMER_HEAD.get());
		hammerRecipes(recipeOutput, ModItemTags.INGOTS_BRONZE, ModItems.BRONZE_HAMMER.get(), ModItems.BRONZE_HAMMER_HEAD.get());
		hammerRecipes(recipeOutput, ModItemTags.INGOTS_STEEL, ModItems.STEEL_HAMMER.get(), ModItems.STEEL_HAMMER_HEAD.get());
		hammerRecipes(recipeOutput, Tags.Items.GEMS_DIAMOND, ModItems.DIAMOND_HAMMER.get(), ModItems.DIAMOND_HAMMER_HEAD.get());
		
		ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ModItems.UNFIRED_CLAY_BUCKET.get())
				.define('C', Items.CLAY_BALL)
				.pattern("C C")
				.pattern(" C ")
				.unlockedBy(getHasName(Items.CLAY_BALL), has(Items.CLAY_BALL))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ModItems.DRILL_BASE.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('P', ModItems.PORTABLE_BATTERY.get())
				.define('R', ModItems.REDSTONE_BLEND.get())
				.pattern("RI ")
				.pattern("IPI")
				.pattern(" II")
				.unlockedBy(getHasName(ModItems.PORTABLE_BATTERY.get()), has(ModItems.PORTABLE_BATTERY.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ModItems.STEEL_DRILL_HEAD.get())
				.define('I', ModItemTags.INGOTS_STEEL)
				.define('B', Tags.Items.STORAGE_BLOCKS_IRON)
				.define('R', ModItems.REDSTONE_BLEND.get())
				.pattern("II ")
				.pattern("IBI")
				.pattern(" IR")
				.unlockedBy("has_steel_ingot", has(ModItemTags.INGOTS_STEEL))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.TOOLS, ModItems.STEEL_DRILL.get())
				.requires(ModItems.DRILL_BASE.get())
				.requires(ModItems.STEEL_DRILL_HEAD.get())
				.unlockedBy(getHasName(ModItems.STEEL_DRILL_HEAD.get()), has(ModItems.STEEL_DRILL_HEAD.get()))
				.unlockedBy("has_steel_ingot", has(ModItemTags.INGOTS_STEEL))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ModItems.UNIM_DRILL_HEAD.get())
				.define('U', ModItemTags.INGOTS_UNIM)
				.define('B', Tags.Items.STORAGE_BLOCKS_IRON)
				.define('R', ModItems.REDSTONE_BLEND.get())
				.pattern("UU ")
				.pattern("UBU")
				.pattern(" UR")
				.unlockedBy("has_unim_ingot", has(ModItemTags.INGOTS_UNIM))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.TOOLS, ModItems.UNIM_DRILL.get())
				.requires(ModItems.DRILL_BASE.get())
				.requires(ModItems.UNIM_DRILL_HEAD.get())
				.unlockedBy(getHasName(ModItems.UNIM_DRILL_HEAD.get()), has(ModItems.UNIM_DRILL_HEAD.get()))
				.unlockedBy("has_unim_ingot", has(ModItemTags.INGOTS_UNIM))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, ModItems.DIAMOND_DRILL_HEAD.get())
				.define('D', Tags.Items.GEMS_DIAMOND)
				.define('B', Tags.Items.STORAGE_BLOCKS_IRON)
				.define('R', ModItems.REDSTONE_BLEND.get())
				.pattern("DD ")
				.pattern("DBD")
				.pattern(" DR")
				.unlockedBy("has_diamond", has(Tags.Items.GEMS_DIAMOND))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.TOOLS, ModItems.DIAMOND_DRILL.get())
				.requires(ModItems.DRILL_BASE.get())
				.requires(ModItems.DIAMOND_DRILL_HEAD.get())
				.unlockedBy(getHasName(ModItems.DIAMOND_DRILL_HEAD.get()), has(ModItems.DIAMOND_DRILL_HEAD.get()))
				.unlockedBy("has_diamond", has(Tags.Items.GEMS_DIAMOND))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, Blocks.COBBLESTONE, 2)
				.define('R', ModItems.ROCK.get())
				.pattern("RR")
				.pattern("RR")
				.unlockedBy(getHasName(ModItems.ROCK.get()), has(ModItems.ROCK.get()))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "cobblestone_with_rock"));
		
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, Blocks.ANDESITE, 2)
				.define('R', ModItems.ANDESITE_ROCK.get())
				.pattern("RR")
				.pattern("RR")
				.unlockedBy(getHasName(ModItems.ANDESITE_ROCK.get()), has(ModItems.ANDESITE_ROCK.get()))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "andesite_with_rock"));
		
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, Blocks.DIORITE, 2)
				.define('R', ModItems.DIORITE_ROCK.get())
				.pattern("RR")
				.pattern("RR")
				.unlockedBy(getHasName(ModItems.DIORITE_ROCK.get()), has(ModItems.DIORITE_ROCK.get()))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "diorite_with_rock"));
		
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, Blocks.GRANITE, 2)
				.define('R', ModItems.GRANITE_ROCK.get())
				.pattern("RR")
				.pattern("RR")
				.unlockedBy(getHasName(ModItems.GRANITE_ROCK.get()), has(ModItems.GRANITE_ROCK.get()))
				.save(recipeOutput, ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "granite_with_rock"));
		
		stairs(recipeOutput, ModBlocks.CEMENT_STAIRS.get(), ModBlocks.CEMENT.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_STAIRS.get(), ModBlocks.CEMENT.get());
		
		slab(recipeOutput, ModBlocks.CEMENT_SLAB.get(), ModBlocks.CEMENT.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_SLAB.get(), ModBlocks.CEMENT.get(), 2);
		
		wall(recipeOutput, ModBlocks.CEMENT_WALL.get(), ModBlocks.CEMENT.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_WALL.get(), ModBlocks.CEMENT.get());
		
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, ModBlocks.CEMENT_PLATE.get(), 4)
				.define('C', ModBlocks.CEMENT.get())
				.pattern("CC")
				.pattern("CC")
				.unlockedBy(getHasName(ModBlocks.CEMENT.get()), has(ModBlocks.CEMENT.get()))
				.save(recipeOutput);
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_PLATE.get(), ModBlocks.CEMENT.get());
		
		stairs(recipeOutput, ModBlocks.CEMENT_PLATE_STAIRS.get(), ModBlocks.CEMENT_PLATE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_PLATE_STAIRS.get(), ModBlocks.CEMENT_PLATE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_PLATE_STAIRS.get(), ModBlocks.CEMENT.get());
		
		slab(recipeOutput, ModBlocks.CEMENT_PLATE_SLAB.get(), ModBlocks.CEMENT_PLATE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_PLATE_SLAB.get(), ModBlocks.CEMENT_PLATE.get(), 2);
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_PLATE_SLAB.get(), ModBlocks.CEMENT.get(), 2);
		
		wall(recipeOutput, ModBlocks.CEMENT_PLATE_WALL.get(), ModBlocks.CEMENT_PLATE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_PLATE_WALL.get(), ModBlocks.CEMENT_PLATE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.CEMENT_PLATE_WALL.get(), ModBlocks.CEMENT.get());
		
		stairs(recipeOutput, ModBlocks.LIMESTONE_STAIRS.get(), ModBlocks.LIMESTONE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_STAIRS.get(), ModBlocks.LIMESTONE.get());
		
		slab(recipeOutput, ModBlocks.LIMESTONE_SLAB.get(), ModBlocks.LIMESTONE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_SLAB.get(), ModBlocks.LIMESTONE.get(), 2);
		
		wall(recipeOutput, ModBlocks.LIMESTONE_WALL.get(), ModBlocks.LIMESTONE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_WALL.get(), ModBlocks.LIMESTONE.get());
		
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, ModBlocks.LIMESTONE_BRICKS.get(), 4)
				.define('L', ModBlocks.LIMESTONE.get())
				.pattern("LL")
				.pattern("LL")
				.unlockedBy(getHasName(ModBlocks.LIMESTONE.get()), has(ModBlocks.LIMESTONE.get()))
				.save(recipeOutput);
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_BRICKS.get(), ModBlocks.LIMESTONE.get());
		
		stairs(recipeOutput, ModBlocks.LIMESTONE_BRICK_STAIRS.get(), ModBlocks.LIMESTONE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_BRICK_STAIRS.get(), ModBlocks.LIMESTONE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_BRICK_STAIRS.get(), ModBlocks.LIMESTONE.get());
		
		slab(recipeOutput, ModBlocks.LIMESTONE_BRICK_SLAB.get(), ModBlocks.LIMESTONE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_BRICK_SLAB.get(), ModBlocks.LIMESTONE_BRICKS.get(), 2);
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_BRICK_SLAB.get(), ModBlocks.LIMESTONE.get(), 2);
		
		wall(recipeOutput, ModBlocks.LIMESTONE_BRICK_WALL.get(), ModBlocks.LIMESTONE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_BRICK_WALL.get(), ModBlocks.LIMESTONE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.LIMESTONE_BRICK_WALL.get(), ModBlocks.LIMESTONE.get());
		
		stairs(recipeOutput, ModBlocks.MARBLE_STAIRS.get(), ModBlocks.MARBLE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_STAIRS.get(), ModBlocks.MARBLE.get());
		
		slab(recipeOutput, ModBlocks.MARBLE_SLAB.get(), ModBlocks.MARBLE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_SLAB.get(), ModBlocks.MARBLE.get(), 2);
		
		wall(recipeOutput, ModBlocks.MARBLE_WALL.get(), ModBlocks.MARBLE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_WALL.get(), ModBlocks.MARBLE.get());
		
		SimpleCookingRecipeBuilder.smelting(Ingredient.of(ModBlocks.MARBLE_COBBLE.get()), RecipeCategory.BUILDING_BLOCKS, ModBlocks.MARBLE.get(), 0.1f, 200)
				.unlockedBy(getHasName(ModBlocks.MARBLE_COBBLE.get()), has(ModBlocks.MARBLE_COBBLE.get()))
				.save(recipeOutput);
		
		stairs(recipeOutput, ModBlocks.MARBLE_COBBLE_STAIRS.get(), ModBlocks.MARBLE_COBBLE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_COBBLE_STAIRS.get(), ModBlocks.MARBLE_COBBLE.get());
		
		slab(recipeOutput, ModBlocks.MARBLE_COBBLE_SLAB.get(), ModBlocks.MARBLE_COBBLE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_COBBLE_SLAB.get(), ModBlocks.MARBLE_COBBLE.get(), 2);
		
		wall(recipeOutput, ModBlocks.MARBLE_COBBLE_WALL.get(), ModBlocks.MARBLE_COBBLE.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_COBBLE_WALL.get(), ModBlocks.MARBLE_COBBLE.get());
		
		stairs(recipeOutput, ModBlocks.MARBLE_BRICK_STAIRS.get(), ModBlocks.MARBLE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_BRICK_STAIRS.get(), ModBlocks.MARBLE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_BRICK_STAIRS.get(), ModBlocks.MARBLE.get());
		
		slab(recipeOutput, ModBlocks.MARBLE_BRICK_SLAB.get(), ModBlocks.MARBLE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_BRICK_SLAB.get(), ModBlocks.MARBLE_BRICKS.get(), 2);
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_BRICK_SLAB.get(), ModBlocks.MARBLE.get(), 2);
		
		wall(recipeOutput, ModBlocks.MARBLE_BRICK_WALL.get(), ModBlocks.MARBLE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_BRICK_WALL.get(), ModBlocks.MARBLE_BRICKS.get());
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_BRICK_WALL.get(), ModBlocks.MARBLE.get());
		
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, ModBlocks.MARBLE_BRICKS.get(), 4)
				.define('M', ModBlocks.MARBLE.get())
				.pattern("MM")
				.pattern("MM")
				.unlockedBy(getHasName(ModBlocks.MARBLE.get()), has(ModBlocks.MARBLE.get()))
				.save(recipeOutput);
		stonecutterResultFromBase(recipeOutput, ModBlocks.MARBLE_BRICKS.get(), ModBlocks.MARBLE.get());
		
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, ModBlocks.MARBLE_LAMP.get(), 2)
				.define('M', ModBlocks.MARBLE_BRICKS.get())
				.define('G', Blocks.GLOWSTONE)
				.pattern(" M ")
				.pattern("MGM")
				.pattern(" M ")
				.unlockedBy(getHasName(ModBlocks.MARBLE_BRICKS.get()), has(ModBlocks.MARBLE_BRICKS.get()))
				.unlockedBy(getHasName(Blocks.GLOWSTONE), has(Blocks.GLOWSTONE))
				.save(recipeOutput);
		
		slab(recipeOutput, ModBlocks.DIRT_SLAB.get(), Blocks.DIRT);
		slab(recipeOutput, ModBlocks.COARSE_DIRT_SLAB.get(), Blocks.COARSE_DIRT);
		
		slab(recipeOutput, ModBlocks.GLASS_SLAB.get(), Blocks.GLASS);
		stonecutterResultFromBase(recipeOutput, ModBlocks.GLASS_SLAB.get(), Blocks.GLASS, 2);
		
		door(recipeOutput, ModItems.STONE_DOOR.get(), Tags.Items.STONE, "has_stone");
		trapDoor(recipeOutput, ModBlocks.STONE_TRAPDOOR.get(), Tags.Items.STONE, "has_stone");
		
		door(recipeOutput, ModItems.SANDSTONE_DOOR.get(), Tags.Items.SANDSTONE, "has_sandstone");
		trapDoor(recipeOutput, ModBlocks.SANDSTONE_TRAPDOOR.get(), Tags.Items.SANDSTONE, "has_sandstone");
		
		door(recipeOutput, ModItems.GLASS_DOOR.get(), Blocks.GLASS);
		trapDoor(recipeOutput, ModBlocks.GLASS_TRAPDOOR.get(), Blocks.GLASS);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.WOODEN_GLASS_FRAME.get(), 8)
				.define('S', Items.STICK)
				.define('G', Tags.Items.GLASS_PANES)
				.pattern("SGS")
				.pattern("SGS")
				.unlockedBy("has_glass_pane", has(Tags.Items.GLASS_PANES))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.KILN.get())
				.define('B', Items.CLAY_BALL)
				.define('C', Tags.Items.COBBLESTONE)
				.define('T', Blocks.TORCH)
				.pattern("BBB")
				.pattern("B B")
				.pattern("CTC")
				.unlockedBy(getHasName(Items.CLAY_BALL), has(Items.CLAY_BALL))
				.unlockedBy("has_cobblestone", has(Tags.Items.COBBLESTONE))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.SMELTER.get())
				.define('F', ItemTags.COALS)
				.define('S', Tags.Items.COBBLESTONE)
				.define('C', ModBlocks.CEMENT.get())
				.pattern("SSS")
				.pattern("S S")
				.pattern("CFC")
				.unlockedBy("has_coal", has(ItemTags.COALS))
				.unlockedBy(getHasName(ModBlocks.CEMENT.get()), has(ModBlocks.CEMENT.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.MILLSTONE.get())
				.define('R', ModItemTags.ROCKS)
				.define('C', Tags.Items.COBBLESTONE)
				.define('S', Items.STICK)
				.pattern("SCS")
				.pattern("CRC")
				.pattern("SCS")
				.unlockedBy("has_rocks", has(ModItemTags.ROCKS))
				.unlockedBy("has_cobblestone", has(Tags.Items.COBBLESTONE))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.OAK_CHOPPING_BLOCK.get())
				.define('L', ItemTags.OAK_LOGS)
				.pattern("LL")
				.unlockedBy("has_oak_log", has(ItemTags.OAK_LOGS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.SPRUCE_CHOPPING_BLOCK.get())
				.define('L', ItemTags.SPRUCE_LOGS)
				.pattern("LL")
				.unlockedBy("has_spruce_log", has(ItemTags.SPRUCE_LOGS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.BIRCH_CHOPPING_BLOCK.get())
				.define('L', ItemTags.BIRCH_LOGS)
				.pattern("LL")
				.unlockedBy("has_birch_log", has(ItemTags.BIRCH_LOGS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.JUNGLE_CHOPPING_BLOCK.get())
				.define('L', ItemTags.JUNGLE_LOGS)
				.pattern("LL")
				.unlockedBy("has_jungle_log", has(ItemTags.JUNGLE_LOGS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.ACACIA_CHOPPING_BLOCK.get())
				.define('L', ItemTags.ACACIA_LOGS)
				.pattern("LL")
				.unlockedBy("has_acacia_log", has(ItemTags.ACACIA_LOGS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.DARK_OAK_CHOPPING_BLOCK.get())
				.define('L', ItemTags.DARK_OAK_LOGS)
				.pattern("LL")
				.unlockedBy("has_dark_oak_log", has(ItemTags.DARK_OAK_LOGS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.MANGROVE_CHOPPING_BLOCK.get())
				.define('L', ItemTags.MANGROVE_LOGS)
				.pattern("LL")
				.unlockedBy("has_mangrove_log", has(ItemTags.MANGROVE_LOGS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.CHERRY_CHOPPING_BLOCK.get())
				.define('L', ItemTags.CHERRY_LOGS)
				.pattern("LL")
				.unlockedBy("has_cherry_log", has(ItemTags.CHERRY_LOGS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.CRIMSON_CHOPPING_BLOCK.get())
				.define('S', ItemTags.CRIMSON_STEMS)
				.pattern("SS")
				.unlockedBy("has_crimson_stem", has(ItemTags.CRIMSON_STEMS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.WARPED_CHOPPING_BLOCK.get())
				.define('S', ItemTags.WARPED_STEMS)
				.pattern("SS")
				.unlockedBy("has_warped_stem", has(ItemTags.WARPED_STEMS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.OAK_DRYING_RACK.get())
				.define('S', Blocks.OAK_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.OAK_SLAB), has(Blocks.OAK_SLAB))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.SPRUCE_DRYING_RACK.get())
				.define('S', Blocks.SPRUCE_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.SPRUCE_SLAB), has(Blocks.SPRUCE_SLAB))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.BIRCH_DRYING_RACK.get())
				.define('S', Blocks.BIRCH_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.BIRCH_SLAB), has(Blocks.BIRCH_SLAB))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.JUNGLE_DRYING_RACK.get())
				.define('S', Blocks.JUNGLE_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.JUNGLE_SLAB), has(Blocks.JUNGLE_SLAB))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.ACACIA_DRYING_RACK.get())
				.define('S', Blocks.ACACIA_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.ACACIA_SLAB), has(Blocks.ACACIA_SLAB))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.DARK_OAK_DRYING_RACK.get())
				.define('S', Blocks.DARK_OAK_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.DARK_OAK_SLAB), has(Blocks.DARK_OAK_SLAB))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.MANGROVE_DRYING_RACK.get())
				.define('S', Blocks.MANGROVE_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.MANGROVE_SLAB), has(Blocks.MANGROVE_SLAB))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.CHERRY_DRYING_RACK.get())
				.define('S', Blocks.CHERRY_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.CHERRY_SLAB), has(Blocks.CHERRY_SLAB))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.CRIMSON_DRYING_RACK.get())
				.define('S', Blocks.CRIMSON_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.CRIMSON_SLAB), has(Blocks.CRIMSON_SLAB))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.WARPED_DRYING_RACK.get())
				.define('S', Blocks.WARPED_SLAB)
				.pattern("SSS")
				.unlockedBy(getHasName(Blocks.WARPED_SLAB), has(Blocks.WARPED_SLAB))
				.save(recipeOutput);
		
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, ModItems.REDSTONE_BLEND.get(), 3)
				.requires(Tags.Items.DUSTS_REDSTONE)
				.requires(ModItemTags.DUSTS_UNIM)
				.requires(ModItemTags.DUSTS_COAL)
				.unlockedBy("has_redstone_dust", has(Tags.Items.DUSTS_REDSTONE))
				.unlockedBy("has_unim_dust", has(ModItemTags.DUSTS_UNIM))
				.unlockedBy("has_coal_dust", has(ModItemTags.DUSTS_COAL))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.MACHINE_FRAME.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('R', ModItems.REDSTONE_BLEND.get())
				.define('S', ModItemTags.INGOTS_STEEL)
				.pattern("SIS")
				.pattern("IRI")
				.pattern("SIS")
				.unlockedBy("has_steel_ingot", has(ModItemTags.INGOTS_STEEL))
				.unlockedBy(getHasName(ModItems.REDSTONE_BLEND.get()), has(ModItems.REDSTONE_BLEND.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.COAL_GENERATOR.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('S', Blocks.SMOOTH_STONE)
				.define('M', ModBlocks.MACHINE_FRAME.get())
				.define('F', Blocks.FURNACE)
				.pattern("III")
				.pattern("IMI")
				.pattern("SFS")
				.unlockedBy(getHasName(ModBlocks.MACHINE_FRAME.get()), has(ModBlocks.MACHINE_FRAME.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModItems.PORTABLE_BATTERY.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('C', Tags.Items.INGOTS_COPPER)
				.define('R', ModItems.REDSTONE_BLEND.get())
				.pattern(" C ")
				.pattern("IRI")
				.pattern("IRI")
				.unlockedBy("has_copper_ingot", has(Tags.Items.INGOTS_COPPER))
				.unlockedBy(getHasName(ModItems.REDSTONE_BLEND.get()), has(ModItems.REDSTONE_BLEND.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.BATTERY.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('S', Blocks.SMOOTH_STONE)
				.define('M', ModBlocks.MACHINE_FRAME.get())
				.define('P', ModItems.PORTABLE_BATTERY.get())
				.pattern("III")
				.pattern("PMP")
				.pattern("SSS")
				.unlockedBy(getHasName(ModBlocks.MACHINE_FRAME.get()), has(ModBlocks.MACHINE_FRAME.get()))
				.unlockedBy(getHasName(ModItems.PORTABLE_BATTERY.get()), has(ModItems.PORTABLE_BATTERY.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModItems.SOLAR_CELL.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('G', Tags.Items.GLASS)
				.define('L', Tags.Items.GEMS_LAPIS)
				.pattern("GGG")
				.pattern("LLL")
				.pattern("III")
				.unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
				.unlockedBy("has_lapis", has(Tags.Items.GEMS_LAPIS))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.SOLAR_PANEL.get())
				.define('C', ModItems.SOLAR_CELL.get())
				.define('S', ModItemTags.INGOTS_STEEL)
				.define('R', ModItems.REDSTONE_BLEND.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.pattern("CCC")
				.pattern("SRS")
				.pattern("III")
				.unlockedBy(getHasName(ModItems.SOLAR_CELL.get()), has(ModItems.SOLAR_CELL.get()))
				.unlockedBy("has_steel_ingot", has(ModItemTags.INGOTS_STEEL))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.LIQUID_TANK.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('S', Blocks.SMOOTH_STONE)
				.define('M', ModBlocks.MACHINE_FRAME.get())
				.define('B', Items.BUCKET)
				.pattern("III")
				.pattern("BMB")
				.pattern("SSS")
				.unlockedBy(getHasName(ModBlocks.MACHINE_FRAME.get()), has(ModBlocks.MACHINE_FRAME.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.ELECTRIC_BREWERY.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('S', Blocks.SMOOTH_STONE)
				.define('M', ModBlocks.MACHINE_FRAME.get())
				.define('B', Items.BUCKET)
				.define('R', Items.BLAZE_ROD)
				.pattern("III")
				.pattern("RMR")
				.pattern("SBS")
				.unlockedBy(getHasName(ModBlocks.MACHINE_FRAME.get()), has(ModBlocks.MACHINE_FRAME.get()))
				.unlockedBy(getHasName(Items.BLAZE_ROD), has(Items.BLAZE_ROD))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.ELECTRIC_SMELTER.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('S', Blocks.SMOOTH_STONE)
				.define('M', ModBlocks.MACHINE_FRAME.get())
				.define('F', ModBlocks.SMELTER.get())
				.pattern("III")
				.pattern("IMI")
				.pattern("SFS")
				.unlockedBy(getHasName(ModBlocks.MACHINE_FRAME.get()), has(ModBlocks.MACHINE_FRAME.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.ELECTRIC_GREENHOUSE.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('S', Blocks.SMOOTH_STONE)
				.define('M', ModBlocks.MACHINE_FRAME.get())
				.define('G', Tags.Items.GLASS)
				.define('H', Items.DIAMOND_HOE)
				.pattern("IGI")
				.pattern("GMG")
				.pattern("SHS")
				.unlockedBy(getHasName(ModBlocks.MACHINE_FRAME.get()), has(ModBlocks.MACHINE_FRAME.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.HYDRAULIC_PRESS.get())
				.define('I', Tags.Items.INGOTS_IRON)
				.define('S', Blocks.SMOOTH_STONE)
				.define('M', ModBlocks.MACHINE_FRAME.get())
				.define('P', Items.PISTON)
				.define('C', Items.CHEST)
				.pattern("III")
				.pattern("PMP")
				.pattern("SCS")
				.unlockedBy(getHasName(ModBlocks.MACHINE_FRAME.get()), has(ModBlocks.MACHINE_FRAME.get()))
				.save(recipeOutput);
		
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, ModBlocks.COPPER_CABLE.get(), 3)
				.define('C', Tags.Items.INGOTS_COPPER)
				.define('R', ModItems.REDSTONE_BLEND.get())
				.pattern("CCC")
				.pattern("RRR")
				.pattern("CCC")
				.unlockedBy("has_copper_ingot", has(Tags.Items.INGOTS_COPPER))
				.unlockedBy(getHasName(ModItems.REDSTONE_BLEND.get()), has(ModItems.REDSTONE_BLEND.get()))
				.save(recipeOutput);
	}
	
	@Override
	protected void generateCookingRecipes(RecipeOutput recipeOutput)
	{
		ImmutableList<ItemLike> UNIM_SMELTABLES = ImmutableList.of(ModItems.UNIM.get(), ModBlocks.UNIM_ORE.get(), ModBlocks.DEEPSLATE_UNIM_ORE.get());
		oreSmelting(recipeOutput, UNIM_SMELTABLES, ModItems.UNIM_INGOT.get(), 0.7F, 200, "unim_ingot");
		oreBlasting(recipeOutput, UNIM_SMELTABLES, ModItems.UNIM_INGOT.get(), 0.7F, 100, "unim_ingot");
		
		ImmutableList<ItemLike> RUBY_SMELTABLES = ImmutableList.of(ModBlocks.RUBY_ORE.get(), ModBlocks.DEEPSLATE_RUBY_ORE.get());
		oreSmelting(recipeOutput, RUBY_SMELTABLES, ModItems.RUBY.get(), 0.7F, 200, "ruby");
		oreBlasting(recipeOutput, RUBY_SMELTABLES, ModItems.RUBY.get(), 1.0F, 100, "ruby");
		
		ImmutableList<ItemLike> ASCABIT_SMELTABLES = ImmutableList.of(ModItems.RAW_ASCABIT.get(), ModBlocks.ASCABIT_ORE.get(), ModBlocks.DEEPSLATE_ASCABIT_ORE.get());
		oreSmelting(recipeOutput, ASCABIT_SMELTABLES, ModItems.ASCABIT_INGOT.get(), 0.7F, 200, "ascabit_ingot");
		oreBlasting(recipeOutput, ASCABIT_SMELTABLES, ModItems.ASCABIT_INGOT.get(), 0.7F, 100, "ascabit_ingot");
		
		ImmutableList<ItemLike> TIN_SMELTABLES = ImmutableList.of(ModItems.RAW_TIN.get(), ModBlocks.TIN_ORE.get(), ModBlocks.DEEPSLATE_TIN_ORE.get());
		oreSmelting(recipeOutput, TIN_SMELTABLES, ModItems.TIN_INGOT.get(), 0.7F, 200, "tin_ingot");
		oreBlasting(recipeOutput, TIN_SMELTABLES, ModItems.TIN_INGOT.get(), 0.7F, 100, "tin_ingot");
		
		ImmutableList<ItemLike> TITANIUM_SMELTABLES = ImmutableList.of(ModItems.RAW_TITANIUM.get(), ModBlocks.TITANIUM_ORE.get(), ModBlocks.DEEPSLATE_TITANIUM_ORE.get());
		oreSmelting(recipeOutput, TITANIUM_SMELTABLES, ModItems.TITANIUM_INGOT.get(), 1.0F, 200, "titanium_ingot");
		oreBlasting(recipeOutput, TITANIUM_SMELTABLES, ModItems.TITANIUM_INGOT.get(), 1.0F, 100, "titanium_ingot");
		
		ImmutableList<ItemLike> ZINC_SMELTABLES = ImmutableList.of(ModItems.RAW_ZINC.get(), ModBlocks.ZINC_ORE.get(), ModBlocks.DEEPSLATE_ZINC_ORE.get());
		oreSmelting(recipeOutput, ZINC_SMELTABLES, ModItems.ZINC_INGOT.get(), 0.7F, 200, "zinc_ingot");
		oreBlasting(recipeOutput, ZINC_SMELTABLES, ModItems.ZINC_INGOT.get(), 0.7F, 100, "zinc_ingot");
		
		ImmutableList<ItemLike> STEEL_SMELTABLES = ImmutableList.of(ModItems.STEEL_FRAGMENTS.get());
		oreSmelting(recipeOutput, STEEL_SMELTABLES, ModItems.STEEL_INGOT.get(), 0.7F, 200, "steel_ingot");
		oreBlasting(recipeOutput, STEEL_SMELTABLES, ModItems.STEEL_INGOT.get(), 0.7F, 100, "steel_ingot");
		
		foodCooking(recipeOutput, ModItems.DOUGH.get(), Items.BREAD, 0.35f, false, "bread_from_dough");
		
		SimpleCookingRecipeBuilder.smelting(Ingredient.of(ModItems.CEMENT_MIXTURE.get()), RecipeCategory.MISC, ModBlocks.CEMENT.get(), 0.1f, 200)
				.unlockedBy(getHasName(ModItems.CEMENT_MIXTURE.get()), has(ModItems.CEMENT_MIXTURE.get()))
				.save(recipeOutput);
		
		SimpleCookingRecipeBuilder.smelting(Ingredient.of(ModItems.UNFIRED_CLAY_BUCKET.get()), RecipeCategory.MISC, ModItems.CLAY_BUCKET.get(), 0.1f, 200)
				.unlockedBy(getHasName(Items.CLAY_BALL), has(Items.CLAY_BALL))
				.unlockedBy(getHasName(ModItems.UNFIRED_CLAY_BUCKET.get()), has(ModItems.UNFIRED_CLAY_BUCKET.get()))
				.save(recipeOutput);
	}
	
	@Override
	protected void generateCustomRecipes(RecipeOutput recipeOutput)
	{
		KilnRecipeBuilder.kiln(Ingredient.of(ModItems.UNFIRED_CLAY_BUCKET.get()), new ItemStack(ModItems.CLAY_BUCKET.get()))
				.unlockedBy(getHasName(Items.CLAY_BALL), has(Items.CLAY_BALL))
				.unlockedBy(getHasName(ModItems.UNFIRED_CLAY_BUCKET.get()), has(ModItems.UNFIRED_CLAY_BUCKET.get()))
				.save(recipeOutput);
		
		KilnRecipeBuilder.kiln(Ingredient.of(Items.CLAY_BALL), new ItemStack(Items.BRICK))
				.unlockedBy(getHasName(Items.CLAY_BALL), has(Items.CLAY_BALL))
				.save(recipeOutput);
		
		KilnRecipeBuilder.kiln(Ingredient.of(ModItems.CEMENT_MIXTURE.get()), new ItemStack(ModBlocks.CEMENT.get()), 300)
				.unlockedBy(getHasName(ModItems.CEMENT_MIXTURE.get()), has(ModItems.CEMENT_MIXTURE.get()))
				.save(recipeOutput);
		
		KilnRecipeBuilder.kiln(Ingredient.of(ItemTags.LOGS_THAT_BURN), new ItemStack(Items.CHARCOAL))
				.unlockedBy("has_log_that_burn", has(ItemTags.LOGS_THAT_BURN))
				.save(recipeOutput);
		
		KilnRecipeBuilder.kiln(Ingredient.of(Blocks.COBBLESTONE), new ItemStack(Blocks.STONE), 400)
				.unlockedBy(getHasName(Blocks.COBBLESTONE), has(Blocks.COBBLESTONE))
				.save(recipeOutput);
		
		KilnRecipeBuilder.kiln(Ingredient.of(Blocks.STONE), new ItemStack(Blocks.SMOOTH_STONE), 400)
				.unlockedBy(getHasName(Blocks.STONE), has(Blocks.STONE))
				.save(recipeOutput);
		
		oreSmelterRecipes(recipeOutput, 0.65f, Items.IRON_INGOT, Tags.Items.RAW_MATERIALS_IRON, Tags.Items.ORES_IRON);
		oreSmelterRecipes(recipeOutput, 0.6f, Items.GOLD_INGOT, Tags.Items.RAW_MATERIALS_GOLD, Tags.Items.ORES_GOLD);
		oreSmelterRecipes(recipeOutput, 0.5f, Items.COPPER_INGOT, Tags.Items.RAW_MATERIALS_COPPER, Tags.Items.ORES_COPPER);
		
		oreSmelterRecipes(recipeOutput, 0.5f, ModItems.TIN_INGOT.get(), ModItemTags.RAW_MATERIALS_TIN, ModItemTags.ORES_TIN);
		oreSmelterRecipes(recipeOutput, 0.5f, ModItems.ZINC_INGOT.get(), ModItemTags.RAW_MATERIALS_ZINC, ModItemTags.ORES_ZINC);
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(ModItemTags.RAW_MATERIALS_TIN)
				}, new ItemStack(ModItems.BRONZE_INGOT.get(), 2), 0.6f)
				.unlockedBy("has_raw_copper", has(Tags.Items.RAW_MATERIALS_COPPER))
				.unlockedBy("has_raw_tin", has(ModItemTags.RAW_MATERIALS_TIN))
				.save(recipeOutput);
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(ModItemTags.ORES_TIN)
				}, new ItemStack(ModItems.BRONZE_INGOT.get(), 2), 0.65f)
				.unlockedBy("has_copper_ore", has(Tags.Items.ORES_COPPER))
				.unlockedBy("has_tin_ore", has(ModItemTags.ORES_TIN))
				.save(recipeOutput, "bronze_ingot_from_ore");
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(ModItemTags.INGOTS_TIN)
				}, new ItemStack(ModItems.BRONZE_INGOT.get(), 2), 0.6f)
				.unlockedBy("has_copper_ingot", has(Tags.Items.INGOTS_COPPER))
				.unlockedBy("has_tin_ingot", has(ModItemTags.INGOTS_TIN))
				.save(recipeOutput, "bronze_ingot_from_ingot");
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(ModItemTags.RAW_MATERIALS_ZINC)
				}, new ItemStack(ModItems.BRASS_INGOT.get(), 2), 0.6f)
				.unlockedBy("has_raw_copper", has(Tags.Items.RAW_MATERIALS_COPPER))
				.unlockedBy("has_raw_zinc", has(ModItemTags.RAW_MATERIALS_ZINC))
				.save(recipeOutput);
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(ModItemTags.ORES_ZINC)
				}, new ItemStack(ModItems.BRASS_INGOT.get(), 2), 0.65f)
				.unlockedBy("has_copper_ore", has(Tags.Items.ORES_COPPER))
				.unlockedBy("has_zinc_ore", has(ModItemTags.ORES_ZINC))
				.save(recipeOutput, "brass_ingot_from_ore");
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(ModItemTags.INGOTS_ZINC)
				}, new ItemStack(ModItems.BRASS_INGOT.get(), 2), 0.6f)
				.unlockedBy("has_copper_ingot", has(Tags.Items.INGOTS_COPPER))
				.unlockedBy("has_zinc_ingot", has(ModItemTags.INGOTS_ZINC))
				.save(recipeOutput, "brass_ingot_from_ingot");
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(Tags.Items.RAW_MATERIALS_IRON),
						Ingredient.of(Tags.Items.RAW_MATERIALS_IRON),
						Ingredient.of(ModItemTags.DUSTS_COAL),
						Ingredient.of(ModItemTags.DUSTS_COAL)
				}, new ItemStack(ModItems.STEEL_INGOT.get()), 0.8f)
				.unlockedBy("has_raw_iron", has(Tags.Items.RAW_MATERIALS_IRON))
				.unlockedBy("has_coal_dust", has(ModItemTags.DUSTS_COAL))
				.save(recipeOutput);
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(Tags.Items.ORES_IRON),
						Ingredient.of(Tags.Items.ORES_IRON),
						Ingredient.of(ModItemTags.DUSTS_COAL),
						Ingredient.of(ModItemTags.DUSTS_COAL)
				}, new ItemStack(ModItems.STEEL_INGOT.get()), 0.85f)
				.unlockedBy("has_iron_ore", has(Tags.Items.ORES_IRON))
				.unlockedBy("has_coal_dust", has(ModItemTags.DUSTS_COAL))
				.save(recipeOutput, "steel_ingot_from_ore");
		
		SmelterRecipeBuilder.smelter(new Ingredient[] {
						Ingredient.of(Tags.Items.INGOTS_IRON),
						Ingredient.of(Tags.Items.INGOTS_IRON),
						Ingredient.of(ModItemTags.DUSTS_COAL),
						Ingredient.of(ModItemTags.DUSTS_COAL)
				}, new ItemStack(ModItems.STEEL_INGOT.get()), 0.8f)
				.unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
				.unlockedBy("has_coal_dust", has(ModItemTags.DUSTS_COAL))
				.save(recipeOutput, "steel_ingot_from_ingot");
		
		oreElectricSmelterRecipes(recipeOutput, Items.IRON_INGOT, Tags.Items.RAW_MATERIALS_IRON, Tags.Items.ORES_IRON);
		oreElectricSmelterRecipes(recipeOutput, Items.GOLD_INGOT, Tags.Items.RAW_MATERIALS_GOLD, Tags.Items.ORES_GOLD);
		oreElectricSmelterRecipes(recipeOutput, Items.COPPER_INGOT, Tags.Items.RAW_MATERIALS_COPPER, Tags.Items.ORES_COPPER);
		
		oreElectricSmelterRecipes(recipeOutput, ModItems.UNIM_INGOT.get(), ModItemTags.DUSTS_UNIM, ModItemTags.ORES_UNIM);
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(ModItemTags.ORES_RUBY),
						Ingredient.of(ModItemTags.ORES_RUBY),
						Ingredient.of(ModItemTags.ORES_RUBY),
						Ingredient.of(ModItemTags.ORES_RUBY)
				}, new ItemStack(ModItems.RUBY.get(), 4))
				.unlockedBy("has_ore", has(ModItemTags.ORES_RUBY))
				.save(recipeOutput, getItemName(ModItems.RUBY.get()) + "_from_ore");
		
		oreElectricSmelterRecipes(recipeOutput, ModItems.ASCABIT_INGOT.get(), ModItemTags.RAW_MATERIALS_ASCABIT, ModItemTags.ORES_ASCABIT);
		oreElectricSmelterRecipes(recipeOutput, ModItems.TIN_INGOT.get(), ModItemTags.RAW_MATERIALS_TIN, ModItemTags.ORES_TIN);
		oreElectricSmelterRecipes(recipeOutput, ModItems.TITANIUM_INGOT.get(), ModItemTags.RAW_MATERIALS_TITANIUM, ModItemTags.ORES_TITANIUM);
		oreElectricSmelterRecipes(recipeOutput, ModItems.ZINC_INGOT.get(), ModItemTags.RAW_MATERIALS_ZINC, ModItemTags.ORES_ZINC);
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(ModItemTags.RAW_MATERIALS_TIN)
				}, new ItemStack(ModItems.BRONZE_INGOT.get(), 4))
				.unlockedBy("has_raw_copper", has(Tags.Items.RAW_MATERIALS_COPPER))
				.unlockedBy("has_raw_tin", has(ModItemTags.RAW_MATERIALS_TIN))
				.save(recipeOutput);
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(ModItemTags.ORES_TIN)
				}, new ItemStack(ModItems.BRONZE_INGOT.get(), 4))
				.unlockedBy("has_copper_ore", has(Tags.Items.ORES_COPPER))
				.unlockedBy("has_tin_ore", has(ModItemTags.ORES_TIN))
				.save(recipeOutput, "bronze_ingot_from_ore");
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(ModItemTags.INGOTS_TIN)
				}, new ItemStack(ModItems.BRONZE_INGOT.get(), 4))
				.unlockedBy("has_copper_ingot", has(Tags.Items.INGOTS_COPPER))
				.unlockedBy("has_tin_ingot", has(ModItemTags.INGOTS_TIN))
				.save(recipeOutput, "bronze_ingot_from_ingot");
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(Tags.Items.RAW_MATERIALS_COPPER),
						Ingredient.of(ModItemTags.RAW_MATERIALS_ZINC)
				}, new ItemStack(ModItems.BRASS_INGOT.get(), 4))
				.unlockedBy("has_raw_copper", has(Tags.Items.RAW_MATERIALS_COPPER))
				.unlockedBy("has_raw_zinc", has(ModItemTags.RAW_MATERIALS_ZINC))
				.save(recipeOutput);
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(Tags.Items.ORES_COPPER),
						Ingredient.of(ModItemTags.ORES_ZINC)
				}, new ItemStack(ModItems.BRASS_INGOT.get(), 4))
				.unlockedBy("has_copper_ore", has(Tags.Items.ORES_COPPER))
				.unlockedBy("has_zinc_ore", has(ModItemTags.ORES_ZINC))
				.save(recipeOutput, "brass_ingot_from_ore");
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(Tags.Items.INGOTS_COPPER),
						Ingredient.of(ModItemTags.INGOTS_ZINC)
				}, new ItemStack(ModItems.BRASS_INGOT.get(), 4))
				.unlockedBy("has_copper_ingot", has(Tags.Items.INGOTS_COPPER))
				.unlockedBy("has_zinc_ingot", has(ModItemTags.INGOTS_ZINC))
				.save(recipeOutput, "brass_ingot_from_ingot");
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(Tags.Items.RAW_MATERIALS_IRON),
						Ingredient.of(Tags.Items.RAW_MATERIALS_IRON),
						Ingredient.of(ModItemTags.DUSTS_COAL),
						Ingredient.of(ModItemTags.DUSTS_COAL)
				}, new ItemStack(ModItems.STEEL_INGOT.get(), 2))
				.unlockedBy("has_raw_iron", has(Tags.Items.RAW_MATERIALS_IRON))
				.unlockedBy("has_coal_dust", has(ModItemTags.DUSTS_COAL))
				.save(recipeOutput);
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(Tags.Items.ORES_IRON),
						Ingredient.of(Tags.Items.ORES_IRON),
						Ingredient.of(ModItemTags.DUSTS_COAL),
						Ingredient.of(ModItemTags.DUSTS_COAL)
				}, new ItemStack(ModItems.STEEL_INGOT.get(), 2))
				.unlockedBy("has_iron_ore", has(Tags.Items.ORES_IRON))
				.unlockedBy("has_coal_dust", has(ModItemTags.DUSTS_COAL))
				.save(recipeOutput, "steel_ingot_from_ore");
		
		ElectricSmelterRecipeBuilder.electricSmelter(new Ingredient[] {
						Ingredient.of(Tags.Items.INGOTS_IRON),
						Ingredient.of(Tags.Items.INGOTS_IRON),
						Ingredient.of(ModItemTags.DUSTS_COAL),
						Ingredient.of(ModItemTags.DUSTS_COAL)
				}, new ItemStack(ModItems.STEEL_INGOT.get(), 2))
				.unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
				.unlockedBy("has_coal_dust", has(ModItemTags.DUSTS_COAL))
				.save(recipeOutput, "steel_ingot_from_ingot");
		
		MillstoneRecipeBuilder.millstone(Ingredient.of(Tags.Items.BONES), new ItemStack(Items.BONE_MEAL, 3))
				.unlockedBy("has_bones", has(Tags.Items.BONES))
				.save(recipeOutput);
		
		MillstoneRecipeBuilder.millstone(Ingredient.of(Blocks.SUGAR_CANE), new ItemStack(Items.SUGAR, 2))
				.unlockedBy(getHasName(Blocks.SUGAR_CANE), has(Blocks.SUGAR_CANE))
				.save(recipeOutput);
		
		MillstoneRecipeBuilder.millstone(Ingredient.of(Items.COAL), new ItemStack(ModItems.PULVERISED_COAL.get()))
				.unlockedBy(getHasName(Items.COAL), has(Items.COAL))
				.save(recipeOutput);
		
		MillstoneRecipeBuilder.millstone(Ingredient.of(Tags.Items.COBBLESTONE), new ItemStack(Blocks.GRAVEL), 600)
				.unlockedBy("has_cobblestone", has(Tags.Items.COBBLESTONE))
				.save(recipeOutput);
		
		MillstoneRecipeBuilder.millstone(Ingredient.of(Tags.Items.GRAVEL), new ItemStack(Blocks.SAND), 500)
				.unlockedBy("has_gravel", has(Tags.Items.GRAVEL))
				.save(recipeOutput);
		
		ChoppingBlockRecipeBuilder.choppingBlock(Ingredient.of(ItemTags.WOODEN_SLABS), new ItemStack(Items.STICK))
				.unlockedBy("has_wooden_slab", has(ItemTags.WOODEN_SLABS))
				.save(recipeOutput);
		
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.OAK_CHOPPING_BLOCK.get(), ItemTags.OAK_LOGS, Blocks.OAK_PLANKS, Blocks.OAK_SLAB);
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.BIRCH_CHOPPING_BLOCK.get(), ItemTags.BIRCH_LOGS, Blocks.BIRCH_PLANKS, Blocks.BIRCH_SLAB);
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.SPRUCE_CHOPPING_BLOCK.get(), ItemTags.SPRUCE_LOGS, Blocks.SPRUCE_PLANKS, Blocks.SPRUCE_SLAB);
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.JUNGLE_CHOPPING_BLOCK.get(), ItemTags.JUNGLE_LOGS, Blocks.JUNGLE_PLANKS, Blocks.JUNGLE_SLAB);
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.DARK_OAK_CHOPPING_BLOCK.get(), ItemTags.DARK_OAK_LOGS, Blocks.DARK_OAK_PLANKS, Blocks.DARK_OAK_SLAB);
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.ACACIA_CHOPPING_BLOCK.get(), ItemTags.ACACIA_LOGS, Blocks.ACACIA_PLANKS, Blocks.ACACIA_SLAB);
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.MANGROVE_CHOPPING_BLOCK.get(), ItemTags.MANGROVE_LOGS, Blocks.MANGROVE_PLANKS, Blocks.MANGROVE_SLAB);
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.CHERRY_CHOPPING_BLOCK.get(), ItemTags.CHERRY_LOGS, Blocks.CHERRY_PLANKS, Blocks.CHERRY_SLAB);
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.CRIMSON_CHOPPING_BLOCK.get(), ItemTags.CRIMSON_STEMS, Blocks.CRIMSON_PLANKS, Blocks.CRIMSON_SLAB);
		woodChoppingBlockRecipes(recipeOutput, ModBlocks.WARPED_CHOPPING_BLOCK.get(), ItemTags.WARPED_STEMS, Blocks.WARPED_PLANKS, Blocks.WARPED_SLAB);
		
		DryingRackRecipeBuilder.dryingRack(Ingredient.of(ModItems.PREPARED_HIDE.get()), Items.LEATHER)
				.unlockedBy(getHasName(ModItems.PREPARED_HIDE.get()), has(ModItems.PREPARED_HIDE.get()))
				.save(recipeOutput);
		
		DryingRackRecipeBuilder.dryingRack(Ingredient.of(Items.BEEF), ModItems.BEEF_JERKY.get())
				.unlockedBy(getHasName(Items.BEEF), has(Items.BEEF))
				.save(recipeOutput);
		
		DryingRackRecipeBuilder.dryingRack(Ingredient.of(Items.ROTTEN_FLESH), ModItems.PURIFIED_MEAT.get())
				.unlockedBy(getHasName(Items.ROTTEN_FLESH), has(Items.ROTTEN_FLESH))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(Items.WHEAT_SEEDS), new ItemStack(Items.WHEAT, 2), SoilType.DIRT)
				.unlockedBy(getHasName(Items.WHEAT_SEEDS), has(Items.WHEAT_SEEDS))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(Items.CARROT), new ItemStack(Items.CARROT, 3), SoilType.DIRT)
				.unlockedBy(getHasName(Items.CARROT), has(Items.CARROT))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(Items.POTATO), new ItemStack(Items.POTATO, 3), SoilType.DIRT)
				.unlockedBy(getHasName(Items.POTATO), has(Items.POTATO))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(Items.BEETROOT_SEEDS), new ItemStack(Items.BEETROOT, 2), SoilType.DIRT)
				.unlockedBy(getHasName(Items.BEETROOT_SEEDS), has(Items.BEETROOT_SEEDS))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(Items.SWEET_BERRIES), new ItemStack(Items.SWEET_BERRIES, 3), SoilType.DIRT)
				.unlockedBy(getHasName(Items.SWEET_BERRIES), has(Items.SWEET_BERRIES))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(Items.NETHER_WART), new ItemStack(Items.NETHER_WART, 3), SoilType.SOULSAND)
				.unlockedBy(getHasName(Items.NETHER_WART), has(Items.NETHER_WART))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(ModItems.TOMATO_SEEDS.get()), new ItemStack(ModItems.TOMATO.get(), 2), SoilType.DIRT)
				.unlockedBy(getHasName(ModItems.TOMATO_SEEDS.get()), has(ModItems.TOMATO_SEEDS.get()))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(ModItems.GRAPE_SEEDS.get()), new ItemStack(ModItems.GRAPE.get(), 2), SoilType.DIRT)
				.unlockedBy(getHasName(ModItems.GRAPE_SEEDS.get()), has(ModItems.GRAPE_SEEDS.get()))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(ModItems.STRAWBERRY_SEEDS.get()), new ItemStack(ModItems.STRAWBERRY.get(), 2), SoilType.DIRT)
				.unlockedBy(getHasName(ModItems.STRAWBERRY_SEEDS.get()), has(ModItems.STRAWBERRY_SEEDS.get()))
				.save(recipeOutput);
		
		// TODO: greenhouse recipe for corn (when greenhouse supports double crops)
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(ModItems.LETTUCE_SEEDS.get()), new ItemStack(ModItems.LETTUCE.get(), 2), SoilType.DIRT)
				.unlockedBy(getHasName(ModItems.LETTUCE_SEEDS.get()), has(ModItems.LETTUCE_SEEDS.get()))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(ModItems.ONION_SEEDS.get()), new ItemStack(ModItems.ONION.get(), 2), SoilType.DIRT)
				.unlockedBy(getHasName(ModItems.ONION_SEEDS.get()), has(ModItems.ONION_SEEDS.get()))
				.save(recipeOutput);
		
		GreenhouseRecipeBuilder.greenhouse(Ingredient.of(ModItems.RICE.get()), new ItemStack(ModItems.RICE.get(), 3), SoilType.DIRT)
				.unlockedBy(getHasName(ModItems.RICE.get()), has(ModItems.RICE.get()))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(ModItemTags.INGOTS_UNIM), new ItemStack(ModItems.UNIM_PLATE.get()))
				.unlockedBy("has_unim_ingot", has(ModItemTags.INGOTS_UNIM))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(ModItems.IRON_PLATE.get()), Ingredient.of(ModItemTags.GEMS_RUBY), new ItemStack(ModItems.RUBY_PLATE.get()))
				.unlockedBy("has_ruby", has(ModItemTags.GEMS_RUBY))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(ModItemTags.INGOTS_ASCABIT), new ItemStack(ModItems.ASCABIT_PLATE.get()))
				.unlockedBy("has_ascabit_ingot", has(ModItemTags.INGOTS_ASCABIT))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(ModItemTags.INGOTS_TIN), new ItemStack(ModItems.TIN_PLATE.get()))
				.unlockedBy("has_tin_ingot", has(ModItemTags.INGOTS_TIN))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(ModItemTags.INGOTS_TITANIUM), new ItemStack(ModItems.TITANIUM_PLATE.get()))
				.unlockedBy("has_titanium_ingot", has(ModItemTags.INGOTS_TITANIUM))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(ModItemTags.INGOTS_ZINC), new ItemStack(ModItems.ZINC_PLATE.get()))
				.unlockedBy("has_zinc_ingot", has(ModItemTags.INGOTS_ZINC))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(ModItemTags.INGOTS_STEEL), new ItemStack(ModItems.STEEL_PLATE.get()))
				.unlockedBy("has_steel_ingot", has(ModItemTags.INGOTS_STEEL))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(ModItemTags.INGOTS_BRONZE), new ItemStack(ModItems.BRONZE_PLATE.get()))
				.unlockedBy("has_bronze_ingot", has(ModItemTags.INGOTS_BRONZE))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(Tags.Items.INGOTS_IRON), new ItemStack(ModItems.IRON_PLATE.get()))
				.unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(Tags.Items.INGOTS_COPPER), new ItemStack(ModItems.COPPER_PLATE.get()))
				.unlockedBy("has_copper_ingot", has(Tags.Items.INGOTS_COPPER))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(Tags.Items.INGOTS_GOLD), new ItemStack(ModItems.GOLD_PLATE.get()))
				.unlockedBy("has_gold_ingot", has(Tags.Items.INGOTS_GOLD))
				.save(recipeOutput);
		
		HydraulicPressRecipeBuilder.press(Ingredient.of(ModItems.IRON_PLATE.get()), Ingredient.of(Tags.Items.GEMS_DIAMOND), new ItemStack(ModItems.DIAMOND_PLATE.get()))
				.unlockedBy("has_diamond", has(Tags.Items.GEMS_DIAMOND))
				.save(recipeOutput);
	}
}
