package de.boy132.minecraftcontentexpansion.datagen.recipe.condition;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.boy132.minecraftcontentexpansion.config.CraftingMode;
import de.boy132.minecraftcontentexpansion.config.ModConfigValues;
import net.minecraftforge.common.crafting.conditions.ICondition;

public record ModuleSettingCondition(String module, String setting) implements ICondition
{
	public static final ModuleSettingCondition STONE_AGE_REMOVE_WOODEN_TOOL_RECIPES = new ModuleSettingCondition("stoneAge", "removeWoodenToolRecipes");
	
	public static final ModuleSettingCondition STONE_AGE_TOOL_CRAFTING_MODE_VANILLA = new ModuleSettingCondition("stoneAge", "toolCraftingModeVanilla");
	public static final ModuleSettingCondition STONE_AGE_TOOL_CRAFTING_MODE_ADVANCED = new ModuleSettingCondition("stoneAge", "toolCraftingModeAdvanced");
	
	public static final ModuleSettingCondition STONE_AGE_WOOD_CRAFTING_MODE_VANILLA = new ModuleSettingCondition("stoneAge", "woodCraftingModeVanilla");
	public static final ModuleSettingCondition STONE_AGE_WOOD_CRAFTING_MODE_ADVANCED = new ModuleSettingCondition("stoneAge", "woodCraftingModeAdvanced");
	
	public static final ModuleSettingCondition METAL_AGE_ARMOR_CRAFTING_MODE_VANILLA = new ModuleSettingCondition("metalAge", "armorCraftingModeVanilla");
	public static final ModuleSettingCondition METAL_AGE_ARMOR_CRAFTING_MODE_ADVANCED = new ModuleSettingCondition("metalAge", "armorCraftingModeAdvanced");
	
	public static final Codec<ModuleSettingCondition> CODEC = RecordCodecBuilder.create(instance -> instance.group(Codec.STRING.fieldOf("module").forGetter(condition -> condition.module), Codec.STRING.fieldOf("setting").forGetter(condition -> condition.setting)).apply(instance, ModuleSettingCondition::new));
	
	@Override
	public Codec<? extends ICondition> codec()
	{
		return CODEC;
	}
	
	@Override
	public boolean test(IContext context)
	{
		if(module.equalsIgnoreCase("stoneAge"))
		{
			if(ModConfigValues.modules_stoneAge.get())
			{
				if(setting.equalsIgnoreCase("removeWoodenToolRecipes"))
					return ModConfigValues.stoneAge_removeWoodenToolRecipes.get();
				
				if(setting.equalsIgnoreCase("toolCraftingModeVanilla"))
					return ModConfigValues.stoneAge_toolCraftingMode.get() == CraftingMode.VANILLA;
				
				if(setting.equalsIgnoreCase("toolCraftingModeAdvanced"))
					return ModConfigValues.stoneAge_toolCraftingMode.get() == CraftingMode.ADVANCED;
				
				if(setting.equalsIgnoreCase("woodCraftingModeVanilla"))
					return ModConfigValues.stoneAge_woodCraftingMode.get() == CraftingMode.VANILLA;
				
				if(setting.equalsIgnoreCase("woodCraftingModeAdvanced"))
					return ModConfigValues.stoneAge_woodCraftingMode.get() == CraftingMode.ADVANCED;
			}
		}
		
		if(module.equalsIgnoreCase("metalAge"))
		{
			if(ModConfigValues.modules_metalAge.get())
			{
				if(setting.equalsIgnoreCase("armorCraftingModeVanilla"))
					return ModConfigValues.metalAge_armorCraftingMode.get() == CraftingMode.VANILLA;
				
				if(setting.equalsIgnoreCase("armorCraftingModeAdvanced"))
					return ModConfigValues.metalAge_armorCraftingMode.get() == CraftingMode.ADVANCED;
			}
		}
		
		if(module.equalsIgnoreCase("industrialAge"))
			return ModConfigValues.modules_industrialAge.get();
		
		return false;
	}
	
	@Override
	public String toString()
	{
		return "module_setting(\"" + module + "\", \"" + setting + "\")";
	}
}