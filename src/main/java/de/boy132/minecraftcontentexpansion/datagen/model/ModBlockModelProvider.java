package de.boy132.minecraftcontentexpansion.datagen.model;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import org.jetbrains.annotations.NotNull;

public class ModBlockModelProvider extends BlockModelProvider
{
	public ModBlockModelProvider(PackOutput packOutput, ExistingFileHelper existingFileHelper)
	{
		super(packOutput, MinecraftContentExpansion.MODID, existingFileHelper);
	}
	
	@Override
	protected void registerModels()
	{
		wallInventory(ForgeRegistries.BLOCKS.getKey(ModBlocks.CEMENT_WALL.get()).getPath() + "_inventory", ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/cement"));
		wallInventory(ForgeRegistries.BLOCKS.getKey(ModBlocks.CEMENT_PLATE_WALL.get()).getPath() + "_inventory", ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/cement_plate"));
		
		wallInventory(ForgeRegistries.BLOCKS.getKey(ModBlocks.LIMESTONE_WALL.get()).getPath() + "_inventory", ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/limestone"));
		wallInventory(ForgeRegistries.BLOCKS.getKey(ModBlocks.LIMESTONE_BRICK_WALL.get()).getPath() + "_inventory", ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/limestone_bricks"));
		
		wallInventory(ForgeRegistries.BLOCKS.getKey(ModBlocks.MARBLE_WALL.get()).getPath() + "_inventory", ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/marble"));
		wallInventory(ForgeRegistries.BLOCKS.getKey(ModBlocks.MARBLE_COBBLE_WALL.get()).getPath() + "_inventory", ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/marble_cobble"));
		wallInventory(ForgeRegistries.BLOCKS.getKey(ModBlocks.MARBLE_BRICK_WALL.get()).getPath() + "_inventory", ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/marble_bricks"));
		
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.OAK_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/oak_log"), ResourceLocation.parse("block/oak_log_top"), ResourceLocation.parse("block/oak_log_top"));
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.SPRUCE_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/spruce_log"), ResourceLocation.parse("block/spruce_log_top"), ResourceLocation.parse("block/spruce_log_top"));
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.BIRCH_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/birch_log"), ResourceLocation.parse("block/birch_log_top"), ResourceLocation.parse("block/birch_log_top"));
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.JUNGLE_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/jungle_log"), ResourceLocation.parse("block/jungle_log_top"), ResourceLocation.parse("block/jungle_log_top"));
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.ACACIA_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/acacia_log"), ResourceLocation.parse("block/acacia_log_top"), ResourceLocation.parse("block/acacia_log_top"));
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.DARK_OAK_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/dark_oak_log"), ResourceLocation.parse("block/dark_oak_log_top"), ResourceLocation.parse("block/dark_oak_log_top"));
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.MANGROVE_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/mangrove_log"), ResourceLocation.parse("block/mangrove_log_top"), ResourceLocation.parse("block/mangrove_log_top"));
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.CHERRY_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/cherry_log"), ResourceLocation.parse("block/cherry_log_top"), ResourceLocation.parse("block/cherry_log_top"));
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.CRIMSON_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/crimson_stem"), ResourceLocation.parse("block/crimson_stem_top"), ResourceLocation.parse("block/crimson_stem_top"));
		slab(ForgeRegistries.BLOCKS.getKey(ModBlocks.WARPED_CHOPPING_BLOCK.get()).getPath(), ResourceLocation.parse("block/warped_stem"), ResourceLocation.parse("block/warped_stem_top"), ResourceLocation.parse("block/warped_stem_top"));
	}
	
	@Override
	public @NotNull String getName()
	{
		return "Mod Block Models";
	}
}
