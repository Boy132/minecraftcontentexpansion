package de.boy132.minecraftcontentexpansion.datagen.model;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.Objects;

public abstract class BaseItemModelProvider extends ItemModelProvider
{
	public BaseItemModelProvider(PackOutput packOutput, ExistingFileHelper existingFileHelper)
	{
		super(packOutput, MinecraftContentExpansion.MODID, existingFileHelper);
	}
	
	protected abstract void registerItemModels();
	
	protected abstract void registerBlockModels();
	
	@Override
	protected void registerModels()
	{
		registerItemModels();
		registerBlockModels();
	}
	
	@Override
	public String getName()
	{
		return "Mod Item Models";
	}
	
	public ItemModelBuilder handheldItem(Item item)
	{
		return handheldItem(Objects.requireNonNull(ForgeRegistries.ITEMS.getKey(item)));
	}
	
	public ItemModelBuilder handheldItem(ResourceLocation item)
	{
		return getBuilder(item.toString()).parent(new ModelFile.UncheckedModelFile("item/handheld")).texture("layer0", ResourceLocation.fromNamespaceAndPath(item.getNamespace(), "item/" + item.getPath()));
	}
	
	protected void basicBlock(Block block)
	{
		ResourceLocation name = ForgeRegistries.BLOCKS.getKey(block);
		if(name != null)
			withExistingParent(name.getPath(), ResourceLocation.fromNamespaceAndPath(name.getNamespace(), BLOCK_FOLDER + "/" + name.getPath()));
	}
}
