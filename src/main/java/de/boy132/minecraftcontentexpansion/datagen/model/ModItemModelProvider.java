package de.boy132.minecraftcontentexpansion.datagen.model;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ModItemModelProvider extends BaseItemModelProvider
{
	public ModItemModelProvider(PackOutput packOutput, ExistingFileHelper existingFileHelper)
	{
		super(packOutput, existingFileHelper);
	}
	
	@Override
	protected void registerItemModels()
	{
		basicItem(ModItems.UNIM.get());
		basicItem(ModItems.UNIM_INGOT.get());
		
		basicItem(ModItems.RUBY.get());
		
		basicItem(ModItems.ASCABIT_INGOT.get());
		basicItem(ModItems.RAW_ASCABIT.get());
		
		basicItem(ModItems.TIN_INGOT.get());
		basicItem(ModItems.RAW_TIN.get());
		
		basicItem(ModItems.BRONZE_INGOT.get());
		
		basicItem(ModItems.TITANIUM_INGOT.get());
		basicItem(ModItems.RAW_TITANIUM.get());
		
		basicItem(ModItems.STEEL_FRAGMENTS.get());
		basicItem(ModItems.STEEL_INGOT.get());
		
		basicItem(ModItems.ZINC_INGOT.get());
		basicItem(ModItems.RAW_ZINC.get());
		
		basicItem(ModItems.BRASS_INGOT.get());
		
		basicItem(ModItems.PULVERISED_COAL.get());
		
		basicItem(ModItems.UNIM_PLATE.get());
		basicItem(ModItems.RUBY_PLATE.get());
		basicItem(ModItems.ASCABIT_PLATE.get());
		basicItem(ModItems.TIN_PLATE.get());
		basicItem(ModItems.TITANIUM_PLATE.get());
		basicItem(ModItems.ZINC_PLATE.get());
		basicItem(ModItems.STEEL_PLATE.get());
		basicItem(ModItems.BRONZE_PLATE.get());
		
		basicItem(ModItems.IRON_PLATE.get());
		basicItem(ModItems.COPPER_PLATE.get());
		basicItem(ModItems.GOLD_PLATE.get());
		basicItem(ModItems.DIAMOND_PLATE.get());
		
		basicItem(ModItems.SMALL_BACKPACK.get());
		basicItem(ModItems.LARGE_BACKPACK.get());
		basicItem(ModItems.ENDER_BACKPACK.get());
		
		basicItem(ModItems.PLANT_FIBER.get());
		basicItem(ModItems.TREE_BARK.get());
		basicItem(ModItems.ANIMAL_HIDE.get());
		basicItem(ModItems.PREPARED_HIDE.get());
		basicItem(ModItems.ANIMAL_FAT.get());
		basicItem(ModItems.DOUGH.get());
		basicItem(ModItems.CHAIN.get());
		basicItem(ModItems.LEATHER_STRIP.get());
		basicItem(ModItems.CEMENT_MIXTURE.get());
		
		basicItem(ModItems.ROCK.get());
		basicItem(ModItems.ANDESITE_ROCK.get());
		basicItem(ModItems.DIORITE_ROCK.get());
		basicItem(ModItems.GRANITE_ROCK.get());
		
		basicItem(ModItems.RAW_BACON.get());
		basicItem(ModItems.COOKED_BACON.get());
		basicItem(ModItems.FRIED_EGG.get());
		basicItem(ModItems.CHEESE.get());
		basicItem(ModItems.PIZZA.get());
		basicItem(ModItems.CHOCOLATE.get());
		basicItem(ModItems.POPCORN.get());
		basicItem(ModItems.PURIFIED_MEAT.get());
		basicItem(ModItems.BEEF_JERKY.get());
		basicItem(ModItems.ONION_RINGS.get());
		basicItem(ModItems.SAUSAGE.get());
		basicItem(ModItems.HOT_DOG.get());
		basicItem(ModItems.GARDEN_SALAD.get());
		basicItem(ModItems.HAMBURGER.get());
		basicItem(ModItems.CHEESEBURGER.get());
		basicItem(ModItems.SUSHI.get());
		
		basicItem(ModItems.APPLE_PIE.get());
		basicItem(ModItems.STRAWBERRY_PIE.get());
		
		basicItem(ModItems.TOMATO_SEEDS.get());
		basicItem(ModItems.TOMATO.get());
		
		basicItem(ModItems.GRAPE_SEEDS.get());
		basicItem(ModItems.GRAPE.get());
		
		basicItem(ModItems.STRAWBERRY_SEEDS.get());
		basicItem(ModItems.STRAWBERRY.get());
		
		basicItem(ModItems.CORN_SEEDS.get());
		basicItem(ModItems.CORN.get());
		
		basicItem(ModItems.LETTUCE_SEEDS.get());
		basicItem(ModItems.LETTUCE.get());
		
		basicItem(ModItems.ONION_SEEDS.get());
		basicItem(ModItems.ONION.get());
		
		basicItem(ModItems.RICE.get());
		
		basicItem(ModItems.VEGETABLE_SOUP.get());
		basicItem(ModItems.PUMPKIN_SOUP.get());
		basicItem(ModItems.CACTUS_SOUP.get());
		basicItem(ModItems.FISH_SOUP.get());
		
		basicItem(ModItems.UNFIRED_CLAY_BUCKET.get());
		basicItem(ModItems.CLAY_BUCKET.get());
		basicItem(ModItems.WATER_CLAY_BUCKET.get());
		
		basicItem(ModItems.PORTABLE_BATTERY.get());
		
		basicItem(ModItems.REDSTONE_BLEND.get());
		basicItem(ModItems.SOLAR_CELL.get());
		
		basicItem(ModItems.STONE_DOOR.get());
		basicItem(ModItems.SANDSTONE_DOOR.get());
		basicItem(ModItems.GLASS_DOOR.get());
		
		basicItem(ModItems.OIL_BUCKET.get());
		
		handheldItem(ModItems.STEEL_DRILL.get());
		handheldItem(ModItems.UNIM_DRILL.get());
		handheldItem(ModItems.DIAMOND_DRILL.get());
		
		handheldItem(ModItems.FLINT_HATCHET.get());
		
		basicItem(ModItems.FLINT_SHEARS.get());
		
		handheldItem(ModItems.FLINT_KNIFE.get());
		handheldItem(ModItems.STONE_KNIFE.get());
		handheldItem(ModItems.IRON_KNIFE.get());
		handheldItem(ModItems.UNIM_KNIFE.get());
		handheldItem(ModItems.DIAMOND_KNIFE.get());
		
		handheldItem(ModItems.IRON_HAMMER.get());
		handheldItem(ModItems.BRONZE_HAMMER.get());
		handheldItem(ModItems.STEEL_HAMMER.get());
		handheldItem(ModItems.DIAMOND_HAMMER.get());
		
		handheldItem(ModItems.UNIM_SWORD.get());
		handheldItem(ModItems.UNIM_PICKAXE.get());
		handheldItem(ModItems.UNIM_AXE.get());
		handheldItem(ModItems.UNIM_SHOVEL.get());
		handheldItem(ModItems.UNIM_HOE.get());
		
		handheldItem(ModItems.RUBY_SWORD.get());
		handheldItem(ModItems.RUBY_PICKAXE.get());
		handheldItem(ModItems.RUBY_AXE.get());
		handheldItem(ModItems.RUBY_SHOVEL.get());
		handheldItem(ModItems.RUBY_HOE.get());
		
		handheldItem(ModItems.ASCABIT_SWORD.get());
		handheldItem(ModItems.ASCABIT_PICKAXE.get());
		handheldItem(ModItems.ASCABIT_AXE.get());
		handheldItem(ModItems.ASCABIT_SHOVEL.get());
		handheldItem(ModItems.ASCABIT_HOE.get());
		
		handheldItem(ModItems.COPPER_SWORD.get());
		handheldItem(ModItems.COPPER_PICKAXE.get());
		handheldItem(ModItems.COPPER_AXE.get());
		handheldItem(ModItems.COPPER_SHOVEL.get());
		handheldItem(ModItems.COPPER_HOE.get());
		
		handheldItem(ModItems.TIN_SWORD.get());
		handheldItem(ModItems.TIN_PICKAXE.get());
		handheldItem(ModItems.TIN_AXE.get());
		handheldItem(ModItems.TIN_SHOVEL.get());
		handheldItem(ModItems.TIN_HOE.get());
		
		handheldItem(ModItems.TITANIUM_SWORD.get());
		handheldItem(ModItems.TITANIUM_PICKAXE.get());
		handheldItem(ModItems.TITANIUM_AXE.get());
		handheldItem(ModItems.TITANIUM_SHOVEL.get());
		handheldItem(ModItems.TITANIUM_HOE.get());
		
		handheldItem(ModItems.ZINC_SWORD.get());
		handheldItem(ModItems.ZINC_PICKAXE.get());
		handheldItem(ModItems.ZINC_AXE.get());
		handheldItem(ModItems.ZINC_SHOVEL.get());
		handheldItem(ModItems.ZINC_HOE.get());
		
		handheldItem(ModItems.STEEL_SWORD.get());
		handheldItem(ModItems.STEEL_PICKAXE.get());
		handheldItem(ModItems.STEEL_AXE.get());
		handheldItem(ModItems.STEEL_SHOVEL.get());
		handheldItem(ModItems.STEEL_HOE.get());
		
		handheldItem(ModItems.BRONZE_SWORD.get());
		handheldItem(ModItems.BRONZE_PICKAXE.get());
		handheldItem(ModItems.BRONZE_AXE.get());
		handheldItem(ModItems.BRONZE_SHOVEL.get());
		handheldItem(ModItems.BRONZE_HOE.get());
		
		basicItem(ModItems.DRILL_BASE.get());
		basicItem(ModItems.STEEL_DRILL_HEAD.get());
		basicItem(ModItems.UNIM_DRILL_HEAD.get());
		basicItem(ModItems.DIAMOND_DRILL_HEAD.get());
		
		basicItem(ModItems.FLINT_HATCHET_HEAD.get());
		
		basicItem(ModItems.FLINT_KNIFE_BLADE.get());
		basicItem(ModItems.STONE_KNIFE_BLADE.get());
		basicItem(ModItems.IRON_KNIFE_BLADE.get());
		basicItem(ModItems.UNIM_KNIFE_BLADE.get());
		basicItem(ModItems.DIAMOND_KNIFE_BLADE.get());
		
		basicItem(ModItems.IRON_HAMMER_HEAD.get());
		basicItem(ModItems.BRONZE_HAMMER_HEAD.get());
		basicItem(ModItems.STEEL_HAMMER_HEAD.get());
		basicItem(ModItems.DIAMOND_HAMMER_HEAD.get());
		
		basicItem(ModItems.STONE_SWORD_BLADE.get());
		basicItem(ModItems.STONE_PICKAXE_HEAD.get());
		basicItem(ModItems.STONE_AXE_HEAD.get());
		basicItem(ModItems.STONE_SHOVEL_HEAD.get());
		basicItem(ModItems.STONE_HOE_HEAD.get());
		
		basicItem(ModItems.IRON_SWORD_BLADE.get());
		basicItem(ModItems.IRON_PICKAXE_HEAD.get());
		basicItem(ModItems.IRON_AXE_HEAD.get());
		basicItem(ModItems.IRON_SHOVEL_HEAD.get());
		basicItem(ModItems.IRON_HOE_HEAD.get());
		
		basicItem(ModItems.GOLDEN_SWORD_BLADE.get());
		basicItem(ModItems.GOLDEN_PICKAXE_HEAD.get());
		basicItem(ModItems.GOLDEN_AXE_HEAD.get());
		basicItem(ModItems.GOLDEN_SHOVEL_HEAD.get());
		basicItem(ModItems.GOLDEN_HOE_HEAD.get());
		
		basicItem(ModItems.UNIM_SWORD_BLADE.get());
		basicItem(ModItems.UNIM_PICKAXE_HEAD.get());
		basicItem(ModItems.UNIM_AXE_HEAD.get());
		basicItem(ModItems.UNIM_SHOVEL_HEAD.get());
		basicItem(ModItems.UNIM_HOE_HEAD.get());
		
		basicItem(ModItems.RUBY_SWORD_BLADE.get());
		basicItem(ModItems.RUBY_PICKAXE_HEAD.get());
		basicItem(ModItems.RUBY_AXE_HEAD.get());
		basicItem(ModItems.RUBY_SHOVEL_HEAD.get());
		basicItem(ModItems.RUBY_HOE_HEAD.get());
		
		basicItem(ModItems.ASCABIT_SWORD_BLADE.get());
		basicItem(ModItems.ASCABIT_PICKAXE_HEAD.get());
		basicItem(ModItems.ASCABIT_AXE_HEAD.get());
		basicItem(ModItems.ASCABIT_SHOVEL_HEAD.get());
		basicItem(ModItems.ASCABIT_HOE_HEAD.get());
		
		basicItem(ModItems.COPPER_SWORD_BLADE.get());
		basicItem(ModItems.COPPER_PICKAXE_HEAD.get());
		basicItem(ModItems.COPPER_AXE_HEAD.get());
		basicItem(ModItems.COPPER_SHOVEL_HEAD.get());
		basicItem(ModItems.COPPER_HOE_HEAD.get());
		
		basicItem(ModItems.TIN_SWORD_BLADE.get());
		basicItem(ModItems.TIN_PICKAXE_HEAD.get());
		basicItem(ModItems.TIN_AXE_HEAD.get());
		basicItem(ModItems.TIN_SHOVEL_HEAD.get());
		basicItem(ModItems.TIN_HOE_HEAD.get());
		
		basicItem(ModItems.TITANIUM_SWORD_BLADE.get());
		basicItem(ModItems.TITANIUM_PICKAXE_HEAD.get());
		basicItem(ModItems.TITANIUM_AXE_HEAD.get());
		basicItem(ModItems.TITANIUM_SHOVEL_HEAD.get());
		basicItem(ModItems.TITANIUM_HOE_HEAD.get());
		
		basicItem(ModItems.ZINC_SWORD_BLADE.get());
		basicItem(ModItems.ZINC_PICKAXE_HEAD.get());
		basicItem(ModItems.ZINC_AXE_HEAD.get());
		basicItem(ModItems.ZINC_SHOVEL_HEAD.get());
		basicItem(ModItems.ZINC_HOE_HEAD.get());
		
		basicItem(ModItems.STEEL_SWORD_BLADE.get());
		basicItem(ModItems.STEEL_PICKAXE_HEAD.get());
		basicItem(ModItems.STEEL_AXE_HEAD.get());
		basicItem(ModItems.STEEL_SHOVEL_HEAD.get());
		basicItem(ModItems.STEEL_HOE_HEAD.get());
		
		basicItem(ModItems.BRONZE_SWORD_BLADE.get());
		basicItem(ModItems.BRONZE_PICKAXE_HEAD.get());
		basicItem(ModItems.BRONZE_AXE_HEAD.get());
		basicItem(ModItems.BRONZE_SHOVEL_HEAD.get());
		basicItem(ModItems.BRONZE_HOE_HEAD.get());
		
		basicItem(ModItems.UNIM_HELMET.get());
		basicItem(ModItems.UNIM_CHESTPLATE.get());
		basicItem(ModItems.UNIM_LEGGINGS.get());
		basicItem(ModItems.UNIM_BOOTS.get());
		
		basicItem(ModItems.RUBY_HELMET.get());
		basicItem(ModItems.RUBY_CHESTPLATE.get());
		basicItem(ModItems.RUBY_LEGGINGS.get());
		basicItem(ModItems.RUBY_BOOTS.get());
		
		basicItem(ModItems.ASCABIT_HELMET.get());
		basicItem(ModItems.ASCABIT_CHESTPLATE.get());
		basicItem(ModItems.ASCABIT_LEGGINGS.get());
		basicItem(ModItems.ASCABIT_BOOTS.get());
		
		basicItem(ModItems.COPPER_HELMET.get());
		basicItem(ModItems.COPPER_CHESTPLATE.get());
		basicItem(ModItems.COPPER_LEGGINGS.get());
		basicItem(ModItems.COPPER_BOOTS.get());
		
		basicItem(ModItems.TIN_HELMET.get());
		basicItem(ModItems.TIN_CHESTPLATE.get());
		basicItem(ModItems.TIN_LEGGINGS.get());
		basicItem(ModItems.TIN_BOOTS.get());
		
		basicItem(ModItems.TITANIUM_HELMET.get());
		basicItem(ModItems.TITANIUM_CHESTPLATE.get());
		basicItem(ModItems.TITANIUM_LEGGINGS.get());
		basicItem(ModItems.TITANIUM_BOOTS.get());
		
		basicItem(ModItems.ZINC_HELMET.get());
		basicItem(ModItems.ZINC_CHESTPLATE.get());
		basicItem(ModItems.ZINC_LEGGINGS.get());
		basicItem(ModItems.ZINC_BOOTS.get());
		
		basicItem(ModItems.BRONZE_HELMET.get());
		basicItem(ModItems.BRONZE_CHESTPLATE.get());
		basicItem(ModItems.BRONZE_LEGGINGS.get());
		basicItem(ModItems.BRONZE_BOOTS.get());
		
		basicItem(ModItems.STEEL_HELMET.get());
		basicItem(ModItems.STEEL_CHESTPLATE.get());
		basicItem(ModItems.STEEL_LEGGINGS.get());
		basicItem(ModItems.STEEL_BOOTS.get());
	}
	
	@Override
	protected void registerBlockModels()
	{
		basicBlock(ModBlocks.UNIM_ORE.get());
		basicBlock(ModBlocks.DEEPSLATE_UNIM_ORE.get());
		basicBlock(ModBlocks.UNIM_BLOCK.get());
		
		basicBlock(ModBlocks.RUBY_ORE.get());
		basicBlock(ModBlocks.DEEPSLATE_RUBY_ORE.get());
		basicBlock(ModBlocks.RUBY_BLOCK.get());
		
		basicBlock(ModBlocks.ASCABIT_ORE.get());
		basicBlock(ModBlocks.DEEPSLATE_ASCABIT_ORE.get());
		basicBlock(ModBlocks.ASCABIT_BLOCK.get());
		basicBlock(ModBlocks.RAW_ASCABIT_BLOCK.get());
		
		basicBlock(ModBlocks.TIN_ORE.get());
		basicBlock(ModBlocks.DEEPSLATE_TIN_ORE.get());
		basicBlock(ModBlocks.TIN_BLOCK.get());
		basicBlock(ModBlocks.RAW_TIN_BLOCK.get());
		
		basicBlock(ModBlocks.TITANIUM_ORE.get());
		basicBlock(ModBlocks.DEEPSLATE_TITANIUM_ORE.get());
		basicBlock(ModBlocks.TITANIUM_BLOCK.get());
		basicBlock(ModBlocks.RAW_TITANIUM_BLOCK.get());
		
		basicBlock(ModBlocks.ZINC_ORE.get());
		basicBlock(ModBlocks.DEEPSLATE_ZINC_ORE.get());
		basicBlock(ModBlocks.ZINC_BLOCK.get());
		basicBlock(ModBlocks.RAW_ZINC_BLOCK.get());
		
		basicBlock(ModBlocks.STEEL_BLOCK.get());
		
		basicBlock(ModBlocks.BRONZE_BLOCK.get());
		
		basicBlock(ModBlocks.BRASS_BLOCK.get());
		
		basicBlock(ModBlocks.CEMENT.get());
		basicBlock(ModBlocks.CEMENT_STAIRS.get());
		basicBlock(ModBlocks.CEMENT_SLAB.get());
		withExistingParent(ModBlocks.CEMENT_WALL.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/cement_wall_inventory"));
		
		basicBlock(ModBlocks.CEMENT_PLATE.get());
		basicBlock(ModBlocks.CEMENT_PLATE_STAIRS.get());
		basicBlock(ModBlocks.CEMENT_PLATE_SLAB.get());
		withExistingParent(ModBlocks.CEMENT_PLATE_WALL.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/cement_plate_wall_inventory"));
		
		basicBlock(ModBlocks.LIMESTONE.get());
		basicBlock(ModBlocks.LIMESTONE_STAIRS.get());
		basicBlock(ModBlocks.LIMESTONE_SLAB.get());
		withExistingParent(ModBlocks.LIMESTONE_WALL.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/limestone_wall_inventory"));
		
		basicBlock(ModBlocks.LIMESTONE_BRICKS.get());
		basicBlock(ModBlocks.LIMESTONE_BRICK_STAIRS.get());
		basicBlock(ModBlocks.LIMESTONE_BRICK_SLAB.get());
		withExistingParent(ModBlocks.LIMESTONE_BRICK_WALL.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/limestone_brick_wall_inventory"));
		
		basicBlock(ModBlocks.MARBLE.get());
		basicBlock(ModBlocks.MARBLE_STAIRS.get());
		basicBlock(ModBlocks.MARBLE_SLAB.get());
		withExistingParent(ModBlocks.MARBLE_WALL.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/marble_wall_inventory"));
		
		basicBlock(ModBlocks.MARBLE_COBBLE.get());
		basicBlock(ModBlocks.MARBLE_COBBLE_STAIRS.get());
		basicBlock(ModBlocks.MARBLE_COBBLE_SLAB.get());
		withExistingParent(ModBlocks.MARBLE_COBBLE_WALL.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/marble_cobble_wall_inventory"));
		
		basicBlock(ModBlocks.MARBLE_BRICKS.get());
		basicBlock(ModBlocks.MARBLE_BRICK_STAIRS.get());
		basicBlock(ModBlocks.MARBLE_BRICK_SLAB.get());
		withExistingParent(ModBlocks.MARBLE_BRICK_WALL.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/marble_brick_wall_inventory"));
		
		basicBlock(ModBlocks.MARBLE_LAMP.get());
		
		basicBlock(ModBlocks.DIRT_SLAB.get());
		basicBlock(ModBlocks.COARSE_DIRT_SLAB.get());
		basicBlock(ModBlocks.DIRT_PATH_SLAB.get());
		basicBlock(ModBlocks.GRASS_BLOCK_SLAB.get());
		
		basicBlock(ModBlocks.GLASS_SLAB.get());
		
		withExistingParent(ModBlocks.STONE_TRAPDOOR.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/stone_trapdoor_bottom"));
		withExistingParent(ModBlocks.SANDSTONE_TRAPDOOR.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/sandstone_trapdoor_bottom"));
		withExistingParent(ModBlocks.GLASS_TRAPDOOR.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/glass_trapdoor_bottom"));
		
		withExistingParent(ModBlocks.WOODEN_GLASS_FRAME.getId().getPath(), "item/generated").texture("layer0", ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/wooden_glass_frame"));
		
		basicBlock(ModBlocks.KILN.get());
		basicBlock(ModBlocks.SMELTER.get());
		basicBlock(ModBlocks.MILLSTONE.get());
		
		basicBlock(ModBlocks.OAK_CHOPPING_BLOCK.get());
		basicBlock(ModBlocks.SPRUCE_CHOPPING_BLOCK.get());
		basicBlock(ModBlocks.BIRCH_CHOPPING_BLOCK.get());
		basicBlock(ModBlocks.JUNGLE_CHOPPING_BLOCK.get());
		basicBlock(ModBlocks.ACACIA_CHOPPING_BLOCK.get());
		basicBlock(ModBlocks.DARK_OAK_CHOPPING_BLOCK.get());
		basicBlock(ModBlocks.MANGROVE_CHOPPING_BLOCK.get());
		basicBlock(ModBlocks.CHERRY_CHOPPING_BLOCK.get());
		basicBlock(ModBlocks.CRIMSON_CHOPPING_BLOCK.get());
		basicBlock(ModBlocks.WARPED_CHOPPING_BLOCK.get());
		
		basicBlock(ModBlocks.OAK_DRYING_RACK.get());
		basicBlock(ModBlocks.SPRUCE_DRYING_RACK.get());
		basicBlock(ModBlocks.BIRCH_DRYING_RACK.get());
		basicBlock(ModBlocks.JUNGLE_DRYING_RACK.get());
		basicBlock(ModBlocks.ACACIA_DRYING_RACK.get());
		basicBlock(ModBlocks.DARK_OAK_DRYING_RACK.get());
		basicBlock(ModBlocks.MANGROVE_DRYING_RACK.get());
		basicBlock(ModBlocks.CHERRY_DRYING_RACK.get());
		basicBlock(ModBlocks.CRIMSON_DRYING_RACK.get());
		basicBlock(ModBlocks.WARPED_DRYING_RACK.get());
		
		basicBlock(ModBlocks.MACHINE_FRAME.get());
		
		basicBlock(ModBlocks.COAL_GENERATOR.get());
		withExistingParent(ModBlocks.BATTERY.getId().getPath(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, BLOCK_FOLDER + "/battery_0"));
		basicBlock(ModBlocks.SOLAR_PANEL.get());
		
		basicBlock(ModBlocks.LIQUID_TANK.get());
		
		basicBlock(ModBlocks.ELECTRIC_BREWERY.get());
		basicBlock(ModBlocks.ELECTRIC_SMELTER.get());
		basicBlock(ModBlocks.ELECTRIC_GREENHOUSE.get());
		
		basicBlock(ModBlocks.HYDRAULIC_PRESS.get());
		
		basicItem(ModBlocks.COPPER_CABLE.getId());
		
		basicItem(ModBlocks.CHOCOLATE_CAKE.getId());
	}
}
