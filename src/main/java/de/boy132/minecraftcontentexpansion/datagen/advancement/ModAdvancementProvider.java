package de.boy132.minecraftcontentexpansion.datagen.advancement;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.tag.ModEntityTypeTags;
import de.boy132.minecraftcontentexpansion.tag.ModItemTags;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementType;
import net.minecraft.advancements.critereon.*;
import net.minecraft.core.HolderLookup;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.data.ForgeAdvancementProvider;

import java.util.function.Consumer;

public class ModAdvancementProvider implements ForgeAdvancementProvider.AdvancementGenerator
{
	@Override
	public void generate(HolderLookup.Provider registries, Consumer<AdvancementHolder> saver, ExistingFileHelper existingFileHelper)
	{
		stoneAgeAdvancements(saver);
		metalAgeAdvancements(saver);
		industrialAgeAdvancements(saver);
	}
	
	private void stoneAgeAdvancements(Consumer<AdvancementHolder> saver)
	{
		AdvancementHolder stoneAge = Advancement.Builder.advancement()
				.display(ModItems.ROCK.get(), Component.translatable("advancements.stoneage.root.title"), Component.translatable("advancements.stoneage.root.description"), ResourceLocation.parse("textures/block/stone.png"), AdvancementType.GOAL, true, false, false)
				.addCriterion("flint", InventoryChangeTrigger.TriggerInstance.hasItems(Items.FLINT))
				.addCriterion("plant_fiber", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.PLANT_FIBER.get()))
				.requirements(AdvancementRequirements.Strategy.OR)
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/root");
		
		AdvancementHolder torch = Advancement.Builder.advancement()
				.display(Blocks.TORCH, Component.translatable("advancements.stoneage.torch.title"), Component.translatable("advancements.stoneage.torch.description"), null, AdvancementType.TASK, true, true, false)
				.parent(stoneAge)
				.addCriterion("torch", InventoryChangeTrigger.TriggerInstance.hasItems(Blocks.TORCH))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/torch");
		
		AdvancementHolder choppingBlock = Advancement.Builder.advancement()
				.display(ModBlocks.OAK_CHOPPING_BLOCK.get(), Component.translatable("advancements.stoneage.choppingblock.title"), Component.translatable("advancements.stoneage.choppingblock.description"), null, AdvancementType.TASK, true, true, false)
				.parent(stoneAge)
				.addCriterion("chopping_blocks", InventoryChangeTrigger.TriggerInstance.hasItems(ItemPredicate.Builder.item().of(ModItemTags.CHOPPING_BLOCKS).build()))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/choppingblock");
		
		AdvancementHolder dryingRack = Advancement.Builder.advancement()
				.display(ModBlocks.OAK_DRYING_RACK.get(), Component.translatable("advancements.stoneage.dryingrack.title"), Component.translatable("advancements.stoneage.dryingrack.description"), null, AdvancementType.TASK, true, true, false)
				.parent(choppingBlock)
				.addCriterion("drying_racks", InventoryChangeTrigger.TriggerInstance.hasItems(ItemPredicate.Builder.item().of(ModItemTags.DRYING_RACKS).build()))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/dryingrack");
		
		AdvancementHolder preparedHide = Advancement.Builder.advancement()
				.display(ModItems.PREPARED_HIDE.get(), Component.translatable("advancements.stoneage.preparedhide.title"), Component.translatable("advancements.stoneage.preparedhide.description"), null, AdvancementType.TASK, true, true, false)
				.parent(dryingRack)
				.addCriterion("prepared_hide", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.PREPARED_HIDE.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/preparedhide");
		
		AdvancementHolder leather = Advancement.Builder.advancement()
				.display(Items.LEATHER, Component.translatable("advancements.stoneage.leather.title"), Component.translatable("advancements.stoneage.leather.description"), null, AdvancementType.TASK, true, true, false)
				.parent(preparedHide)
				.addCriterion("leather", InventoryChangeTrigger.TriggerInstance.hasItems(Items.LEATHER))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/leather");
		
		AdvancementHolder firstTool = Advancement.Builder.advancement()
				.display(ModItems.FLINT_HATCHET.get(), Component.translatable("advancements.stoneage.firsttool.title"), Component.translatable("advancements.stoneage.firsttool.description"), null, AdvancementType.TASK, true, true, false)
				.parent(stoneAge)
				.addCriterion("flint_hatchet", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.FLINT_HATCHET.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/firsttool");
		
		EntityPredicate.Builder rockBuilder = EntityPredicate.Builder.entity().of(ModEntityTypeTags.ROCKS);
		AdvancementHolder throwRock = Advancement.Builder.advancement()
				.display(ModItems.ROCK.get(), Component.translatable("advancements.stoneage.throwrock.title"), Component.translatable("advancements.stoneage.throwrock.description"), null, AdvancementType.CHALLENGE, true, true, false)
				.parent(firstTool)
				.addCriterion("throw_rock", KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity(), DamageSourcePredicate.Builder.damageType().tag(TagPredicate.is(DamageTypeTags.IS_PROJECTILE)).direct(rockBuilder)))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/throwrock");
		
		AdvancementHolder stoneMaker = Advancement.Builder.advancement()
				.display(Blocks.COBBLESTONE, Component.translatable("advancements.stoneage.stonemaker.title"), Component.translatable("advancements.stoneage.stonemaker.description"), null, AdvancementType.TASK, true, true, false)
				.parent(firstTool)
				.addCriterion("cobblestone", InventoryChangeTrigger.TriggerInstance.hasItems(Blocks.COBBLESTONE))
				.addCriterion("andesite", InventoryChangeTrigger.TriggerInstance.hasItems(Blocks.ANDESITE))
				.addCriterion("diorite", InventoryChangeTrigger.TriggerInstance.hasItems(Blocks.DIORITE))
				.addCriterion("granite", InventoryChangeTrigger.TriggerInstance.hasItems(Blocks.GRANITE))
				.requirements(AdvancementRequirements.Strategy.OR)
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/stonemaker");
		
		AdvancementHolder millstone = Advancement.Builder.advancement()
				.display(ModBlocks.MILLSTONE.get(), Component.translatable("advancements.stoneage.millstone.title"), Component.translatable("advancements.stoneage.millstone.description"), null, AdvancementType.TASK, true, true, false)
				.parent(stoneMaker)
				.addCriterion("millstone", InventoryChangeTrigger.TriggerInstance.hasItems(ModBlocks.MILLSTONE.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/millstone");
		
		AdvancementHolder toolhead = Advancement.Builder.advancement()
				.display(ModItems.STONE_PICKAXE_HEAD.get(), Component.translatable("advancements.stoneage.toolhead.title"), Component.translatable("advancements.stoneage.toolhead.description"), null, AdvancementType.TASK, true, true, false)
				.parent(stoneMaker)
				.addCriterion("pickaxe_head", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.STONE_PICKAXE_HEAD.get()))
				.addCriterion("axe_head", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.STONE_AXE_HEAD.get()))
				.addCriterion("shovel_head", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.STONE_SHOVEL_HEAD.get()))
				.addCriterion("hoe_head", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.STONE_HOE_HEAD.get()))
				.requirements(AdvancementRequirements.Strategy.OR)
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/toolhead");
		
		AdvancementHolder betterTool = Advancement.Builder.advancement()
				.display(Items.STONE_PICKAXE, Component.translatable("advancements.stoneage.bettertool.title"), Component.translatable("advancements.stoneage.bettertool.description"), null, AdvancementType.TASK, true, true, false)
				.parent(toolhead)
				.addCriterion("stone_pickaxe", InventoryChangeTrigger.TriggerInstance.hasItems(Items.STONE_PICKAXE))
				.addCriterion("stone_axe", InventoryChangeTrigger.TriggerInstance.hasItems(Items.STONE_AXE))
				.addCriterion("stone_shovel", InventoryChangeTrigger.TriggerInstance.hasItems(Items.STONE_SHOVEL))
				.addCriterion("stone_hoe", InventoryChangeTrigger.TriggerInstance.hasItems(Items.STONE_HOE))
				.requirements(AdvancementRequirements.Strategy.OR)
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/bettertool");
		
		AdvancementHolder kiln = Advancement.Builder.advancement()
				.display(ModBlocks.KILN.get(), Component.translatable("advancements.stoneage.kiln.title"), Component.translatable("advancements.stoneage.kiln.description"), null, AdvancementType.TASK, true, true, false)
				.parent(stoneMaker)
				.addCriterion("kiln", InventoryChangeTrigger.TriggerInstance.hasItems(ModBlocks.KILN.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/kiln");
		
		AdvancementHolder clayBucket = Advancement.Builder.advancement()
				.display(ModItems.CLAY_BUCKET.get(), Component.translatable("advancements.stoneage.claybucket.title"), Component.translatable("advancements.stoneage.claybucket.description"), null, AdvancementType.TASK, true, true, false)
				.parent(kiln)
				.addCriterion("clay_bucket", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.CLAY_BUCKET.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/claybucket");
		
		AdvancementHolder cement = Advancement.Builder.advancement()
				.display(ModItems.CEMENT_MIXTURE.get(), Component.translatable("advancements.stoneage.cement.title"), Component.translatable("advancements.stoneage.cement.description"), null, AdvancementType.TASK, true, true, false)
				.parent(clayBucket)
				.addCriterion("cement", InventoryChangeTrigger.TriggerInstance.hasItems(ModBlocks.CEMENT.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/cement");
		
		AdvancementHolder smelter = Advancement.Builder.advancement()
				.display(ModBlocks.SMELTER.get(), Component.translatable("advancements.stoneage.smelter.title"), Component.translatable("advancements.stoneage.smelter.description"), null, AdvancementType.GOAL, true, true, false)
				.parent(cement)
				.addCriterion("smelter", InventoryChangeTrigger.TriggerInstance.hasItems(ModBlocks.SMELTER.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":stoneage/smelter");
	}
	
	private void metalAgeAdvancements(Consumer<AdvancementHolder> saver)
	{
		AdvancementHolder metalAge = Advancement.Builder.advancement()
				.display(ModItems.BRONZE_INGOT.get(), Component.translatable("advancements.metalage.root.title"), Component.translatable("advancements.metalage.root.description"), ResourceLocation.parse("textures/block/iron_ore.png"), AdvancementType.GOAL, true, false, false)
				.addCriterion("iron_ingot", InventoryChangeTrigger.TriggerInstance.hasItems(Items.IRON_INGOT))
				.addCriterion("copper_ingot", InventoryChangeTrigger.TriggerInstance.hasItems(Items.COPPER_INGOT))
				.addCriterion("gold_ingot", InventoryChangeTrigger.TriggerInstance.hasItems(Items.GOLD_INGOT))
				.addCriterion("tin_ingot", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.TIN_INGOT.get()))
				.addCriterion("zinc_ingot", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.ZINC_INGOT.get()))
				.addCriterion("bronze_ingot", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.BRONZE_INGOT.get()))
				.addCriterion("brass_ingot", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.BRASS_INGOT.get()))
				.requirements(AdvancementRequirements.Strategy.OR)
				.save(saver, MinecraftContentExpansion.MODID + ":metalage/root");
		
		AdvancementHolder darkIron = Advancement.Builder.advancement()
				.display(ModItems.STEEL_INGOT.get(), Component.translatable("advancements.metalage.darkiron.title"), Component.translatable("advancements.metalage.darkiron.description"), null, AdvancementType.GOAL, true, true, false)
				.parent(metalAge)
				.addCriterion("steel_ingot", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.STEEL_INGOT.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":metalage/darkiron");
		
		AdvancementHolder hammer = Advancement.Builder.advancement()
				.display(ModItems.BRONZE_HAMMER.get(), Component.translatable("advancements.metalage.hammer.title"), Component.translatable("advancements.metalage.hammer.description"), null, AdvancementType.TASK, true, true, false)
				.parent(metalAge)
				.addCriterion("hammers", InventoryChangeTrigger.TriggerInstance.hasItems(ItemPredicate.Builder.item().of(ModItemTags.TOOLS_HAMMERS).build()))
				.requirements(AdvancementRequirements.Strategy.OR)
				.save(saver, MinecraftContentExpansion.MODID + ":metalage/hammer");
		
		AdvancementHolder protection = Advancement.Builder.advancement()
				.display(ModItems.BRONZE_CHESTPLATE.get(), Component.translatable("advancements.metalage.protection.title"), Component.translatable("advancements.metalage.protection.description"), null, AdvancementType.TASK, true, true, false)
				.parent(hammer)
				.addCriterion("iron_helmet", InventoryChangeTrigger.TriggerInstance.hasItems(Items.IRON_HELMET))
				.addCriterion("iron_chestplate", InventoryChangeTrigger.TriggerInstance.hasItems(Items.IRON_CHESTPLATE))
				.addCriterion("iron_leggings", InventoryChangeTrigger.TriggerInstance.hasItems(Items.IRON_LEGGINGS))
				.addCriterion("iron_boots", InventoryChangeTrigger.TriggerInstance.hasItems(Items.IRON_BOOTS))
				.addCriterion("bronze_helmet", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.BRONZE_HELMET.get()))
				.addCriterion("bronze_chestplate", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.BRONZE_CHESTPLATE.get()))
				.addCriterion("bronze_leggings", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.BRONZE_LEGGINGS.get()))
				.addCriterion("bronze_boots", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.BRONZE_BOOTS.get()))
				.requirements(AdvancementRequirements.Strategy.OR)
				.save(saver, MinecraftContentExpansion.MODID + ":metalage/protection");
		
		AdvancementHolder pickaxe = Advancement.Builder.advancement()
				.display(ModItems.BRONZE_PICKAXE.get(), Component.translatable("advancements.metalage.pickaxe.title"), Component.translatable("advancements.metalage.pickaxe.description"), null, AdvancementType.TASK, true, true, false)
				.parent(metalAge)
				.addCriterion("iron_pickaxe", InventoryChangeTrigger.TriggerInstance.hasItems(Items.IRON_PICKAXE))
				.addCriterion("bronze_pickaxe", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.BRONZE_PICKAXE.get()))
				.requirements(AdvancementRequirements.Strategy.OR)
				.save(saver, MinecraftContentExpansion.MODID + ":metalage/pickaxe");
	}
	
	private void industrialAgeAdvancements(Consumer<AdvancementHolder> saver)
	{
		AdvancementHolder industrialAge = Advancement.Builder.advancement()
				.display(ModItems.STEEL_INGOT.get(), Component.translatable("advancements.industrialage.root.title"), Component.translatable("advancements.industrialage.root.description"), ResourceLocation.parse("textures/block/iron_block.png"), AdvancementType.GOAL, true, false, false)
				.addCriterion("steel_ingot", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.STEEL_INGOT.get()))
				.requirements(AdvancementRequirements.Strategy.OR)
				.save(saver, MinecraftContentExpansion.MODID + ":industrialage/root");
		
		AdvancementHolder redstoneBlend = Advancement.Builder.advancement()
				.display(ModItems.REDSTONE_BLEND.get(), Component.translatable("advancements.industrialage.redstoneblend.title"), Component.translatable("advancements.industrialage.redstoneblend.description"), null, AdvancementType.TASK, true, true, false)
				.parent(industrialAge)
				.addCriterion("redstone_blend", InventoryChangeTrigger.TriggerInstance.hasItems(ModItems.REDSTONE_BLEND.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":metalage/redstoneblend");
		
		AdvancementHolder machineFrame = Advancement.Builder.advancement()
				.display(ModBlocks.MACHINE_FRAME.get(), Component.translatable("advancements.industrialage.machineframe.title"), Component.translatable("advancements.industrialage.machineframe.description"), null, AdvancementType.TASK, true, true, false)
				.parent(redstoneBlend)
				.addCriterion("machine_frame", InventoryChangeTrigger.TriggerInstance.hasItems(ModBlocks.MACHINE_FRAME.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":metalage/machineframe");
		
		AdvancementHolder greenEnergy = Advancement.Builder.advancement()
				.display(ModBlocks.SOLAR_PANEL.get(), Component.translatable("advancements.industrialage.greenenergy.title"), Component.translatable("advancements.industrialage.greenenergy.description"), null, AdvancementType.TASK, true, true, false)
				.parent(redstoneBlend)
				.addCriterion("solar_panel", InventoryChangeTrigger.TriggerInstance.hasItems(ModBlocks.SOLAR_PANEL.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":metalage/greenenergy");
		
		AdvancementHolder blackEnergy = Advancement.Builder.advancement()
				.display(ModBlocks.COAL_GENERATOR.get(), Component.translatable("advancements.industrialage.blackenergy.title"), Component.translatable("advancements.industrialage.blackenergy.description"), null, AdvancementType.TASK, true, true, false)
				.parent(machineFrame)
				.addCriterion("coal_generator", InventoryChangeTrigger.TriggerInstance.hasItems(ModBlocks.COAL_GENERATOR.get()))
				.save(saver, MinecraftContentExpansion.MODID + ":metalage/blackenergy");
	}
}
