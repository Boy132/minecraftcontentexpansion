package de.boy132.minecraftcontentexpansion.datagen;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.datagen.advancement.ModAdvancementProvider;
import de.boy132.minecraftcontentexpansion.datagen.blockstate.ModBlockStateProvider;
import de.boy132.minecraftcontentexpansion.datagen.language.ModEnglishLanguageProvider;
import de.boy132.minecraftcontentexpansion.datagen.language.ModGermanLanguageProvider;
import de.boy132.minecraftcontentexpansion.datagen.loot.ModBlockLootTables;
import de.boy132.minecraftcontentexpansion.datagen.loot.ModGlobalLootModifierProvider;
import de.boy132.minecraftcontentexpansion.datagen.loot.ModLootTableProvider;
import de.boy132.minecraftcontentexpansion.datagen.model.ModBlockModelProvider;
import de.boy132.minecraftcontentexpansion.datagen.model.ModItemModelProvider;
import de.boy132.minecraftcontentexpansion.datagen.recipe.ModRecipeProvider;
import de.boy132.minecraftcontentexpansion.datagen.tag.ModBlockTagsProvider;
import de.boy132.minecraftcontentexpansion.datagen.tag.ModEntityTypeTagsProvider;
import de.boy132.minecraftcontentexpansion.datagen.tag.ModItemTagsProvider;
import de.boy132.minecraftcontentexpansion.datagen.worldgen.ModWorldGenProvider;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.PackOutput;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.data.ForgeAdvancementProvider;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Mod.EventBusSubscriber(modid = MinecraftContentExpansion.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGenerators
{
	@SubscribeEvent
	public static void gatherData(GatherDataEvent event)
	{
		DataGenerator generator = event.getGenerator();
		PackOutput packOutput = generator.getPackOutput();
		CompletableFuture<HolderLookup.Provider> lookupProvider = event.getLookupProvider();
		ExistingFileHelper existingFileHelper = event.getExistingFileHelper();
		
		generator.addProvider(event.includeClient(), new ModBlockStateProvider(packOutput, existingFileHelper));
		generator.addProvider(event.includeClient(), new ModBlockModelProvider(packOutput, existingFileHelper));
		generator.addProvider(event.includeClient(), new ModItemModelProvider(packOutput, existingFileHelper));
		
		generator.addProvider(event.includeServer(), new ModRecipeProvider(packOutput));
		
		generator.addProvider(event.includeServer(), new ModLootTableProvider(packOutput, Collections.emptySet(), List.of(new LootTableProvider.SubProviderEntry(ModBlockLootTables::new, LootContextParamSets.BLOCK))));
		generator.addProvider(event.includeServer(), new ModGlobalLootModifierProvider(packOutput));
		
		generator.addProvider(event.includeServer(), new ModWorldGenProvider(packOutput, lookupProvider));
		
		generator.addProvider(event.includeClient(), new ModEnglishLanguageProvider(packOutput));
		generator.addProvider(event.includeClient(), new ModGermanLanguageProvider(packOutput));
		
		generator.addProvider(event.includeServer(), new ForgeAdvancementProvider(packOutput, lookupProvider, existingFileHelper, List.of(new ModAdvancementProvider())));
		
		BlockTagsProvider blockTagsProvider = new ModBlockTagsProvider(packOutput, lookupProvider, existingFileHelper);
		generator.addProvider(event.includeServer(), blockTagsProvider);
		generator.addProvider(event.includeServer(), new ModItemTagsProvider(packOutput, lookupProvider, existingFileHelper, blockTagsProvider));
		generator.addProvider(event.includeServer(), new ModEntityTypeTagsProvider(packOutput, lookupProvider, existingFileHelper));
	}
}
