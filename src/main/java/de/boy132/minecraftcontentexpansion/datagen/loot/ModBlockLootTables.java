package de.boy132.minecraftcontentexpansion.datagen.loot;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.crop.*;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.advancements.critereon.StatePropertiesPredicate;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.level.storage.loot.predicates.LootItemBlockStatePropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;

import java.util.Collections;

public class ModBlockLootTables extends BaseBlockLoot
{
	public ModBlockLootTables()
	{
		super(Collections.emptySet(), FeatureFlags.REGISTRY.allFlags());
	}
	
	@Override
	protected void addBlocks()
	{
		add(ModBlocks.UNIM_ORE.get(), createRandomOreDrops(ModBlocks.UNIM_ORE.get(), ModItems.UNIM.get(), 1, 3));
		add(ModBlocks.DEEPSLATE_UNIM_ORE.get(), createRandomOreDrops(ModBlocks.DEEPSLATE_UNIM_ORE.get(), ModItems.UNIM.get(), 1, 3));
		dropSelf(ModBlocks.UNIM_BLOCK.get());
		
		add(ModBlocks.RUBY_ORE.get(), createOreDrop(ModBlocks.RUBY_ORE.get(), ModItems.RUBY.get()));
		add(ModBlocks.DEEPSLATE_RUBY_ORE.get(), createOreDrop(ModBlocks.DEEPSLATE_RUBY_ORE.get(), ModItems.RUBY.get()));
		dropSelf(ModBlocks.RUBY_BLOCK.get());
		
		add(ModBlocks.ASCABIT_ORE.get(), createOreDrop(ModBlocks.ASCABIT_ORE.get(), ModItems.RAW_ASCABIT.get()));
		add(ModBlocks.DEEPSLATE_ASCABIT_ORE.get(), createOreDrop(ModBlocks.DEEPSLATE_ASCABIT_ORE.get(), ModItems.RAW_ASCABIT.get()));
		dropSelf(ModBlocks.ASCABIT_BLOCK.get());
		dropSelf(ModBlocks.RAW_ASCABIT_BLOCK.get());
		
		add(ModBlocks.TIN_ORE.get(), createOreDrop(ModBlocks.TIN_ORE.get(), ModItems.RAW_TIN.get()));
		add(ModBlocks.DEEPSLATE_TIN_ORE.get(), createOreDrop(ModBlocks.DEEPSLATE_TIN_ORE.get(), ModItems.RAW_TIN.get()));
		dropSelf(ModBlocks.TIN_BLOCK.get());
		dropSelf(ModBlocks.RAW_TIN_BLOCK.get());
		
		add(ModBlocks.TITANIUM_ORE.get(), createOreDrop(ModBlocks.TITANIUM_ORE.get(), ModItems.RAW_TITANIUM.get()));
		add(ModBlocks.DEEPSLATE_TITANIUM_ORE.get(), createOreDrop(ModBlocks.DEEPSLATE_TITANIUM_ORE.get(), ModItems.RAW_TITANIUM.get()));
		dropSelf(ModBlocks.TITANIUM_BLOCK.get());
		dropSelf(ModBlocks.RAW_TITANIUM_BLOCK.get());
		
		add(ModBlocks.ZINC_ORE.get(), createOreDrop(ModBlocks.ZINC_ORE.get(), ModItems.RAW_ZINC.get()));
		add(ModBlocks.DEEPSLATE_ZINC_ORE.get(), createOreDrop(ModBlocks.DEEPSLATE_ZINC_ORE.get(), ModItems.RAW_ZINC.get()));
		dropSelf(ModBlocks.ZINC_BLOCK.get());
		dropSelf(ModBlocks.RAW_ZINC_BLOCK.get());
		
		dropSelf(ModBlocks.STEEL_BLOCK.get());
		
		dropSelf(ModBlocks.BRONZE_BLOCK.get());
		
		dropSelf(ModBlocks.BRASS_BLOCK.get());
		
		dropSelf(ModBlocks.CEMENT.get());
		dropSelf(ModBlocks.CEMENT_STAIRS.get());
		add(ModBlocks.CEMENT_SLAB.get(), this::createSlabItemTable);
		dropSelf(ModBlocks.CEMENT_WALL.get());
		
		dropSelf(ModBlocks.CEMENT_PLATE.get());
		dropSelf(ModBlocks.CEMENT_PLATE_STAIRS.get());
		add(ModBlocks.CEMENT_PLATE_SLAB.get(), this::createSlabItemTable);
		dropSelf(ModBlocks.CEMENT_PLATE_WALL.get());
		
		dropSelf(ModBlocks.LIMESTONE.get());
		dropSelf(ModBlocks.LIMESTONE_STAIRS.get());
		add(ModBlocks.LIMESTONE_SLAB.get(), this::createSlabItemTable);
		dropSelf(ModBlocks.LIMESTONE_WALL.get());
		
		dropSelf(ModBlocks.LIMESTONE_BRICKS.get());
		dropSelf(ModBlocks.LIMESTONE_BRICK_STAIRS.get());
		add(ModBlocks.LIMESTONE_BRICK_SLAB.get(), this::createSlabItemTable);
		dropSelf(ModBlocks.LIMESTONE_BRICK_WALL.get());
		
		dropSelf(ModBlocks.MARBLE.get());
		dropSelf(ModBlocks.MARBLE_STAIRS.get());
		add(ModBlocks.MARBLE_SLAB.get(), this::createSlabItemTable);
		dropSelf(ModBlocks.MARBLE_WALL.get());
		
		dropSelf(ModBlocks.MARBLE_COBBLE.get());
		dropSelf(ModBlocks.MARBLE_COBBLE_STAIRS.get());
		add(ModBlocks.MARBLE_COBBLE_SLAB.get(), this::createSlabItemTable);
		dropSelf(ModBlocks.MARBLE_COBBLE_WALL.get());
		
		dropSelf(ModBlocks.MARBLE_BRICKS.get());
		dropSelf(ModBlocks.MARBLE_BRICK_STAIRS.get());
		add(ModBlocks.MARBLE_BRICK_SLAB.get(), this::createSlabItemTable);
		dropSelf(ModBlocks.MARBLE_BRICK_WALL.get());
		
		dropSelf(ModBlocks.MARBLE_LAMP.get());
		
		add(ModBlocks.DIRT_SLAB.get(), this::createSlabItemTable);
		add(ModBlocks.COARSE_DIRT_SLAB.get(), this::createSlabItemTable);
		add(ModBlocks.DIRT_PATH_SLAB.get(), block -> createDirtSlab(ModBlocks.DIRT_PATH_SLAB.get(), ModBlocks.DIRT_SLAB.get()));
		add(ModBlocks.GRASS_BLOCK_SLAB.get(), block -> createDirtSlab(ModBlocks.GRASS_BLOCK_SLAB.get(), ModBlocks.DIRT_SLAB.get()));
		
		add(ModBlocks.GLASS_SLAB.get(), this::createSlabItemTable);
		
		add(ModBlocks.STONE_DOOR.get(), this::createDoorTable);
		dropSelf(ModBlocks.STONE_TRAPDOOR.get());
		
		add(ModBlocks.SANDSTONE_DOOR.get(), this::createDoorTable);
		dropSelf(ModBlocks.SANDSTONE_TRAPDOOR.get());
		
		add(ModBlocks.GLASS_DOOR.get(), this::createDoorTable);
		dropSelf(ModBlocks.GLASS_TRAPDOOR.get());
		
		dropWhenSilkTouch(ModBlocks.WOODEN_GLASS_FRAME.get());
		
		dropSelf(ModBlocks.KILN.get());
		dropSelf(ModBlocks.SMELTER.get());
		dropSelf(ModBlocks.MILLSTONE.get());
		
		dropSelf(ModBlocks.OAK_CHOPPING_BLOCK.get());
		dropSelf(ModBlocks.SPRUCE_CHOPPING_BLOCK.get());
		dropSelf(ModBlocks.BIRCH_CHOPPING_BLOCK.get());
		dropSelf(ModBlocks.JUNGLE_CHOPPING_BLOCK.get());
		dropSelf(ModBlocks.ACACIA_CHOPPING_BLOCK.get());
		dropSelf(ModBlocks.DARK_OAK_CHOPPING_BLOCK.get());
		dropSelf(ModBlocks.MANGROVE_CHOPPING_BLOCK.get());
		dropSelf(ModBlocks.CHERRY_CHOPPING_BLOCK.get());
		dropSelf(ModBlocks.CRIMSON_CHOPPING_BLOCK.get());
		dropSelf(ModBlocks.WARPED_CHOPPING_BLOCK.get());
		
		dropSelf(ModBlocks.OAK_DRYING_RACK.get());
		dropSelf(ModBlocks.SPRUCE_DRYING_RACK.get());
		dropSelf(ModBlocks.BIRCH_DRYING_RACK.get());
		dropSelf(ModBlocks.JUNGLE_DRYING_RACK.get());
		dropSelf(ModBlocks.ACACIA_DRYING_RACK.get());
		dropSelf(ModBlocks.DARK_OAK_DRYING_RACK.get());
		dropSelf(ModBlocks.MANGROVE_DRYING_RACK.get());
		dropSelf(ModBlocks.CHERRY_DRYING_RACK.get());
		dropSelf(ModBlocks.CRIMSON_DRYING_RACK.get());
		dropSelf(ModBlocks.WARPED_DRYING_RACK.get());
		
		dropSelf(ModBlocks.MACHINE_FRAME.get());
		
		dropSelf(ModBlocks.COAL_GENERATOR.get());
		dropSelf(ModBlocks.BATTERY.get());
		dropSelf(ModBlocks.SOLAR_PANEL.get());
		
		dropSelf(ModBlocks.LIQUID_TANK.get());
		
		dropSelf(ModBlocks.ELECTRIC_BREWERY.get());
		dropSelf(ModBlocks.ELECTRIC_SMELTER.get());
		dropSelf(ModBlocks.ELECTRIC_GREENHOUSE.get());
		
		dropSelf(ModBlocks.HYDRAULIC_PRESS.get());
		
		dropSelf(ModBlocks.COPPER_CABLE.get());
		
		LootItemCondition.Builder tomatoesLootCondition = LootItemBlockStatePropertyCondition.hasBlockStateProperties(ModBlocks.TOMATOES.get()).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(TomatoCropBlock.AGE, TomatoCropBlock.MAX_AGE));
		add(ModBlocks.TOMATOES.get(), createCropDrops(ModBlocks.TOMATOES.get(), ModItems.TOMATO.get(), ModItems.TOMATO_SEEDS.get(), tomatoesLootCondition));
		
		LootItemCondition.Builder grapesLootCondition = LootItemBlockStatePropertyCondition.hasBlockStateProperties(ModBlocks.GRAPES.get()).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(GrapeCropBlock.AGE, GrapeCropBlock.MAX_AGE));
		add(ModBlocks.GRAPES.get(), createCropDrops(ModBlocks.GRAPES.get(), ModItems.GRAPE.get(), ModItems.GRAPE_SEEDS.get(), grapesLootCondition));
		
		LootItemCondition.Builder strawberryLootCondition = LootItemBlockStatePropertyCondition.hasBlockStateProperties(ModBlocks.STRAWBERRIES.get()).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(StrawberryCropBlock.AGE, StrawberryCropBlock.MAX_AGE));
		add(ModBlocks.STRAWBERRIES.get(), createCropDrops(ModBlocks.STRAWBERRIES.get(), ModItems.STRAWBERRY.get(), ModItems.STRAWBERRY_SEEDS.get(), strawberryLootCondition));
		
		LootItemCondition.Builder cornLootCondition = LootItemBlockStatePropertyCondition.hasBlockStateProperties(ModBlocks.CORNS.get()).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(CornDoubleCropBlock.AGE, CornDoubleCropBlock.MAX_AGE));
		add(ModBlocks.CORNS.get(), createCropDrops(ModBlocks.CORNS.get(), ModItems.CORN.get(), ModItems.CORN_SEEDS.get(), cornLootCondition));
		
		LootItemCondition.Builder lettuceLootCondition = LootItemBlockStatePropertyCondition.hasBlockStateProperties(ModBlocks.LETTUCES.get()).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(LettuceCropBlock.AGE, LettuceCropBlock.MAX_AGE));
		add(ModBlocks.LETTUCES.get(), createCropDrops(ModBlocks.LETTUCES.get(), ModItems.LETTUCE.get(), ModItems.LETTUCE_SEEDS.get(), lettuceLootCondition));
		
		LootItemCondition.Builder onionLootCondition = LootItemBlockStatePropertyCondition.hasBlockStateProperties(ModBlocks.ONIONS.get()).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(OnionCropBlock.AGE, OnionCropBlock.MAX_AGE));
		add(ModBlocks.ONIONS.get(), createCropDrops(ModBlocks.ONIONS.get(), ModItems.ONION.get(), ModItems.ONION_SEEDS.get(), onionLootCondition));
		
		LootItemCondition.Builder riceLootCondition = LootItemBlockStatePropertyCondition.hasBlockStateProperties(ModBlocks.RICE.get()).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(RiceCropBlock.AGE, RiceCropBlock.MAX_AGE));
		add(ModBlocks.RICE.get(), createCropDropsWithoutSeed(ModBlocks.RICE.get(), ModItems.RICE.get(), riceLootCondition));
	}
	
	@Override
	protected void addCustomBlocks()
	{
	
	}
}
