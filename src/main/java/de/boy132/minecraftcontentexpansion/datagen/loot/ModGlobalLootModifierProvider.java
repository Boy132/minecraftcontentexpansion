package de.boy132.minecraftcontentexpansion.datagen.loot;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.loot.condition.AdditionLootModifier;
import de.boy132.minecraftcontentexpansion.loot.condition.ModuleSettingLootItemCondition;
import de.boy132.minecraftcontentexpansion.loot.condition.ReplaceLootModifier;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.data.PackOutput;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.AnyOfCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemBlockStatePropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition;
import net.minecraftforge.common.data.GlobalLootModifierProvider;

public class ModGlobalLootModifierProvider extends GlobalLootModifierProvider
{
	public ModGlobalLootModifierProvider(PackOutput packOutput)
	{
		super(packOutput, MinecraftContentExpansion.MODID);
	}
	
	@Override
	protected void start()
	{
		addToGrassDrops();
		replaceStoneDrops();
		replaceAnimalDrops();
	}
	
	private void addToGrassDrops()
	{
		// Grass & Fern
		add("plant_fiber_from_grass", new AdditionLootModifier(new LootItemCondition[] {
				AnyOfCondition.anyOf(LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.SHORT_GRASS), LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.FERN)).build()
		}, ModItems.PLANT_FIBER.get(), 0.25f, 1, 1, false));
		
		add("stick_from_grass", new AdditionLootModifier(new LootItemCondition[] {
				AnyOfCondition.anyOf(LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.SHORT_GRASS), LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.FERN)).build()
		}, Items.STICK, 0.2f, 1, 1, false));
		
		add("flint_from_grass", new AdditionLootModifier(new LootItemCondition[] {
				AnyOfCondition.anyOf(LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.SHORT_GRASS), LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.FERN)).build()
		}, Items.FLINT, 0.15f, 1, 1, false));
	}
	
	private void replaceStoneDrops()
	{
		// Stone(s)
		add("rocks_from_stone", new ReplaceLootModifier(new LootItemCondition[] {
				LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.STONE).build(),
				ModuleSettingLootItemCondition.STONE_AGE_STONE_DROPS_ROCKS
		}, Blocks.COBBLESTONE.asItem(), ModItems.ROCK.get(), 1, 3, false));
		
		add("rocks_from_andesite", new ReplaceLootModifier(new LootItemCondition[] {
				LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.ANDESITE).build(),
				ModuleSettingLootItemCondition.STONE_AGE_STONE_DROPS_ROCKS
		}, Blocks.ANDESITE.asItem(), ModItems.ANDESITE_ROCK.get(), 1, 3, false));
		
		add("rocks_from_diorite", new ReplaceLootModifier(new LootItemCondition[] {
				LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.DIORITE).build(),
				ModuleSettingLootItemCondition.STONE_AGE_STONE_DROPS_ROCKS
		}, Blocks.DIORITE.asItem(), ModItems.DIORITE_ROCK.get(), 1, 3, false));
		
		add("rocks_from_granite", new ReplaceLootModifier(new LootItemCondition[] {
				LootItemBlockStatePropertyCondition.hasBlockStateProperties(Blocks.GRANITE).build(),
				ModuleSettingLootItemCondition.STONE_AGE_STONE_DROPS_ROCKS
		}, Blocks.GRANITE.asItem(), ModItems.GRANITE_ROCK.get(), 1, 3, false));
	}
	
	private void replaceAnimalDrops()
	{
		// Cow
		LootItemCondition cow = LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, new EntityPredicate.Builder().of(EntityType.COW).build()).build();
		add("animal_hide_from_cow", new ReplaceLootModifier(new LootItemCondition[] {
				cow,
				ModuleSettingLootItemCondition.STONE_AGE_LEATHER_CRAFTING_MODE_ADVANCED
		}, Items.LEATHER, ModItems.ANIMAL_HIDE.get(), 1, 2, true));
		
		add("animal_fat_from_cow", new AdditionLootModifier(new LootItemCondition[] {cow}, ModItems.ANIMAL_FAT.get(), 0.6f, 1, 2, true));
		
		// Sheep
		LootItemCondition sheep = LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, new EntityPredicate.Builder().of(EntityType.SHEEP).build()).build();
		add("animal_hide_from_sheep", new AdditionLootModifier(new LootItemCondition[] {
				sheep,
				ModuleSettingLootItemCondition.STONE_AGE_LEATHER_CRAFTING_MODE_ADVANCED
		}, ModItems.ANIMAL_HIDE.get(), 0.6f, 1, 1, true));
		
		add("animal_fat_from_sheep", new AdditionLootModifier(new LootItemCondition[] {sheep}, ModItems.ANIMAL_FAT.get(), 0.5f, 1, 2, true));
		
		// Pig
		LootItemCondition pig = LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, new EntityPredicate.Builder().of(EntityType.PIG).build()).build();
		add("animal_hide_from_pig", new AdditionLootModifier(new LootItemCondition[] {
				pig,
				ModuleSettingLootItemCondition.STONE_AGE_LEATHER_CRAFTING_MODE_ADVANCED
		}, ModItems.ANIMAL_HIDE.get(), 0.9f, 1, 1, true));
		
		add("animal_fat_from_pig", new AdditionLootModifier(new LootItemCondition[] {pig}, ModItems.ANIMAL_FAT.get(), 0.8f, 1, 2, true));
		
		// Chicken
		LootItemCondition chicken = LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, new EntityPredicate.Builder().of(EntityType.CHICKEN).build()).build();
		add("animal_fat_from_chicken", new AdditionLootModifier(new LootItemCondition[] {chicken}, ModItems.ANIMAL_FAT.get(), 0.6f, 1, 1, true));
	}
	
	@Override
	public String getName()
	{
		return "Mod Global Loot Modifiers";
	}
}
