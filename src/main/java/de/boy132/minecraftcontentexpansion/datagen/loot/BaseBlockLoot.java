package de.boy132.minecraftcontentexpansion.datagen.loot;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import net.minecraft.advancements.critereon.StatePropertiesPredicate;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.properties.SlabType;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.predicates.LootItemBlockStatePropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.minecraftforge.registries.RegistryObject;

import java.util.Set;

public abstract class BaseBlockLoot extends BlockLootSubProvider
{
	protected BaseBlockLoot(Set<Item> explosionResistant, FeatureFlagSet enabledFeatures)
	{
		super(explosionResistant, enabledFeatures);
	}
	
	protected abstract void addBlocks();
	
	protected abstract void addCustomBlocks();
	
	@Override
	protected void generate()
	{
		addBlocks();
		addCustomBlocks();
	}
	
	@Override
	protected Iterable<Block> getKnownBlocks()
	{
		return ModBlocks.BLOCKS.getEntries().stream().flatMap(RegistryObject::stream)::iterator;
	}
	
	protected LootTable.Builder createRandomOreDrops(Block block, ItemLike dropItem, int min, int max)
	{
		return createSilkTouchDispatchTable(block, applyExplosionDecay(block, LootItem.lootTableItem(dropItem).apply(SetItemCountFunction.setCount(UniformGenerator.between(min, max))).apply(ApplyBonusCount.addOreBonusCount(Enchantments.BLOCK_FORTUNE))));
	}
	
	protected LootTable.Builder createCropDropsWithoutSeed(CropBlock crop, ItemLike drop, LootItemCondition.Builder lootCondition)
	{
		return applyExplosionDecay(crop, LootTable.lootTable().withPool(LootPool.lootPool().add(LootItem.lootTableItem(drop))).withPool(LootPool.lootPool().when(lootCondition).add(LootItem.lootTableItem(drop).apply(ApplyBonusCount.addBonusBinomialDistributionCount(Enchantments.BLOCK_FORTUNE, 0.5714286F, 3)))));
	}
	
	protected LootTable.Builder createDirtSlab(Block slab, Block drop)
	{
		return LootTable.lootTable().withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1.0F)).add(applyExplosionDecay(slab, LootItem.lootTableItem(drop).apply(SetItemCountFunction.setCount(ConstantValue.exactly(2.0F)).when(LootItemBlockStatePropertyCondition.hasBlockStateProperties(slab).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(SlabBlock.TYPE, SlabType.DOUBLE)))))));
	}
}
