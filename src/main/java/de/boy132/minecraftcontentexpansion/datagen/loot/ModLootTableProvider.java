package de.boy132.minecraftcontentexpansion.datagen.loot;

import net.minecraft.data.PackOutput;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.resources.ResourceLocation;

import java.util.List;
import java.util.Set;

public class ModLootTableProvider extends LootTableProvider
{
	public ModLootTableProvider(PackOutput packOutput, Set<ResourceLocation> requiredTables, List<SubProviderEntry> subProviders)
	{
		super(packOutput, requiredTables, subProviders);
	}
}
