package de.boy132.minecraftcontentexpansion.datagen.language;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.entchantments.ModEnchantments;
import de.boy132.minecraftcontentexpansion.entity.ModEntityTypes;
import de.boy132.minecraftcontentexpansion.item.ModCreativeModeTabs;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.keybinds.ModKeyMappings;
import de.boy132.minecraftcontentexpansion.stats.ModStats;
import net.minecraft.data.PackOutput;

public class ModEnglishLanguageProvider extends BaseLanguageProvider
{
	public ModEnglishLanguageProvider(PackOutput packOutput)
	{
		super(packOutput, "en_us");
	}
	
	@Override
	protected void addCreativeTabs()
	{
		addCreativeModeTab(ModCreativeModeTabs.ITEMS.getId().getPath(), "MCE Items");
		addCreativeModeTab(ModCreativeModeTabs.BLOCKS.getId().getPath(), "MCE Blocks");
		addCreativeModeTab(ModCreativeModeTabs.ARMOR.getId().getPath(), "MCE Armor");
		addCreativeModeTab(ModCreativeModeTabs.TOOLS.getId().getPath(), "MCE Tools");
		addCreativeModeTab(ModCreativeModeTabs.TOOL_HEADS.getId().getPath(), "MCE Tool Heads");
		addCreativeModeTab(ModCreativeModeTabs.FOOD.getId().getPath(), "MCE Food");
	}
	
	@Override
	protected void addItems()
	{
		addItem(ModItems.UNIM, "Unim");
		addItem(ModItems.UNIM_INGOT, "Unim Ingot");
		
		addItem(ModItems.RUBY, "Ruby");
		
		addItem(ModItems.ASCABIT_INGOT, "Ascabit Ingot");
		addItem(ModItems.RAW_ASCABIT, "Raw Ascabit");
		
		addItem(ModItems.TIN_INGOT, "Tin Ingot");
		addItem(ModItems.RAW_TIN, "Raw Tin");
		
		addItem(ModItems.TITANIUM_INGOT, "Titanium Ingot");
		addItem(ModItems.RAW_TITANIUM, "Raw Titanium");
		
		addItem(ModItems.ZINC_INGOT, "Zinc Ingot");
		addItem(ModItems.RAW_ZINC, "Raw Zinc");
		
		addItem(ModItems.STEEL_FRAGMENTS, "Steel Fragments");
		addItem(ModItems.STEEL_INGOT, "Steel Ingots");
		
		addItem(ModItems.BRONZE_INGOT, "Bronze Ingot");
		
		addItem(ModItems.BRASS_INGOT, "Brass Ingot");
		
		addItem(ModItems.PULVERISED_COAL, "Pulverised Coal");
		
		addItem(ModItems.UNIM_PLATE, "Unim Plate");
		addItem(ModItems.RUBY_PLATE, "Ruby Plate");
		addItem(ModItems.ASCABIT_PLATE, "Ascabit Plate");
		addItem(ModItems.TIN_PLATE, "Tin Plate");
		addItem(ModItems.TITANIUM_PLATE, "Titanium Plate");
		addItem(ModItems.ZINC_PLATE, "Zinc Plate");
		addItem(ModItems.STEEL_PLATE, "Steel Plate");
		addItem(ModItems.BRONZE_PLATE, "Bronze Plate");
		
		addItem(ModItems.IRON_PLATE, "Iron Plate");
		addItem(ModItems.COPPER_PLATE, "Copper Plate");
		addItem(ModItems.GOLD_PLATE, "Gold Plate");
		addItem(ModItems.DIAMOND_PLATE, "Diamond Plate");
		
		addItem(ModItems.SMALL_BACKPACK, "Small Backpack");
		addItem(ModItems.LARGE_BACKPACK, "Large Backpack");
		addItem(ModItems.ENDER_BACKPACK, "Ender Backpack");
		
		addItem(ModItems.PLANT_FIBER, "Plant Fiber");
		addItem(ModItems.TREE_BARK, "Tree Bark");
		addItem(ModItems.ANIMAL_HIDE, "Animal Hide");
		addItem(ModItems.PREPARED_HIDE, "Prepared Hide");
		addItem(ModItems.ANIMAL_FAT, "Animal Fat");
		addItem(ModItems.DOUGH, "Dough");
		addItem(ModItems.CHAIN, "Chain");
		addItem(ModItems.LEATHER_STRIP, "Leather Strip");
		addItem(ModItems.CEMENT_MIXTURE, "Cement Mixture");
		
		addItem(ModItems.ROCK, "Rock");
		addItem(ModItems.ANDESITE_ROCK, "Andesite Rock");
		addItem(ModItems.DIORITE_ROCK, "Diorite Rock");
		addItem(ModItems.GRANITE_ROCK, "Granite Rock");
		
		addItem(ModItems.RAW_BACON, "Raw Bacon");
		addItem(ModItems.COOKED_BACON, "Cooked Bacon");
		addItem(ModItems.FRIED_EGG, "Fried Egg");
		addItem(ModItems.CHEESE, "Cheese");
		addItem(ModItems.PIZZA, "Pizza");
		addItem(ModItems.CHOCOLATE, "Chocolate");
		addItem(ModItems.POPCORN, "Popcorn");
		addItem(ModItems.PURIFIED_MEAT, "Purified Meat");
		addItem(ModItems.BEEF_JERKY, "Beef Jerky");
		addItem(ModItems.ONION_RINGS, "Onion Rings");
		addItem(ModItems.SAUSAGE, "Sausage");
		addItem(ModItems.HOT_DOG, "Hot Dog");
		addItem(ModItems.GARDEN_SALAD, "Garden Salad");
		addItem(ModItems.HAMBURGER, "Hamburger");
		addItem(ModItems.CHEESEBURGER, "Cheeseburger");
		addItem(ModItems.SUSHI, "Sushi");
		
		addItem(ModItems.APPLE_PIE, "Apple Pie");
		addItem(ModItems.STRAWBERRY_PIE, "Strawberry Pie");
		
		addItem(ModItems.TOMATO_SEEDS, "Tomato Seeds");
		addItem(ModItems.TOMATO, "Tomato");
		
		addItem(ModItems.GRAPE_SEEDS, "Grape Seeds");
		addItem(ModItems.GRAPE, "Grape");
		
		addItem(ModItems.STRAWBERRY_SEEDS, "Strawberry Seeds");
		addItem(ModItems.STRAWBERRY, "Strawberry");
		
		addItem(ModItems.CORN_SEEDS, "Corn Seeds");
		addItem(ModItems.CORN, "Corn");
		
		addItem(ModItems.LETTUCE_SEEDS, "Lettuce Seeds");
		addItem(ModItems.LETTUCE, "Lettuce");
		
		addItem(ModItems.ONION_SEEDS, "Onion Seeds");
		addItem(ModItems.ONION, "Onion");
		
		addItem(ModItems.RICE, "Rice");
		
		addItem(ModItems.VEGETABLE_SOUP, "Vegetable Soup");
		addItem(ModItems.PUMPKIN_SOUP, "Pumpkin Soup");
		addItem(ModItems.CACTUS_SOUP, "Cactus Soup");
		addItem(ModItems.FISH_SOUP, "Fish Soup");
		
		addItem(ModItems.UNFIRED_CLAY_BUCKET, "Unfired Clay Bucket");
		addItem(ModItems.CLAY_BUCKET, "Clay Bucket");
		addItem(ModItems.WATER_CLAY_BUCKET, "Water Clay Bucket");
		
		addItem(ModItems.PORTABLE_BATTERY, "Portable Battery");
		
		addItem(ModItems.REDSTONE_BLEND, "Redstone Blend");
		addItem(ModItems.SOLAR_CELL, "Solar Cell");
		
		addItem(ModItems.STONE_DOOR, "Stone Door");
		addItem(ModItems.SANDSTONE_DOOR, "Sandstone Door");
		addItem(ModItems.GLASS_DOOR, "Glass Door");
		
		addItem(ModItems.OIL_BUCKET, "Oil Bucket");
		
		addItem(ModItems.FLINT_HATCHET, "Flint Hatchet");
		
		addItem(ModItems.FLINT_SHEARS, "Flint Shears");
		
		addItem(ModItems.FLINT_KNIFE, "Flint Knife");
		addItem(ModItems.STONE_KNIFE, "Stone Knife");
		addItem(ModItems.IRON_KNIFE, "Iron Knife");
		addItem(ModItems.UNIM_KNIFE, "Unim Knife");
		addItem(ModItems.DIAMOND_KNIFE, "Diamond Knife");
		
		addItem(ModItems.IRON_HAMMER, "Iron Hammer");
		addItem(ModItems.BRONZE_HAMMER, "Bronze Hammer");
		addItem(ModItems.STEEL_HAMMER, "Steel Hammer");
		addItem(ModItems.DIAMOND_HAMMER, "Diamond Hammer");
		
		addItem(ModItems.UNIM_SWORD, "Unim Sword");
		addItem(ModItems.UNIM_PICKAXE, "Unim Pickaxe");
		addItem(ModItems.UNIM_AXE, "Unim Axe");
		addItem(ModItems.UNIM_SHOVEL, "Unim Shovel");
		addItem(ModItems.UNIM_HOE, "Unim Hoe");
		
		addItem(ModItems.RUBY_SWORD, "Ruby Sword");
		addItem(ModItems.RUBY_PICKAXE, "Ruby Pickaxe");
		addItem(ModItems.RUBY_AXE, "Ruby Axe");
		addItem(ModItems.RUBY_SHOVEL, "Ruby Shovel");
		addItem(ModItems.RUBY_HOE, "Ruby Hoe");
		
		addItem(ModItems.ASCABIT_SWORD, "Ascabit Sword");
		addItem(ModItems.ASCABIT_PICKAXE, "Ascabit Pickaxe");
		addItem(ModItems.ASCABIT_AXE, "Ascabit Axe");
		addItem(ModItems.ASCABIT_SHOVEL, "Ascabit Shovel");
		addItem(ModItems.ASCABIT_HOE, "Ascabit Hoe");
		
		addItem(ModItems.COPPER_SWORD, "Copper Sword");
		addItem(ModItems.COPPER_PICKAXE, "Copper Pickaxe");
		addItem(ModItems.COPPER_AXE, "Copper Axe");
		addItem(ModItems.COPPER_SHOVEL, "Copper Shovel");
		addItem(ModItems.COPPER_HOE, "Copper Hoe");
		
		addItem(ModItems.TIN_SWORD, "Tin Sword");
		addItem(ModItems.TIN_PICKAXE, "Tin Pickaxe");
		addItem(ModItems.TIN_AXE, "Tin Axe");
		addItem(ModItems.TIN_SHOVEL, "Tin Shovel");
		addItem(ModItems.TIN_HOE, "Tin Hoe");
		
		addItem(ModItems.TITANIUM_SWORD, "Titanium Sword");
		addItem(ModItems.TITANIUM_PICKAXE, "Titanium Pickaxe");
		addItem(ModItems.TITANIUM_AXE, "Titanium Axe");
		addItem(ModItems.TITANIUM_SHOVEL, "Titanium Shovel");
		addItem(ModItems.TITANIUM_HOE, "Titanium Hoe");
		
		addItem(ModItems.ZINC_SWORD, "Zinc Sword");
		addItem(ModItems.ZINC_PICKAXE, "Zinc Pickaxe");
		addItem(ModItems.ZINC_AXE, "Zinc Axe");
		addItem(ModItems.ZINC_SHOVEL, "Zinc Shovel");
		addItem(ModItems.ZINC_HOE, "Zinc Hoe");
		
		addItem(ModItems.STEEL_SWORD, "Steel Sword");
		addItem(ModItems.STEEL_PICKAXE, "Steel Pickaxe");
		addItem(ModItems.STEEL_AXE, "Steel Axe");
		addItem(ModItems.STEEL_SHOVEL, "Steel Shovel");
		addItem(ModItems.STEEL_HOE, "Steel Hoe");
		
		addItem(ModItems.BRONZE_SWORD, "Bronze Sword");
		addItem(ModItems.BRONZE_PICKAXE, "Bronze Pickaxe");
		addItem(ModItems.BRONZE_AXE, "Bronze Axe");
		addItem(ModItems.BRONZE_SHOVEL, "Bronze Shovel");
		addItem(ModItems.BRONZE_HOE, "Bronze Hoe");
		
		addItem(ModItems.STEEL_DRILL, "Steel Drill");
		addItem(ModItems.UNIM_DRILL, "Unim Drill");
		addItem(ModItems.DIAMOND_DRILL, "Diamond Drill");
		
		addItem(ModItems.DRILL_BASE, "Drill Base");
		addItem(ModItems.STEEL_DRILL_HEAD, "Steel Drill Head");
		addItem(ModItems.UNIM_DRILL_HEAD, "Unim Drill Head");
		addItem(ModItems.DIAMOND_DRILL_HEAD, "Diamond Drill Head");
		
		addItem(ModItems.FLINT_HATCHET_HEAD, "Flint Hatchet Head");
		
		addItem(ModItems.FLINT_KNIFE_BLADE, "Flint Knife Blade");
		addItem(ModItems.STONE_KNIFE_BLADE, "Stone Knife Blade");
		addItem(ModItems.IRON_KNIFE_BLADE, "Iron Knife Blade");
		addItem(ModItems.UNIM_KNIFE_BLADE, "Unim Knife Blade");
		addItem(ModItems.DIAMOND_KNIFE_BLADE, "Diamond Knife Blade");
		
		addItem(ModItems.IRON_HAMMER_HEAD, "Iron Hammer Head");
		addItem(ModItems.BRONZE_HAMMER_HEAD, "Bronze Hammer Head");
		addItem(ModItems.STEEL_HAMMER_HEAD, "Steel Hammer Head");
		addItem(ModItems.DIAMOND_HAMMER_HEAD, "Diamond Hammer Head");
		
		addItem(ModItems.STONE_SWORD_BLADE, "Stone Sword Blade");
		addItem(ModItems.STONE_PICKAXE_HEAD, "Stone Pickaxe Head");
		addItem(ModItems.STONE_AXE_HEAD, "Stone Axe Head");
		addItem(ModItems.STONE_SHOVEL_HEAD, "Stone Shovel Head");
		addItem(ModItems.STONE_HOE_HEAD, "Stone Hoe Head");
		
		addItem(ModItems.IRON_SWORD_BLADE, "Iron Sword Blade");
		addItem(ModItems.IRON_PICKAXE_HEAD, "Iron Pickaxe Head");
		addItem(ModItems.IRON_AXE_HEAD, "Iron Axe Head");
		addItem(ModItems.IRON_SHOVEL_HEAD, "Iron Shovel Head");
		addItem(ModItems.IRON_HOE_HEAD, "Iron Hoe Head");
		
		addItem(ModItems.GOLDEN_SWORD_BLADE, "Golden Sword Blade");
		addItem(ModItems.GOLDEN_PICKAXE_HEAD, "Golden Pickaxe Head");
		addItem(ModItems.GOLDEN_AXE_HEAD, "Golden Axe Head");
		addItem(ModItems.GOLDEN_SHOVEL_HEAD, "Golden Shovel Head");
		addItem(ModItems.GOLDEN_HOE_HEAD, "Golden Hoe Head");
		
		addItem(ModItems.UNIM_SWORD_BLADE, "Unim Sword Blade");
		addItem(ModItems.UNIM_PICKAXE_HEAD, "Unim Pickaxe Head");
		addItem(ModItems.UNIM_AXE_HEAD, "Unim Axe Head");
		addItem(ModItems.UNIM_SHOVEL_HEAD, "Unim Shovel Head");
		addItem(ModItems.UNIM_HOE_HEAD, "Unim Hoe Head");
		
		addItem(ModItems.RUBY_SWORD_BLADE, "Ruby Sword Blade");
		addItem(ModItems.RUBY_PICKAXE_HEAD, "Ruby Pickaxe Head");
		addItem(ModItems.RUBY_AXE_HEAD, "Ruby Axe Head");
		addItem(ModItems.RUBY_SHOVEL_HEAD, "Ruby Shovel Head");
		addItem(ModItems.RUBY_HOE_HEAD, "Ruby Hoe Head");
		
		addItem(ModItems.ASCABIT_SWORD_BLADE, "Ascabit Sword Blade");
		addItem(ModItems.ASCABIT_PICKAXE_HEAD, "Ascabit Pickaxe Head");
		addItem(ModItems.ASCABIT_AXE_HEAD, "Ascabit Axe Head");
		addItem(ModItems.ASCABIT_SHOVEL_HEAD, "Ascabit Shovel Head");
		addItem(ModItems.ASCABIT_HOE_HEAD, "Ascabit Hoe Head");
		
		addItem(ModItems.COPPER_SWORD_BLADE, "Copper Sword Blade");
		addItem(ModItems.COPPER_PICKAXE_HEAD, "Copper Pickaxe Head");
		addItem(ModItems.COPPER_AXE_HEAD, "Copper Axe Head");
		addItem(ModItems.COPPER_SHOVEL_HEAD, "Copper Shovel Head");
		addItem(ModItems.COPPER_HOE_HEAD, "Copper Hoe Head");
		
		addItem(ModItems.TIN_SWORD_BLADE, "Tin Sword Blade");
		addItem(ModItems.TIN_PICKAXE_HEAD, "Tin Pickaxe Head");
		addItem(ModItems.TIN_AXE_HEAD, "Tin Axe Head");
		addItem(ModItems.TIN_SHOVEL_HEAD, "Tin Shovel Head");
		addItem(ModItems.TIN_HOE_HEAD, "Tin Hoe Head");
		
		addItem(ModItems.TITANIUM_SWORD_BLADE, "Titanium Sword Blade");
		addItem(ModItems.TITANIUM_PICKAXE_HEAD, "Titanium Pickaxe Head");
		addItem(ModItems.TITANIUM_AXE_HEAD, "Titanium Axe Head");
		addItem(ModItems.TITANIUM_SHOVEL_HEAD, "Titanium Shovel Head");
		addItem(ModItems.TITANIUM_HOE_HEAD, "Titanium Hoe Head");
		
		addItem(ModItems.ZINC_SWORD_BLADE, "Zinc Sword Blade");
		addItem(ModItems.ZINC_PICKAXE_HEAD, "Zinc Pickaxe Head");
		addItem(ModItems.ZINC_AXE_HEAD, "Zinc Axe Head");
		addItem(ModItems.ZINC_SHOVEL_HEAD, "Zinc Shovel Head");
		addItem(ModItems.ZINC_HOE_HEAD, "Zinc Hoe Head");
		
		addItem(ModItems.STEEL_SWORD_BLADE, "Steel Sword Blade");
		addItem(ModItems.STEEL_PICKAXE_HEAD, "Steel Pickaxe Head");
		addItem(ModItems.STEEL_AXE_HEAD, "Steel Axe Head");
		addItem(ModItems.STEEL_SHOVEL_HEAD, "Steel Shovel Head");
		addItem(ModItems.STEEL_HOE_HEAD, "Steel Hoe Head");
		
		addItem(ModItems.BRONZE_SWORD_BLADE, "Bronze Sword Blade");
		addItem(ModItems.BRONZE_PICKAXE_HEAD, "Bronze Pickaxe Head");
		addItem(ModItems.BRONZE_AXE_HEAD, "Bronze Axe Head");
		addItem(ModItems.BRONZE_SHOVEL_HEAD, "Bronze Shovel Head");
		addItem(ModItems.BRONZE_HOE_HEAD, "Bronze Hoe Head");
		
		addItem(ModItems.UNIM_HELMET, "Unim Helmet");
		addItem(ModItems.UNIM_CHESTPLATE, "Unim Chestplate");
		addItem(ModItems.UNIM_LEGGINGS, "Unim Leggings");
		addItem(ModItems.UNIM_BOOTS, "Unim Boots");
		
		addItem(ModItems.RUBY_HELMET, "Ruby Helmet");
		addItem(ModItems.RUBY_CHESTPLATE, "Ruby Chestplate");
		addItem(ModItems.RUBY_LEGGINGS, "Ruby Leggings");
		addItem(ModItems.RUBY_BOOTS, "Ruby Boots");
		
		addItem(ModItems.ASCABIT_HELMET, "Ascabit Helmet");
		addItem(ModItems.ASCABIT_CHESTPLATE, "Ascabit Chestplate");
		addItem(ModItems.ASCABIT_LEGGINGS, "Ascabit Leggings");
		addItem(ModItems.ASCABIT_BOOTS, "Ascabit Boots");
		
		addItem(ModItems.COPPER_HELMET, "Copper Helmet");
		addItem(ModItems.COPPER_CHESTPLATE, "Copper Chestplate");
		addItem(ModItems.COPPER_LEGGINGS, "Copper Leggings");
		addItem(ModItems.COPPER_BOOTS, "Copper Boots");
		
		addItem(ModItems.TIN_HELMET, "Tin Helmet");
		addItem(ModItems.TIN_CHESTPLATE, "Tin Chestplate");
		addItem(ModItems.TIN_LEGGINGS, "Tin Leggings");
		addItem(ModItems.TIN_BOOTS, "Tin Boots");
		
		addItem(ModItems.TITANIUM_HELMET, "Titanium Helmet");
		addItem(ModItems.TITANIUM_CHESTPLATE, "Titanium Chestplate");
		addItem(ModItems.TITANIUM_LEGGINGS, "Titanium Leggings");
		addItem(ModItems.TITANIUM_BOOTS, "Titanium Boots");
		
		addItem(ModItems.ZINC_HELMET, "Zinc Helmet");
		addItem(ModItems.ZINC_CHESTPLATE, "Zinc Chestplate");
		addItem(ModItems.ZINC_LEGGINGS, "Zinc Leggings");
		addItem(ModItems.ZINC_BOOTS, "Zinc Boots");
		
		addItem(ModItems.BRONZE_HELMET, "Bronze Helmet");
		addItem(ModItems.BRONZE_CHESTPLATE, "Bronze Chestplate");
		addItem(ModItems.BRONZE_LEGGINGS, "Bronze Leggings");
		addItem(ModItems.BRONZE_BOOTS, "Bronze Boots");
		
		addItem(ModItems.STEEL_HELMET, "Steel Helmet");
		addItem(ModItems.STEEL_CHESTPLATE, "Steel Chestplate");
		addItem(ModItems.STEEL_LEGGINGS, "Steel Leggings");
		addItem(ModItems.STEEL_BOOTS, "Steel Boots");
	}
	
	@Override
	protected void addBlocks()
	{
		addBlock(ModBlocks.UNIM_ORE, "Unim Ore");
		addBlock(ModBlocks.DEEPSLATE_UNIM_ORE, "Deepslate Unim Ore");
		addBlock(ModBlocks.UNIM_BLOCK, "Unim Block");
		
		addBlock(ModBlocks.RUBY_ORE, "Ruby Ore");
		addBlock(ModBlocks.DEEPSLATE_RUBY_ORE, "Deepslate Ruby Ore");
		addBlock(ModBlocks.RUBY_BLOCK, "Ruby Block");
		
		addBlock(ModBlocks.ASCABIT_ORE, "Ascabit Ore");
		addBlock(ModBlocks.DEEPSLATE_ASCABIT_ORE, "Deepslate Ascabit Ore");
		addBlock(ModBlocks.ASCABIT_BLOCK, "Ascabit Block");
		addBlock(ModBlocks.RAW_ASCABIT_BLOCK, "Raw Ascabit Block");
		
		addBlock(ModBlocks.TIN_ORE, "Tin Ore");
		addBlock(ModBlocks.DEEPSLATE_TIN_ORE, "Deepslate Tin Ore");
		addBlock(ModBlocks.TIN_BLOCK, "Tin Block");
		addBlock(ModBlocks.RAW_TIN_BLOCK, "Raw Tin Block");
		
		addBlock(ModBlocks.TITANIUM_ORE, "Titanium Ore");
		addBlock(ModBlocks.DEEPSLATE_TITANIUM_ORE, "Deepslate Titanium Ore");
		addBlock(ModBlocks.TITANIUM_BLOCK, "Titanium Block");
		addBlock(ModBlocks.RAW_TITANIUM_BLOCK, "Raw Titanium Block");
		
		addBlock(ModBlocks.ZINC_ORE, "Zinc Ore");
		addBlock(ModBlocks.DEEPSLATE_ZINC_ORE, "Deepslate Zinc Ore");
		addBlock(ModBlocks.ZINC_BLOCK, "Zinc Block");
		addBlock(ModBlocks.RAW_ZINC_BLOCK, "Raw Zinc Block");
		
		addBlock(ModBlocks.STEEL_BLOCK, "Steel Block");
		
		addBlock(ModBlocks.BRONZE_BLOCK, "Bronze Block");
		
		addBlock(ModBlocks.BRASS_BLOCK, "Brass Block");
		
		addBlock(ModBlocks.CEMENT, "Cement");
		addBlock(ModBlocks.CEMENT_STAIRS, "Cement Stairs");
		addBlock(ModBlocks.CEMENT_SLAB, "Cement Slab");
		addBlock(ModBlocks.CEMENT_WALL, "Cement Wall");
		
		addBlock(ModBlocks.CEMENT_PLATE, "Cement Plate");
		addBlock(ModBlocks.CEMENT_PLATE_STAIRS, "Cement Plate Stairs");
		addBlock(ModBlocks.CEMENT_PLATE_SLAB, "Cement Plate Slab");
		addBlock(ModBlocks.CEMENT_PLATE_WALL, "Cement Plate Wall");
		
		addBlock(ModBlocks.LIMESTONE, "Limestone");
		addBlock(ModBlocks.LIMESTONE_STAIRS, "Limestone Stairs");
		addBlock(ModBlocks.LIMESTONE_SLAB, "Limestone Slab");
		addBlock(ModBlocks.LIMESTONE_WALL, "Limestone Wall");
		
		addBlock(ModBlocks.LIMESTONE_BRICKS, "Limestone Bricks");
		addBlock(ModBlocks.LIMESTONE_BRICK_STAIRS, "Limestone Brick Stairs");
		addBlock(ModBlocks.LIMESTONE_BRICK_SLAB, "Limestone Brick Slab");
		addBlock(ModBlocks.LIMESTONE_BRICK_WALL, "Limestone Brick Wall");
		
		addBlock(ModBlocks.MARBLE, "Marble");
		addBlock(ModBlocks.MARBLE_STAIRS, "Marble Stairs");
		addBlock(ModBlocks.MARBLE_SLAB, "Marble Slab");
		addBlock(ModBlocks.MARBLE_WALL, "Marble Wall");
		
		addBlock(ModBlocks.MARBLE_COBBLE, "Marble Cobblestone");
		addBlock(ModBlocks.MARBLE_COBBLE_STAIRS, "Marble Cobblestone Stairs");
		addBlock(ModBlocks.MARBLE_COBBLE_SLAB, "Marble Cobblestone Slab");
		addBlock(ModBlocks.MARBLE_COBBLE_WALL, "Marble Cobblestone Wall");
		
		addBlock(ModBlocks.MARBLE_BRICKS, "Marble Bricks");
		addBlock(ModBlocks.MARBLE_BRICK_STAIRS, "Marble Brick Stairs");
		addBlock(ModBlocks.MARBLE_BRICK_SLAB, "Marble Brick Slab");
		addBlock(ModBlocks.MARBLE_BRICK_WALL, "Marble Brick Wall");
		
		addBlock(ModBlocks.MARBLE_LAMP, "Marble Lamp");
		
		addBlock(ModBlocks.DIRT_SLAB, "Dirt Slab");
		addBlock(ModBlocks.COARSE_DIRT_SLAB, "Coarse Dirt Slab");
		addBlock(ModBlocks.DIRT_PATH_SLAB, "Dirt Path Slab");
		addBlock(ModBlocks.GRASS_BLOCK_SLAB, "Grass Block Slab");
		
		addBlock(ModBlocks.GLASS_SLAB, "Glass Slab");
		
		//addBlock(ModBlocks.STONE_DOOR, "Stone Door");
		addBlock(ModBlocks.STONE_TRAPDOOR, "Stone Trapdoor");
		
		//addBlock(ModBlocks.SANDSTONE_DOOR, "Sandstone Door");
		addBlock(ModBlocks.SANDSTONE_TRAPDOOR, "Sandstone Trapdoor");
		
		//addBlock(ModBlocks.GLASS_DOOR, "Glass Door");
		addBlock(ModBlocks.GLASS_TRAPDOOR, "Glass Trapdoor");
		
		addBlock(ModBlocks.WOODEN_GLASS_FRAME, "Wooden Glass Frame");
		
		addBlock(ModBlocks.KILN, "Kiln");
		addBlock(ModBlocks.SMELTER, "Smelter");
		addBlock(ModBlocks.MILLSTONE, "Millstone");
		
		add("inventory." + MODID + ".chopping_block", "Chopping Block");
		addBlock(ModBlocks.OAK_CHOPPING_BLOCK, "Oak Chopping Block");
		addBlock(ModBlocks.SPRUCE_CHOPPING_BLOCK, "Spruce Chopping Block");
		addBlock(ModBlocks.BIRCH_CHOPPING_BLOCK, "Birch Chopping Block");
		addBlock(ModBlocks.JUNGLE_CHOPPING_BLOCK, "Jungle Chopping Block");
		addBlock(ModBlocks.ACACIA_CHOPPING_BLOCK, "Acacia Chopping Block");
		addBlock(ModBlocks.DARK_OAK_CHOPPING_BLOCK, "Dark Oak Chopping Block");
		addBlock(ModBlocks.MANGROVE_CHOPPING_BLOCK, "Mangrove Chopping Block");
		addBlock(ModBlocks.CHERRY_CHOPPING_BLOCK, "Cherry Chopping Block");
		addBlock(ModBlocks.CRIMSON_CHOPPING_BLOCK, "Crimson Chopping Block");
		addBlock(ModBlocks.WARPED_CHOPPING_BLOCK, "Warped Chopping Block");
		
		add("inventory." + MODID + ".drying_rack", "Drying Rack");
		addBlock(ModBlocks.OAK_DRYING_RACK, "Oak Drying Rack");
		addBlock(ModBlocks.SPRUCE_DRYING_RACK, "Spruce Drying Rack");
		addBlock(ModBlocks.BIRCH_DRYING_RACK, "Birch Drying Rack");
		addBlock(ModBlocks.JUNGLE_DRYING_RACK, "Jungle Drying Rack");
		addBlock(ModBlocks.ACACIA_DRYING_RACK, "Acacia Drying Rack");
		addBlock(ModBlocks.DARK_OAK_DRYING_RACK, "Dark Oak Drying Rack");
		addBlock(ModBlocks.MANGROVE_DRYING_RACK, "Mangrove Drying Rack");
		addBlock(ModBlocks.CHERRY_DRYING_RACK, "Cherry Drying Rack");
		addBlock(ModBlocks.CRIMSON_DRYING_RACK, "Crimson Drying Rack");
		addBlock(ModBlocks.WARPED_DRYING_RACK, "Warped Drying Rack");
		
		addBlock(ModBlocks.MACHINE_FRAME, "Machine Frame");
		
		addBlock(ModBlocks.COAL_GENERATOR, "Coal Generator");
		addBlock(ModBlocks.BATTERY, "Battery");
		addBlock(ModBlocks.SOLAR_PANEL, "Solar Panel");
		
		addBlock(ModBlocks.LIQUID_TANK, "Liquid Tank");
		
		addBlock(ModBlocks.ELECTRIC_BREWERY, "Electric Brewery");
		addBlock(ModBlocks.ELECTRIC_SMELTER, "Electric Smelter");
		addBlock(ModBlocks.ELECTRIC_GREENHOUSE, "Electric Greenhouse");
		
		addBlock(ModBlocks.HYDRAULIC_PRESS, "Hydraulic Press");
		
		addBlock(ModBlocks.COPPER_CABLE, "Copper Cable");
		
		addBlock(ModBlocks.TOMATOES, "Tomatoes");
		addBlock(ModBlocks.GRAPES, "Grapes");
		addBlock(ModBlocks.STRAWBERRIES, "Strawberries");
		addBlock(ModBlocks.CORNS, "Corns");
		addBlock(ModBlocks.LETTUCES, "Lettuces");
		addBlock(ModBlocks.ONIONS, "Onions");
		addBlock(ModBlocks.RICE, "Rice");
		
		addBlock(ModBlocks.CHOCOLATE_CAKE, "Chocolate Cake");
		
		addBlock(ModBlocks.OIL, "Oil");
	}
	
	@Override
	protected void addEntityTypes()
	{
		addEntityType(ModEntityTypes.ROCK, "Rock");
		addEntityType(ModEntityTypes.ANDESITE_ROCK, "Andesite Rock");
		addEntityType(ModEntityTypes.DIORITE_ROCK, "Diorite Rock");
		addEntityType(ModEntityTypes.GRANITE_ROCK, "Granite Rock");
	}
	
	@Override
	protected void addAdvancements()
	{
		add("advancements.stoneage.root.title", "Stone Age");
		add("advancements.stoneage.root.description", "Welcome to the REAL stone age!");
		
		add("advancements.stoneage.torch.title", "Light up");
		add("advancements.stoneage.torch.description", "Use some animal fat on a stick to light up your area!");
		
		add("advancements.stoneage.choppingblock.title", "Chop Chop");
		add("advancements.stoneage.choppingblock.description", "Craft a chopping block to chop some logs!");
		
		add("advancements.stoneage.dryingrack.title", "No item display");
		add("advancements.stoneage.dryingrack.description", "Craft a drying rack to dry hide!");
		
		add("advancements.stoneage.preparedhide.title", "Prepare yourself");
		add("advancements.stoneage.preparedhide.description", "Craft some prepared hide!");
		
		add("advancements.stoneage.leather.title", "Leather? Works");
		add("advancements.stoneage.leather.description", "Dry some hide and make leather!");
		
		add("advancements.stoneage.firsttool.title", "First Tool");
		add("advancements.stoneage.firsttool.description", "Use some flint, plant fiber and sticks to make a primitive tool!");
		
		add("advancements.stoneage.throwrock.title", "Catch the rock");
		add("advancements.stoneage.throwrock.description", "Kill something (or someone) with a rock!");
		
		add("advancements.stoneage.stonemaker.title", "Stone Maker");
		add("advancements.stoneage.stonemaker.description", "Craft some cobblestone out of rocks!");
		
		add("advancements.stoneage.millstone.title", "The Grind is real");
		add("advancements.stoneage.millstone.description", "Craft a millstone!");
		
		add("advancements.stoneage.toolhead.title", "Matters of the head");
		add("advancements.stoneage.toolhead.description", "Form a tool head out of rocks!");
		
		add("advancements.stoneage.bettertool.title", "The Upgrade");
		add("advancements.stoneage.bettertool.description", "Make your first stone pickaxe!");
		
		add("advancements.stoneage.kiln.title", "Another furnace");
		add("advancements.stoneage.kiln.description", "Craft a kiln!");
		
		add("advancements.stoneage.claybucket.title", "Cheap Bucket");
		add("advancements.stoneage.claybucket.description", "Craft a clay bucket!");
		
		add("advancements.stoneage.cement.title", "Cement mixer");
		add("advancements.stoneage.cement.description", "Use a kiln to make cement!");
		
		add("advancements.stoneage.smelter.title", "Heat Up");
		add("advancements.stoneage.smelter.description", "Craft a smelter to make some ingots!");
		
		add("advancements.metalage.root.title", "Metal Age");
		add("advancements.metalage.root.description", "Welcome to the metal age!");
		
		add("advancements.metalage.hammer.title", "Hammer Time");
		add("advancements.metalage.hammer.description", "Craft a hammer!");
		
		add("advancements.metalage.pickaxe.title", "Time to Mine");
		add("advancements.metalage.pickaxe.description", "Craft a pickaxe out of metal!");
		
		add("advancements.metalage.protection.title", "Protection");
		add("advancements.metalage.protection.description", "Craft some metal armor!");
		
		add("advancements.metalage.darkiron.title", "Dark Iron");
		add("advancements.metalage.darkiron.description", "Smelt some steel!");
		
		add("advancements.industrialage.root.title", "Industrial Age");
		add("advancements.industrialage.root.description", "Welcome to the industrial age!");
		
		add("advancements.industrialage.redstoneblend.title", "Blender");
		add("advancements.industrialage.redstoneblend.description", "Craft some redstone blend!");
		
		add("advancements.industrialage.machineframe.title", "Framing");
		add("advancements.industrialage.machineframe.description", "Craft a machine frame!");
		
		add("advancements.industrialage.greenenergy.title", "Green Energy");
		add("advancements.industrialage.greenenergy.description", "Craft a solar panel!");
		
		add("advancements.industrialage.blackenergy.title", "Black Energy");
		add("advancements.industrialage.blackenergy.description", "Craft a coal generator!");
	}
	
	@Override
	protected void addEnchantments()
	{
		addEnchantment(ModEnchantments.POWER_SAVING, "Power Saving", "Reduces the energy usage by half.");
	}
	
	@Override
	protected void addStats()
	{
		addStat(ModStats.CHOPPED_WOOD.getId().getPath(), "Chopped Wood");
	}
	
	@Override
	protected void addGuideTexts()
	{
		add("guide." + MODID + ".title", "MCE Guide");
		add("guide." + MODID + ".landing", "Welcome to the MCE guide!");
		
		add("guide." + MODID + ".category.stone_age.name", "Stone Age");
		add("guide." + MODID + ".category.stone_age.description", "TODO");
		
		add("guide." + MODID + ".category.metal_age.name", "Metal Age");
		add("guide." + MODID + ".category.metal_age.description", "TODO");
		
		add("guide." + MODID + ".category.industrial_age.name", "Industrial Age");
		add("guide." + MODID + ".category.industrial_age.description", "TODO");
		
		add("guide." + MODID + ".entry.torch.name", "Torch");
		add("guide." + MODID + ".entry.torch.text", "A primitive way to make $(item)Torches$(clear) is to use $(item)Animal Fat$(clear) and a $(item)Stick$(clear).$(br)$(item)Animal Fat$(clear) can be obtained by killing animals.");
		
		add("guide." + MODID + ".entry.chopping_block.name", "Chopping Block");
		add("guide." + MODID + ".entry.chopping_block.text", "The $(item)Chopping Block$(clear) can be used to effectively process wood, e.g. making $(item)Planks$(clear) or $(item)Slabs$(clear).$(br)Simply punch the wood with your $(item)Axe$(clear) to process it.$(br2)$(italic)Note: You can use any type of wood to make a $(item)Chopping Block$(clear).");
		
		add("guide." + MODID + ".entry.leather.name", "Leather");
		add("guide." + MODID + ".entry.leather.text1", "Mobs like cows now drop $(item)Animal Hide$(clear) instead of ready to use $(item)Leather$(clear).$(br)You first need to prepare the hide.");
		add("guide." + MODID + ".entry.leather.text2", "Then put the $(item)Prepared Hide$(clear) on a $(item)Drying Rack$(clear) to get $(item)Leather$(clear). Drying will take some time.$(br2)$(italic)Note: You can use any type of wood to make a $(item)Drying Rack$(clear).");
		
		add("guide." + MODID + ".entry.first_tool.name", "First Tool");
		add("guide." + MODID + ".entry.first_tool.text1", "You first need to get some $(item)Flint$(clear), $(item)Plant fiber$(clear) and $(item)Sticks$(clear) to craft your first primitive tool:$(br)the $(item)Flint Hatchet$(clear).");
		add("guide." + MODID + ".entry.first_tool.text2", "You can obtain $(item)Plant fiber$(clear) by breaking $(item)Grass$(clear) and rarely when breaking $(item)Leaves$(clear). $(item)Plant Fiber$(clear) is important for any primitive tool.");
		
		add("guide." + MODID + ".entry.stonemaker.name", "Make stone");
		add("guide." + MODID + ".entry.stonemaker.text", "As you may already noticed breaking $(item)Stone$(clear) will give you small $(item)Rocks$(clear) instead of the full blocks.$(br2)Use 4 $(item)Rocks$(clear) to craft 2 $(item)Cobblestone$(clear).");
		
		add("guide." + MODID + ".entry.better_tool.name", "Better Tool");
		add("guide." + MODID + ".entry.better_tool.text", "In order to make better tools you need a $(item)Tool Head$(clear), a $(item)Stick$(clear) and a $(item)Plant Fiber$(clear).$(br2)You first want to make a $(item)Stone Pickaxe$(clear).");
	}
	
	@Override
	protected void addOtherTexts()
	{
		add("jei." + MODID + ".plant_fiber.info", "Can be obtained by breaking grass or rarely when breaking leaves.");
		add("jei." + MODID + ".flint.info", "Can be obtained by breaking grass or when digging gravel.");
		add("jei." + MODID + ".tree_bark.info", "Can be obtained by stripping logs.");
		
		add("config.jade.plugin_" + MODID + ".smelter", "Smelter heat");
		add("jade." + MODID + ".smelter.heat", "% Heat");
		
		add("config.jade.plugin_" + MODID + ".drying_rack", "Drying Rack ticks");
		add("jade." + MODID + ".drying_rack.ticks", "% dried");
		
		add("inventory." + MODID + ".millstone.nowater", "No flowing water nearby!");
		
		add("inventory." + MODID + ".solar_panel.cantseesky", "Can't see sky!");
		add("inventory." + MODID + ".solar_panel.nighttime", "No sunlight! (night time)");
		
		add(ModKeyMappings.KEY_CATEGORY, "MCE Keys");
		add("key." + MODID + ".open_backpack", "Open Backpack");
		
		add("updater." + MODID + ".uptodate", "§e[Updater]§r §2MCE§r is up-to-date!");
		add("updater." + MODID + ".beta", "§e[Updater]§r You are using a §cBeta§r version of §2MCE§r.");
		add("updater." + MODID + ".outdated", "§e[Updater]§r §2MCE§r is outdated! Your version: §c%s§r, newest version: §c%s§r");
		add("updater." + MODID + ".failed", "§e[Updater]§r Version check for §2MCE§r failed.");
		
		add("item.nouse", "§oThis item has currently no use!");
	}
}
