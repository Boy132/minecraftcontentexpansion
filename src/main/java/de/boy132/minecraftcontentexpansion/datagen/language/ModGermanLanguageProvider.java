package de.boy132.minecraftcontentexpansion.datagen.language;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.entchantments.ModEnchantments;
import de.boy132.minecraftcontentexpansion.entity.ModEntityTypes;
import de.boy132.minecraftcontentexpansion.item.ModCreativeModeTabs;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.keybinds.ModKeyMappings;
import de.boy132.minecraftcontentexpansion.stats.ModStats;
import net.minecraft.data.PackOutput;

public class ModGermanLanguageProvider extends BaseLanguageProvider
{
	public ModGermanLanguageProvider(PackOutput packOutput)
	{
		super(packOutput, "de_de");
	}
	
	@Override
	protected void addCreativeTabs()
	{
		addCreativeModeTab(ModCreativeModeTabs.ITEMS.getId().getPath(), "MCE Items");
		addCreativeModeTab(ModCreativeModeTabs.BLOCKS.getId().getPath(), "MCE Blöcke");
		addCreativeModeTab(ModCreativeModeTabs.ARMOR.getId().getPath(), "MCE Rüstung");
		addCreativeModeTab(ModCreativeModeTabs.TOOLS.getId().getPath(), "MCE Werkzeuge");
		addCreativeModeTab(ModCreativeModeTabs.TOOL_HEADS.getId().getPath(), "MCE Werkzeugköpfe");
		addCreativeModeTab(ModCreativeModeTabs.FOOD.getId().getPath(), "MCE Nahrung");
	}
	
	@Override
	protected void addItems()
	{
		addItem(ModItems.UNIM, "Unim");
		addItem(ModItems.UNIM_INGOT, "Unimbarren");
		
		addItem(ModItems.RUBY, "Rubin");
		
		addItem(ModItems.ASCABIT_INGOT, "Askabitbarren");
		addItem(ModItems.RAW_ASCABIT, "Rohaskabit");
		
		addItem(ModItems.TIN_INGOT, "Zinnbarren");
		addItem(ModItems.RAW_TIN, "Rohzinn");
		
		addItem(ModItems.TITANIUM_INGOT, "Titanbarren");
		addItem(ModItems.RAW_TITANIUM, "Rohtitan");
		
		addItem(ModItems.ZINC_INGOT, "Zinkbarren");
		addItem(ModItems.RAW_ZINC, "Rohzink");
		
		addItem(ModItems.STEEL_FRAGMENTS, "Stahlfragmente");
		addItem(ModItems.STEEL_INGOT, "Stahlbarren");
		
		addItem(ModItems.BRONZE_INGOT, "Bronzebarren");
		
		addItem(ModItems.BRASS_INGOT, "Messingbarren");
		
		addItem(ModItems.PULVERISED_COAL, "Pulverisierte Kohle");
		
		addItem(ModItems.UNIM_PLATE, "Unimplatte");
		addItem(ModItems.RUBY_PLATE, "Rubinplatte");
		addItem(ModItems.ASCABIT_PLATE, "Askabitplatte");
		addItem(ModItems.TIN_PLATE, "Zinnplatte");
		addItem(ModItems.TITANIUM_PLATE, "Titanplatte");
		addItem(ModItems.ZINC_PLATE, "Zinkplatte");
		addItem(ModItems.STEEL_PLATE, "Stahlplatte");
		addItem(ModItems.BRONZE_PLATE, "Bronzeplatte");
		
		addItem(ModItems.IRON_PLATE, "Eisenplatte");
		addItem(ModItems.COPPER_PLATE, "Kupferplatte");
		addItem(ModItems.GOLD_PLATE, "Goldplatte");
		addItem(ModItems.DIAMOND_PLATE, "Diamantplatte");
		
		addItem(ModItems.SMALL_BACKPACK, "Kleiner Rucksack");
		addItem(ModItems.LARGE_BACKPACK, "Großer Rucksack");
		addItem(ModItems.ENDER_BACKPACK, "Ender Rucksack");
		
		addItem(ModItems.PLANT_FIBER, "Pflanzenfaser");
		addItem(ModItems.TREE_BARK, "Baumrinde");
		addItem(ModItems.ANIMAL_HIDE, "Tierhaut");
		addItem(ModItems.PREPARED_HIDE, "Vorbereitete Haut");
		addItem(ModItems.ANIMAL_FAT, "Tierfett");
		addItem(ModItems.DOUGH, "Teig");
		addItem(ModItems.CHAIN, "Kette");
		addItem(ModItems.LEATHER_STRIP, "Lederstreifen");
		addItem(ModItems.CEMENT_MIXTURE, "Zementmixtur");
		
		addItem(ModItems.ROCK, "Stein");
		addItem(ModItems.ANDESITE_ROCK, "Andesit Stein");
		addItem(ModItems.DIORITE_ROCK, "Diorit Stein");
		addItem(ModItems.GRANITE_ROCK, "Granit Stein");
		
		addItem(ModItems.RAW_BACON, "Roher Speck");
		addItem(ModItems.COOKED_BACON, "Gebratener Speck");
		addItem(ModItems.FRIED_EGG, "Spiegelei");
		addItem(ModItems.CHEESE, "Käse");
		addItem(ModItems.PIZZA, "Pizza");
		addItem(ModItems.CHOCOLATE, "Schokolade");
		addItem(ModItems.POPCORN, "Popcorn");
		addItem(ModItems.PURIFIED_MEAT, "Gereinigtes Fleisch");
		addItem(ModItems.BEEF_JERKY, "Beef Jerky");
		addItem(ModItems.ONION_RINGS, "Zwiebelringe");
		addItem(ModItems.SAUSAGE, "Wurst");
		addItem(ModItems.HOT_DOG, "Hot Dog");
		addItem(ModItems.GARDEN_SALAD, "Gartensalat");
		addItem(ModItems.HAMBURGER, "Hamburger");
		addItem(ModItems.CHEESEBURGER, "Cheeseburger");
		addItem(ModItems.SUSHI, "Sushi");
		
		addItem(ModItems.APPLE_PIE, "Apfelkuchen");
		addItem(ModItems.STRAWBERRY_PIE, "Erdbeerkuchen");
		
		addItem(ModItems.TOMATO_SEEDS, "Tomatensamen");
		addItem(ModItems.TOMATO, "Tomate");
		
		addItem(ModItems.GRAPE_SEEDS, "Weintraubensamen");
		addItem(ModItems.GRAPE, "Weintraube");
		
		addItem(ModItems.STRAWBERRY_SEEDS, "Erdbeersamen");
		addItem(ModItems.STRAWBERRY, "Erdbeere");
		
		addItem(ModItems.CORN_SEEDS, "Maissamen");
		addItem(ModItems.CORN, "Mais");
		
		addItem(ModItems.LETTUCE_SEEDS, "Salatsamen");
		addItem(ModItems.LETTUCE, "Salat");
		
		addItem(ModItems.ONION_SEEDS, "Zwiebelsamen");
		addItem(ModItems.ONION, "Zwiebel");
		
		addItem(ModItems.RICE, "Reis");
		
		addItem(ModItems.VEGETABLE_SOUP, "Gemüsesuppe");
		addItem(ModItems.PUMPKIN_SOUP, "Kürbissuppe");
		addItem(ModItems.CACTUS_SOUP, "Kaktussuppe");
		addItem(ModItems.FISH_SOUP, "Fischsuppe");
		
		addItem(ModItems.UNFIRED_CLAY_BUCKET, "ungebrannter Lehmeimer");
		addItem(ModItems.CLAY_BUCKET, "Lehmeimer");
		addItem(ModItems.WATER_CLAY_BUCKET, "Wasser Lehmeimer");
		
		addItem(ModItems.PORTABLE_BATTERY, "Tragbare Batterie");
		
		addItem(ModItems.REDSTONE_BLEND, "Redstone Mischung");
		addItem(ModItems.SOLAR_CELL, "Solarzelle");
		
		addItem(ModItems.STONE_DOOR, "Steintür");
		addItem(ModItems.SANDSTONE_DOOR, "Sandsteintür");
		addItem(ModItems.GLASS_DOOR, "Glastür");
		
		addItem(ModItems.OIL_BUCKET, "Öleimer");
		
		addItem(ModItems.FLINT_HATCHET, "Feuersteinbeil");
		
		addItem(ModItems.FLINT_SHEARS, "Feuersteinschere");
		
		addItem(ModItems.FLINT_KNIFE, "Feuersteinmesser");
		addItem(ModItems.STONE_KNIFE, "Steinmesser");
		addItem(ModItems.IRON_KNIFE, "Eisenmesser");
		addItem(ModItems.UNIM_KNIFE, "Unimmesser");
		addItem(ModItems.DIAMOND_KNIFE, "Diamantmesser");
		
		addItem(ModItems.IRON_HAMMER, "Eisenhammer");
		addItem(ModItems.BRONZE_HAMMER, "Bronzehammer");
		addItem(ModItems.STEEL_HAMMER, "Stahlhammer");
		addItem(ModItems.DIAMOND_HAMMER, "Diamanthammer");
		
		addItem(ModItems.UNIM_SWORD, "Unimschwert");
		addItem(ModItems.UNIM_PICKAXE, "Unimspitzhacke");
		addItem(ModItems.UNIM_AXE, "Unimaxt");
		addItem(ModItems.UNIM_SHOVEL, "Unimschaufel");
		addItem(ModItems.UNIM_HOE, "Unimhacke");
		
		addItem(ModItems.RUBY_SWORD, "Rubinschwert");
		addItem(ModItems.RUBY_PICKAXE, "Rubinspitzhacke");
		addItem(ModItems.RUBY_AXE, "Rubinaxt");
		addItem(ModItems.RUBY_SHOVEL, "Rubinschaufel");
		addItem(ModItems.RUBY_HOE, "Rubinhacke");
		
		addItem(ModItems.ASCABIT_SWORD, "Askabitschwert");
		addItem(ModItems.ASCABIT_PICKAXE, "Askabitspitzhacke");
		addItem(ModItems.ASCABIT_AXE, "Askabitaxt");
		addItem(ModItems.ASCABIT_SHOVEL, "Askabitschaufel");
		addItem(ModItems.ASCABIT_HOE, "Askabithacke");
		
		addItem(ModItems.COPPER_SWORD, "Kupferschwert");
		addItem(ModItems.COPPER_PICKAXE, "Kupferspitzhacke");
		addItem(ModItems.COPPER_AXE, "Kupferaxt");
		addItem(ModItems.COPPER_SHOVEL, "Kupferschaufel");
		addItem(ModItems.COPPER_HOE, "Kupferhacke");
		
		addItem(ModItems.TIN_SWORD, "Zinnschwert");
		addItem(ModItems.TIN_PICKAXE, "Zinnspitzhacke");
		addItem(ModItems.TIN_AXE, "Zinnaxt");
		addItem(ModItems.TIN_SHOVEL, "Zinnschaufel");
		addItem(ModItems.TIN_HOE, "Zinnhacke");
		
		addItem(ModItems.TITANIUM_SWORD, "Titanschwert");
		addItem(ModItems.TITANIUM_PICKAXE, "Titanspitzhacke");
		addItem(ModItems.TITANIUM_AXE, "Titanaxt");
		addItem(ModItems.TITANIUM_SHOVEL, "Titanschaufel");
		addItem(ModItems.TITANIUM_HOE, "Titanhacke");
		
		addItem(ModItems.ZINC_SWORD, "Zinkschwert");
		addItem(ModItems.ZINC_PICKAXE, "Zinkspitzhacke");
		addItem(ModItems.ZINC_AXE, "Zinkaxt");
		addItem(ModItems.ZINC_SHOVEL, "Zinkschaufel");
		addItem(ModItems.ZINC_HOE, "Zinkhacke");
		
		addItem(ModItems.STEEL_SWORD, "Stahlschwert");
		addItem(ModItems.STEEL_PICKAXE, "Stahlspitzhacke");
		addItem(ModItems.STEEL_AXE, "Stahlaxt");
		addItem(ModItems.STEEL_SHOVEL, "Stahlschaufel");
		addItem(ModItems.STEEL_HOE, "Stahlhacke");
		
		addItem(ModItems.BRONZE_SWORD, "Bronzeschwert");
		addItem(ModItems.BRONZE_PICKAXE, "Bronzespitzhacke");
		addItem(ModItems.BRONZE_AXE, "Bronzeaxt");
		addItem(ModItems.BRONZE_SHOVEL, "Bronzeschaufel");
		addItem(ModItems.BRONZE_HOE, "Bronzehacke");
		
		addItem(ModItems.STEEL_DRILL, "Stahlbohrer");
		addItem(ModItems.UNIM_DRILL, "Unimbohrer");
		addItem(ModItems.DIAMOND_DRILL, "Diamandbohrer");
		
		addItem(ModItems.DRILL_BASE, "Bohrerbasis");
		addItem(ModItems.STEEL_DRILL_HEAD, "Stahlbohrerkopf");
		addItem(ModItems.UNIM_DRILL_HEAD, "Unimbohrerkopf");
		addItem(ModItems.DIAMOND_DRILL_HEAD, "Diamantbohrerkopf");
		
		addItem(ModItems.FLINT_HATCHET_HEAD, "Feuersteinbeilkopf");
		
		addItem(ModItems.FLINT_KNIFE_BLADE, "Feuersteinmesserklinge");
		addItem(ModItems.STONE_KNIFE_BLADE, "Steinmesserklinge");
		addItem(ModItems.IRON_KNIFE_BLADE, "Eisenmesserklinge");
		addItem(ModItems.UNIM_KNIFE_BLADE, "Unimmesserklinge");
		addItem(ModItems.DIAMOND_KNIFE_BLADE, "Diamantmesserklinge");
		
		addItem(ModItems.IRON_HAMMER_HEAD, "Eisenhammerkopf");
		addItem(ModItems.BRONZE_HAMMER_HEAD, "Bronzehammerkopf");
		addItem(ModItems.STEEL_HAMMER_HEAD, "Stahlhammerkopf");
		addItem(ModItems.DIAMOND_HAMMER_HEAD, "Diamanthammerkopf");
		
		addItem(ModItems.STONE_SWORD_BLADE, "Steinschwertklinge");
		addItem(ModItems.STONE_PICKAXE_HEAD, "Steinspitzhackenkopf");
		addItem(ModItems.STONE_AXE_HEAD, "Steinaxtkopf");
		addItem(ModItems.STONE_SHOVEL_HEAD, "Steinschaufelkopf");
		addItem(ModItems.STONE_HOE_HEAD, "Steinhackenkopf");
		
		addItem(ModItems.IRON_SWORD_BLADE, "Eisenschwertklinge");
		addItem(ModItems.IRON_PICKAXE_HEAD, "Eisenspitzhackenkopf");
		addItem(ModItems.IRON_AXE_HEAD, "Eisenaxtkopf");
		addItem(ModItems.IRON_SHOVEL_HEAD, "Eisenschaufelkopf");
		addItem(ModItems.IRON_HOE_HEAD, "Eisenhackenkopf");
		
		addItem(ModItems.GOLDEN_SWORD_BLADE, "Goldene Schwertklinge");
		addItem(ModItems.GOLDEN_PICKAXE_HEAD, "Goldener Spitzhackenkopf");
		addItem(ModItems.GOLDEN_AXE_HEAD, "Goldener Axtkopf");
		addItem(ModItems.GOLDEN_SHOVEL_HEAD, "Goldener Schaufelkopf");
		addItem(ModItems.GOLDEN_HOE_HEAD, "Goldener Hackenkopf");
		
		addItem(ModItems.UNIM_SWORD_BLADE, "Unimschwertklinge");
		addItem(ModItems.UNIM_PICKAXE_HEAD, "Unimspitzhackenkopf");
		addItem(ModItems.UNIM_AXE_HEAD, "Unimaxtkopf");
		addItem(ModItems.UNIM_SHOVEL_HEAD, "Unimschaufelkopf");
		addItem(ModItems.UNIM_HOE_HEAD, "Unimhackenkopf");
		
		addItem(ModItems.RUBY_SWORD_BLADE, "Rubinschwertklinge");
		addItem(ModItems.RUBY_PICKAXE_HEAD, "Rubinspitzhackenkopf");
		addItem(ModItems.RUBY_AXE_HEAD, "Rubinaxtkopf");
		addItem(ModItems.RUBY_SHOVEL_HEAD, "Rubinschaufelkopf");
		addItem(ModItems.RUBY_HOE_HEAD, "Rubinhackenkopf");
		
		addItem(ModItems.ASCABIT_SWORD_BLADE, "Askabitschwertklinge");
		addItem(ModItems.ASCABIT_PICKAXE_HEAD, "Askabitspitzhackenkopf");
		addItem(ModItems.ASCABIT_AXE_HEAD, "Askabitaxtkopf");
		addItem(ModItems.ASCABIT_SHOVEL_HEAD, "Askabitschaufelkopf");
		addItem(ModItems.ASCABIT_HOE_HEAD, "Askabithackenkopf");
		
		addItem(ModItems.COPPER_SWORD_BLADE, "Kupferschwertklinge");
		addItem(ModItems.COPPER_PICKAXE_HEAD, "Kupferspitzhackenkopf");
		addItem(ModItems.COPPER_AXE_HEAD, "Kupferaxtkopf");
		addItem(ModItems.COPPER_SHOVEL_HEAD, "Kupferschaufelkopf");
		addItem(ModItems.COPPER_HOE_HEAD, "Kupferhackenkopf");
		
		addItem(ModItems.TIN_SWORD_BLADE, "Zinnschwertklinge");
		addItem(ModItems.TIN_PICKAXE_HEAD, "Zinnspitzhackenkopf");
		addItem(ModItems.TIN_AXE_HEAD, "Zinnaxtkopf");
		addItem(ModItems.TIN_SHOVEL_HEAD, "Zinnschaufelkopf");
		addItem(ModItems.TIN_HOE_HEAD, "Zinnhackenkopf");
		
		addItem(ModItems.TITANIUM_SWORD_BLADE, "Titanschwertklinge");
		addItem(ModItems.TITANIUM_PICKAXE_HEAD, "Titanspitzhackenkopf");
		addItem(ModItems.TITANIUM_AXE_HEAD, "Titanaxtkopf");
		addItem(ModItems.TITANIUM_SHOVEL_HEAD, "Titanschaufelkopf");
		addItem(ModItems.TITANIUM_HOE_HEAD, "Titanhackenkopf");
		
		addItem(ModItems.ZINC_SWORD_BLADE, "Zinkschwertklinge");
		addItem(ModItems.ZINC_PICKAXE_HEAD, "Zinkspitzhackenkopf");
		addItem(ModItems.ZINC_AXE_HEAD, "Zinkaxtkopf");
		addItem(ModItems.ZINC_SHOVEL_HEAD, "Zinkschaufelkopf");
		addItem(ModItems.ZINC_HOE_HEAD, "Zinkhackenkopf");
		
		addItem(ModItems.STEEL_SWORD_BLADE, "Stahlschwertklinge");
		addItem(ModItems.STEEL_PICKAXE_HEAD, "Stahlspitzhackenkopf");
		addItem(ModItems.STEEL_AXE_HEAD, "Stahlaxtkopf");
		addItem(ModItems.STEEL_SHOVEL_HEAD, "Stahlschaufelkopf");
		addItem(ModItems.STEEL_HOE_HEAD, "Stahlhackenkopf");
		
		addItem(ModItems.BRONZE_SWORD_BLADE, "Bronzeschwertklinge");
		addItem(ModItems.BRONZE_PICKAXE_HEAD, "Bronzespitzhackenkopf");
		addItem(ModItems.BRONZE_AXE_HEAD, "Bronzeaxtkopf");
		addItem(ModItems.BRONZE_SHOVEL_HEAD, "Bronzeschaufelkopf");
		addItem(ModItems.BRONZE_HOE_HEAD, "Bronzehackenkopf");
		
		addItem(ModItems.UNIM_HELMET, "Unimhelm");
		addItem(ModItems.UNIM_CHESTPLATE, "Unimbrustpanzer");
		addItem(ModItems.UNIM_LEGGINGS, "Unimbeinschutz");
		addItem(ModItems.UNIM_BOOTS, "Unimschuhe");
		
		addItem(ModItems.RUBY_HELMET, "Rubinhelm");
		addItem(ModItems.RUBY_CHESTPLATE, "Rubinbrustpanzer");
		addItem(ModItems.RUBY_LEGGINGS, "Rubinbeinschutz");
		addItem(ModItems.RUBY_BOOTS, "Rubinschuhe");
		
		addItem(ModItems.ASCABIT_HELMET, "Askabithelm");
		addItem(ModItems.ASCABIT_CHESTPLATE, "Askabitbrustpanzer");
		addItem(ModItems.ASCABIT_LEGGINGS, "Askabitbeinschutz");
		addItem(ModItems.ASCABIT_BOOTS, "Askabitschuhe");
		
		addItem(ModItems.COPPER_HELMET, "Kupferhelm");
		addItem(ModItems.COPPER_CHESTPLATE, "Kupferbrustpanzer");
		addItem(ModItems.COPPER_LEGGINGS, "Kupferbeinschutz");
		addItem(ModItems.COPPER_BOOTS, "Kupferschuhe");
		
		addItem(ModItems.TIN_HELMET, "Zinnhelm");
		addItem(ModItems.TIN_CHESTPLATE, "Zinnbrustpanzer");
		addItem(ModItems.TIN_LEGGINGS, "Zinnbeinschutz");
		addItem(ModItems.TIN_BOOTS, "Zinnschuhe");
		
		addItem(ModItems.TITANIUM_HELMET, "Titanhelm");
		addItem(ModItems.TITANIUM_CHESTPLATE, "Titanbrustpanzer");
		addItem(ModItems.TITANIUM_LEGGINGS, "Titanbeinschutz");
		addItem(ModItems.TITANIUM_BOOTS, "Titanschuhe");
		
		addItem(ModItems.ZINC_HELMET, "Zinkhelm");
		addItem(ModItems.ZINC_CHESTPLATE, "Zinkbrustpanzer");
		addItem(ModItems.ZINC_LEGGINGS, "Zinkbeinschutz");
		addItem(ModItems.ZINC_BOOTS, "Zinkschuhe");
		
		addItem(ModItems.BRONZE_HELMET, "Bronzehelm");
		addItem(ModItems.BRONZE_CHESTPLATE, "Bronzebrustpanzer");
		addItem(ModItems.BRONZE_LEGGINGS, "Bronzebeinschutz");
		addItem(ModItems.BRONZE_BOOTS, "Bronzeschuhe");
		
		addItem(ModItems.STEEL_HELMET, "Stahlhelm");
		addItem(ModItems.STEEL_CHESTPLATE, "Stahlbrustpanzer");
		addItem(ModItems.STEEL_LEGGINGS, "Stahlbeinschutz");
		addItem(ModItems.STEEL_BOOTS, "Stahlschuhe");
	}
	
	@Override
	protected void addBlocks()
	{
		addBlock(ModBlocks.UNIM_ORE, "Unimerz");
		addBlock(ModBlocks.DEEPSLATE_UNIM_ORE, "Tiefenschieferunimerz");
		addBlock(ModBlocks.UNIM_BLOCK, "Unimblock");
		
		addBlock(ModBlocks.RUBY_ORE, "Rubinerz");
		addBlock(ModBlocks.DEEPSLATE_RUBY_ORE, "Tiefenschieferrubinerz");
		addBlock(ModBlocks.RUBY_BLOCK, "Rubinblock");
		
		addBlock(ModBlocks.ASCABIT_ORE, "Askabiterz");
		addBlock(ModBlocks.DEEPSLATE_ASCABIT_ORE, "Tiefenschieferaskabiterz");
		addBlock(ModBlocks.ASCABIT_BLOCK, "Askabitblock");
		addBlock(ModBlocks.RAW_ASCABIT_BLOCK, "Rohaskabitblock");
		
		addBlock(ModBlocks.TIN_ORE, "Zinnerz");
		addBlock(ModBlocks.DEEPSLATE_TIN_ORE, "Tiefenschieferzinnerz");
		addBlock(ModBlocks.TIN_BLOCK, "Zinnblock");
		addBlock(ModBlocks.RAW_TIN_BLOCK, "Rohzinnblock");
		
		addBlock(ModBlocks.TITANIUM_ORE, "Titanerz");
		addBlock(ModBlocks.DEEPSLATE_TITANIUM_ORE, "Tiefenschiefertitanerz");
		addBlock(ModBlocks.TITANIUM_BLOCK, "Titanblock");
		addBlock(ModBlocks.RAW_TITANIUM_BLOCK, "Rohtitanblock");
		
		addBlock(ModBlocks.ZINC_ORE, "Zinkerz");
		addBlock(ModBlocks.DEEPSLATE_ZINC_ORE, "Tiefenschieferzinkerz");
		addBlock(ModBlocks.ZINC_BLOCK, "Zinkblock");
		addBlock(ModBlocks.RAW_ZINC_BLOCK, "Rohzinkblock");
		
		addBlock(ModBlocks.STEEL_BLOCK, "Stahlblock");
		
		addBlock(ModBlocks.BRONZE_BLOCK, "Bronzeblock");
		
		addBlock(ModBlocks.BRASS_BLOCK, "Messingblock");
		
		addBlock(ModBlocks.CEMENT, "Zement");
		addBlock(ModBlocks.CEMENT_STAIRS, "Zementtreppe");
		addBlock(ModBlocks.CEMENT_SLAB, "Zementstufe");
		addBlock(ModBlocks.CEMENT_WALL, "Zementmauer");
		
		addBlock(ModBlocks.CEMENT_PLATE, "Zementplatten");
		addBlock(ModBlocks.CEMENT_PLATE_STAIRS, "Zementplattentreppe");
		addBlock(ModBlocks.CEMENT_PLATE_SLAB, "Zementplattenstufe");
		addBlock(ModBlocks.CEMENT_PLATE_WALL, "Zementplattenmauer");
		
		addBlock(ModBlocks.LIMESTONE, "Kalkstein");
		addBlock(ModBlocks.LIMESTONE_STAIRS, "Kalksteintreppe");
		addBlock(ModBlocks.LIMESTONE_SLAB, "Kalksteinstufe");
		addBlock(ModBlocks.LIMESTONE_WALL, "Kalksteinmauer");
		
		addBlock(ModBlocks.LIMESTONE_BRICKS, "Kalksteinziegel");
		addBlock(ModBlocks.LIMESTONE_BRICK_STAIRS, "Kalksteinziegeltreppe");
		addBlock(ModBlocks.LIMESTONE_BRICK_SLAB, "Kalksteinziegelstufe");
		addBlock(ModBlocks.LIMESTONE_BRICK_WALL, "Kalksteinziegelmauer");
		
		addBlock(ModBlocks.MARBLE, "Marmor");
		addBlock(ModBlocks.MARBLE_STAIRS, "Marmortreppe");
		addBlock(ModBlocks.MARBLE_SLAB, "Marmorstufe");
		addBlock(ModBlocks.MARBLE_WALL, "Marmormauer");
		
		addBlock(ModBlocks.MARBLE_COBBLE, "Marmorbruchstein");
		addBlock(ModBlocks.MARBLE_COBBLE_STAIRS, "Marmorbruchsteintreppe");
		addBlock(ModBlocks.MARBLE_COBBLE_SLAB, "Marmorbruchsteinstufe");
		addBlock(ModBlocks.MARBLE_COBBLE_WALL, "Marmorbruchsteinmauer");
		
		addBlock(ModBlocks.MARBLE_BRICKS, "Marmorziegel");
		addBlock(ModBlocks.MARBLE_BRICK_STAIRS, "Marmorziegeltreppe");
		addBlock(ModBlocks.MARBLE_BRICK_SLAB, "Marmorziegelstufe");
		addBlock(ModBlocks.MARBLE_BRICK_WALL, "Marmorziegelkmauer");
		
		addBlock(ModBlocks.MARBLE_LAMP, "Marmorlampe");
		
		addBlock(ModBlocks.DIRT_SLAB, "Erdstufe");
		addBlock(ModBlocks.COARSE_DIRT_SLAB, "Grobe Erdstufe");
		addBlock(ModBlocks.DIRT_PATH_SLAB, "Trampelpfadstufe");
		addBlock(ModBlocks.GRASS_BLOCK_SLAB, "Grasblockstufe");
		
		addBlock(ModBlocks.GLASS_SLAB, "Glasstufe");
		
		//addBlock(ModBlocks.STONE_DOOR, "Steintür");
		addBlock(ModBlocks.STONE_TRAPDOOR, "Steinfalltür");
		
		//addBlock(ModBlocks.SANDSTONE_DOOR, "Sandsteintür");
		addBlock(ModBlocks.SANDSTONE_TRAPDOOR, "Sandsteinfalltür");
		
		//addBlock(ModBlocks.GLASS_DOOR, "Glastür");
		addBlock(ModBlocks.GLASS_TRAPDOOR, "Glasfalltür");
		
		addBlock(ModBlocks.WOODEN_GLASS_FRAME, "Hölzerner Glasrahmen");
		
		addBlock(ModBlocks.KILN, "Brennofen");
		addBlock(ModBlocks.SMELTER, "Schmelzerei");
		addBlock(ModBlocks.MILLSTONE, "Mühlstein");
		
		add("inventory." + MODID + ".chopping_block", "Hackklotz");
		addBlock(ModBlocks.OAK_CHOPPING_BLOCK, "Eichenholz Hackklotz");
		addBlock(ModBlocks.SPRUCE_CHOPPING_BLOCK, "Fichtenholz Hackklotz");
		addBlock(ModBlocks.BIRCH_CHOPPING_BLOCK, "Birkenholz Hackklotz");
		addBlock(ModBlocks.JUNGLE_CHOPPING_BLOCK, "Tropenholz Hackklotz");
		addBlock(ModBlocks.ACACIA_CHOPPING_BLOCK, "Akazienholz Hackklotz");
		addBlock(ModBlocks.DARK_OAK_CHOPPING_BLOCK, "Schwarzeichenholz Hackklotz");
		addBlock(ModBlocks.MANGROVE_CHOPPING_BLOCK, "Mangrovenholz Hackklotz");
		addBlock(ModBlocks.CHERRY_CHOPPING_BLOCK, "Kirschholz Hackklotz");
		addBlock(ModBlocks.CRIMSON_CHOPPING_BLOCK, "Karmesinstiel Hackklotz");
		addBlock(ModBlocks.WARPED_CHOPPING_BLOCK, "Wirrstiel Hackklotz");
		
		add("inventory." + MODID + ".drying_rack", "Trockengestell");
		addBlock(ModBlocks.OAK_DRYING_RACK, "Eichenholz Trockengestell");
		addBlock(ModBlocks.SPRUCE_DRYING_RACK, "Fichtenholz Trockengestell");
		addBlock(ModBlocks.BIRCH_DRYING_RACK, "Birkenholz Trockengestell");
		addBlock(ModBlocks.JUNGLE_DRYING_RACK, "Tropenholz Trockengestell");
		addBlock(ModBlocks.ACACIA_DRYING_RACK, "Akazienholz Trockengestell");
		addBlock(ModBlocks.DARK_OAK_DRYING_RACK, "Schwarzeichenholz Trockengestell");
		addBlock(ModBlocks.MANGROVE_DRYING_RACK, "Mangrovenholz Trockengestell");
		addBlock(ModBlocks.CHERRY_DRYING_RACK, "Kirschholz Trockengestell");
		addBlock(ModBlocks.CRIMSON_DRYING_RACK, "Karmesinstiel Trockengestell");
		addBlock(ModBlocks.WARPED_DRYING_RACK, "Wirrstiel Trockengestell");
		
		addBlock(ModBlocks.MACHINE_FRAME, "Maschinenrahmen");
		
		addBlock(ModBlocks.COAL_GENERATOR, "Kohlegenerator");
		addBlock(ModBlocks.BATTERY, "Batterie");
		addBlock(ModBlocks.SOLAR_PANEL, "Solarmodul");
		
		addBlock(ModBlocks.LIQUID_TANK, "Flüssigkeitstank");
		
		addBlock(ModBlocks.ELECTRIC_BREWERY, "Elektrische Brauerei");
		addBlock(ModBlocks.ELECTRIC_SMELTER, "Elektrische Schmelzerei");
		addBlock(ModBlocks.ELECTRIC_GREENHOUSE, "Elektrisches Gewächshaus");
		
		addBlock(ModBlocks.HYDRAULIC_PRESS, "Hydraulische Presse");
		
		addBlock(ModBlocks.COPPER_CABLE, "Kupferkabel");
		
		addBlock(ModBlocks.TOMATOES, "Tomaten");
		addBlock(ModBlocks.GRAPES, "Weintrauben");
		addBlock(ModBlocks.STRAWBERRIES, "Erdbeeren");
		addBlock(ModBlocks.CORNS, "Mais");
		addBlock(ModBlocks.LETTUCES, "Salate");
		addBlock(ModBlocks.ONIONS, "Zwiebeln");
		addBlock(ModBlocks.RICE, "Reis");
		
		addBlock(ModBlocks.CHOCOLATE_CAKE, "Schokoladenkuchen");
		
		addBlock(ModBlocks.OIL, "Öl");
	}
	
	@Override
	protected void addEntityTypes()
	{
		addEntityType(ModEntityTypes.ROCK, "Stein");
		addEntityType(ModEntityTypes.ANDESITE_ROCK, "Andesit Stein");
		addEntityType(ModEntityTypes.DIORITE_ROCK, "Diorit Stein");
		addEntityType(ModEntityTypes.GRANITE_ROCK, "Granit Stein");
	}
	
	@Override
	protected void addAdvancements()
	{
		add("advancements.stoneage.root.title", "Steinzeitalter");
		add("advancements.stoneage.root.description", "Willkommen in der ECHTEN Steinzeit!");
		
		add("advancements.stoneage.torch.title", "Erleuchtung");
		add("advancements.stoneage.torch.description", "Verwende etwas Tierfett auf einem Stock, um deine Umgebung auszuleuchten!");
		
		add("advancements.stoneage.choppingblock.title", "Frisches Hack");
		add("advancements.stoneage.choppingblock.description", "Stelle einen Hackklotz her, um Holz zu verarbeiten!");
		
		add("advancements.stoneage.dryingrack.title", "Kein Item Display");
		add("advancements.stoneage.dryingrack.description", "Stelle ein Trockengestell her, um Leder zu trocknen!");
		
		add("advancements.stoneage.preparedhide.title", "Vorbereitung is alles");
		add("advancements.stoneage.preparedhide.description", "Stelle etwas vorbereitete Haut her!");
		
		add("advancements.stoneage.leather.title", "Leder? Läuft");
		add("advancements.stoneage.leather.description", "Trockne etwas Tierhaut und mache Leder!");
		
		add("advancements.stoneage.firsttool.title", "Erstes Werkzeug");
		add("advancements.stoneage.firsttool.description", "Verwende etwas Feuerstein, Pflanzenfasern und Stöcke, um ein primitives Werkzeug herzustellen!");
		
		add("advancements.stoneage.throwrock.title", "Fang den Stein");
		add("advancements.stoneage.throwrock.description", "Töte etwas (oder jemanden) mit einem Stein!");
		
		add("advancements.stoneage.stonemaker.title", "Steinmacher");
		add("advancements.stoneage.stonemaker.description", "Stelle aus ein paar Steinen Bruchstein her!");
		
		add("advancements.stoneage.millstone.title", "Der Grind ist echt");
		add("advancements.stoneage.millstone.description", "Stelle einen Mühlstein her!");
		
		add("advancements.stoneage.toolhead.title", "Kopfsache");
		add("advancements.stoneage.toolhead.description", "Forme einen Werkzeugkopf aus Steinen!");
		
		add("advancements.stoneage.bettertool.title", "Die Aufrüstung");
		add("advancements.stoneage.bettertool.description", "Stelle eine Steinspitzhacke her!");
		
		add("advancements.stoneage.kiln.title", "Der andere Ofen");
		add("advancements.stoneage.kiln.description", "Stelle einen Brennofen her!");
		
		add("advancements.stoneage.claybucket.title", "Günstiger Eimer");
		add("advancements.stoneage.claybucket.description", "Stelle einen Lehmeimer her!");
		
		add("advancements.stoneage.cement.title", "Zementmischer");
		add("advancements.stoneage.cement.description", "Nutze einen Brennofen, um Zement herzustellen!");
		
		add("advancements.stoneage.smelter.title", "Jetzt wird's heiß");
		add("advancements.stoneage.smelter.description", "Stelle eine Schmelzerei her, um Barren herzustellen!");
		
		add("advancements.metalage.root.title", "Metallzeitalter");
		add("advancements.metalage.root.description", "Willkommen im Metallzeitalter!");
		
		add("advancements.metalage.hammer.title", "Hammer Ham'mer");
		add("advancements.metalage.hammer.description", "Stelle einen Hammer her!");
		
		add("advancements.metalage.pickaxe.title", "M(e)ine Spitzhacke");
		add("advancements.metalage.pickaxe.description", "Stelle eine Spitzhacke aus Metall her!");
		
		add("advancements.metalage.protection.title", "Schutz");
		add("advancements.metalage.protection.description", "Stelle eine Rüstung aus Metall her!");
		
		add("advancements.metalage.darkiron.title", "Dunkles Eisen");
		add("advancements.metalage.darkiron.description", "Schmelze etwas Stahl!");
		
		add("advancements.industrialage.root.title", "Industriezeitalter");
		add("advancements.industrialage.root.description", "Willkommen im Industriezeitalter!");
		
		add("advancements.industrialage.redstoneblend.title", "Gute Mische");
		add("advancements.industrialage.redstoneblend.description", "Stelle etwas Redstone Mischung her!");
		
		add("advancements.industrialage.machineframe.title", "Rahmenbedingungen");
		add("advancements.industrialage.machineframe.description", "Stelle einen Maschinenrahmen her!");
		
		add("advancements.industrialage.greenenergy.title", "Grüne Energie");
		add("advancements.industrialage.greenenergy.description", "Stelle ein Solarmodul her!");
		
		add("advancements.industrialage.blackenergy.title", "Schwarze Energie");
		add("advancements.industrialage.blackenergy.description", "Stelle einen Kohlegenerator her!");
	}
	
	@Override
	protected void addEnchantments()
	{
		addEnchantment(ModEnchantments.POWER_SAVING, "Energiesparung", "Reduziert den Energieverbrauch um die Hälfte.");
	}
	
	@Override
	protected void addStats()
	{
		addStat(ModStats.CHOPPED_WOOD.getId().getPath(), "Holz gehackt");
	}
	
	@Override
	protected void addGuideTexts()
	{
		add("guide." + MODID + ".title", "MCE Leitfaden");
		add("guide." + MODID + ".landing", "Willkommen beim MCE Leitfaden!");
		
		add("guide." + MODID + ".category.stone_age.name", "Steinzeitalter");
		add("guide." + MODID + ".category.stone_age.description", "TODO");
		
		add("guide." + MODID + ".category.metal_age.name", "Metallzeitalter");
		add("guide." + MODID + ".category.metal_age.description", "TODO");
		
		add("guide." + MODID + ".category.industrial_age.name", "Industriezeitalter");
		add("guide." + MODID + ".category.industrial_age.description", "TODO");
		
		add("guide." + MODID + ".entry.torch.name", "Fackel");
		add("guide." + MODID + ".entry.torch.text", "Ein primitiver Weg, um $(item)Fackeln$(clear) herzusellen ist $(item)Tierfett$(clear) zu verwenden.$(br)$(item)Tierfett$(clear) kann durch das Töten von Tieren erlangt werden.");
		
		add("guide." + MODID + ".entry.chopping_block.name", "Hackklotz");
		add("guide." + MODID + ".entry.chopping_block.text", "Der $(item)Hackklotz$(clear) kann verwendet werden, um effektiv Holz zu verarbeiten, z.B. um $(item)Planken$(clear) oder $(item)Stufen$(clear) herzustellen.$(br)Schlage einfach mit einer $(item)Axt$(clear) auf das Holz, um es zu verarbeiten.$(br2)$(italic)Hinweis: Du kannst jede Holzart verwenden, um einen $(item)Hackklotz$(clear) herzustellen.");
		
		add("guide." + MODID + ".entry.leather.name", "Leder");
		add("guide." + MODID + ".entry.leather.text1", "Mobs wie Kühe lassen jetzt $(item)Tierhaut$(clear) fallen, anstatt fertiges $(item)Leder$(clear).$(br)Du musst die Haut zuerst vorbereiten.");
		add("guide." + MODID + ".entry.leather.text2", "Lege dann die $(item)Vorbereitete Haut$(clear) auf eine $(item)Trockengestell$(clear), um $(item)Leder$(clear) zu erhalten. Das Trocknen wird einige Zeit dauern.$(br2)$(italic)Hinweis: Du kannst jede Holzart verwenden, um ein $(item)Trockengestell$(clear) herzustellen.");
		
		add("guide." + MODID + ".entry.first_tool.name", "Erstes Werkzeug");
		add("guide." + MODID + ".entry.first_tool.text1", "Du musst zuerst etwas $(item)Feuerstein$(clear), $(item)Pflanzenfaser$(clear) und $(item)Stöcke$(clear) besorgen, um dein erstes primitives Werkzeug herzustellen:$(br)das $(item)Feuersteinbeil$(clear).");
		add("guide." + MODID + ".entry.first_tool.text2", "Du kannst $(item)Planzenfasern$(clear) durch das Abbauen von $(item)Gras$(clear) und selten auch bei $(item)Blättern$(clear) erhalten. $(item)Pflanzenfasern$(clear) sind wichtig für jede Art von primitivem Werkzeug.");
		
		add("guide." + MODID + ".entry.stonemaker.name", "Stein herstellen");
		add("guide." + MODID + ".entry.stonemaker.text", "Wie du vielleicht schon bemerkt hast, entstehen beim Abbauen von $(item)Stein$(clear) kleinere $(item)Steine$(clear) anstelle des ganzen Blockes.$(br2)Verwende 4 kleine $(item)Steine$(clear), um 2 $(item)Bruchstein$(clear) herzustellen.");
		
		add("guide." + MODID + ".entry.better_tool.name", "Besseres Werkzeug");
		add("guide." + MODID + ".entry.better_tool.text", "Um bessere Werkzeuge herzustellen, benötigst du einen $(item)Werkzeugkopf$(clear), einen $(item)Stock$(clear) und $(item)Planzenfaser$(clear).$(br2)Als erstes solltest du eine $(item)Steinspitzhacke$(clear) herstellen.");
	}
	
	@Override
	protected void addOtherTexts()
	{
		add("jei." + MODID + ".plant_fiber.info", "Kann durch das Abbauen von Gras oder selten von Blättern erhalten werden.");
		add("jei." + MODID + ".flint.info", "Kann durch das Abbauen von Gras oder Kies erhalten werden.");
		add("jei." + MODID + ".tree_bark.info", "Kann durch das Entrinden von Holz erhalten werden.");
		
		add("config.jade.plugin_" + MODID + ".smelter", "Schmelzerei Hitze");
		add("jade." + MODID + ".smelter.heat", "% Hitze");
		
		add("config.jade.plugin_" + MODID + ".drying_rack", "Trockengestell Ticks");
		add("jade." + MODID + ".drying_rack.ticks", "% getrocknet");
		
		add("inventory." + MODID + ".millstone.nowater", "Kein fließendes Wasser in der Nähe!");
		
		add("inventory." + MODID + ".solar_panel.cantseesky", "Himmel ist nicht sichtbar!");
		add("inventory." + MODID + ".solar_panel.nighttime", "Kein Sonnenlicht! (Nacht)");
		
		add(ModKeyMappings.KEY_CATEGORY, "MCE Tasten");
		add("key." + MODID + ".open_backpack", "Rucksack öffnen");
		
		add("updater." + MODID + ".uptodate", "§e[Updater]§r §2MCE§r ist up-to-date!");
		add("updater." + MODID + ".beta", "§e[Updater]§r Du verwendest eine §cBeta§r Version von §2MCE§r.");
		add("updater." + MODID + ".outdated", "§e[Updater]§r §2MCE§r ist veraltet! Deine Version: §c%s§r, neuste Version: §c%s§r");
		add("updater." + MODID + ".failed", "§e[Updater]§r Versionsüberprüfung für §2MCE§r fehlgeschlagen.");
		
		add("item.nouse", "§oDieses Item hat derzeit keine Verwendung!");
	}
}
