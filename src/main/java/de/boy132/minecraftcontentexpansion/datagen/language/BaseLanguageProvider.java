package de.boy132.minecraftcontentexpansion.datagen.language;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.data.PackOutput;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraftforge.common.data.LanguageProvider;

import java.util.function.Supplier;

public abstract class BaseLanguageProvider extends LanguageProvider
{
	protected final String MODID = MinecraftContentExpansion.MODID;
	
	protected String locale;
	
	public BaseLanguageProvider(PackOutput packOutput, String locale)
	{
		super(packOutput, MinecraftContentExpansion.MODID, locale);
		this.locale = locale;
	}
	
	protected abstract void addCreativeTabs();
	
	protected abstract void addItems();
	
	protected abstract void addBlocks();
	
	protected abstract void addEntityTypes();
	
	protected abstract void addAdvancements();
	
	protected abstract void addEnchantments();
	
	protected abstract void addStats();
	
	protected abstract void addGuideTexts();
	
	protected abstract void addOtherTexts();
	
	@Override
	protected void addTranslations()
	{
		addCreativeTabs();
		
		addItems();
		addBlocks();
		
		addEntityTypes();
		
		addAdvancements();
		
		addEnchantments();
		
		addStats();
		
		addGuideTexts();
		
		addOtherTexts();
	}
	
	@Override
	public String getName()
	{
		return "Mod Languages (" + locale + ")";
	}
	
	protected void addCreativeModeTab(String tab, String name)
	{
		add("item_group." + MinecraftContentExpansion.MODID + "." + tab, name);
	}
	
	protected void addEnchantment(Enchantment enchantment, String name, String description)
	{
		add(enchantment.getDescriptionId(), name);
		add(enchantment.getDescriptionId() + ".desc", description);
	}
	
	protected void addEnchantment(Supplier<? extends Enchantment> enchantment, String name, String description)
	{
		addEnchantment(enchantment.get(), name, description);
	}
	
	protected void addStat(String stat, String name)
	{
		add("stat." + MinecraftContentExpansion.MODID + "." + stat, name);
	}
}
