package de.boy132.minecraftcontentexpansion.compat.jei.hydraulicpress;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.compat.jei.JEICompat;
import de.boy132.minecraftcontentexpansion.recipe.hydraulicpress.HydraulicPressRecipe;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;

public class HydraulicPressRecipeCategory extends AbstractHydraulicPressRecipeCategory<HydraulicPressRecipe>
{
	public static final RecipeType<HydraulicPressRecipe> RECIPE_TYPE = RecipeType.create(MinecraftContentExpansion.MODID, "hydraulic_press", HydraulicPressRecipe.class);
	
	private final Component name;
	private final IDrawable background;
	private final IDrawable icon;
	
	public HydraulicPressRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper);
		name = Component.translatable("block." + MinecraftContentExpansion.MODID + ".hydraulic_press");
		background = guiHelper.createDrawable(BACKGROUND, 46, 16, 124, 54);
		icon = guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(ModBlocks.HYDRAULIC_PRESS.get()));
	}
	
	@Override
	public void draw(HydraulicPressRecipe recipe, IRecipeSlotsView recipeSlotsView, GuiGraphics guiGraphics, double mouseX, double mouseY)
	{
		ANIMATED_PRESS.draw(guiGraphics, 52 - 46, 15 - 16);
		
		JEICompat.drawTime(recipe.getPressTime(), guiGraphics, background.getWidth(), 20);
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, HydraulicPressRecipe recipe, IFocusGroup focuses)
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 53 - 46, 50 - 16).addIngredients(recipe.getIngredients().get(0));
		builder.addSlot(RecipeIngredientRole.INPUT, 53 - 46, 32 - 16).addIngredients(recipe.getIngredients().get(1));
		
		builder.addSlot(RecipeIngredientRole.OUTPUT, 98 - 46, 50 - 16).addItemStack(recipe.getResultItem(null));
	}
	
	@Override
	public RecipeType<HydraulicPressRecipe> getRecipeType()
	{
		return RECIPE_TYPE;
	}
	
	@Override
	public Component getTitle()
	{
		return name;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return icon;
	}
}
