package de.boy132.minecraftcontentexpansion.compat.jei.hydraulicpress;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.resources.ResourceLocation;

public abstract class AbstractHydraulicPressRecipeCategory<T> implements IRecipeCategory<T>
{
	protected final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/hydraulic_press.png");
	
	protected final IDrawableAnimated ANIMATED_PRESS;
	
	public AbstractHydraulicPressRecipeCategory(IGuiHelper guiHelper)
	{
		ANIMATED_PRESS = guiHelper.createAnimatedDrawable(guiHelper.createDrawable(BACKGROUND, 176, 73, 15, 17), 200, IDrawableAnimated.StartDirection.TOP, false);
	}
}
