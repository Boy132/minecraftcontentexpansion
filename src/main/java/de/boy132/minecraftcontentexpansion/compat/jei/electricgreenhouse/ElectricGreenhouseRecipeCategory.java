package de.boy132.minecraftcontentexpansion.compat.jei.electricgreenhouse;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.ElectricGreenhouseBlockEntity;
import de.boy132.minecraftcontentexpansion.compat.jei.JEICompat;
import de.boy132.minecraftcontentexpansion.recipe.greenhouse.GreenhouseRecipe;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;

public class ElectricGreenhouseRecipeCategory extends AbstractElectricGreenhouseRecipeCategory<GreenhouseRecipe>
{
	public static final RecipeType<GreenhouseRecipe> RECIPE_TYPE = RecipeType.create(MinecraftContentExpansion.MODID, "electric_greenhouse", GreenhouseRecipe.class);
	
	private final Component name;
	private final IDrawable background;
	private final IDrawable icon;
	
	public ElectricGreenhouseRecipeCategory(IGuiHelper guiHelper)
	{
		name = Component.translatable("block." + MinecraftContentExpansion.MODID + ".electric_greenhouse");
		background = guiHelper.createDrawable(BACKGROUND, 14, 16, 84, 44);
		icon = guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(ModBlocks.ELECTRIC_GREENHOUSE.get()));
	}
	
	@Override
	public void draw(GreenhouseRecipe recipe, IRecipeSlotsView recipeSlotsView, GuiGraphics guiGraphics, double mouseX, double mouseY)
	{
		JEICompat.drawTime(ElectricGreenhouseBlockEntity.NEEDED_GROW_TIME, guiGraphics, background.getWidth(), 35);
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, GreenhouseRecipe recipe, IFocusGroup focuses)
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 18 - 14, 20 - 16).addIngredients(recipe.getIngredients().get(0));
		builder.addSlot(RecipeIngredientRole.CATALYST, 18 - 14, 40 - 16).addIngredients(getSoilBlock(recipe.getSoilType()));
		builder.addSlot(RecipeIngredientRole.OUTPUT, 78 - 14, 20 - 16).addItemStack(recipe.getResultItem(null));
	}
	
	@Override
	public RecipeType<GreenhouseRecipe> getRecipeType()
	{
		return RECIPE_TYPE;
	}
	
	@Override
	public Component getTitle()
	{
		return name;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return icon;
	}
}
