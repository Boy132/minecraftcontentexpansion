package de.boy132.minecraftcontentexpansion.compat.jei.choppingblock;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.recipe.choppingblock.ChoppingBlockRecipe;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class ChoppingBlockRecipeCategory implements IRecipeCategory<ChoppingBlockRecipe>
{
	public static final RecipeType<ChoppingBlockRecipe> RECIPE_TYPE = RecipeType.create(MinecraftContentExpansion.MODID, "chopping_block", ChoppingBlockRecipe.class);
	
	private final Component name;
	private final IDrawable background;
	private final IDrawable icon;
	
	public ChoppingBlockRecipeCategory(IGuiHelper guiHelper)
	{
		name = Component.translatable("inventory." + MinecraftContentExpansion.MODID + ".chopping_block");
		background = guiHelper.createDrawable(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/chopping_block.png"), 13, 8, 96, 36);
		icon = guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(ModBlocks.OAK_CHOPPING_BLOCK.get()));
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, ChoppingBlockRecipe recipe, IFocusGroup focuses)
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 5, 6).addIngredients(recipe.getIngredients().get(0));
		builder.addSlot(RecipeIngredientRole.OUTPUT, 75, 6).addItemStack(recipe.getResultItem(null));
	}
	
	@Override
	public RecipeType<ChoppingBlockRecipe> getRecipeType()
	{
		return RECIPE_TYPE;
	}
	
	@Override
	public Component getTitle()
	{
		return name;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return icon;
	}
}
