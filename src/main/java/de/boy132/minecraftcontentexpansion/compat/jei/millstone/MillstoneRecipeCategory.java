package de.boy132.minecraftcontentexpansion.compat.jei.millstone;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.compat.jei.JEICompat;
import de.boy132.minecraftcontentexpansion.recipe.millstone.MillstoneRecipe;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;

public class MillstoneRecipeCategory extends AbstractMillstoneRecipeCategory<MillstoneRecipe>
{
	public static final RecipeType<MillstoneRecipe> RECIPE_TYPE = RecipeType.create(MinecraftContentExpansion.MODID, "millstone", MillstoneRecipe.class);
	
	private final Component name;
	private final IDrawable background;
	private final IDrawable icon;
	
	public MillstoneRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper);
		name = Component.translatable("block." + MinecraftContentExpansion.MODID + ".millstone");
		background = guiHelper.createDrawable(BACKGROUND, 24 + 14, 20 + 4, 90, 46);
		icon = guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(ModBlocks.MILLSTONE.get()));
	}
	
	@Override
	public void draw(MillstoneRecipe recipe, IRecipeSlotsView recipeSlotsView, GuiGraphics guiGraphics, double mouseX, double mouseY)
	{
		ANIMATED_ARROW.draw(guiGraphics, 43 - 14, 14 - 4);
		
		JEICompat.drawTime(recipe.getMillTime(), guiGraphics, background.getWidth(), 37);
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, MillstoneRecipe recipe, IFocusGroup focuses)
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 20 - 14, 14 - 4).addIngredients(recipe.getIngredients().get(0));
		builder.addSlot(RecipeIngredientRole.OUTPUT, 78 - 14, 15 - 4).addItemStack(recipe.getResultItem(null));
	}
	
	@Override
	public RecipeType<MillstoneRecipe> getRecipeType()
	{
		return RECIPE_TYPE;
	}
	
	@Override
	public Component getTitle()
	{
		return name;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return icon;
	}
}
