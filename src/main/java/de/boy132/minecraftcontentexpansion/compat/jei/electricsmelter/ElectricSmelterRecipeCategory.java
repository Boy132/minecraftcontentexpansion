package de.boy132.minecraftcontentexpansion.compat.jei.electricsmelter;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.compat.jei.JEICompat;
import de.boy132.minecraftcontentexpansion.recipe.electricsmelter.ElectricSmelterRecipe;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;

public class ElectricSmelterRecipeCategory extends AbstractElectricSmelterRecipeCategory<ElectricSmelterRecipe>
{
	public static final RecipeType<ElectricSmelterRecipe> RECIPE_TYPE = RecipeType.create(MinecraftContentExpansion.MODID, "electric_smelter", ElectricSmelterRecipe.class);
	
	private final Component name;
	private final IDrawable background;
	private final IDrawable icon;
	
	public ElectricSmelterRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper);
		name = Component.translatable("block." + MinecraftContentExpansion.MODID + ".electric_smelter");
		background = guiHelper.createDrawable(BACKGROUND, 38, 16, 118, 54);
		icon = guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(ModBlocks.ELECTRIC_SMELTER.get()));
	}
	
	@Override
	public void draw(ElectricSmelterRecipe recipe, IRecipeSlotsView recipeSlotsView, GuiGraphics guiGraphics, double mouseX, double mouseY)
	{
		ANIMATED_ARROW.draw(guiGraphics, 88 - 38, 34 - 16);
		
		JEICompat.drawTime(recipe.getSmeltTime(), guiGraphics, background.getWidth(), 45);
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, ElectricSmelterRecipe recipe, IFocusGroup focuses)
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 44 - 38, 25 - 16).addIngredients(recipe.getIngredients().get(0));
		builder.addSlot(RecipeIngredientRole.INPUT, 62 - 38, 25 - 16).addIngredients(recipe.getIngredients().get(1));
		builder.addSlot(RecipeIngredientRole.INPUT, 44 - 38, 43 - 16).addIngredients(recipe.getIngredients().get(2));
		builder.addSlot(RecipeIngredientRole.INPUT, 62 - 38, 43 - 16).addIngredients(recipe.getIngredients().get(3));
		
		builder.addSlot(RecipeIngredientRole.OUTPUT, 125 - 38, 35 - 16).addItemStack(recipe.getResultItem(null));
	}
	
	@Override
	public RecipeType<ElectricSmelterRecipe> getRecipeType()
	{
		return RECIPE_TYPE;
	}
	
	@Override
	public Component getTitle()
	{
		return name;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return icon;
	}
}
