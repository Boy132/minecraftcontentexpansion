package de.boy132.minecraftcontentexpansion.compat.jei.kiln;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.compat.jei.JEICompat;
import de.boy132.minecraftcontentexpansion.recipe.kiln.KilnRecipe;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;

public class KilnRecipeCategory extends AbstractKilnRecipeCategory<KilnRecipe>
{
	public static final RecipeType<KilnRecipe> RECIPE_TYPE = RecipeType.create(MinecraftContentExpansion.MODID, "kiln", KilnRecipe.class);
	
	private final Component name;
	private final IDrawable background;
	private final IDrawable icon;
	
	public KilnRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper);
		name = Component.translatable("block." + MinecraftContentExpansion.MODID + ".kiln");
		background = guiHelper.createDrawable(BACKGROUND, 10, 18, 140, 55);
		icon = guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(ModBlocks.KILN.get()));
	}
	
	@Override
	public void draw(KilnRecipe recipe, IRecipeSlotsView recipeSlotsView, GuiGraphics guiGraphics, double mouseX, double mouseY)
	{
		ANIMATED_FLAME.draw(guiGraphics, 9, 33);
		ANIMATED_ARROW.draw(guiGraphics, 71, 14);
		
		JEICompat.drawTime(recipe.getSmeltTime(), guiGraphics, background.getWidth(), 45);
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, KilnRecipe recipe, IFocusGroup focuses)
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 48, 14).addIngredients(recipe.getIngredients().get(0));
		builder.addSlot(RecipeIngredientRole.OUTPUT, 106, 15).addItemStack(recipe.getResultItem(null));
	}
	
	@Override
	public RecipeType<KilnRecipe> getRecipeType()
	{
		return RECIPE_TYPE;
	}
	
	@Override
	public Component getTitle()
	{
		return name;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return icon;
	}
}
