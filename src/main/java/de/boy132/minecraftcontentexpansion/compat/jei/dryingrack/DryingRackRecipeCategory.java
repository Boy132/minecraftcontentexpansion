package de.boy132.minecraftcontentexpansion.compat.jei.dryingrack;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.compat.jei.JEICompat;
import de.boy132.minecraftcontentexpansion.recipe.dryingrack.DryingRackRecipe;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class DryingRackRecipeCategory implements IRecipeCategory<DryingRackRecipe>
{
	public static final RecipeType<DryingRackRecipe> RECIPE_TYPE = RecipeType.create(MinecraftContentExpansion.MODID, "drying_rack", DryingRackRecipe.class);
	
	private final Component name;
	private final IDrawable background;
	private final IDrawable icon;
	
	public DryingRackRecipeCategory(IGuiHelper guiHelper)
	{
		name = Component.translatable("inventory." + MinecraftContentExpansion.MODID + ".drying_rack");
		background = guiHelper.createDrawable(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/drying_rack.png"), 4 + 10, 4 + 6, 84, 40);
		icon = guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(ModBlocks.OAK_DRYING_RACK.get()));
	}
	
	@Override
	public void draw(DryingRackRecipe recipe, IRecipeSlotsView recipeSlotsView, GuiGraphics guiGraphics, double mouseX, double mouseY)
	{
		JEICompat.drawTime(recipe.getDryingTime(), guiGraphics, background.getWidth(), 32);
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, DryingRackRecipe recipe, IFocusGroup focuses)
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 14 - 10, 16 - 6).addIngredients(recipe.getIngredients().get(0));
		builder.addSlot(RecipeIngredientRole.OUTPUT, 74 - 10, 16 - 6).addItemStack(recipe.getResultItem(null));
	}
	
	@Override
	public RecipeType<DryingRackRecipe> getRecipeType()
	{
		return RECIPE_TYPE;
	}
	
	@Override
	public Component getTitle()
	{
		return name;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return icon;
	}
}
