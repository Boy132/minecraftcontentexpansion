package de.boy132.minecraftcontentexpansion.compat.jei;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.compat.jei.choppingblock.ChoppingBlockRecipeCategory;
import de.boy132.minecraftcontentexpansion.compat.jei.dryingrack.DryingRackRecipeCategory;
import de.boy132.minecraftcontentexpansion.compat.jei.electricgreenhouse.ElectricGreenhouseRecipeCategory;
import de.boy132.minecraftcontentexpansion.compat.jei.electricsmelter.ElectricSmelterRecipeCategory;
import de.boy132.minecraftcontentexpansion.compat.jei.hydraulicpress.HydraulicPressRecipeCategory;
import de.boy132.minecraftcontentexpansion.compat.jei.kiln.KilnRecipeCategory;
import de.boy132.minecraftcontentexpansion.compat.jei.millstone.MillstoneRecipeCategory;
import de.boy132.minecraftcontentexpansion.compat.jei.smelter.SmelterRecipeCategory;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.screen.electricbrewery.ElectricBreweryScreen;
import de.boy132.minecraftcontentexpansion.screen.electricgreenhouse.ElectricGreenhouseScreen;
import de.boy132.minecraftcontentexpansion.screen.electricsmelter.ElectricSmelterMenu;
import de.boy132.minecraftcontentexpansion.screen.electricsmelter.ElectricSmelterScreen;
import de.boy132.minecraftcontentexpansion.screen.hydraulicpress.HydraulicPressMenu;
import de.boy132.minecraftcontentexpansion.screen.hydraulicpress.HydraulicPressScreen;
import de.boy132.minecraftcontentexpansion.screen.kiln.KilnMenu;
import de.boy132.minecraftcontentexpansion.screen.kiln.KilnScreen;
import de.boy132.minecraftcontentexpansion.screen.millstone.MillstoneMenu;
import de.boy132.minecraftcontentexpansion.screen.millstone.MillstoneScreen;
import de.boy132.minecraftcontentexpansion.screen.smelter.SmelterMenu;
import de.boy132.minecraftcontentexpansion.screen.smelter.SmelterScreen;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.constants.RecipeTypes;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.registration.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.RecipeHolder;
import net.minecraft.world.item.crafting.RecipeManager;

import java.util.Objects;

@JeiPlugin
public class JEICompat implements IModPlugin
{
	@Override
	public ResourceLocation getPluginUid()
	{
		return ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "jei_plugin");
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration registration)
	{
		IJeiHelpers jeiHelpers = registration.getJeiHelpers();
		IGuiHelper guiHelper = jeiHelpers.getGuiHelper();
		
		registration.addRecipeCategories(new KilnRecipeCategory(guiHelper));
		registration.addRecipeCategories(new SmelterRecipeCategory(guiHelper));
		registration.addRecipeCategories(new MillstoneRecipeCategory(guiHelper));
		
		registration.addRecipeCategories(new ChoppingBlockRecipeCategory(guiHelper));
		
		registration.addRecipeCategories(new DryingRackRecipeCategory(guiHelper));
		
		registration.addRecipeCategories(new ElectricSmelterRecipeCategory(guiHelper));
		
		registration.addRecipeCategories(new ElectricGreenhouseRecipeCategory(guiHelper));
		
		registration.addRecipeCategories(new HydraulicPressRecipeCategory(guiHelper));
	}
	
	@Override
	public void registerRecipes(IRecipeRegistration registration)
	{
		//registration.addIngredientInfo(new ItemStack(ItemRegistry.STEEL_FRAME.get()), VanillaTypes.ITEM_STACK, Component.translatable("jei.steel_frame.info"));
		registration.addIngredientInfo(new ItemStack(ModItems.PLANT_FIBER.get()), VanillaTypes.ITEM_STACK, Component.translatable("jei." + MinecraftContentExpansion.MODID + ".plant_fiber.info"));
		registration.addIngredientInfo(new ItemStack(Items.FLINT), VanillaTypes.ITEM_STACK, Component.translatable("jei." + MinecraftContentExpansion.MODID + ".flint.info"));
		registration.addIngredientInfo(new ItemStack(ModItems.TREE_BARK.get()), VanillaTypes.ITEM_STACK, Component.translatable("jei." + MinecraftContentExpansion.MODID + ".tree_bark.info"));
		
		RecipeManager recipeManager = Objects.requireNonNull(Minecraft.getInstance().level).getRecipeManager();
		
		registration.addRecipes(KilnRecipeCategory.RECIPE_TYPE, recipeManager.getAllRecipesFor(ModRecipeTypes.KILN.get()).stream().map(RecipeHolder::value).toList());
		registration.addRecipes(SmelterRecipeCategory.RECIPE_TYPE, recipeManager.getAllRecipesFor(ModRecipeTypes.SMELTER.get()).stream().map(RecipeHolder::value).toList());
		registration.addRecipes(MillstoneRecipeCategory.RECIPE_TYPE, recipeManager.getAllRecipesFor(ModRecipeTypes.MILLSTONE.get()).stream().map(RecipeHolder::value).toList());
		
		registration.addRecipes(ChoppingBlockRecipeCategory.RECIPE_TYPE, recipeManager.getAllRecipesFor(ModRecipeTypes.CHOPPING_BLOCK.get()).stream().map(RecipeHolder::value).toList());
		
		registration.addRecipes(DryingRackRecipeCategory.RECIPE_TYPE, recipeManager.getAllRecipesFor(ModRecipeTypes.DRYING_RACK.get()).stream().map(RecipeHolder::value).toList());
		
		registration.addRecipes(ElectricSmelterRecipeCategory.RECIPE_TYPE, recipeManager.getAllRecipesFor(ModRecipeTypes.ELECTRIC_SMELTER.get()).stream().map(RecipeHolder::value).toList());
		
		registration.addRecipes(ElectricGreenhouseRecipeCategory.RECIPE_TYPE, recipeManager.getAllRecipesFor(ModRecipeTypes.GREENHOUSE.get()).stream().map(RecipeHolder::value).toList());
		
		registration.addRecipes(HydraulicPressRecipeCategory.RECIPE_TYPE, recipeManager.getAllRecipesFor(ModRecipeTypes.HYDRAULIC_PRESS.get()).stream().map(RecipeHolder::value).toList());
	}
	
	@Override
	public void registerRecipeCatalysts(IRecipeCatalystRegistration registration)
	{
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.KILN.get()), KilnRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.SMELTER.get()), SmelterRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.MILLSTONE.get()), MillstoneRecipeCategory.RECIPE_TYPE);
		
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.OAK_CHOPPING_BLOCK.get()), ChoppingBlockRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.SPRUCE_CHOPPING_BLOCK.get()), ChoppingBlockRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.BIRCH_CHOPPING_BLOCK.get()), ChoppingBlockRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.JUNGLE_CHOPPING_BLOCK.get()), ChoppingBlockRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.ACACIA_CHOPPING_BLOCK.get()), ChoppingBlockRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.DARK_OAK_CHOPPING_BLOCK.get()), ChoppingBlockRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.MANGROVE_CHOPPING_BLOCK.get()), ChoppingBlockRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.CRIMSON_CHOPPING_BLOCK.get()), ChoppingBlockRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.WARPED_CHOPPING_BLOCK.get()), ChoppingBlockRecipeCategory.RECIPE_TYPE);
		
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.OAK_DRYING_RACK.get()), DryingRackRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.SPRUCE_DRYING_RACK.get()), DryingRackRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.BIRCH_DRYING_RACK.get()), DryingRackRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.JUNGLE_DRYING_RACK.get()), DryingRackRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.ACACIA_DRYING_RACK.get()), DryingRackRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.DARK_OAK_DRYING_RACK.get()), DryingRackRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.MANGROVE_DRYING_RACK.get()), DryingRackRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.CRIMSON_DRYING_RACK.get()), DryingRackRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.WARPED_DRYING_RACK.get()), DryingRackRecipeCategory.RECIPE_TYPE);
		
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.ELECTRIC_BREWERY.get()), RecipeTypes.BREWING);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.ELECTRIC_SMELTER.get()), ElectricSmelterRecipeCategory.RECIPE_TYPE);
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.ELECTRIC_GREENHOUSE.get()), ElectricGreenhouseRecipeCategory.RECIPE_TYPE);
		
		registration.addRecipeCatalyst(new ItemStack(ModBlocks.HYDRAULIC_PRESS.get()), HydraulicPressRecipeCategory.RECIPE_TYPE);
	}
	
	@Override
	public void registerRecipeTransferHandlers(IRecipeTransferRegistration registration)
	{
		registration.addRecipeTransferHandler(KilnMenu.class, ModMenuTypes.KILN.get(), KilnRecipeCategory.RECIPE_TYPE, 37, 1, 0, 36);
		registration.addRecipeTransferHandler(SmelterMenu.class, ModMenuTypes.SMELTER.get(), SmelterRecipeCategory.RECIPE_TYPE, 37, 4, 0, 36);
		registration.addRecipeTransferHandler(MillstoneMenu.class, ModMenuTypes.MILLSTONE.get(), MillstoneRecipeCategory.RECIPE_TYPE, 36, 1, 0, 35);
		
		// TODO: ELECTRIC_BREWERY
		registration.addRecipeTransferHandler(ElectricSmelterMenu.class, ModMenuTypes.ELECTRIC_SMELTER.get(), ElectricSmelterRecipeCategory.RECIPE_TYPE, 36, 2, 0, 35);
		// TODO: ELECTRIC_GREENHOUSE
		
		registration.addRecipeTransferHandler(HydraulicPressMenu.class, ModMenuTypes.HYDRAULIC_PRESS.get(), HydraulicPressRecipeCategory.RECIPE_TYPE, 36, 2, 0, 35);
	}
	
	@Override
	public void registerGuiHandlers(IGuiHandlerRegistration registration)
	{
		registration.addRecipeClickArea(KilnScreen.class, 79, 30, 26, 23, KilnRecipeCategory.RECIPE_TYPE);
		registration.addRecipeClickArea(SmelterScreen.class, 106, 30, 26, 23, SmelterRecipeCategory.RECIPE_TYPE);
		registration.addRecipeClickArea(MillstoneScreen.class, 65, 30, 26, 23, MillstoneRecipeCategory.RECIPE_TYPE);
		
		registration.addRecipeClickArea(ElectricBreweryScreen.class, 55, 29, 28, 11, RecipeTypes.BREWING);
		registration.addRecipeClickArea(ElectricSmelterScreen.class, 87, 31, 26, 23, ElectricSmelterRecipeCategory.RECIPE_TYPE);
		registration.addRecipeClickArea(ElectricGreenhouseScreen.class, 54, 30, 14, 18, ElectricGreenhouseRecipeCategory.RECIPE_TYPE);
		
		registration.addRecipeClickArea(HydraulicPressScreen.class, 72, 50, 23, 16, HydraulicPressRecipeCategory.RECIPE_TYPE);
	}
	
	public static void drawTime(int ticks, GuiGraphics guiGraphics, int x, int y)
	{
		if(ticks > 0)
		{
			Font fontRenderer = Minecraft.getInstance().font;
			Component timeString = Component.translatable("gui.jei.category.smelting.time.seconds", ticks / 20);
			int stringWidth = fontRenderer.width(timeString);
			guiGraphics.drawString(fontRenderer, timeString, x - stringWidth, y, 0xFF808080, false);
		}
	}
}
