package de.boy132.minecraftcontentexpansion.compat.jei.millstone;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.resources.ResourceLocation;

public abstract class AbstractMillstoneRecipeCategory<T> implements IRecipeCategory<T>
{
	protected final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/millstone.png");
	
	protected final IDrawableAnimated ANIMATED_ARROW;
	
	public AbstractMillstoneRecipeCategory(IGuiHelper guiHelper)
	{
		ANIMATED_ARROW = guiHelper.createAnimatedDrawable(guiHelper.createDrawable(BACKGROUND, 176, 16, 24, 17), 200, IDrawableAnimated.StartDirection.LEFT, false);
	}
}
