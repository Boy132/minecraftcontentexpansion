package de.boy132.minecraftcontentexpansion.compat.jei.electricsmelter;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.resources.ResourceLocation;

public abstract class AbstractElectricSmelterRecipeCategory<T> implements IRecipeCategory<T>
{
	protected final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/electric_smelter.png");
	
	protected final IDrawableAnimated ANIMATED_ARROW;
	
	public AbstractElectricSmelterRecipeCategory(IGuiHelper guiHelper)
	{
		ANIMATED_ARROW = guiHelper.createAnimatedDrawable(guiHelper.createDrawable(BACKGROUND, 176, 0, 24, 17), 200, IDrawableAnimated.StartDirection.LEFT, false);
	}
}
