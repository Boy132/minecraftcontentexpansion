package de.boy132.minecraftcontentexpansion.compat.jei.smelter;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.compat.jei.JEICompat;
import de.boy132.minecraftcontentexpansion.recipe.smelter.SmelterRecipe;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableStatic;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;

import java.util.List;

public class SmelterRecipeCategory extends AbstractSmelterRecipeCategory<SmelterRecipe>
{
	public static final RecipeType<SmelterRecipe> RECIPE_TYPE = RecipeType.create(MinecraftContentExpansion.MODID, "smelter", SmelterRecipe.class);
	
	private final Component name;
	private final IDrawable background;
	private final IDrawable icon;
	
	private final IGuiHelper guiHelper;
	
	public SmelterRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper);
		name = Component.translatable("block." + MinecraftContentExpansion.MODID + ".smelter");
		background = guiHelper.createDrawable(BACKGROUND, 4, 4, 160, 78);
		icon = guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(ModBlocks.SMELTER.get()));
		
		this.guiHelper = guiHelper;
	}
	
	@Override
	public void draw(SmelterRecipe recipe, IRecipeSlotsView recipeSlotsView, GuiGraphics guiGraphics, double mouseX, double mouseY)
	{
		ANIMATED_FLAME.draw(guiGraphics, 32, 49);
		ANIMATED_ARROW.draw(guiGraphics, 102, 30);
		
		JEICompat.drawTime(recipe.getSmeltTime(), guiGraphics, background.getWidth(), 70);
		
		int minHeat = (int) (73 * recipe.getMinHeat());
		IDrawableStatic heat = guiHelper.createDrawable(BACKGROUND, 176, 31 + (73 - minHeat), 16, minHeat);
		heat.draw(guiGraphics, 4, 2 + (73 - minHeat));
	}
	
	@Override
	public List<Component> getTooltipStrings(SmelterRecipe recipe, IRecipeSlotsView recipeSlotsView, double mouseX, double mouseY)
	{
		if(mouseX >= 2 && mouseY >= 0 && mouseX < (2 + 20) && mouseY < 77)
			return List.of(Component.literal(Math.round(recipe.getMinHeat() * 100) + " %"));
		
		return super.getTooltipStrings(recipe, recipeSlotsView, mouseX, mouseY);
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, SmelterRecipe recipe, IFocusGroup focuses)
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 61, 21).addIngredients(recipe.getIngredients().get(0));
		builder.addSlot(RecipeIngredientRole.INPUT, 79, 21).addIngredients(recipe.getIngredients().get(1));
		builder.addSlot(RecipeIngredientRole.INPUT, 61, 39).addIngredients(recipe.getIngredients().get(2));
		builder.addSlot(RecipeIngredientRole.INPUT, 79, 39).addIngredients(recipe.getIngredients().get(3));
		
		builder.addSlot(RecipeIngredientRole.OUTPUT, 137, 30).addItemStack(recipe.getResultItem(null));
	}
	
	@Override
	public RecipeType<SmelterRecipe> getRecipeType()
	{
		return RECIPE_TYPE;
	}
	
	@Override
	public Component getTitle()
	{
		return name;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return icon;
	}
}
