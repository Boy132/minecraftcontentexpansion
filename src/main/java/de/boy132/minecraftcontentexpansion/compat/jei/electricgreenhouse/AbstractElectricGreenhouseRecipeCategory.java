package de.boy132.minecraftcontentexpansion.compat.jei.electricgreenhouse;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.SoilType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;

public abstract class AbstractElectricGreenhouseRecipeCategory<T> implements IRecipeCategory<T>
{
	protected final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/greenhouse.png");
	
	protected Ingredient getSoilBlock(SoilType soilType)
	{
		if(soilType == SoilType.DIRT)
			return Ingredient.of(Items.DIRT, Items.FARMLAND);
		
		if(soilType == SoilType.SAND)
			return Ingredient.of(Items.SAND);
		
		if(soilType == SoilType.SOULSAND)
			return Ingredient.of(Items.SOUL_SAND);
		
		return Ingredient.EMPTY;
	}
}
