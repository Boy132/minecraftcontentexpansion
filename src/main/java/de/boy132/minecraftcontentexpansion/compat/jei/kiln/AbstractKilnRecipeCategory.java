package de.boy132.minecraftcontentexpansion.compat.jei.kiln;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.drawable.IDrawableStatic;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.resources.ResourceLocation;

public abstract class AbstractKilnRecipeCategory<T> implements IRecipeCategory<T>
{
	protected final ResourceLocation BACKGROUND = ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "textures/gui/kiln.png");
	
	protected final IDrawableStatic STATIC_FLAME;
	protected final IDrawableAnimated ANIMATED_FLAME;
	protected final IDrawableAnimated ANIMATED_ARROW;
	
	public AbstractKilnRecipeCategory(IGuiHelper guiHelper)
	{
		STATIC_FLAME = guiHelper.createDrawable(BACKGROUND, 176, 0, 14, 14);
		ANIMATED_FLAME = guiHelper.createAnimatedDrawable(STATIC_FLAME, 300, IDrawableAnimated.StartDirection.TOP, true);
		ANIMATED_ARROW = guiHelper.createAnimatedDrawable(guiHelper.createDrawable(BACKGROUND, 176, 14, 24, 17), 200, IDrawableAnimated.StartDirection.LEFT, false);
	}
}
