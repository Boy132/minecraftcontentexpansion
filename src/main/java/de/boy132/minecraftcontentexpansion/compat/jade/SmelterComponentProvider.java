package de.boy132.minecraftcontentexpansion.compat.jade;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.smelter.SmelterBlockEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import snownee.jade.api.BlockAccessor;
import snownee.jade.api.IBlockComponentProvider;
import snownee.jade.api.IServerDataProvider;
import snownee.jade.api.ITooltip;
import snownee.jade.api.config.IPluginConfig;
import snownee.jade.api.ui.IElementHelper;

public class SmelterComponentProvider implements IBlockComponentProvider, IServerDataProvider<BlockAccessor>
{
	public static SmelterComponentProvider INSTANCE = new SmelterComponentProvider();
	
	@Override
	public void appendTooltip(ITooltip tooltip, BlockAccessor accessor, IPluginConfig config)
	{
		if(accessor.getServerData().contains("smeltTime") && accessor.getServerData().contains("neededSmeltTime"))
		{
			int smeltTime = accessor.getServerData().getInt("smeltTime");
			if(smeltTime > 0)
			{
				IElementHelper helper = IElementHelper.get();
				
				int neededSmeltTime = accessor.getServerData().getInt("neededSmeltTime");
				tooltip.add(helper.progress((float) smeltTime / neededSmeltTime));
			}
		}
		
		if(accessor.getServerData().contains("heat"))
		{
			float heat = accessor.getServerData().getInt("heat");
			tooltip.add(Component.literal(String.valueOf(Math.round((heat / SmelterBlockEntity.MAX_HEAT) * 100f)))
					.append(Component.translatable("jade." + MinecraftContentExpansion.MODID + ".smelter.heat")));
		}
	}
	
	@Override
	public void appendServerData(CompoundTag data, BlockAccessor blockAccessor)
	{
		SmelterBlockEntity smelterBlockEntity = (SmelterBlockEntity) blockAccessor.getBlockEntity();
		CompoundTag smelterTag = smelterBlockEntity.saveWithoutMetadata();
		
		data.putInt("heat", smelterTag.getInt("heat"));
		data.putInt("smeltTime", smelterTag.getInt("smeltTime"));
		data.putInt("neededSmeltTime", smelterTag.getInt("neededSmeltTime"));
	}
	
	@Override
	public ResourceLocation getUid()
	{
		return ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "smelter");
	}
}
