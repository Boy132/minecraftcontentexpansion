package de.boy132.minecraftcontentexpansion.compat.jade;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.dryingrack.DryingRackBlockEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import snownee.jade.api.BlockAccessor;
import snownee.jade.api.IBlockComponentProvider;
import snownee.jade.api.IServerDataProvider;
import snownee.jade.api.ITooltip;
import snownee.jade.api.config.IPluginConfig;

public class DryingRackComponentProvider implements IBlockComponentProvider, IServerDataProvider<BlockAccessor>
{
	public static DryingRackComponentProvider INSTANCE = new DryingRackComponentProvider();
	
	@Override
	public void appendTooltip(ITooltip tooltip, BlockAccessor accessor, IPluginConfig config)
	{
		if(accessor.getServerData().contains("dryingTicks") && accessor.getServerData().contains("neededDryingTicks"))
		{
			int dryingTicks = accessor.getServerData().getInt("dryingTicks");
			if(dryingTicks > 0)
			{
				float neededDryingTicks = accessor.getServerData().getInt("neededDryingTicks");
				tooltip.add(Component.literal(String.valueOf(Math.round((dryingTicks / neededDryingTicks) * 100f)))
						.append(Component.translatable("jade." + MinecraftContentExpansion.MODID + ".drying_rack.ticks")));
			}
		}
	}
	
	@Override
	public void appendServerData(CompoundTag data, BlockAccessor blockAccessor)
	{
		DryingRackBlockEntity dryingRackBlockEntity = (DryingRackBlockEntity) blockAccessor.getBlockEntity();
		CompoundTag dryingRackTag = dryingRackBlockEntity.saveWithoutMetadata();
		
		data.putInt("dryingTicks", dryingRackTag.getInt("dryingTicks"));
		data.putInt("neededDryingTicks", dryingRackTag.getInt("neededDryingTicks"));
	}
	
	@Override
	public ResourceLocation getUid()
	{
		return ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "drying_rack");
	}
}
