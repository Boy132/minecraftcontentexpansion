package de.boy132.minecraftcontentexpansion.compat.jade;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.dryingrack.DryingRackBlock;
import de.boy132.minecraftcontentexpansion.block.entity.dryingrack.DryingRackBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.smelter.SmelterBlock;
import de.boy132.minecraftcontentexpansion.block.entity.smelter.SmelterBlockEntity;
import snownee.jade.api.IWailaClientRegistration;
import snownee.jade.api.IWailaCommonRegistration;
import snownee.jade.api.IWailaPlugin;
import snownee.jade.api.WailaPlugin;

@WailaPlugin(MinecraftContentExpansion.MODID)
public class JadeCompat implements IWailaPlugin
{
	@Override
	public void register(IWailaCommonRegistration registration)
	{
		registration.registerBlockDataProvider(SmelterComponentProvider.INSTANCE, SmelterBlockEntity.class);
		registration.registerBlockDataProvider(DryingRackComponentProvider.INSTANCE, DryingRackBlockEntity.class);
	}
	
	@Override
	public void registerClient(IWailaClientRegistration registration)
	{
		registration.registerBlockComponent(SmelterComponentProvider.INSTANCE, SmelterBlock.class);
		registration.registerBlockComponent(DryingRackComponentProvider.INSTANCE, DryingRackBlock.class);
	}
}
