package de.boy132.minecraftcontentexpansion.fluid;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fluids.FluidType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import org.joml.Vector3f;

public class ModFluidTypes
{
	private static final DeferredRegister<FluidType> FLUID_TYPES = DeferredRegister.create(ForgeRegistries.Keys.FLUID_TYPES, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<FluidType> OIL = FLUID_TYPES.register("oil", () -> new BaseFluidType(FluidType.Properties.create().descriptionId("block.minecraftcontentexpansion.oil").supportsBoating(false).canHydrate(false).canConvertToSource(false).canSwim(false).canDrown(false).density(3000).viscosity(6000).motionScale(0.05D).pathType(BlockPathTypes.LAVA), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/oil_still"), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "block/oil_flow"), null, 0xFF222222, new Vector3f(34f / 255f, 34f / 255f, 34f / 255f), 2f));
	
	public static void register(IEventBus eventBus)
	{
		FLUID_TYPES.register(eventBus);
	}
}
