package de.boy132.minecraftcontentexpansion.fluid;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModFluids
{
	private static final DeferredRegister<Fluid> FLUIDS = DeferredRegister.create(ForgeRegistries.FLUIDS, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<FlowingFluid> OIL = FLUIDS.register("oil", () -> new ForgeFlowingFluid.Source(ModFluids.OIL_PROPERTIES));
	public static final RegistryObject<FlowingFluid> FLOWING_OIL = FLUIDS.register("flowing_oil", () -> new ForgeFlowingFluid.Flowing(ModFluids.OIL_PROPERTIES));
	private static final ForgeFlowingFluid.Properties OIL_PROPERTIES = new ForgeFlowingFluid.Properties(ModFluidTypes.OIL, OIL, FLOWING_OIL).block(ModBlocks.OIL).bucket(ModItems.OIL_BUCKET);
	
	public static void register(IEventBus eventBus)
	{
		FLUIDS.register(eventBus);
	}
}
