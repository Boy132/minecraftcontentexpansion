package de.boy132.minecraftcontentexpansion.item.entity;

import de.boy132.minecraftcontentexpansion.entity.rock.BaseRockEntity;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

import java.util.function.Function;

public class RockItem extends Item
{
	private final Function<Player, BaseRockEntity> entitySupplier;
	
	public RockItem(Properties properties, Function<Player, BaseRockEntity> entitySupplier)
	{
		super(properties);
		this.entitySupplier = entitySupplier;
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		
		level.playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.SNOWBALL_THROW, SoundSource.NEUTRAL, 0.5F, 0.4F / (level.random.nextFloat() * 0.4F + 0.8F));
		
		if(!level.isClientSide)
		{
			player.getCooldowns().addCooldown(this, 100);
			
			BaseRockEntity entityRock = entitySupplier.apply(player);
			entityRock.setItem(itemstack);
			entityRock.setOwner(player);
			entityRock.shootFromRotation(player, player.getXRot(), player.getYRot(), 0.0F, 1.5F, 1.0F);
			level.addFreshEntity(entityRock);
		}
		
		player.awardStat(Stats.ITEM_USED.get(this));
		
		if(!player.getAbilities().instabuild)
			itemstack.shrink(1);
		
		return InteractionResultHolder.sidedSuccess(itemstack, level.isClientSide());
	}
}
