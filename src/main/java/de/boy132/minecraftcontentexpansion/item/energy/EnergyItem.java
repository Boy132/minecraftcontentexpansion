package de.boy132.minecraftcontentexpansion.item.energy;

import de.boy132.minecraftcontentexpansion.entchantments.ModEnchantments;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.energy.IEnergyStorage;

import java.text.NumberFormat;
import java.util.List;

public class EnergyItem extends Item implements IEnergyItem
{
	private final int capacity;
	private final int maxReceive;
	private final int maxExtract;
	
	protected int energyUsage;
	
	public EnergyItem(int capacity, int maxReceive, int maxExtract, Properties properties)
	{
		super(properties);
		this.capacity = capacity;
		this.maxReceive = maxReceive;
		this.maxExtract = maxExtract;
	}
	
	public EnergyItem(int capacity, int maxTransfer, Properties properties)
	{
		this(capacity, maxTransfer, maxTransfer, properties);
	}
	
	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, CompoundTag compoundNBT)
	{
		return new EnergyCapabilityProvider(stack, this);
	}
	
	public ItemStack getCreativeTabStack()
	{
		ItemStack withEnergy = new ItemStack(this);
		withEnergy.getOrCreateTag().putInt("energy", capacity);
		return withEnergy;
	}
	
	@Override
	public boolean isBarVisible(ItemStack stack)
	{
		IEnergyStorage energyStorage = stack.getCapability(ForgeCapabilities.ENERGY).orElse(null);
		if(energyStorage != null)
			return energyStorage.getEnergyStored() < energyStorage.getMaxEnergyStored();
		
		return super.isBarVisible(stack);
	}
	
	@Override
	public int getBarColor(ItemStack stack)
	{
		if(stack.getCapability(ForgeCapabilities.ENERGY).isPresent())
			return Mth.hsvToRgb(0.5f, 0.5f, 1f);
		
		return super.getBarColor(stack);
	}
	
	@Override
	public int getBarWidth(ItemStack stack)
	{
		IEnergyStorage energyStorage = stack.getCapability(ForgeCapabilities.ENERGY).orElse(null);
		if(energyStorage != null)
			return Math.round((float) energyStorage.getEnergyStored() * 13.0F / (float) energyStorage.getMaxEnergyStored());
		
		return super.getBarWidth(stack);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level world, List<Component> toolTips, TooltipFlag flag)
	{
		stack.getCapability(ForgeCapabilities.ENERGY).ifPresent(energyStorage ->
		{
			NumberFormat numberFormat = NumberFormat.getNumberInstance();
			toolTips.add(Component.literal(numberFormat.format(energyStorage.getEnergyStored()) + " / " + numberFormat.format(energyStorage.getMaxEnergyStored()) + " FE"));
		});
		
		super.appendHoverText(stack, world, toolTips, flag);
	}
	
	@Override
	public int getMaxEnergyStored(ItemStack stack)
	{
		return capacity;
	}
	
	@Override
	public int getMaxReceive(ItemStack stack)
	{
		return maxReceive;
	}
	
	@Override
	public int getMaxExtract(ItemStack stack)
	{
		return maxExtract;
	}
	
	public int getEnergyUsage(ItemStack stack)
	{
		if(energyUsage == 0)
			return 0;
		
		if(EnchantmentHelper.getEnchantments(stack).containsKey(ModEnchantments.POWER_SAVING.get()))
			return Math.max(1, energyUsage / 2);
		
		return energyUsage;
	}
}
