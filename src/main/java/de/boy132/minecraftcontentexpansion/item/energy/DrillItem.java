package de.boy132.minecraftcontentexpansion.item.energy;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import de.boy132.minecraftcontentexpansion.entchantments.ModEnchantments;
import net.minecraft.core.BlockPos;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.TierSortingRegistry;
import net.minecraftforge.common.ToolAction;
import net.minecraftforge.common.ToolActions;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.energy.IEnergyStorage;

public class DrillItem extends EnergyItem
{
	private final Tier tier;
	
	public DrillItem(Tier tier, int capacity, int maxTransfer, int energyUsage, Properties properties)
	{
		super(capacity, maxTransfer, properties);
		this.tier = tier;
		this.energyUsage = energyUsage;
	}
	
	public Tier getTier()
	{
		return tier;
	}
	
	@Override
	public int getEnchantmentValue()
	{
		return tier.getEnchantmentValue();
	}
	
	public float getEfficiency()
	{
		return tier.getSpeed() + 0.8f;
	}
	
	@Override
	public boolean isEnchantable(ItemStack stack)
	{
		return true;
	}
	
	@Override
	public boolean isBookEnchantable(ItemStack stack, ItemStack book)
	{
		if(EnchantmentHelper.getEnchantments(book).containsKey(Enchantments.MENDING) || EnchantmentHelper.getEnchantments(book).containsKey(Enchantments.UNBREAKING))
			return false;
		
		return super.isBookEnchantable(stack, book);
	}
	
	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, Enchantment enchantment)
	{
		return enchantment == Enchantments.BLOCK_EFFICIENCY || enchantment == ModEnchantments.POWER_SAVING.get();
	}
	
	@Override
	public boolean hurtEnemy(ItemStack stack, LivingEntity target, LivingEntity attacker)
	{
		if(!attacker.level().isClientSide && (!(attacker instanceof Player) || !((Player) attacker).getAbilities().instabuild))
		{
			stack.getCapability(ForgeCapabilities.ENERGY).ifPresent(energyStorage ->
			{
				int energyUsage = getEnergyUsage(stack) / 2;
				energyStorage.extractEnergy(energyUsage, false);
			});
		}
		
		return true;
	}
	
	@Override
	public boolean mineBlock(ItemStack stack, Level level, BlockState state, BlockPos pos, LivingEntity miner)
	{
		if(!level.isClientSide && state.getDestroySpeed(level, pos) != 0.0F)
			stack.getCapability(ForgeCapabilities.ENERGY).ifPresent(energyStorage -> energyStorage.extractEnergy(getEnergyUsage(stack), false));
		
		return true;
	}
	
	@Override
	public boolean canPerformAction(ItemStack stack, ToolAction toolAction)
	{
		return toolAction == ToolActions.PICKAXE_DIG || toolAction == ToolActions.SHOVEL_DIG;
	}
	
	@Override
	public boolean isCorrectToolForDrops(ItemStack stack, BlockState state)
	{
		if(state.is(BlockTags.MINEABLE_WITH_PICKAXE) || state.is(BlockTags.MINEABLE_WITH_SHOVEL))
			return TierSortingRegistry.isCorrectTierForDrops(tier, state);
		
		return false;
	}
	
	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state)
	{
		IEnergyStorage energyStorage = stack.getCapability(ForgeCapabilities.ENERGY).orElse(null);
		if(energyStorage == null)
			return super.getDestroySpeed(stack, state);
		
		int energy = energyStorage.getEnergyStored();
		if(energy < getEnergyUsage(stack))
			return 0.5f;
		
		float efficiency = getEfficiency();
		return (state.is(BlockTags.MINEABLE_WITH_PICKAXE) || state.is(BlockTags.MINEABLE_WITH_SHOVEL)) ? efficiency : 1.0F;
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot slot, ItemStack stack)
	{
		if(slot == EquipmentSlot.MAINHAND)
		{
			float attackDamage = tier.getAttackDamageBonus() / 2;
			
			IEnergyStorage energyStorage = stack.getCapability(ForgeCapabilities.ENERGY).orElse(null);
			if(energyStorage != null)
			{
				int energyUsage = getEnergyUsage(stack) / 2;
				int energyExtracted = energyStorage.extractEnergy(energyUsage, true);
				if(energyExtracted > 0)
				{
					float extraDamage = attackDamage * energyExtracted / energyUsage;
					attackDamage += extraDamage;
				}
			}
			
			ImmutableMultimap.Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
			builder.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon modifier", attackDamage, AttributeModifier.Operation.ADDITION));
			builder.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", -2.8f, AttributeModifier.Operation.ADDITION));
			return builder.build();
		}
		
		return super.getAttributeModifiers(slot, stack);
	}
}
