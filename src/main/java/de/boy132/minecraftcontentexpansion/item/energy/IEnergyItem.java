package de.boy132.minecraftcontentexpansion.item.energy;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;

public interface IEnergyItem
{
	int getMaxEnergyStored(ItemStack stack);
	
	int getMaxReceive(ItemStack stack);
	
	int getMaxExtract(ItemStack stack);
	
	default boolean checkTag(ItemStack stack)
	{
		return stack.hasTag() && stack.getTag().contains("energy");
	}
	
	default int getEnergyStored(ItemStack stack)
	{
		if(!checkTag(stack))
			return 0;
		
		return stack.getTag().getInt("energy");
	}
	
	default int receiveEnergy(ItemStack stack, int maxReceive, boolean simulate)
	{
		if(!checkTag(stack))
			stack.setTag(new CompoundTag());
		
		int energy = stack.getTag().getInt("energy");
		int energyReceived = Math.min(getMaxEnergyStored(stack) - energy, Math.min(maxReceive, getMaxReceive(stack)));
		
		if(!simulate)
		{
			energy += energyReceived;
			stack.getTag().putInt("energy", energy);
		}
		
		return energyReceived;
	}
	
	default int extractEnergy(ItemStack stack, int maxExtract, boolean simulate)
	{
		if(!checkTag(stack))
			return 0;
		
		int energy = stack.getTag().getInt("energy");
		int energyExtracted = Math.min(maxExtract, getMaxExtract(stack));
		
		if(!simulate)
		{
			energy -= energyExtracted;
			stack.getTag().putInt("energy", energy);
		}
		
		return energyExtracted;
	}
}
