package de.boy132.minecraftcontentexpansion.item.energy;

import net.minecraft.core.Direction;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.IEnergyStorage;

public class EnergyCapabilityProvider implements ICapabilityProvider
{
	private final LazyOptional<IEnergyStorage> lazyEnergyStorage;
	
	public EnergyCapabilityProvider(ItemStack stack, IEnergyItem item)
	{
		lazyEnergyStorage = LazyOptional.of(() -> new IEnergyStorage()
		{
			@Override
			public int receiveEnergy(int maxReceive, boolean simulate)
			{
				return item.receiveEnergy(stack, maxReceive, simulate);
			}
			
			@Override
			public int extractEnergy(int maxExtract, boolean simulate)
			{
				return item.extractEnergy(stack, maxExtract, simulate);
			}
			
			@Override
			public int getMaxEnergyStored()
			{
				return item.getMaxEnergyStored(stack);
			}
			
			@Override
			public int getEnergyStored()
			{
				return item.getEnergyStored(stack);
			}
			
			@Override
			public boolean canReceive()
			{
				return true;
			}
			
			@Override
			public boolean canExtract()
			{
				return true;
			}
		});
	}
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability)
	{
		return getCapability(capability, null);
	}
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction direction)
	{
		if(capability == ForgeCapabilities.ENERGY)
			return lazyEnergyStorage.cast();
		
		return LazyOptional.empty();
	}
}
