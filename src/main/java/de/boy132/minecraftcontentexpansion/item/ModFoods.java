package de.boy132.minecraftcontentexpansion.item;

import net.minecraft.world.food.FoodProperties;

public class ModFoods
{
	public static final FoodProperties RAW_BACON = new FoodProperties.Builder().nutrition(2).saturationMod(0.3F).build();
	public static final FoodProperties COOKED_BACON = new FoodProperties.Builder().nutrition(6).saturationMod(0.5F).build();
	public static final FoodProperties FRIED_EGG = new FoodProperties.Builder().nutrition(5).saturationMod(0.3F).build();
	public static final FoodProperties CHEESE = new FoodProperties.Builder().nutrition(4).saturationMod(0.3F).build();
	public static final FoodProperties PIZZA = new FoodProperties.Builder().nutrition(7).saturationMod(0.55F).build();
	public static final FoodProperties CHOCOLATE = new FoodProperties.Builder().nutrition(3).saturationMod(0.2F).build();
	public static final FoodProperties POPCORN = new FoodProperties.Builder().nutrition(3).saturationMod(0.3F).build();
	public static final FoodProperties PURIFIED_MEAT = new FoodProperties.Builder().nutrition(4).saturationMod(0.4F).build();
	public static final FoodProperties BEEF_JERKY = new FoodProperties.Builder().nutrition(6).saturationMod(0.5F).build();
	public static final FoodProperties ONION_RINGS = new FoodProperties.Builder().nutrition(5).saturationMod(0.4F).build();
	public static final FoodProperties SAUSAGE = new FoodProperties.Builder().nutrition(5).saturationMod(0.5F).build();
	public static final FoodProperties HOT_DOG = new FoodProperties.Builder().nutrition(7).saturationMod(0.65F).build();
	public static final FoodProperties GARDEN_SALAD = new FoodProperties.Builder().nutrition(6).saturationMod(0.4F).build();
	public static final FoodProperties HAMBURGER = new FoodProperties.Builder().nutrition(9).saturationMod(0.75F).build();
	public static final FoodProperties CHEESEBURGER = new FoodProperties.Builder().nutrition(10).saturationMod(0.8F).build();
	public static final FoodProperties SUSHI = new FoodProperties.Builder().nutrition(5).saturationMod(0.5F).build();
	
	public static final FoodProperties APPLE_PIE = new FoodProperties.Builder().nutrition(8).saturationMod(0.3F).build();
	public static final FoodProperties STRAWBERRY_PIE = new FoodProperties.Builder().nutrition(7).saturationMod(0.3F).build();
	
	public static final FoodProperties TOMATO = new FoodProperties.Builder().nutrition(3).saturationMod(0.4F).build();
	public static final FoodProperties GRAPE = new FoodProperties.Builder().nutrition(2).saturationMod(0.4F).build();
	public static final FoodProperties STRAWBERRY = new FoodProperties.Builder().nutrition(2).saturationMod(0.1F).build();
	public static final FoodProperties CORN = new FoodProperties.Builder().nutrition(3).saturationMod(0.4F).build();
	public static final FoodProperties LETTUCE = new FoodProperties.Builder().nutrition(2).saturationMod(0.2F).build();
	public static final FoodProperties ONION = new FoodProperties.Builder().nutrition(2).saturationMod(0.1F).build();
	public static final FoodProperties RICE = new FoodProperties.Builder().nutrition(2).saturationMod(0.1F).build();
	
	public static final FoodProperties VEGETABLE_SOUP = new FoodProperties.Builder().nutrition(7).saturationMod(0.6F).build();
	public static final FoodProperties PUMPKIN_SOUP = new FoodProperties.Builder().nutrition(6).saturationMod(0.6F).build();
	public static final FoodProperties CACTUS_SOUP = new FoodProperties.Builder().nutrition(4).saturationMod(0.6F).build();
	public static final FoodProperties FISH_SOUP = new FoodProperties.Builder().nutrition(6).saturationMod(0.6F).build();
}
