package de.boy132.minecraftcontentexpansion.item;

import com.google.common.collect.ImmutableMap;
import net.minecraft.core.BlockPos;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;

import java.util.Map;

public class BarkItem extends Item
{
	private static final Map<Block, Block> strippedToBlockMap = (new ImmutableMap.Builder<Block, Block>()).put(Blocks.STRIPPED_OAK_WOOD, Blocks.OAK_WOOD).put(Blocks.STRIPPED_OAK_LOG, Blocks.OAK_LOG).put(Blocks.STRIPPED_DARK_OAK_WOOD, Blocks.DARK_OAK_WOOD).put(Blocks.STRIPPED_DARK_OAK_LOG, Blocks.DARK_OAK_LOG).put(Blocks.STRIPPED_ACACIA_WOOD, Blocks.ACACIA_WOOD).put(Blocks.STRIPPED_ACACIA_LOG, Blocks.ACACIA_LOG).put(Blocks.STRIPPED_CHERRY_WOOD, Blocks.CHERRY_WOOD).put(Blocks.STRIPPED_CHERRY_LOG, Blocks.CHERRY_LOG).put(Blocks.STRIPPED_BIRCH_WOOD, Blocks.BIRCH_WOOD).put(Blocks.STRIPPED_BIRCH_LOG, Blocks.BIRCH_LOG).put(Blocks.STRIPPED_JUNGLE_WOOD, Blocks.JUNGLE_WOOD).put(Blocks.STRIPPED_JUNGLE_LOG, Blocks.JUNGLE_LOG).put(Blocks.STRIPPED_SPRUCE_WOOD, Blocks.SPRUCE_WOOD).put(Blocks.STRIPPED_SPRUCE_LOG, Blocks.SPRUCE_LOG).put(Blocks.STRIPPED_MANGROVE_WOOD, Blocks.MANGROVE_WOOD).put(Blocks.STRIPPED_MANGROVE_LOG, Blocks.MANGROVE_LOG).build();
	
	public BarkItem(Properties properties)
	{
		super(properties);
	}
	
	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Level level = context.getLevel();
		BlockPos pos = context.getClickedPos();
		BlockState clickedState = level.getBlockState(pos);
		Block block = clickedState.getBlock();
		if(isStrippedLog(block))
		{
			Player player = context.getPlayer();
			if(!level.isClientSide())
			{
				if(!player.getAbilities().instabuild)
					context.getItemInHand().shrink(1);
				
				level.setBlock(pos, strippedToBlockMap.get(block).withPropertiesOf(clickedState), 3);
			} else
				level.playSound(player, pos, SoundEvents.AXE_STRIP, SoundSource.BLOCKS, 1.0F, 1.0F);
			
			return InteractionResult.sidedSuccess(!level.isClientSide);
		}
		
		return super.useOn(context);
	}
	
	public static boolean isStrippedLog(Block block)
	{
		return strippedToBlockMap.containsKey(block);
	}
	
	public static boolean isValidLog(Block block)
	{
		return strippedToBlockMap.containsValue(block);
	}
}
