package de.boy132.minecraftcontentexpansion.item.bucket;

import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public class ClayBucketItem extends ModBucketItem
{
	public ClayBucketItem(Supplier<? extends Fluid> fluid, Properties properties)
	{
		super(fluid, properties);
	}
	
	public ClayBucketItem(Fluid fluid, Properties properties)
	{
		this(() -> fluid, properties);
	}
	
	@Override
	public RegistryObject<Item> getEmptyBucket()
	{
		return ModItems.CLAY_BUCKET;
	}
	
	@Override
	public RegistryObject<Item> getWaterBucket()
	{
		return ModItems.WATER_CLAY_BUCKET;
	}
	
	@Override
	public boolean hasLavaBucket()
	{
		return false;
	}
	
	@Override
	public RegistryObject<Item> getLavalBucket()
	{
		return null;
	}
}
