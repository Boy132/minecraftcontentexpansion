package de.boy132.minecraftcontentexpansion.item.bucket;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.dispenser.BlockSource;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.*;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.BucketPickup;
import net.minecraft.world.level.block.DispenserBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public abstract class ModBucketItem extends BucketItem
{
	protected Fluid content;
	
	public ModBucketItem(Supplier<? extends Fluid> fluid, Properties builder)
	{
		super(fluid, builder);
		content = fluid.get();
	}
	
	public ModBucketItem(Fluid fluid, Properties properties)
	{
		this(() -> fluid, properties);
	}
	
	public abstract RegistryObject<Item> getEmptyBucket();
	
	public abstract RegistryObject<Item> getWaterBucket();
	
	public abstract boolean hasLavaBucket();
	
	public abstract RegistryObject<Item> getLavalBucket();
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack stackInHand = player.getItemInHand(hand);
		BlockHitResult blockHitResult = getPlayerPOVHitResult(level, player, content == Fluids.EMPTY ? ClipContext.Fluid.SOURCE_ONLY : ClipContext.Fluid.NONE);
		
		InteractionResultHolder<ItemStack> bucketUseResult = ForgeEventFactory.onBucketUse(player, level, stackInHand, blockHitResult);
		if(bucketUseResult != null)
			return bucketUseResult;
		
		if(blockHitResult.getType() == HitResult.Type.MISS)
			return InteractionResultHolder.pass(stackInHand);
		
		if(blockHitResult.getType() != HitResult.Type.BLOCK)
			return InteractionResultHolder.pass(stackInHand);
		
		BlockPos pos = blockHitResult.getBlockPos();
		Direction direction = blockHitResult.getDirection();
		BlockPos relativePos = pos.relative(direction);
		
		if(level.mayInteract(player, pos) && player.mayUseItemAt(relativePos, direction, stackInHand))
		{
			BlockState state = level.getBlockState(pos);
			if(content == Fluids.EMPTY)
			{
				if(state.getBlock() instanceof BucketPickup bucketpickup)
				{
					ItemStack pickupBlock = bucketpickup.pickupBlock(player, level, pos, state);
					// Custom
					pickupBlock = convertPickupBlock(this, pickupBlock);
					
					if(!pickupBlock.isEmpty())
					{
						player.awardStat(Stats.ITEM_USED.get(this));
						bucketpickup.getPickupSound(state).ifPresent((soundEvent) -> player.playSound(soundEvent, 1.0F, 1.0F));
						level.gameEvent(player, GameEvent.FLUID_PICKUP, pos);
						ItemStack filledResult = ItemUtils.createFilledResult(stackInHand, player, pickupBlock);
						if(!level.isClientSide)
							CriteriaTriggers.FILLED_BUCKET.trigger((ServerPlayer) player, pickupBlock);
						
						return InteractionResultHolder.sidedSuccess(filledResult, level.isClientSide());
					}
				}
				
				return InteractionResultHolder.fail(stackInHand);
			} else
			{
				BlockPos checkPos = canBlockContainFluid(level, pos, state) ? pos : relativePos;
				if(emptyContents(player, level, checkPos, blockHitResult, stackInHand))
				{
					checkExtraContent(player, level, stackInHand, checkPos);
					if(player instanceof ServerPlayer)
						CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayer) player, checkPos, stackInHand);
					
					player.awardStat(Stats.ITEM_USED.get(this));
					return InteractionResultHolder.sidedSuccess(!player.getAbilities().instabuild ? new ItemStack(getEmptyBucket().get()) : stackInHand, level.isClientSide());
				} else
					return InteractionResultHolder.fail(stackInHand);
			}
		} else
			return InteractionResultHolder.fail(stackInHand);
	}
	
	public static ItemStack convertPickupBlock(ModBucketItem bucketItem, ItemStack pickupBlock)
	{
		if(pickupBlock.getItem() == Items.WATER_BUCKET)
			return new ItemStack(bucketItem.getWaterBucket().get(), pickupBlock.getCount());
		else if(pickupBlock.getItem() == Items.LAVA_BUCKET && bucketItem.hasLavaBucket())
			return new ItemStack(bucketItem.getLavalBucket().get(), pickupBlock.getCount());
		
		return pickupBlock;
	}
	
	public static void registerDispenserBehaviorForEmpty(ModBucketItem emptyBucket)
	{
		DispenserBlock.registerBehavior(emptyBucket, new DefaultDispenseItemBehavior()
		{
			private final DefaultDispenseItemBehavior defaultDispenseItemBehavior = new DefaultDispenseItemBehavior();
			
			public ItemStack execute(BlockSource blockSource, ItemStack itemStack)
			{
				LevelAccessor level = blockSource.level();
				BlockPos blockPos = blockSource.pos().relative(blockSource.state().getValue(DispenserBlock.FACING));
				BlockState blockState = level.getBlockState(blockPos);
				Block block = blockState.getBlock();
				if(block instanceof BucketPickup)
				{
					ItemStack pickupBlock = ((BucketPickup) block).pickupBlock(null, level, blockPos, blockState);
					// Custom
					pickupBlock = convertPickupBlock(emptyBucket, pickupBlock);
					
					if(pickupBlock.isEmpty())
						return super.execute(blockSource, itemStack);
					
					level.gameEvent(null, GameEvent.FLUID_PICKUP, blockPos);
					Item item = pickupBlock.getItem();
					itemStack.shrink(1);
					
					if(itemStack.isEmpty())
						return new ItemStack(item);
					
					if(blockSource.blockEntity().addItem(new ItemStack(item)) < 0)
						defaultDispenseItemBehavior.dispense(blockSource, new ItemStack(item));
					
					return itemStack;
				} else
					return super.execute(blockSource, itemStack);
			}
		});
	}
	
	public static void registerDispenserBehaviorForFilled(ModBucketItem filledBucket)
	{
		DispenserBlock.registerBehavior(filledBucket, new DefaultDispenseItemBehavior()
		{
			private final DefaultDispenseItemBehavior defaultDispenseItemBehavior = new DefaultDispenseItemBehavior();
			
			public ItemStack execute(BlockSource blockSource, ItemStack itemStack)
			{
				DispensibleContainerItem dispensibleContainerItem = (DispensibleContainerItem) itemStack.getItem();
				BlockPos blockPos = blockSource.pos().relative(blockSource.state().getValue(DispenserBlock.FACING));
				Level level = blockSource.level();
				if(dispensibleContainerItem.emptyContents(null, level, blockPos, null, itemStack))
				{
					dispensibleContainerItem.checkExtraContent(null, level, itemStack, blockPos);
					return new ItemStack(filledBucket.getEmptyBucket().get());
				} else
					return defaultDispenseItemBehavior.dispense(blockSource, itemStack);
			}
		});
	}
}
