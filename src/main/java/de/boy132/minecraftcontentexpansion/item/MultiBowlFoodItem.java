package de.boy132.minecraftcontentexpansion.item;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;

public class MultiBowlFoodItem extends Item
{
	public MultiBowlFoodItem(Properties properties)
	{
		super(properties);
	}
	
	public ItemStack finishUsingItem(ItemStack stack, Level level, LivingEntity entity)
	{
		ItemStack superStack = super.finishUsingItem(stack, level, entity);
		
		if(stack.isEmpty())
			return new ItemStack(Items.BOWL);
		
		if(entity instanceof Player player && !player.getAbilities().instabuild)
		{
			ItemStack bowlStack = new ItemStack(Items.BOWL);
			if(!player.getInventory().add(bowlStack))
				player.drop(bowlStack, false);
		}
		
		return superStack;
	}
}
