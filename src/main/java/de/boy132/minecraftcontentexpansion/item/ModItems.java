package de.boy132.minecraftcontentexpansion.item;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.entity.rock.AndesiteRockEntity;
import de.boy132.minecraftcontentexpansion.entity.rock.DioriteRockEntity;
import de.boy132.minecraftcontentexpansion.entity.rock.GraniteRockEntity;
import de.boy132.minecraftcontentexpansion.entity.rock.RockEntity;
import de.boy132.minecraftcontentexpansion.fluid.ModFluids;
import de.boy132.minecraftcontentexpansion.item.armor.ModArmorMaterials;
import de.boy132.minecraftcontentexpansion.item.backpack.BackpackItem;
import de.boy132.minecraftcontentexpansion.item.backpack.EnderBackpackItem;
import de.boy132.minecraftcontentexpansion.item.bucket.ClayBucketItem;
import de.boy132.minecraftcontentexpansion.item.energy.DrillItem;
import de.boy132.minecraftcontentexpansion.item.energy.EnergyItem;
import de.boy132.minecraftcontentexpansion.item.entity.RockItem;
import de.boy132.minecraftcontentexpansion.item.tool.HammerItem;
import de.boy132.minecraftcontentexpansion.item.tool.KnifeItem;
import de.boy132.minecraftcontentexpansion.item.tool.ModTiers;
import de.boy132.minecraftcontentexpansion.item.tool.MultiDiggerItem;
import net.minecraft.world.item.*;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModItems
{
	public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MinecraftContentExpansion.MODID);
	
	// ============================================================ Items ============================================================ //
	public static final RegistryObject<Item> UNIM = ITEMS.register("unim", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> UNIM_INGOT = ITEMS.register("unim_ingot", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> UNIM_PLATE = ITEMS.register("unim_plate", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> RUBY = ITEMS.register("ruby", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> RUBY_PLATE = ITEMS.register("ruby_plate", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> ASCABIT_INGOT = ITEMS.register("ascabit_ingot", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> RAW_ASCABIT = ITEMS.register("raw_ascabit", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> ASCABIT_PLATE = ITEMS.register("ascabit_plate", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> TIN_INGOT = ITEMS.register("tin_ingot", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> RAW_TIN = ITEMS.register("raw_tin", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> TIN_PLATE = ITEMS.register("tin_plate", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> TITANIUM_INGOT = ITEMS.register("titanium_ingot", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> RAW_TITANIUM = ITEMS.register("raw_titanium", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> TITANIUM_PLATE = ITEMS.register("titanium_plate", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> ZINC_INGOT = ITEMS.register("zinc_ingot", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> RAW_ZINC = ITEMS.register("raw_zinc", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> ZINC_PLATE = ITEMS.register("zinc_plate", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> STEEL_FRAGMENTS = ITEMS.register("steel_fragments", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> STEEL_INGOT = ITEMS.register("steel_ingot", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> STEEL_PLATE = ITEMS.register("steel_plate", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> PULVERISED_COAL = ITEMS.register("pulverised_coal", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> BRONZE_INGOT = ITEMS.register("bronze_ingot", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> BRONZE_PLATE = ITEMS.register("bronze_plate", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> BRASS_INGOT = ITEMS.register("brass_ingot", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> IRON_PLATE = ITEMS.register("iron_plate", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> COPPER_PLATE = ITEMS.register("copper_plate", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> GOLD_PLATE = ITEMS.register("gold_plate", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> DIAMOND_PLATE = ITEMS.register("diamond_plate", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> SMALL_BACKPACK = ITEMS.register("small_backpack", () -> new BackpackItem(new Item.Properties().stacksTo(1), 2));
	public static final RegistryObject<Item> LARGE_BACKPACK = ITEMS.register("large_backpack", () -> new BackpackItem(new Item.Properties().stacksTo(1), 4));
	public static final RegistryObject<Item> ENDER_BACKPACK = ITEMS.register("ender_backpack", () -> new EnderBackpackItem(new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> PLANT_FIBER = ITEMS.register("plant_fiber", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> TREE_BARK = ITEMS.register("tree_bark", () -> new BarkItem(new Item.Properties()));
	public static final RegistryObject<Item> ANIMAL_HIDE = ITEMS.register("animal_hide", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> PREPARED_HIDE = ITEMS.register("prepared_hide", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> ANIMAL_FAT = ITEMS.register("animal_fat", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> DOUGH = ITEMS.register("dough", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> CHAIN = ITEMS.register("chain", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> LEATHER_STRIP = ITEMS.register("leather_strip", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> CEMENT_MIXTURE = ITEMS.register("cement_mixture", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> ROCK = ITEMS.register("rock", () -> new RockItem(new Item.Properties().stacksTo(32), (player) -> new RockEntity(player.level(), player)));
	public static final RegistryObject<Item> ANDESITE_ROCK = ITEMS.register("andesite_rock", () -> new RockItem(new Item.Properties().stacksTo(32), (player) -> new AndesiteRockEntity(player.level(), player)));
	public static final RegistryObject<Item> DIORITE_ROCK = ITEMS.register("diorite_rock", () -> new RockItem(new Item.Properties().stacksTo(32), (player) -> new DioriteRockEntity(player.level(), player)));
	public static final RegistryObject<Item> GRANITE_ROCK = ITEMS.register("granite_rock", () -> new RockItem(new Item.Properties().stacksTo(32), (player) -> new GraniteRockEntity(player.level(), player)));
	
	public static final RegistryObject<Item> RAW_BACON = ITEMS.register("raw_bacon", () -> new Item(new Item.Properties().food(ModFoods.RAW_BACON)));
	public static final RegistryObject<Item> COOKED_BACON = ITEMS.register("cooked_bacon", () -> new Item(new Item.Properties().food(ModFoods.COOKED_BACON)));
	public static final RegistryObject<Item> FRIED_EGG = ITEMS.register("fried_egg", () -> new Item(new Item.Properties().food(ModFoods.FRIED_EGG)));
	public static final RegistryObject<Item> CHEESE = ITEMS.register("cheese", () -> new Item(new Item.Properties().food(ModFoods.CHEESE)));
	public static final RegistryObject<Item> PIZZA = ITEMS.register("pizza", () -> new Item(new Item.Properties().food(ModFoods.PIZZA)));
	public static final RegistryObject<Item> CHOCOLATE = ITEMS.register("chocolate", () -> new Item(new Item.Properties().food(ModFoods.CHOCOLATE)));
	public static final RegistryObject<Item> POPCORN = ITEMS.register("popcorn", () -> new Item(new Item.Properties().food(ModFoods.POPCORN)));
	public static final RegistryObject<Item> PURIFIED_MEAT = ITEMS.register("purified_meat", () -> new Item(new Item.Properties().food(ModFoods.PURIFIED_MEAT)));
	public static final RegistryObject<Item> BEEF_JERKY = ITEMS.register("beef_jerky", () -> new Item(new Item.Properties().food(ModFoods.BEEF_JERKY)));
	public static final RegistryObject<Item> ONION_RINGS = ITEMS.register("onion_rings", () -> new Item(new Item.Properties().food(ModFoods.ONION_RINGS)));
	public static final RegistryObject<Item> SAUSAGE = ITEMS.register("sausage", () -> new Item(new Item.Properties().food(ModFoods.SAUSAGE)));
	public static final RegistryObject<Item> HOT_DOG = ITEMS.register("hot_dog", () -> new Item(new Item.Properties().food(ModFoods.HOT_DOG).stacksTo(16)));
	public static final RegistryObject<Item> GARDEN_SALAD = ITEMS.register("garden_salad", () -> new MultiBowlFoodItem(new Item.Properties().food(ModFoods.GARDEN_SALAD).stacksTo(16)));
	public static final RegistryObject<Item> HAMBURGER = ITEMS.register("hamburger", () -> new Item(new Item.Properties().food(ModFoods.HAMBURGER).stacksTo(16)));
	public static final RegistryObject<Item> CHEESEBURGER = ITEMS.register("cheeseburger", () -> new Item(new Item.Properties().food(ModFoods.CHEESEBURGER).stacksTo(16)));
	public static final RegistryObject<Item> SUSHI = ITEMS.register("sushi", () -> new Item(new Item.Properties().food(ModFoods.SUSHI)));
	
	public static final RegistryObject<Item> APPLE_PIE = ITEMS.register("apple_pie", () -> new Item(new Item.Properties().food(ModFoods.APPLE_PIE)));
	public static final RegistryObject<Item> STRAWBERRY_PIE = ITEMS.register("strawberry_pie", () -> new Item(new Item.Properties().food(ModFoods.STRAWBERRY_PIE)));
	
	public static final RegistryObject<Item> TOMATO_SEEDS = ITEMS.register("tomato_seeds", () -> new ItemNameBlockItem(ModBlocks.TOMATOES.get(), new Item.Properties()));
	public static final RegistryObject<Item> TOMATO = ITEMS.register("tomato", () -> new Item(new Item.Properties().food(ModFoods.TOMATO)));
	
	public static final RegistryObject<Item> GRAPE_SEEDS = ITEMS.register("grape_seeds", () -> new ItemNameBlockItem(ModBlocks.GRAPES.get(), new Item.Properties()));
	public static final RegistryObject<Item> GRAPE = ITEMS.register("grape", () -> new Item(new Item.Properties().food(ModFoods.GRAPE)));
	
	public static final RegistryObject<Item> STRAWBERRY_SEEDS = ITEMS.register("strawberry_seeds", () -> new ItemNameBlockItem(ModBlocks.STRAWBERRIES.get(), new Item.Properties()));
	public static final RegistryObject<Item> STRAWBERRY = ITEMS.register("strawberry", () -> new Item(new Item.Properties().food(ModFoods.STRAWBERRY)));
	
	public static final RegistryObject<Item> CORN_SEEDS = ITEMS.register("corn_seeds", () -> new ItemNameBlockItem(ModBlocks.CORNS.get(), new Item.Properties()));
	public static final RegistryObject<Item> CORN = ITEMS.register("corn", () -> new Item(new Item.Properties().food(ModFoods.CORN)));
	
	public static final RegistryObject<Item> LETTUCE_SEEDS = ITEMS.register("lettuce_seeds", () -> new ItemNameBlockItem(ModBlocks.LETTUCES.get(), new Item.Properties()));
	public static final RegistryObject<Item> LETTUCE = ITEMS.register("lettuce", () -> new Item(new Item.Properties().food(ModFoods.LETTUCE)));
	
	public static final RegistryObject<Item> ONION_SEEDS = ITEMS.register("onion_seeds", () -> new ItemNameBlockItem(ModBlocks.ONIONS.get(), new Item.Properties()));
	public static final RegistryObject<Item> ONION = ITEMS.register("onion", () -> new Item(new Item.Properties().food(ModFoods.ONION)));
	
	public static final RegistryObject<Item> RICE = ITEMS.register("rice", () -> new ItemNameBlockItem(ModBlocks.RICE.get(), new Item.Properties().food(ModFoods.RICE)));
	
	public static final RegistryObject<Item> VEGETABLE_SOUP = ITEMS.register("vegetable_soup", () -> new BowlFoodItem(new Item.Properties().stacksTo(1).food(ModFoods.VEGETABLE_SOUP)));
	public static final RegistryObject<Item> PUMPKIN_SOUP = ITEMS.register("pumpkin_soup", () -> new BowlFoodItem(new Item.Properties().stacksTo(1).food(ModFoods.PUMPKIN_SOUP)));
	public static final RegistryObject<Item> CACTUS_SOUP = ITEMS.register("cactus_soup", () -> new BowlFoodItem(new Item.Properties().stacksTo(1).food(ModFoods.CACTUS_SOUP)));
	public static final RegistryObject<Item> FISH_SOUP = ITEMS.register("fish_soup", () -> new BowlFoodItem(new Item.Properties().stacksTo(1).food(ModFoods.FISH_SOUP)));
	
	public static final RegistryObject<Item> UNFIRED_CLAY_BUCKET = ITEMS.register("unfired_clay_bucket", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> CLAY_BUCKET = ITEMS.register("clay_bucket", () -> new ClayBucketItem(Fluids.EMPTY, new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> WATER_CLAY_BUCKET = ITEMS.register("water_clay_bucket", () -> new ClayBucketItem(Fluids.WATER, new Item.Properties().craftRemainder(CLAY_BUCKET.get()).stacksTo(1)));
	
	public static final RegistryObject<Item> PORTABLE_BATTERY = ITEMS.register("portable_battery", () -> new EnergyItem(10000, 100, new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON)));
	
	public static final RegistryObject<Item> REDSTONE_BLEND = ITEMS.register("redstone_blend", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> SOLAR_CELL = ITEMS.register("solar_cell", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> STONE_DOOR = ITEMS.register("stone_door", () -> new DoubleHighBlockItem(ModBlocks.STONE_DOOR.get(), new Item.Properties()));
	public static final RegistryObject<Item> SANDSTONE_DOOR = ITEMS.register("sandstone_door", () -> new DoubleHighBlockItem(ModBlocks.SANDSTONE_DOOR.get(), new Item.Properties()));
	public static final RegistryObject<Item> GLASS_DOOR = ITEMS.register("glass_door", () -> new DoubleHighBlockItem(ModBlocks.GLASS_DOOR.get(), new Item.Properties()));
	
	public static final RegistryObject<Item> OIL_BUCKET = ITEMS.register("oil_bucket", () -> new BucketItem(ModFluids.OIL, new Item.Properties().stacksTo(1).craftRemainder(Items.BUCKET)));
	
	// ============================================================ TOOLS ============================================================ //
	public static final RegistryObject<Item> STEEL_DRILL = ITEMS.register("steel_drill", () -> new DrillItem(ModTiers.STEEL, 20000, 200, 60, new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON)));
	public static final RegistryObject<Item> UNIM_DRILL = ITEMS.register("unim_drill", () -> new DrillItem(ModTiers.UNIM, 30000, 300, 50, new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON)));
	public static final RegistryObject<Item> DIAMOND_DRILL = ITEMS.register("diamond_drill", () -> new DrillItem(Tiers.DIAMOND, 40000, 400, 40, new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON)));
	
	public static final RegistryObject<Item> FLINT_HATCHET = ITEMS.register("flint_hatchet", () -> new MultiDiggerItem(0f, -2.5f, ModTiers.FLINT, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> FLINT_SHEARS = ITEMS.register("flint_shears", () -> new ShearsItem(new Item.Properties().defaultDurability(104)));
	
	public static final RegistryObject<Item> FLINT_KNIFE = ITEMS.register("flint_knife", () -> new KnifeItem(ModTiers.FLINT, new Item.Properties().defaultDurability(ModTiers.FLINT.getUses() / 3)));
	public static final RegistryObject<Item> STONE_KNIFE = ITEMS.register("stone_knife", () -> new KnifeItem(Tiers.STONE, new Item.Properties().defaultDurability(Tiers.STONE.getUses() / 3)));
	public static final RegistryObject<Item> IRON_KNIFE = ITEMS.register("iron_knife", () -> new KnifeItem(Tiers.IRON, new Item.Properties().defaultDurability(Tiers.IRON.getUses() / 3)));
	public static final RegistryObject<Item> UNIM_KNIFE = ITEMS.register("unim_knife", () -> new KnifeItem(ModTiers.UNIM, new Item.Properties().defaultDurability(ModTiers.UNIM.getUses() / 3)));
	public static final RegistryObject<Item> DIAMOND_KNIFE = ITEMS.register("diamond_knife", () -> new KnifeItem(Tiers.DIAMOND, new Item.Properties().defaultDurability(Tiers.DIAMOND.getUses() / 3)));
	
	public static final RegistryObject<Item> IRON_HAMMER = ITEMS.register("iron_hammer", () -> new HammerItem(Tiers.IRON, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> BRONZE_HAMMER = ITEMS.register("bronze_hammer", () -> new HammerItem(ModTiers.BRONZE, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> STEEL_HAMMER = ITEMS.register("steel_hammer", () -> new HammerItem(ModTiers.STEEL, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> DIAMOND_HAMMER = ITEMS.register("diamond_hammer", () -> new HammerItem(Tiers.DIAMOND, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> UNIM_SWORD = ITEMS.register("unim_sword", () -> new SwordItem(ModTiers.UNIM, 3, -2.4F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> UNIM_PICKAXE = ITEMS.register("unim_pickaxe", () -> new PickaxeItem(ModTiers.UNIM, 1, -2.8F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> UNIM_AXE = ITEMS.register("unim_axe", () -> new AxeItem(ModTiers.UNIM, 6.0F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> UNIM_SHOVEL = ITEMS.register("unim_shovel", () -> new ShovelItem(ModTiers.UNIM, 1.5F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> UNIM_HOE = ITEMS.register("unim_hoe", () -> new HoeItem(ModTiers.UNIM, -2, 1.0F, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> RUBY_SWORD = ITEMS.register("ruby_sword", () -> new SwordItem(ModTiers.RUBY, 3, -2.4F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> RUBY_PICKAXE = ITEMS.register("ruby_pickaxe", () -> new PickaxeItem(ModTiers.RUBY, 1, -2.8F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> RUBY_AXE = ITEMS.register("ruby_axe", () -> new AxeItem(ModTiers.RUBY, 6.0F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> RUBY_SHOVEL = ITEMS.register("ruby_shovel", () -> new ShovelItem(ModTiers.RUBY, 1.5F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> RUBY_HOE = ITEMS.register("ruby_hoe", () -> new HoeItem(ModTiers.RUBY, -2, 1.0F, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> ASCABIT_SWORD = ITEMS.register("ascabit_sword", () -> new SwordItem(ModTiers.ASCABIT, 3, -2.4F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> ASCABIT_PICKAXE = ITEMS.register("ascabit_pickaxe", () -> new PickaxeItem(ModTiers.ASCABIT, 1, -2.8F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> ASCABIT_AXE = ITEMS.register("ascabit_axe", () -> new AxeItem(ModTiers.ASCABIT, 6.0F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> ASCABIT_SHOVEL = ITEMS.register("ascabit_shovel", () -> new ShovelItem(ModTiers.ASCABIT, 1.5F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> ASCABIT_HOE = ITEMS.register("ascabit_hoe", () -> new HoeItem(ModTiers.ASCABIT, -2, 1.0F, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> COPPER_SWORD = ITEMS.register("copper_sword", () -> new SwordItem(ModTiers.COPPER, 3, -2.4F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> COPPER_PICKAXE = ITEMS.register("copper_pickaxe", () -> new PickaxeItem(ModTiers.COPPER, 1, -2.8F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> COPPER_AXE = ITEMS.register("copper_axe", () -> new AxeItem(ModTiers.COPPER, 6.0F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> COPPER_SHOVEL = ITEMS.register("copper_shovel", () -> new ShovelItem(ModTiers.COPPER, 1.5F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> COPPER_HOE = ITEMS.register("copper_hoe", () -> new HoeItem(ModTiers.COPPER, -2, 1.0F, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> TIN_SWORD = ITEMS.register("tin_sword", () -> new SwordItem(ModTiers.TIN, 3, -2.4F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> TIN_PICKAXE = ITEMS.register("tin_pickaxe", () -> new PickaxeItem(ModTiers.TIN, 1, -2.8F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> TIN_AXE = ITEMS.register("tin_axe", () -> new AxeItem(ModTiers.TIN, 6.0F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> TIN_SHOVEL = ITEMS.register("tin_shovel", () -> new ShovelItem(ModTiers.TIN, 1.5F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> TIN_HOE = ITEMS.register("tin_hoe", () -> new HoeItem(ModTiers.TIN, -2, 1.0F, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> TITANIUM_SWORD = ITEMS.register("titanium_sword", () -> new SwordItem(ModTiers.TITANIUM, 3, -2.4F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> TITANIUM_PICKAXE = ITEMS.register("titanium_pickaxe", () -> new PickaxeItem(ModTiers.TITANIUM, 1, -2.8F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> TITANIUM_AXE = ITEMS.register("titanium_axe", () -> new AxeItem(ModTiers.TITANIUM, 6.0F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> TITANIUM_SHOVEL = ITEMS.register("titanium_shovel", () -> new ShovelItem(ModTiers.TITANIUM, 1.5F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> TITANIUM_HOE = ITEMS.register("titanium_hoe", () -> new HoeItem(ModTiers.TITANIUM, -2, 1.0F, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> ZINC_SWORD = ITEMS.register("zinc_sword", () -> new SwordItem(ModTiers.ZINC, 3, -2.4F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> ZINC_PICKAXE = ITEMS.register("zinc_pickaxe", () -> new PickaxeItem(ModTiers.ZINC, 1, -2.8F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> ZINC_AXE = ITEMS.register("zinc_axe", () -> new AxeItem(ModTiers.ZINC, 6.0F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> ZINC_SHOVEL = ITEMS.register("zinc_shovel", () -> new ShovelItem(ModTiers.ZINC, 1.5F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> ZINC_HOE = ITEMS.register("zinc_hoe", () -> new HoeItem(ModTiers.ZINC, -2, 1.0F, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> STEEL_SWORD = ITEMS.register("steel_sword", () -> new SwordItem(ModTiers.STEEL, 3, -2.4F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> STEEL_PICKAXE = ITEMS.register("steel_pickaxe", () -> new PickaxeItem(ModTiers.STEEL, 1, -2.8F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> STEEL_AXE = ITEMS.register("steel_axe", () -> new AxeItem(ModTiers.STEEL, 6.0F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> STEEL_SHOVEL = ITEMS.register("steel_shovel", () -> new ShovelItem(ModTiers.STEEL, 1.5F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> STEEL_HOE = ITEMS.register("steel_hoe", () -> new HoeItem(ModTiers.STEEL, -2, 1.0F, new Item.Properties().stacksTo(1)));
	
	public static final RegistryObject<Item> BRONZE_SWORD = ITEMS.register("bronze_sword", () -> new SwordItem(ModTiers.BRONZE, 3, -2.4F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> BRONZE_PICKAXE = ITEMS.register("bronze_pickaxe", () -> new PickaxeItem(ModTiers.BRONZE, 1, -2.8F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> BRONZE_AXE = ITEMS.register("bronze_axe", () -> new AxeItem(ModTiers.BRONZE, 6.0F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> BRONZE_SHOVEL = ITEMS.register("bronze_shovel", () -> new ShovelItem(ModTiers.BRONZE, 1.5F, -3.0F, new Item.Properties().stacksTo(1)));
	public static final RegistryObject<Item> BRONZE_HOE = ITEMS.register("bronze_hoe", () -> new HoeItem(ModTiers.BRONZE, -2, 1.0F, new Item.Properties().stacksTo(1)));
	
	// ============================================================ TOOL HEADS ============================================================ //
	public static final RegistryObject<Item> DRILL_BASE = ITEMS.register("drill_base", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> STEEL_DRILL_HEAD = ITEMS.register("steel_drill_head", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> UNIM_DRILL_HEAD = ITEMS.register("unim_drill_head", () -> new Item(new Item.Properties()));
	public static final RegistryObject<Item> DIAMOND_DRILL_HEAD = ITEMS.register("diamond_drill_head", () -> new Item(new Item.Properties()));
	
	public static final RegistryObject<Item> FLINT_HATCHET_HEAD = ITEMS.register("flint_hatchet_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> FLINT_KNIFE_BLADE = ITEMS.register("flint_knife_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STONE_KNIFE_BLADE = ITEMS.register("stone_knife_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> IRON_KNIFE_BLADE = ITEMS.register("iron_knife_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> UNIM_KNIFE_BLADE = ITEMS.register("unim_knife_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> DIAMOND_KNIFE_BLADE = ITEMS.register("diamond_knife_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> IRON_HAMMER_HEAD = ITEMS.register("iron_hammer_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> BRONZE_HAMMER_HEAD = ITEMS.register("bronze_hammer_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STEEL_HAMMER_HEAD = ITEMS.register("steel_hammer_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> DIAMOND_HAMMER_HEAD = ITEMS.register("diamond_hammer_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> STONE_SWORD_BLADE = ITEMS.register("stone_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STONE_PICKAXE_HEAD = ITEMS.register("stone_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STONE_AXE_HEAD = ITEMS.register("stone_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STONE_SHOVEL_HEAD = ITEMS.register("stone_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STONE_HOE_HEAD = ITEMS.register("stone_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> IRON_SWORD_BLADE = ITEMS.register("iron_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> IRON_PICKAXE_HEAD = ITEMS.register("iron_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> IRON_AXE_HEAD = ITEMS.register("iron_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> IRON_SHOVEL_HEAD = ITEMS.register("iron_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> IRON_HOE_HEAD = ITEMS.register("iron_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> GOLDEN_SWORD_BLADE = ITEMS.register("golden_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> GOLDEN_PICKAXE_HEAD = ITEMS.register("golden_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> GOLDEN_AXE_HEAD = ITEMS.register("golden_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> GOLDEN_SHOVEL_HEAD = ITEMS.register("golden_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> GOLDEN_HOE_HEAD = ITEMS.register("golden_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> UNIM_SWORD_BLADE = ITEMS.register("unim_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> UNIM_PICKAXE_HEAD = ITEMS.register("unim_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> UNIM_AXE_HEAD = ITEMS.register("unim_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> UNIM_SHOVEL_HEAD = ITEMS.register("unim_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> UNIM_HOE_HEAD = ITEMS.register("unim_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> RUBY_SWORD_BLADE = ITEMS.register("ruby_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> RUBY_PICKAXE_HEAD = ITEMS.register("ruby_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> RUBY_AXE_HEAD = ITEMS.register("ruby_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> RUBY_SHOVEL_HEAD = ITEMS.register("ruby_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> RUBY_HOE_HEAD = ITEMS.register("ruby_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> ASCABIT_SWORD_BLADE = ITEMS.register("ascabit_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> ASCABIT_PICKAXE_HEAD = ITEMS.register("ascabit_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> ASCABIT_AXE_HEAD = ITEMS.register("ascabit_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> ASCABIT_SHOVEL_HEAD = ITEMS.register("ascabit_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> ASCABIT_HOE_HEAD = ITEMS.register("ascabit_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> COPPER_SWORD_BLADE = ITEMS.register("copper_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> COPPER_PICKAXE_HEAD = ITEMS.register("copper_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> COPPER_AXE_HEAD = ITEMS.register("copper_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> COPPER_SHOVEL_HEAD = ITEMS.register("copper_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> COPPER_HOE_HEAD = ITEMS.register("copper_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> TIN_SWORD_BLADE = ITEMS.register("tin_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> TIN_PICKAXE_HEAD = ITEMS.register("tin_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> TIN_AXE_HEAD = ITEMS.register("tin_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> TIN_SHOVEL_HEAD = ITEMS.register("tin_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> TIN_HOE_HEAD = ITEMS.register("tin_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> TITANIUM_SWORD_BLADE = ITEMS.register("titanium_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> TITANIUM_PICKAXE_HEAD = ITEMS.register("titanium_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> TITANIUM_AXE_HEAD = ITEMS.register("titanium_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> TITANIUM_SHOVEL_HEAD = ITEMS.register("titanium_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> TITANIUM_HOE_HEAD = ITEMS.register("titanium_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> ZINC_SWORD_BLADE = ITEMS.register("zinc_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> ZINC_PICKAXE_HEAD = ITEMS.register("zinc_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> ZINC_AXE_HEAD = ITEMS.register("zinc_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> ZINC_SHOVEL_HEAD = ITEMS.register("zinc_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> ZINC_HOE_HEAD = ITEMS.register("zinc_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> STEEL_SWORD_BLADE = ITEMS.register("steel_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STEEL_PICKAXE_HEAD = ITEMS.register("steel_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STEEL_AXE_HEAD = ITEMS.register("steel_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STEEL_SHOVEL_HEAD = ITEMS.register("steel_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> STEEL_HOE_HEAD = ITEMS.register("steel_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	public static final RegistryObject<Item> BRONZE_SWORD_BLADE = ITEMS.register("bronze_sword_blade", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> BRONZE_PICKAXE_HEAD = ITEMS.register("bronze_pickaxe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> BRONZE_AXE_HEAD = ITEMS.register("bronze_axe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> BRONZE_SHOVEL_HEAD = ITEMS.register("bronze_shovel_head", () -> new Item(new Item.Properties().stacksTo(16)));
	public static final RegistryObject<Item> BRONZE_HOE_HEAD = ITEMS.register("bronze_hoe_head", () -> new Item(new Item.Properties().stacksTo(16)));
	
	// ============================================================ ARMOR ============================================================ //
	public static final RegistryObject<Item> UNIM_HELMET = ITEMS.register("unim_helmet", () -> new ArmorItem(ModArmorMaterials.UNIM, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final RegistryObject<Item> UNIM_CHESTPLATE = ITEMS.register("unim_chestplate", () -> new ArmorItem(ModArmorMaterials.UNIM, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final RegistryObject<Item> UNIM_LEGGINGS = ITEMS.register("unim_leggings", () -> new ArmorItem(ModArmorMaterials.UNIM, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final RegistryObject<Item> UNIM_BOOTS = ITEMS.register("unim_boots", () -> new ArmorItem(ModArmorMaterials.UNIM, ArmorItem.Type.BOOTS, new Item.Properties()));
	
	public static final RegistryObject<Item> RUBY_HELMET = ITEMS.register("ruby_helmet", () -> new ArmorItem(ModArmorMaterials.RUBY, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final RegistryObject<Item> RUBY_CHESTPLATE = ITEMS.register("ruby_chestplate", () -> new ArmorItem(ModArmorMaterials.RUBY, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final RegistryObject<Item> RUBY_LEGGINGS = ITEMS.register("ruby_leggings", () -> new ArmorItem(ModArmorMaterials.RUBY, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final RegistryObject<Item> RUBY_BOOTS = ITEMS.register("ruby_boots", () -> new ArmorItem(ModArmorMaterials.RUBY, ArmorItem.Type.BOOTS, new Item.Properties()));
	
	public static final RegistryObject<Item> ASCABIT_HELMET = ITEMS.register("ascabit_helmet", () -> new ArmorItem(ModArmorMaterials.ASCABIT, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final RegistryObject<Item> ASCABIT_CHESTPLATE = ITEMS.register("ascabit_chestplate", () -> new ArmorItem(ModArmorMaterials.ASCABIT, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final RegistryObject<Item> ASCABIT_LEGGINGS = ITEMS.register("ascabit_leggings", () -> new ArmorItem(ModArmorMaterials.ASCABIT, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final RegistryObject<Item> ASCABIT_BOOTS = ITEMS.register("ascabit_boots", () -> new ArmorItem(ModArmorMaterials.ASCABIT, ArmorItem.Type.BOOTS, new Item.Properties()));
	
	public static final RegistryObject<Item> COPPER_HELMET = ITEMS.register("copper_helmet", () -> new ArmorItem(ModArmorMaterials.COPPER, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final RegistryObject<Item> COPPER_CHESTPLATE = ITEMS.register("copper_chestplate", () -> new ArmorItem(ModArmorMaterials.COPPER, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final RegistryObject<Item> COPPER_LEGGINGS = ITEMS.register("copper_leggings", () -> new ArmorItem(ModArmorMaterials.COPPER, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final RegistryObject<Item> COPPER_BOOTS = ITEMS.register("copper_boots", () -> new ArmorItem(ModArmorMaterials.COPPER, ArmorItem.Type.BOOTS, new Item.Properties()));
	
	public static final RegistryObject<Item> TIN_HELMET = ITEMS.register("tin_helmet", () -> new ArmorItem(ModArmorMaterials.TIN, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final RegistryObject<Item> TIN_CHESTPLATE = ITEMS.register("tin_chestplate", () -> new ArmorItem(ModArmorMaterials.TIN, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final RegistryObject<Item> TIN_LEGGINGS = ITEMS.register("tin_leggings", () -> new ArmorItem(ModArmorMaterials.TIN, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final RegistryObject<Item> TIN_BOOTS = ITEMS.register("tin_boots", () -> new ArmorItem(ModArmorMaterials.TIN, ArmorItem.Type.BOOTS, new Item.Properties()));
	
	public static final RegistryObject<Item> TITANIUM_HELMET = ITEMS.register("titanium_helmet", () -> new ArmorItem(ModArmorMaterials.TITANIUM, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final RegistryObject<Item> TITANIUM_CHESTPLATE = ITEMS.register("titanium_chestplate", () -> new ArmorItem(ModArmorMaterials.TITANIUM, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final RegistryObject<Item> TITANIUM_LEGGINGS = ITEMS.register("titanium_leggings", () -> new ArmorItem(ModArmorMaterials.TITANIUM, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final RegistryObject<Item> TITANIUM_BOOTS = ITEMS.register("titanium_boots", () -> new ArmorItem(ModArmorMaterials.TITANIUM, ArmorItem.Type.BOOTS, new Item.Properties()));
	
	public static final RegistryObject<Item> ZINC_HELMET = ITEMS.register("zinc_helmet", () -> new ArmorItem(ModArmorMaterials.ZINC, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final RegistryObject<Item> ZINC_CHESTPLATE = ITEMS.register("zinc_chestplate", () -> new ArmorItem(ModArmorMaterials.ZINC, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final RegistryObject<Item> ZINC_LEGGINGS = ITEMS.register("zinc_leggings", () -> new ArmorItem(ModArmorMaterials.ZINC, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final RegistryObject<Item> ZINC_BOOTS = ITEMS.register("zinc_boots", () -> new ArmorItem(ModArmorMaterials.ZINC, ArmorItem.Type.BOOTS, new Item.Properties()));
	
	public static final RegistryObject<Item> STEEL_HELMET = ITEMS.register("steel_helmet", () -> new ArmorItem(ModArmorMaterials.STEEL, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final RegistryObject<Item> STEEL_CHESTPLATE = ITEMS.register("steel_chestplate", () -> new ArmorItem(ModArmorMaterials.STEEL, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final RegistryObject<Item> STEEL_LEGGINGS = ITEMS.register("steel_leggings", () -> new ArmorItem(ModArmorMaterials.STEEL, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final RegistryObject<Item> STEEL_BOOTS = ITEMS.register("steel_boots", () -> new ArmorItem(ModArmorMaterials.STEEL, ArmorItem.Type.BOOTS, new Item.Properties()));
	
	public static final RegistryObject<Item> BRONZE_HELMET = ITEMS.register("bronze_helmet", () -> new ArmorItem(ModArmorMaterials.BRONZE, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final RegistryObject<Item> BRONZE_CHESTPLATE = ITEMS.register("bronze_chestplate", () -> new ArmorItem(ModArmorMaterials.BRONZE, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final RegistryObject<Item> BRONZE_LEGGINGS = ITEMS.register("bronze_leggings", () -> new ArmorItem(ModArmorMaterials.BRONZE, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final RegistryObject<Item> BRONZE_BOOTS = ITEMS.register("bronze_boots", () -> new ArmorItem(ModArmorMaterials.BRONZE, ArmorItem.Type.BOOTS, new Item.Properties()));
	
	public static void register(IEventBus eventBus)
	{
		ITEMS.register(eventBus);
	}
}
