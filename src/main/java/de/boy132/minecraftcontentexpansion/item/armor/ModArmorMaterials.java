package de.boy132.minecraftcontentexpansion.item.armor;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.Util;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.LazyLoadedValue;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;

import java.util.EnumMap;
import java.util.function.Supplier;

public enum ModArmorMaterials implements ArmorMaterial
{
	UNIM(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "unim_armor"), 18, Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 2);
		map.put(ArmorItem.Type.LEGGINGS, 6);
		map.put(ArmorItem.Type.CHESTPLATE, 7);
		map.put(ArmorItem.Type.HELMET, 3);
	}), 9, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.of(ModItems.UNIM_INGOT.get())),
	RUBY(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "ruby_armor"), 16, Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 2);
		map.put(ArmorItem.Type.LEGGINGS, 5);
		map.put(ArmorItem.Type.CHESTPLATE, 6);
		map.put(ArmorItem.Type.HELMET, 2);
	}), 9, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.of(ModItems.RUBY.get())),
	ASCABIT(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "ascabit_armor"), 16, Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 2);
		map.put(ArmorItem.Type.LEGGINGS, 5);
		map.put(ArmorItem.Type.CHESTPLATE, 6);
		map.put(ArmorItem.Type.HELMET, 2);
	}), 9, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.of(ModItems.ASCABIT_INGOT.get())),
	COPPER(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "copper_armor"), 8, Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 1);
		map.put(ArmorItem.Type.LEGGINGS, 4);
		map.put(ArmorItem.Type.CHESTPLATE, 5);
		map.put(ArmorItem.Type.HELMET, 2);
	}), 7, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.of(Items.COPPER_INGOT)),
	TIN(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "tin_armor"), 8, Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 1);
		map.put(ArmorItem.Type.LEGGINGS, 4);
		map.put(ArmorItem.Type.CHESTPLATE, 5);
		map.put(ArmorItem.Type.HELMET, 2);
	}), 7, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.of(ModItems.TIN_INGOT.get())),
	TITANIUM(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "titanium_armor"), 35, Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 2);
		map.put(ArmorItem.Type.LEGGINGS, 5);
		map.put(ArmorItem.Type.CHESTPLATE, 6);
		map.put(ArmorItem.Type.HELMET, 2);
	}), 10, SoundEvents.ARMOR_EQUIP_IRON, 1.0F, 0.1F, () -> Ingredient.of(ModItems.TIN_INGOT.get())),
	ZINC(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "zinc_armor"), 9, Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 1);
		map.put(ArmorItem.Type.LEGGINGS, 4);
		map.put(ArmorItem.Type.CHESTPLATE, 5);
		map.put(ArmorItem.Type.HELMET, 2);
	}), 7, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.of(ModItems.ZINC_INGOT.get())),
	BRONZE(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "bronze_armor"), 14, Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 2);
		map.put(ArmorItem.Type.LEGGINGS, 4);
		map.put(ArmorItem.Type.CHESTPLATE, 5);
		map.put(ArmorItem.Type.HELMET, 2);
	}), 8, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.of(ModItems.BRONZE_INGOT.get())),
	STEEL(ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "steel_armor"), 24, Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 2);
		map.put(ArmorItem.Type.LEGGINGS, 5);
		map.put(ArmorItem.Type.CHESTPLATE, 6);
		map.put(ArmorItem.Type.HELMET, 2);
	}), 8, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.of(ModItems.STEEL_INGOT.get()));
	
	private static final EnumMap<ArmorItem.Type, Integer> HEALTH_FUNCTION_FOR_TYPE = Util.make(new EnumMap<>(ArmorItem.Type.class), (map) ->
	{
		map.put(ArmorItem.Type.BOOTS, 13);
		map.put(ArmorItem.Type.LEGGINGS, 15);
		map.put(ArmorItem.Type.CHESTPLATE, 16);
		map.put(ArmorItem.Type.HELMET, 11);
	});
	private final ResourceLocation name;
	private final int durabilityMultiplier;
	private final EnumMap<ArmorItem.Type, Integer> protectionFunctionForType;
	private final int enchantmentValue;
	private final SoundEvent sound;
	private final float toughness;
	private final float knockbackResistance;
	private final LazyLoadedValue<Ingredient> repairIngredient;
	
	ModArmorMaterials(ResourceLocation name, int durabilityMultiplier, EnumMap<ArmorItem.Type, Integer> protectionFunctionForType, int enchantmentValue, SoundEvent sound, float toughness, float knockbackResistance, Supplier<Ingredient> repairIngredient)
	{
		this.name = name;
		this.durabilityMultiplier = durabilityMultiplier;
		this.protectionFunctionForType = protectionFunctionForType;
		this.enchantmentValue = enchantmentValue;
		this.sound = sound;
		this.toughness = toughness;
		this.knockbackResistance = knockbackResistance;
		this.repairIngredient = new LazyLoadedValue<>(repairIngredient);
	}
	
	public int getDurabilityForType(ArmorItem.Type type)
	{
		return HEALTH_FUNCTION_FOR_TYPE.get(type) * durabilityMultiplier;
	}
	
	public int getDefenseForType(ArmorItem.Type type)
	{
		return protectionFunctionForType.get(type);
	}
	
	public int getEnchantmentValue()
	{
		return enchantmentValue;
	}
	
	public SoundEvent getEquipSound()
	{
		return sound;
	}
	
	public Ingredient getRepairIngredient()
	{
		return repairIngredient.get();
	}
	
	public float getToughness()
	{
		return toughness;
	}
	
	public float getKnockbackResistance()
	{
		return knockbackResistance;
	}
	
	public String getName()
	{
		return name.toString();
	}
}
