package de.boy132.minecraftcontentexpansion.item.tool;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.Tiers;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.ForgeTier;
import net.minecraftforge.common.TierSortingRegistry;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.List;

public class ModTiers
{
	public static final TagKey<Block> NEEDS_FLINT_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_flint_tool"));
	public static final Tier FLINT = TierSortingRegistry.registerTier(new ForgeTier(0, 110, 1.5F, 0.0F, 4, NEEDS_FLINT_TOOL, () -> Ingredient.of(Items.FLINT)), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "flint"), List.of(Tiers.WOOD), List.of(Tiers.STONE));
	
	public static final TagKey<Block> NEEDS_UNIM_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_unim_tool"));
	public static final Tier UNIM = TierSortingRegistry.registerTier(new ForgeTier(2, 550, 7.0F, 2.0F, 14, NEEDS_UNIM_TOOL, () -> Ingredient.of(ModItems.UNIM_INGOT.get())), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "unim"), List.of(Tiers.IRON), List.of(Tiers.DIAMOND));
	
	public static final TagKey<Block> NEEDS_RUBY_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_ruby_tool"));
	public static final Tier RUBY = TierSortingRegistry.registerTier(new ForgeTier(2, 420, 7.0F, 2.0F, 14, NEEDS_RUBY_TOOL, () -> Ingredient.of(ModItems.RUBY.get())), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "ruby"), List.of(Tiers.IRON), List.of(UNIM));
	
	public static final TagKey<Block> NEEDS_ASCABIT_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_ascabit_tool"));
	public static final Tier ASCABIT = TierSortingRegistry.registerTier(new ForgeTier(2, 320, 6.0F, 2.0F, 13, NEEDS_ASCABIT_TOOL, () -> Ingredient.of(ModItems.ASCABIT_INGOT.get())), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "ascabit"), List.of(Tiers.IRON), List.of(UNIM));
	
	public static final TagKey<Block> NEEDS_COPPER_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_copper_tool"));
	public static final Tier COPPER = TierSortingRegistry.registerTier(new ForgeTier(2, 231, 5.0F, 1.0F, 7, NEEDS_COPPER_TOOL, () -> Ingredient.of(Items.COPPER_INGOT)), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "copper"), List.of(Tiers.STONE), List.of(Tiers.IRON));
	
	public static final TagKey<Block> NEEDS_TIN_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_tin_tool"));
	public static final Tier TIN = TierSortingRegistry.registerTier(new ForgeTier(2, 231, 5.0F, 1.0F, 7, NEEDS_TIN_TOOL, () -> Ingredient.of(ModItems.TIN_INGOT.get())), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "tin"), List.of(Tiers.STONE), List.of(Tiers.IRON));
	
	public static final TagKey<Block> NEEDS_TITANIUM_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_titanium_tool"));
	public static final Tier TITANIUM = TierSortingRegistry.registerTier(new ForgeTier(3, 830, 7.0F, 3.0F, 15, NEEDS_TITANIUM_TOOL, () -> Ingredient.of(ModItems.TITANIUM_INGOT.get())), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "titanium"), List.of(Tiers.DIAMOND), List.of(Tiers.NETHERITE));
	
	public static final TagKey<Block> NEEDS_ZINC_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_zinc_tool"));
	public static final Tier ZINC = TierSortingRegistry.registerTier(new ForgeTier(2, 265, 5.0F, 1.0F, 8, NEEDS_ZINC_TOOL, () -> Ingredient.of(ModItems.ZINC_INGOT.get())), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "zinc"), List.of(Tiers.STONE), List.of(Tiers.IRON));
	
	public static final TagKey<Block> NEEDS_STEEL_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_steel_tool"));
	public static final Tier STEEL = TierSortingRegistry.registerTier(new ForgeTier(2, 700, 6.0F, 3.0F, 10, NEEDS_STEEL_TOOL, () -> Ingredient.of(ModItems.STEEL_INGOT.get())), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "steel"), List.of(Tiers.IRON), List.of(Tiers.DIAMOND));
	
	public static final TagKey<Block> NEEDS_BRONZE_TOOL = TagKey.create(ForgeRegistries.BLOCKS.getRegistryKey(), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "needs_bronze_tool"));
	public static final Tier BRONZE = TierSortingRegistry.registerTier(new ForgeTier(2, 520, 5.0F, 2.0F, 12, NEEDS_BRONZE_TOOL, () -> Ingredient.of(ModItems.BRONZE_INGOT.get())), ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, "bronze"), List.of(COPPER, TIN), List.of(Tiers.IRON));
}

