package de.boy132.minecraftcontentexpansion.item.tool;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.TieredItem;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class HammerItem extends TieredItem
{
	private final Multimap<Attribute, AttributeModifier> defaultModifiers;
	
	public HammerItem(Tier tier, Properties properties)
	{
		super(tier, properties);
		
		float attackDamage = 8 + tier.getAttackDamageBonus();
		ImmutableMultimap.Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		builder.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon modifier", attackDamage, AttributeModifier.Operation.ADDITION));
		builder.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", -3.3f, AttributeModifier.Operation.ADDITION));
		defaultModifiers = builder.build();
	}
	
	@Override
	public boolean hasCraftingRemainingItem(ItemStack itemStack)
	{
		return true;
	}
	
	@Override
	public ItemStack getCraftingRemainingItem(ItemStack itemStack)
	{
		ItemStack stack = itemStack.copy();
		
		if(stack.hurt(1, RandomSource.create(), null))
		{
			stack.shrink(1);
			stack.setDamageValue(0);
		}
		
		return stack;
	}
	
	@Override
	public boolean canAttackBlock(BlockState state, Level level, BlockPos blockPos, Player player)
	{
		return !player.isCreative();
	}
	
	@Override
	public boolean hurtEnemy(ItemStack stack, LivingEntity target, LivingEntity attacker)
	{
		stack.hurtAndBreak(2, attacker, (entity) -> entity.broadcastBreakEvent(EquipmentSlot.MAINHAND));
		return true;
	}
	
	@Override
	public boolean mineBlock(ItemStack stack, Level level, BlockState state, BlockPos pos, LivingEntity miner)
	{
		if(!level.isClientSide && state.getDestroySpeed(level, pos) != 0.0F)
			stack.hurtAndBreak(2, miner, (entity) -> entity.broadcastBreakEvent(EquipmentSlot.MAINHAND));
		
		return true;
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot slot)
	{
		return slot == EquipmentSlot.MAINHAND ? defaultModifiers : super.getDefaultAttributeModifiers(slot);
	}
}
