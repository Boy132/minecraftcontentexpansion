package de.boy132.minecraftcontentexpansion.item;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.item.energy.EnergyItem;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

@Mod.EventBusSubscriber(modid = MinecraftContentExpansion.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModCreativeModeTabs
{
	private static final DeferredRegister<CreativeModeTab> CREATIVE_MOD_TABS = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<CreativeModeTab> ITEMS = CREATIVE_MOD_TABS.register("items", () -> CreativeModeTab.builder().title(Component.translatable("item_group." + MinecraftContentExpansion.MODID + ".items")).icon(() -> new ItemStack(ModItems.UNIM.get())).withTabsBefore(CreativeModeTabs.SPAWN_EGGS).build());
	public static final RegistryObject<CreativeModeTab> BLOCKS = CREATIVE_MOD_TABS.register("blocks", () -> CreativeModeTab.builder().title(Component.translatable("item_group." + MinecraftContentExpansion.MODID + ".blocks")).icon(() -> new ItemStack(ModBlocks.UNIM_ORE.get())).withTabsBefore(ITEMS.getId()).build());
	public static final RegistryObject<CreativeModeTab> ARMOR = CREATIVE_MOD_TABS.register("armor", () -> CreativeModeTab.builder().title(Component.translatable("item_group." + MinecraftContentExpansion.MODID + ".armor")).icon(() -> new ItemStack(ModItems.UNIM_CHESTPLATE.get())).withTabsBefore(BLOCKS.getId()).build());
	public static final RegistryObject<CreativeModeTab> TOOLS = CREATIVE_MOD_TABS.register("tools", () -> CreativeModeTab.builder().title(Component.translatable("item_group." + MinecraftContentExpansion.MODID + ".tools")).icon(() -> new ItemStack(ModItems.UNIM_PICKAXE.get())).withTabsBefore(ARMOR.getId()).build());
	public static final RegistryObject<CreativeModeTab> TOOL_HEADS = CREATIVE_MOD_TABS.register("tool_heads", () -> CreativeModeTab.builder().title(Component.translatable("item_group." + MinecraftContentExpansion.MODID + ".tool_heads")).icon(() -> new ItemStack(ModItems.UNIM_PICKAXE_HEAD.get())).withTabsBefore(TOOLS.getId()).build());
	public static final RegistryObject<CreativeModeTab> FOOD = CREATIVE_MOD_TABS.register("food", () -> CreativeModeTab.builder().title(Component.translatable("item_group." + MinecraftContentExpansion.MODID + ".food")).icon(() -> new ItemStack(ModItems.TOMATO.get())).withTabsBefore(TOOL_HEADS.getId()).build());
	
	public static void register(IEventBus eventBus)
	{
		CREATIVE_MOD_TABS.register(eventBus);
	}
	
	@SubscribeEvent
	public static void buildContents(BuildCreativeModeTabContentsEvent event)
	{
		if(event.getTabKey() == ITEMS.getKey())
		{
			event.accept(ModItems.UNIM.get());
			event.accept(ModItems.UNIM_INGOT.get());
			event.accept(ModItems.UNIM_PLATE.get());
			
			event.accept(ModItems.RUBY.get());
			event.accept(ModItems.RUBY_PLATE.get());
			
			event.accept(ModItems.RAW_ASCABIT.get());
			event.accept(ModItems.ASCABIT_INGOT.get());
			event.accept(ModItems.ASCABIT_PLATE.get());
			
			event.accept(ModItems.RAW_TIN.get());
			event.accept(ModItems.TIN_INGOT.get());
			event.accept(ModItems.TIN_PLATE.get());
			
			event.accept(ModItems.RAW_TITANIUM.get());
			event.accept(ModItems.TITANIUM_INGOT.get());
			event.accept(ModItems.TITANIUM_PLATE.get());
			
			event.accept(ModItems.RAW_ZINC.get());
			event.accept(ModItems.ZINC_INGOT.get());
			event.accept(ModItems.ZINC_PLATE.get());
			
			event.accept(ModItems.STEEL_FRAGMENTS.get());
			event.accept(ModItems.STEEL_INGOT.get());
			event.accept(ModItems.STEEL_PLATE.get());
			
			event.accept(ModItems.PULVERISED_COAL.get());
			
			event.accept(ModItems.BRONZE_INGOT.get());
			event.accept(ModItems.BRONZE_PLATE.get());
			
			event.accept(ModItems.BRASS_INGOT.get());
			
			event.accept(ModItems.IRON_PLATE.get());
			event.accept(ModItems.COPPER_PLATE.get());
			event.accept(ModItems.GOLD_PLATE.get());
			event.accept(ModItems.DIAMOND_PLATE.get());
			
			event.accept(ModItems.SMALL_BACKPACK.get());
			event.accept(ModItems.LARGE_BACKPACK.get());
			event.accept(ModItems.ENDER_BACKPACK.get());
			
			event.accept(ModItems.PLANT_FIBER.get());
			event.accept(ModItems.TREE_BARK.get());
			event.accept(ModItems.ANIMAL_HIDE.get());
			event.accept(ModItems.PREPARED_HIDE.get());
			event.accept(ModItems.ANIMAL_FAT.get());
			event.accept(ModItems.DOUGH.get());
			event.accept(ModItems.CHAIN.get());
			event.accept(ModItems.LEATHER_STRIP.get());
			event.accept(ModItems.CEMENT_MIXTURE.get());
			
			event.accept(ModItems.ROCK.get());
			event.accept(ModItems.ANDESITE_ROCK.get());
			event.accept(ModItems.DIORITE_ROCK.get());
			event.accept(ModItems.GRANITE_ROCK.get());
			
			event.accept(ModItems.UNFIRED_CLAY_BUCKET.get());
			event.accept(ModItems.CLAY_BUCKET.get());
			event.accept(ModItems.WATER_CLAY_BUCKET.get());
			
			event.accept(ModItems.PORTABLE_BATTERY.get());
			event.accept(((EnergyItem) ModItems.PORTABLE_BATTERY.get()).getCreativeTabStack());
			
			event.accept(ModBlocks.COPPER_CABLE.get());
			
			event.accept(ModItems.REDSTONE_BLEND.get());
			event.accept(ModItems.SOLAR_CELL.get());
			
			event.accept(ModItems.OIL_BUCKET.get());
		}
		
		if(event.getTabKey() == BLOCKS.getKey())
		{
			event.accept(ModBlocks.UNIM_ORE.get());
			event.accept(ModBlocks.DEEPSLATE_UNIM_ORE.get());
			event.accept(ModBlocks.UNIM_BLOCK.get());
			
			event.accept(ModBlocks.RUBY_ORE.get());
			event.accept(ModBlocks.DEEPSLATE_RUBY_ORE.get());
			event.accept(ModBlocks.RUBY_BLOCK.get());
			
			event.accept(ModBlocks.ASCABIT_ORE.get());
			event.accept(ModBlocks.DEEPSLATE_ASCABIT_ORE.get());
			event.accept(ModBlocks.ASCABIT_BLOCK.get());
			event.accept(ModBlocks.RAW_ASCABIT_BLOCK.get());
			
			event.accept(ModBlocks.TIN_ORE.get());
			event.accept(ModBlocks.DEEPSLATE_TIN_ORE.get());
			event.accept(ModBlocks.TIN_BLOCK.get());
			event.accept(ModBlocks.RAW_TIN_BLOCK.get());
			
			event.accept(ModBlocks.TITANIUM_ORE.get());
			event.accept(ModBlocks.DEEPSLATE_TITANIUM_ORE.get());
			event.accept(ModBlocks.TITANIUM_BLOCK.get());
			event.accept(ModBlocks.RAW_TITANIUM_BLOCK.get());
			
			event.accept(ModBlocks.ZINC_ORE.get());
			event.accept(ModBlocks.DEEPSLATE_ZINC_ORE.get());
			event.accept(ModBlocks.ZINC_BLOCK.get());
			event.accept(ModBlocks.RAW_ZINC_BLOCK.get());
			
			event.accept(ModBlocks.STEEL_BLOCK.get());
			
			event.accept(ModBlocks.BRONZE_BLOCK.get());
			
			event.accept(ModBlocks.BRASS_BLOCK.get());
			
			event.accept(ModBlocks.CEMENT.get());
			event.accept(ModBlocks.CEMENT_STAIRS.get());
			event.accept(ModBlocks.CEMENT_SLAB.get());
			event.accept(ModBlocks.CEMENT_WALL.get());
			
			event.accept(ModBlocks.CEMENT_PLATE.get());
			event.accept(ModBlocks.CEMENT_PLATE_STAIRS.get());
			event.accept(ModBlocks.CEMENT_PLATE_SLAB.get());
			event.accept(ModBlocks.CEMENT_PLATE_WALL.get());
			
			event.accept(ModBlocks.LIMESTONE.get());
			event.accept(ModBlocks.LIMESTONE_STAIRS.get());
			event.accept(ModBlocks.LIMESTONE_SLAB.get());
			event.accept(ModBlocks.LIMESTONE_WALL.get());
			
			event.accept(ModBlocks.LIMESTONE_BRICKS.get());
			event.accept(ModBlocks.LIMESTONE_BRICK_STAIRS.get());
			event.accept(ModBlocks.LIMESTONE_BRICK_SLAB.get());
			event.accept(ModBlocks.LIMESTONE_BRICK_WALL.get());
			
			event.accept(ModBlocks.MARBLE.get());
			event.accept(ModBlocks.MARBLE_STAIRS.get());
			event.accept(ModBlocks.MARBLE_SLAB.get());
			event.accept(ModBlocks.MARBLE_WALL.get());
			
			event.accept(ModBlocks.MARBLE_COBBLE.get());
			event.accept(ModBlocks.MARBLE_COBBLE_STAIRS.get());
			event.accept(ModBlocks.MARBLE_COBBLE_SLAB.get());
			event.accept(ModBlocks.MARBLE_COBBLE_WALL.get());
			
			event.accept(ModBlocks.MARBLE_BRICKS.get());
			event.accept(ModBlocks.MARBLE_BRICK_STAIRS.get());
			event.accept(ModBlocks.MARBLE_BRICK_SLAB.get());
			event.accept(ModBlocks.MARBLE_BRICK_WALL.get());
			
			event.accept(ModBlocks.MARBLE_LAMP.get());
			
			event.accept(ModBlocks.DIRT_SLAB.get());
			event.accept(ModBlocks.COARSE_DIRT_SLAB.get());
			event.accept(ModBlocks.DIRT_PATH_SLAB.get());
			event.accept(ModBlocks.GRASS_BLOCK_SLAB.get());
			
			event.accept(ModBlocks.GLASS_SLAB.get());
			
			event.accept(ModItems.STONE_DOOR.get());
			event.accept(ModBlocks.STONE_TRAPDOOR.get());
			
			event.accept(ModItems.SANDSTONE_DOOR.get());
			event.accept(ModBlocks.SANDSTONE_TRAPDOOR.get());
			
			event.accept(ModItems.GLASS_DOOR.get());
			event.accept(ModBlocks.GLASS_TRAPDOOR.get());
			
			event.accept(ModBlocks.WOODEN_GLASS_FRAME.get());
			
			event.accept(ModBlocks.KILN.get());
			event.accept(ModBlocks.SMELTER.get());
			event.accept(ModBlocks.MILLSTONE.get());
			
			event.accept(ModBlocks.OAK_CHOPPING_BLOCK.get());
			event.accept(ModBlocks.SPRUCE_CHOPPING_BLOCK.get());
			event.accept(ModBlocks.BIRCH_CHOPPING_BLOCK.get());
			event.accept(ModBlocks.JUNGLE_CHOPPING_BLOCK.get());
			event.accept(ModBlocks.ACACIA_CHOPPING_BLOCK.get());
			event.accept(ModBlocks.DARK_OAK_CHOPPING_BLOCK.get());
			event.accept(ModBlocks.MANGROVE_CHOPPING_BLOCK.get());
			event.accept(ModBlocks.CHERRY_CHOPPING_BLOCK.get());
			event.accept(ModBlocks.CRIMSON_CHOPPING_BLOCK.get());
			event.accept(ModBlocks.WARPED_CHOPPING_BLOCK.get());
			
			event.accept(ModBlocks.OAK_DRYING_RACK.get());
			event.accept(ModBlocks.SPRUCE_DRYING_RACK.get());
			event.accept(ModBlocks.BIRCH_DRYING_RACK.get());
			event.accept(ModBlocks.JUNGLE_DRYING_RACK.get());
			event.accept(ModBlocks.ACACIA_DRYING_RACK.get());
			event.accept(ModBlocks.DARK_OAK_DRYING_RACK.get());
			event.accept(ModBlocks.MANGROVE_DRYING_RACK.get());
			event.accept(ModBlocks.CHERRY_DRYING_RACK.get());
			event.accept(ModBlocks.CRIMSON_DRYING_RACK.get());
			event.accept(ModBlocks.WARPED_DRYING_RACK.get());
			
			event.accept(ModBlocks.MACHINE_FRAME.get());
			
			event.accept(ModBlocks.COAL_GENERATOR.get());
			event.accept(ModBlocks.BATTERY.get());
			event.accept(ModBlocks.SOLAR_PANEL.get());
			
			event.accept(ModBlocks.LIQUID_TANK.get());
			
			event.accept(ModBlocks.ELECTRIC_BREWERY.get());
			event.accept(ModBlocks.ELECTRIC_SMELTER.get());
			event.accept(ModBlocks.ELECTRIC_GREENHOUSE.get());
			
			event.accept(ModBlocks.HYDRAULIC_PRESS.get());
		}
		
		if(event.getTabKey() == ARMOR.getKey())
		{
			event.accept(ModItems.UNIM_HELMET.get());
			event.accept(ModItems.UNIM_CHESTPLATE.get());
			event.accept(ModItems.UNIM_LEGGINGS.get());
			event.accept(ModItems.UNIM_BOOTS.get());
			
			event.accept(ModItems.RUBY_HELMET.get());
			event.accept(ModItems.RUBY_CHESTPLATE.get());
			event.accept(ModItems.RUBY_LEGGINGS.get());
			event.accept(ModItems.RUBY_BOOTS.get());
			
			event.accept(ModItems.ASCABIT_HELMET.get());
			event.accept(ModItems.ASCABIT_CHESTPLATE.get());
			event.accept(ModItems.ASCABIT_LEGGINGS.get());
			event.accept(ModItems.ASCABIT_BOOTS.get());
			
			event.accept(ModItems.COPPER_HELMET.get());
			event.accept(ModItems.COPPER_CHESTPLATE.get());
			event.accept(ModItems.COPPER_LEGGINGS.get());
			event.accept(ModItems.COPPER_BOOTS.get());
			
			event.accept(ModItems.TIN_HELMET.get());
			event.accept(ModItems.TIN_CHESTPLATE.get());
			event.accept(ModItems.TIN_LEGGINGS.get());
			event.accept(ModItems.TIN_BOOTS.get());
			
			event.accept(ModItems.TITANIUM_HELMET.get());
			event.accept(ModItems.TITANIUM_CHESTPLATE.get());
			event.accept(ModItems.TITANIUM_LEGGINGS.get());
			event.accept(ModItems.TITANIUM_BOOTS.get());
			
			event.accept(ModItems.ZINC_HELMET.get());
			event.accept(ModItems.ZINC_CHESTPLATE.get());
			event.accept(ModItems.ZINC_LEGGINGS.get());
			event.accept(ModItems.ZINC_BOOTS.get());
			
			event.accept(ModItems.STEEL_HELMET.get());
			event.accept(ModItems.STEEL_CHESTPLATE.get());
			event.accept(ModItems.STEEL_LEGGINGS.get());
			event.accept(ModItems.STEEL_BOOTS.get());
			
			event.accept(ModItems.BRONZE_HELMET.get());
			event.accept(ModItems.BRONZE_CHESTPLATE.get());
			event.accept(ModItems.BRONZE_LEGGINGS.get());
			event.accept(ModItems.BRONZE_BOOTS.get());
		}
		
		if(event.getTabKey() == TOOLS.getKey())
		{
			event.accept(ModItems.FLINT_HATCHET.get());
			event.accept(ModItems.FLINT_SHEARS.get());
			
			event.accept(ModItems.FLINT_KNIFE.get());
			event.accept(ModItems.STONE_KNIFE.get());
			event.accept(ModItems.IRON_KNIFE.get());
			event.accept(ModItems.UNIM_KNIFE.get());
			event.accept(ModItems.DIAMOND_KNIFE.get());
			
			event.accept(ModItems.IRON_HAMMER.get());
			event.accept(ModItems.BRONZE_HAMMER.get());
			event.accept(ModItems.STEEL_HAMMER.get());
			event.accept(ModItems.DIAMOND_HAMMER.get());
			
			event.accept(ModItems.STEEL_DRILL.get());
			event.accept(((EnergyItem) ModItems.STEEL_DRILL.get()).getCreativeTabStack());
			
			event.accept(ModItems.UNIM_DRILL.get());
			event.accept(((EnergyItem) ModItems.UNIM_DRILL.get()).getCreativeTabStack());
			
			event.accept(ModItems.DIAMOND_DRILL.get());
			event.accept(((EnergyItem) ModItems.DIAMOND_DRILL.get()).getCreativeTabStack());
			
			event.accept(ModItems.UNIM_SWORD.get());
			event.accept(ModItems.UNIM_PICKAXE.get());
			event.accept(ModItems.UNIM_AXE.get());
			event.accept(ModItems.UNIM_SHOVEL.get());
			event.accept(ModItems.UNIM_HOE.get());
			
			event.accept(ModItems.RUBY_SWORD.get());
			event.accept(ModItems.RUBY_PICKAXE.get());
			event.accept(ModItems.RUBY_AXE.get());
			event.accept(ModItems.RUBY_SHOVEL.get());
			event.accept(ModItems.RUBY_HOE.get());
			
			event.accept(ModItems.ASCABIT_SWORD.get());
			event.accept(ModItems.ASCABIT_PICKAXE.get());
			event.accept(ModItems.ASCABIT_AXE.get());
			event.accept(ModItems.ASCABIT_SHOVEL.get());
			event.accept(ModItems.ASCABIT_HOE.get());
			
			event.accept(ModItems.COPPER_SWORD.get());
			event.accept(ModItems.COPPER_PICKAXE.get());
			event.accept(ModItems.COPPER_AXE.get());
			event.accept(ModItems.COPPER_SHOVEL.get());
			event.accept(ModItems.COPPER_HOE.get());
			
			event.accept(ModItems.TIN_SWORD.get());
			event.accept(ModItems.TIN_PICKAXE.get());
			event.accept(ModItems.TIN_AXE.get());
			event.accept(ModItems.TIN_SHOVEL.get());
			event.accept(ModItems.TIN_HOE.get());
			
			event.accept(ModItems.TITANIUM_SWORD.get());
			event.accept(ModItems.TITANIUM_PICKAXE.get());
			event.accept(ModItems.TITANIUM_AXE.get());
			event.accept(ModItems.TITANIUM_SHOVEL.get());
			event.accept(ModItems.TITANIUM_HOE.get());
			
			event.accept(ModItems.ZINC_SWORD.get());
			event.accept(ModItems.ZINC_PICKAXE.get());
			event.accept(ModItems.ZINC_AXE.get());
			event.accept(ModItems.ZINC_SHOVEL.get());
			event.accept(ModItems.ZINC_HOE.get());
			
			event.accept(ModItems.STEEL_SWORD.get());
			event.accept(ModItems.STEEL_PICKAXE.get());
			event.accept(ModItems.STEEL_AXE.get());
			event.accept(ModItems.STEEL_SHOVEL.get());
			event.accept(ModItems.STEEL_HOE.get());
			
			event.accept(ModItems.BRONZE_SWORD.get());
			event.accept(ModItems.BRONZE_PICKAXE.get());
			event.accept(ModItems.BRONZE_AXE.get());
			event.accept(ModItems.BRONZE_SHOVEL.get());
			event.accept(ModItems.BRONZE_HOE.get());
		}
		
		if(event.getTabKey() == TOOL_HEADS.getKey())
		{
			event.accept(ModItems.DRILL_BASE.get());
			event.accept(ModItems.STEEL_DRILL_HEAD.get());
			event.accept(ModItems.UNIM_DRILL_HEAD.get());
			event.accept(ModItems.DIAMOND_DRILL_HEAD.get());
			
			event.accept(ModItems.FLINT_HATCHET_HEAD.get());
			
			event.accept(ModItems.FLINT_KNIFE_BLADE.get());
			event.accept(ModItems.STONE_KNIFE_BLADE.get());
			event.accept(ModItems.IRON_KNIFE_BLADE.get());
			event.accept(ModItems.UNIM_KNIFE_BLADE.get());
			event.accept(ModItems.DIAMOND_KNIFE_BLADE.get());
			
			event.accept(ModItems.IRON_HAMMER_HEAD.get());
			event.accept(ModItems.BRONZE_HAMMER_HEAD.get());
			event.accept(ModItems.STEEL_HAMMER_HEAD.get());
			event.accept(ModItems.DIAMOND_HAMMER_HEAD.get());
			
			event.accept(ModItems.STONE_SWORD_BLADE.get());
			event.accept(ModItems.STONE_PICKAXE_HEAD.get());
			event.accept(ModItems.STONE_AXE_HEAD.get());
			event.accept(ModItems.STONE_SHOVEL_HEAD.get());
			event.accept(ModItems.STONE_HOE_HEAD.get());
			
			event.accept(ModItems.IRON_SWORD_BLADE.get());
			event.accept(ModItems.IRON_PICKAXE_HEAD.get());
			event.accept(ModItems.IRON_AXE_HEAD.get());
			event.accept(ModItems.IRON_SHOVEL_HEAD.get());
			event.accept(ModItems.IRON_HOE_HEAD.get());
			
			event.accept(ModItems.GOLDEN_SWORD_BLADE.get());
			event.accept(ModItems.GOLDEN_PICKAXE_HEAD.get());
			event.accept(ModItems.GOLDEN_AXE_HEAD.get());
			event.accept(ModItems.GOLDEN_SHOVEL_HEAD.get());
			event.accept(ModItems.GOLDEN_HOE_HEAD.get());
			
			event.accept(ModItems.UNIM_SWORD_BLADE.get());
			event.accept(ModItems.UNIM_PICKAXE_HEAD.get());
			event.accept(ModItems.UNIM_AXE_HEAD.get());
			event.accept(ModItems.UNIM_SHOVEL_HEAD.get());
			event.accept(ModItems.UNIM_HOE_HEAD.get());
			
			event.accept(ModItems.RUBY_SWORD_BLADE.get());
			event.accept(ModItems.RUBY_PICKAXE_HEAD.get());
			event.accept(ModItems.RUBY_AXE_HEAD.get());
			event.accept(ModItems.RUBY_SHOVEL_HEAD.get());
			event.accept(ModItems.RUBY_HOE_HEAD.get());
			
			event.accept(ModItems.ASCABIT_SWORD_BLADE.get());
			event.accept(ModItems.ASCABIT_PICKAXE_HEAD.get());
			event.accept(ModItems.ASCABIT_AXE_HEAD.get());
			event.accept(ModItems.ASCABIT_SHOVEL_HEAD.get());
			event.accept(ModItems.ASCABIT_HOE_HEAD.get());
			
			event.accept(ModItems.COPPER_SWORD_BLADE.get());
			event.accept(ModItems.COPPER_PICKAXE_HEAD.get());
			event.accept(ModItems.COPPER_AXE_HEAD.get());
			event.accept(ModItems.COPPER_SHOVEL_HEAD.get());
			event.accept(ModItems.COPPER_HOE_HEAD.get());
			
			event.accept(ModItems.TIN_SWORD_BLADE.get());
			event.accept(ModItems.TIN_PICKAXE_HEAD.get());
			event.accept(ModItems.TIN_AXE_HEAD.get());
			event.accept(ModItems.TIN_SHOVEL_HEAD.get());
			event.accept(ModItems.TIN_HOE_HEAD.get());
			
			event.accept(ModItems.TITANIUM_SWORD_BLADE.get());
			event.accept(ModItems.TITANIUM_PICKAXE_HEAD.get());
			event.accept(ModItems.TITANIUM_AXE_HEAD.get());
			event.accept(ModItems.TITANIUM_SHOVEL_HEAD.get());
			event.accept(ModItems.TITANIUM_HOE_HEAD.get());
			
			event.accept(ModItems.ZINC_SWORD_BLADE.get());
			event.accept(ModItems.ZINC_PICKAXE_HEAD.get());
			event.accept(ModItems.ZINC_AXE_HEAD.get());
			event.accept(ModItems.ZINC_SHOVEL_HEAD.get());
			event.accept(ModItems.ZINC_HOE_HEAD.get());
			
			event.accept(ModItems.STEEL_SWORD_BLADE.get());
			event.accept(ModItems.STEEL_PICKAXE_HEAD.get());
			event.accept(ModItems.STEEL_AXE_HEAD.get());
			event.accept(ModItems.STEEL_SHOVEL_HEAD.get());
			event.accept(ModItems.STEEL_HOE_HEAD.get());
			
			event.accept(ModItems.BRONZE_SWORD_BLADE.get());
			event.accept(ModItems.BRONZE_PICKAXE_HEAD.get());
			event.accept(ModItems.BRONZE_AXE_HEAD.get());
			event.accept(ModItems.BRONZE_SHOVEL_HEAD.get());
			event.accept(ModItems.BRONZE_HOE_HEAD.get());
		}
		
		if(event.getTabKey() == FOOD.getKey())
		{
			event.accept(ModItems.RAW_BACON.get());
			event.accept(ModItems.COOKED_BACON.get());
			event.accept(ModItems.FRIED_EGG.get());
			event.accept(ModItems.CHEESE.get());
			event.accept(ModItems.PIZZA.get());
			event.accept(ModItems.CHOCOLATE.get());
			event.accept(ModItems.POPCORN.get());
			event.accept(ModItems.PURIFIED_MEAT.get());
			event.accept(ModItems.BEEF_JERKY.get());
			event.accept(ModItems.ONION_RINGS.get());
			event.accept(ModItems.SAUSAGE.get());
			event.accept(ModItems.HOT_DOG.get());
			event.accept(ModItems.GARDEN_SALAD.get());
			event.accept(ModItems.HAMBURGER.get());
			event.accept(ModItems.CHEESEBURGER.get());
			event.accept(ModItems.SUSHI.get());
			
			event.accept(ModItems.APPLE_PIE.get());
			event.accept(ModItems.STRAWBERRY_PIE.get());
			
			event.accept(ModItems.TOMATO_SEEDS.get());
			event.accept(ModItems.TOMATO.get());
			
			event.accept(ModItems.GRAPE_SEEDS.get());
			event.accept(ModItems.GRAPE.get());
			
			event.accept(ModItems.STRAWBERRY_SEEDS.get());
			event.accept(ModItems.STRAWBERRY.get());
			
			event.accept(ModItems.CORN_SEEDS.get());
			event.accept(ModItems.CORN.get());
			
			event.accept(ModItems.LETTUCE_SEEDS.get());
			event.accept(ModItems.LETTUCE.get());
			
			event.accept(ModItems.ONION_SEEDS.get());
			event.accept(ModItems.ONION.get());
			
			event.accept(ModItems.RICE.get());
			
			event.accept(ModItems.VEGETABLE_SOUP.get());
			event.accept(ModItems.PUMPKIN_SOUP.get());
			event.accept(ModItems.CACTUS_SOUP.get());
			event.accept(ModItems.FISH_SOUP.get());
			
			event.accept(ModBlocks.CHOCOLATE_CAKE.get());
		}
	}
}
