package de.boy132.minecraftcontentexpansion.item.backpack;

import de.boy132.minecraftcontentexpansion.screen.backpack.BackpackMenu;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class BackpackItem extends Item
{
	private final int rowCount;
	
	public BackpackItem(Properties properties, int rowCount)
	{
		super(properties);
		this.rowCount = rowCount;
	}
	
	public int getRowCount()
	{
		return rowCount;
	}
	
	public int getInventorySize()
	{
		return getRowCount() * 9;
	}
	
	public IItemHandler getInventory(ItemStack stack)
	{
		ItemStackHandler stackHandler = new ItemStackHandler(getInventorySize());
		stackHandler.deserializeNBT(stack.getOrCreateTag().getCompound("Inventory"));
		return stackHandler;
	}
	
	public void saveInventory(ItemStack stack, IItemHandler itemHandler)
	{
		if(itemHandler instanceof ItemStackHandler)
			stack.getOrCreateTag().put("Inventory", ((ItemStackHandler) itemHandler).serializeNBT());
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		if(!level.isClientSide)
			((ServerPlayer) player).openMenu(new SimpleMenuProvider((id, inventory, target) -> new BackpackMenu(id, inventory), player.getItemInHand(hand).getHoverName()), (friendlyByteBuf) -> friendlyByteBuf.writeItemStack(player.getItemInHand(hand), false));
		
		return new InteractionResultHolder<>(InteractionResult.SUCCESS, player.getItemInHand(hand));
	}
	
	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> toolTips, TooltipFlag flag)
	{
		super.appendHoverText(stack, level, toolTips, flag);
		
		int firstStacksCounter = 0;
		int totalItemStacks = 0;
		
		IItemHandler itemHandler = getInventory(stack);
		for(int slot = 0; slot < itemHandler.getSlots(); slot++)
		{
			ItemStack slotStack = itemHandler.getStackInSlot(slot);
			if(!slotStack.isEmpty())
			{
				totalItemStacks++;
				if(firstStacksCounter <= 4)
				{
					firstStacksCounter++;
					MutableComponent mutableComponent = slotStack.getHoverName().copy();
					mutableComponent.append(" x")
							.append(String.valueOf(slotStack.getCount()));
					toolTips.add(mutableComponent);
				}
			}
		}
		
		int moreItemStacks = totalItemStacks - firstStacksCounter;
		if(moreItemStacks > 0)
			toolTips.add(Component.translatable("container.shulkerBox.more", moreItemStacks).withStyle(ChatFormatting.ITALIC));
	}
}
