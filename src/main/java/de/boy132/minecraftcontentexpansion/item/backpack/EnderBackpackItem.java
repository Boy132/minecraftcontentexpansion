package de.boy132.minecraftcontentexpansion.item.backpack;

import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ChestMenu;
import net.minecraft.world.inventory.PlayerEnderChestContainer;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class EnderBackpackItem extends Item
{
	public EnderBackpackItem(Properties properties)
	{
		super(properties);
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		if(!level.isClientSide)
		{
			PlayerEnderChestContainer enderChestInventory = player.getEnderChestInventory();
			player.openMenu(new SimpleMenuProvider((id, inventory, target) -> ChestMenu.threeRows(id, inventory, enderChestInventory), Component.translatable("item.minecraftcontentexpansion.ender_backpack")));
		}
		
		return new InteractionResultHolder<>(InteractionResult.SUCCESS, player.getItemInHand(hand));
	}
}
