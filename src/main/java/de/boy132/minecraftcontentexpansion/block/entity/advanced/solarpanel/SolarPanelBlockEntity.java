package de.boy132.minecraftcontentexpansion.block.entity.advanced.solarpanel;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.solarpanel.SolarPanelMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.Nullable;

public class SolarPanelBlockEntity extends AdvancedBlockEntity
{
	public static final int CAPACITY = 50000;
	
	protected final ContainerData data;
	
	public SolarPanelBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.SOLAR_PANEL.get(), pos, state);
		energy = new Energy(CAPACITY, 0, 100);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				if(index == 0)
					return SolarPanelBlockEntity.this.getEnergyStorage().getEnergyIO();
				return 0;
			}
			
			@Override
			public void set(int index, int value)
			{
				if(index == 0)
					SolarPanelBlockEntity.this.getEnergyStorage().setEnergyIO(value);
			}
			
			@Override
			public int getCount()
			{
				return 1;
			}
		};
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".solar_panel");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new SolarPanelMenu(id, inventory, this, data);
	}
	
	@Override
	public boolean canDoWork()
	{
		return canSeeSky() && isDayTime();
	}
	
	public boolean canSeeSky()
	{
		return level != null && level.canSeeSkyFromBelowWater(worldPosition);
	}
	
	public boolean isDayTime()
	{
		if(level == null)
			return false;
		
		long time = level.getDayTime() % 24000;
		return time >= 0 && time <= 12500;
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, SolarPanelBlockEntity solarPanelBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		int production = solarPanelBlockEntity.canDoWork() ? 10 : 0;
		AdvancedBlockEntity.handleTransfer(solarPanelBlockEntity, production);
	}
}
