package de.boy132.minecraftcontentexpansion.block.entity;

import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;

import java.util.function.BiPredicate;

public class LimitedItemStackHandler extends ItemStackHandler
{
	private final ItemStackHandler original;
	private final BiPredicate<Integer, ItemStack> canInsert;
	private final BiPredicate<Integer, ItemStack> canExtract;
	
	public LimitedItemStackHandler(ItemStackHandler original, BiPredicate<Integer, ItemStack> canInsert, BiPredicate<Integer, ItemStack> canExtract)
	{
		super(original.getSlots());
		this.original = original;
		this.canInsert = canInsert;
		this.canExtract = canExtract;
	}
	
	@Override
	public void setStackInSlot(int slot, @NotNull ItemStack stack)
	{
		if(canInsert.test(slot, stack))
			original.setStackInSlot(slot, stack);
	}
	
	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return original.getStackInSlot(slot);
	}
	
	@Override
	public @NotNull ItemStack insertItem(int slot, @NotNull ItemStack stack, boolean simulate)
	{
		return canInsert.test(slot, stack) ? original.insertItem(slot, stack, simulate) : stack;
	}
	
	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		return canExtract.test(slot, getStackInSlot(slot)) ? original.extractItem(slot, amount, simulate) : ItemStack.EMPTY;
	}
	
	@Override
	public boolean isItemValid(int slot, @NotNull ItemStack stack)
	{
		return canInsert.test(slot, stack) && original.isItemValid(slot, stack);
	}
	
	@Override
	public int getSlots()
	{
		return original.getSlots();
	}
	
	@Override
	public int getSlotLimit(int slot)
	{
		return original.getSlotLimit(slot);
	}
}
