package de.boy132.minecraftcontentexpansion.block.entity.choppingblock;

import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.recipe.choppingblock.ChoppingBlockRecipe;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeHolder;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ChoppingBlockEntity extends BlockEntity
{
	public static final int SLOT = 0;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(1)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
			if(level != null)
				level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 2);
		}
		
		@Override
		protected int getStackLimit(int slot, @NotNull ItemStack stack)
		{
			return 1;
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	public ChoppingBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.CHOPPING_BLOCK.get(), pos, state);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
			return lazyItemHandler.cast();
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
	}
	
	@Override
	public CompoundTag getUpdateTag()
	{
		CompoundTag compoundTag = super.getUpdateTag();
		saveAdditional(compoundTag);
		return compoundTag;
	}
	
	@Override
	public void handleUpdateTag(CompoundTag compoundTag)
	{
		super.handleUpdateTag(compoundTag);
		load(compoundTag);
	}
	
	@Nullable
	@Override
	public Packet<ClientGamePacketListener> getUpdatePacket()
	{
		return ClientboundBlockEntityDataPacket.create(this);
	}
	
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	public boolean canDoWork()
	{
		ItemStack input = itemHandler.getStackInSlot(SLOT);
		if(input.isEmpty())
			return false;
		
		ChoppingBlockRecipe choppingBlockRecipe = getRecipe();
		if(choppingBlockRecipe == null)
			return false;
		
		ItemStack result = choppingBlockRecipe.assemble(null, level.registryAccess());
		return !result.isEmpty();
	}
	
	public ChoppingBlockRecipe getRecipe()
	{
		SimpleContainer container = new SimpleContainer(itemHandler.getSlots());
		for(int i = 0; i < itemHandler.getSlots(); i++)
			container.setItem(i, itemHandler.getStackInSlot(i));
		
		Optional<RecipeHolder<ChoppingBlockRecipe>> recipe = level.getRecipeManager().getRecipeFor(ModRecipeTypes.CHOPPING_BLOCK.get(), container, level);
		if(recipe.isPresent())
			return recipe.get().value();
		
		return null;
	}
	
	public static ChoppingBlockRecipe getRecipe(Level level, ItemStack input)
	{
		SimpleContainer container = new SimpleContainer(1);
		container.setItem(0, input);
		
		Optional<RecipeHolder<ChoppingBlockRecipe>> recipe = level.getRecipeManager().getRecipeFor(ModRecipeTypes.CHOPPING_BLOCK.get(), container, level);
		if(recipe.isPresent())
			return recipe.get().value();
		
		return null;
	}
}
