package de.boy132.minecraftcontentexpansion.block.entity.advanced.electricsmelter;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.recipe.electricsmelter.ElectricSmelterRecipe;
import de.boy132.minecraftcontentexpansion.screen.electricsmelter.ElectricSmelterMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeHolder;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ElectricSmelterBlockEntity extends AdvancedBlockEntity
{
	public static final int CAPACITY = 200000;
	
	public static final int STANDARD_SMELT_TIME = 300;
	
	public static final int OUTPUT_SLOT = 0;
	public static final int INPUT_SLOT_START = 1;
	public static final int INPUT_SLOT_COUNT = 4;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(5)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
		
		@Override
		public int getSlotLimit(int slot)
		{
			if(slot >= INPUT_SLOT_START)
				return 16;
			
			return super.getSlotLimit(slot);
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	protected final ContainerData data;
	
	protected int smeltTime;
	protected int neededSmeltTime;
	
	public ElectricSmelterBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.ELECTRIC_SMELTER.get(), pos, state);
		energy = new Energy(CAPACITY, 1000, 0);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> ElectricSmelterBlockEntity.this.getEnergyStorage().getEnergyIO();
					case 1 -> ElectricSmelterBlockEntity.this.smeltTime;
					case 2 -> ElectricSmelterBlockEntity.this.neededSmeltTime;
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				switch(index)
				{
					case 0 -> ElectricSmelterBlockEntity.this.getEnergyStorage().setEnergyIO(value);
					case 1 -> ElectricSmelterBlockEntity.this.smeltTime = value;
					case 2 -> ElectricSmelterBlockEntity.this.neededSmeltTime = value;
				}
			}
			
			@Override
			public int getCount()
			{
				return 3;
			}
		};
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".electric_smelter");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new ElectricSmelterMenu(id, inventory, this, data);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
			return lazyItemHandler.cast();
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		compoundTag.putInt("smeltTime", smeltTime);
		compoundTag.putInt("neededSmeltTime", neededSmeltTime);
		
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
		
		smeltTime = compoundTag.getInt("smeltTime");
		neededSmeltTime = compoundTag.getInt("neededSmeltTime");
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	public boolean isBurning()
	{
		return smeltTime > 0;
	}
	
	@Override
	public boolean canDoWork()
	{
		for(int i = INPUT_SLOT_START; i < INPUT_SLOT_START + INPUT_SLOT_COUNT; i++)
		{
			ItemStack input = itemHandler.getStackInSlot(i);
			if(input.isEmpty())
				return false;
		}
		
		if(getEnergyStorage().getEnergyStored() < 100)
			return false;
		
		ElectricSmelterRecipe electricSmelterRecipe = getRecipe();
		if(electricSmelterRecipe == null)
			return false;
		
		ItemStack result = electricSmelterRecipe.assemble(null, level.registryAccess());
		if(result.isEmpty())
			return false;
		
		ItemStack output = itemHandler.getStackInSlot(OUTPUT_SLOT);
		if(output.isEmpty())
			return true;
		
		if(!ItemStack.isSameItem(output, result))
			return false;
		
		int res = output.getCount() + result.getCount();
		return res <= 64 && res <= output.getMaxStackSize();
	}
	
	public ElectricSmelterRecipe getRecipe()
	{
		SimpleContainer container = new SimpleContainer(itemHandler.getSlots());
		for(int i = 0; i < INPUT_SLOT_COUNT; i++)
			container.setItem(i, itemHandler.getStackInSlot(INPUT_SLOT_START + i));
		
		Optional<RecipeHolder<ElectricSmelterRecipe>> recipe = level.getRecipeManager().getRecipeFor(ModRecipeTypes.ELECTRIC_SMELTER.get(), container, level);
		if(recipe.isPresent())
			return recipe.get().value();
		
		return null;
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, ElectricSmelterBlockEntity electricSmelterBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		
		int consume = 0;
		if(electricSmelterBlockEntity.canDoWork() && !level.hasNeighborSignal(pos))
		{
			electricSmelterBlockEntity.smeltTime++;
			
			ElectricSmelterRecipe electricSmelterRecipe = electricSmelterBlockEntity.getRecipe();
			electricSmelterBlockEntity.neededSmeltTime = electricSmelterRecipe.getSmeltTime();
			if(electricSmelterBlockEntity.smeltTime > electricSmelterBlockEntity.neededSmeltTime)
			{
				// set to 1 instead of 0 so that the lit animation works smoothly
				electricSmelterBlockEntity.smeltTime = 1;
				
				ItemStack result = electricSmelterRecipe.assemble(null, electricSmelterBlockEntity.level.registryAccess());
				
				for(int i = INPUT_SLOT_START; i < INPUT_SLOT_START + INPUT_SLOT_COUNT; i++)
					electricSmelterBlockEntity.itemHandler.extractItem(i, 1, false);
				
				electricSmelterBlockEntity.itemHandler.insertItem(OUTPUT_SLOT, result, false);
			}
			
			consume = 100;
			
			hasChanged = true;
		} else
			electricSmelterBlockEntity.smeltTime = 0;
		
		if(state.getValue(ElectricSmelterBlock.LIT) != electricSmelterBlockEntity.isBurning())
		{
			hasChanged = true;
			
			state = state.setValue(ElectricSmelterBlock.LIT, electricSmelterBlockEntity.isBurning());
			level.setBlock(pos, state, 3);
		}
		
		AdvancedBlockEntity.handleTransfer(electricSmelterBlockEntity, -consume);
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
