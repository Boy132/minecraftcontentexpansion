package de.boy132.minecraftcontentexpansion.block.entity.millstone;

import com.mojang.serialization.MapCodec;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.jetbrains.annotations.Nullable;

public class MillstoneBlock extends BaseEntityBlock
{
	public static final MapCodec<MillstoneBlock> CODEC = simpleCodec(MillstoneBlock::new);
	
	public MillstoneBlock(Properties properties)
	{
		super(properties);
	}
	
	@Override
	protected MapCodec<MillstoneBlock> codec()
	{
		return CODEC;
	}
	
	public static final VoxelShape SHAPE1 = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 10.0D, 16.0D);
	public static final VoxelShape SHAPE2 = Block.box(2.0D, 10.0D, 2.0D, 14.0D, 14.0D, 14.0D);
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter getter, BlockPos pos, CollisionContext context)
	{
		return Shapes.or(SHAPE1, SHAPE2);
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.MODEL;
	}
	
	@Override
	public void onRemove(BlockState state, Level level, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(state.getBlock() != newState.getBlock())
		{
			BlockEntity blockEntity = level.getBlockEntity(pos);
			if(blockEntity instanceof MillstoneBlockEntity millstoneBlockEntity)
				millstoneBlockEntity.dropContents();
		}
		
		super.onRemove(state, level, pos, newState, isMoving);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hitResult)
	{
		if(!level.isClientSide())
		{
			BlockEntity blockEntity = level.getBlockEntity(pos);
			if(blockEntity instanceof MillstoneBlockEntity millstoneBlockEntity)
				((ServerPlayer) player).openMenu(millstoneBlockEntity, pos);
		}
		
		return InteractionResult.sidedSuccess(level.isClientSide());
	}
	
	@Nullable
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new MillstoneBlockEntity(pos, state);
	}
	
	@Nullable
	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> entityType)
	{
		return createTickerHelper(entityType, ModBlockEntities.MILLSTONE.get(), MillstoneBlockEntity::tick);
	}
}
