package de.boy132.minecraftcontentexpansion.block.entity.advanced;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.BaseBlockEntity;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.alchemy.PotionUtils;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidType;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

public abstract class AdvancedBlockEntity extends BaseBlockEntity
{
	protected Energy energy;
	protected Fluid fluid;
	
	public AdvancedBlockEntity(BlockEntityType<?> blockEntityType, BlockPos pos, BlockState state)
	{
		super(blockEntityType, pos, state);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(energy != null && energy.getCapability(capability, side) != null)
			return energy.getCapability(capability, side);
		
		if(fluid != null && fluid.getCapability(capability, side) != null)
			return fluid.getCapability(capability, side);
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		
		if(energy != null)
			energy.init();
		
		if(fluid != null)
			fluid.init();
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		
		if(energy != null)
			energy.invalidate();
		
		if(fluid != null)
			fluid.invalidate();
	}
	
	public CompoundTag saveCustom(CompoundTag compoundTag)
	{
		if(energy != null)
			energy.save(compoundTag);
		
		if(fluid != null)
			fluid.save(compoundTag);
		
		return compoundTag;
	}
	
	public void loadCustom(CompoundTag compoundTag)
	{
		if(energy != null)
			energy.load(compoundTag);
		
		if(fluid != null)
			fluid.load(compoundTag);
	}
	
	@Override
	public void saveToItem(ItemStack stack)
	{
		BlockItem.setBlockEntityData(stack, getType(), saveCustom(new CompoundTag()));
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		saveCustom(compoundTag);
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		loadCustom(compoundTag);
	}
	
	@Override
	public CompoundTag getUpdateTag()
	{
		CompoundTag compoundTag = super.getUpdateTag();
		saveAdditional(compoundTag);
		return compoundTag;
	}
	
	@Override
	public void handleUpdateTag(CompoundTag compoundTag)
	{
		super.handleUpdateTag(compoundTag);
		load(compoundTag);
	}
	
	@Nullable
	@Override
	public Packet<ClientGamePacketListener> getUpdatePacket()
	{
		return ClientboundBlockEntityDataPacket.create(this);
	}
	
	public ModEnergyStorage getEnergyStorage()
	{
		return energy != null ? energy.getEnergyStorage() : null;
	}
	
	public FluidTank getFluidTank()
	{
		return fluid != null ? fluid.getFluidTank() : null;
	}
	
	public static boolean isItemValidForFluid(FluidTank fluidTank, ItemStack itemStack)
	{
		if(itemStack.isEmpty())
			return false;
		
		if(itemStack.getCapability(ForgeCapabilities.FLUID_HANDLER_ITEM).isPresent())
		{
			IFluidHandler fluidHandler = itemStack.getCapability(ForgeCapabilities.FLUID_HANDLER_ITEM).orElse(null);
			FluidStack fluidStack = fluidHandler.getFluidInTank(0);
			
			if(fluidStack.isEmpty())
				return fluidTank.getFluidAmount() >= FluidType.BUCKET_VOLUME;
			else
				return fluidTank.isFluidValid(fluidStack);
		} else
		{
			if(itemStack.getItem() == Items.WATER_BUCKET || itemStack.getItem() == ModItems.WATER_CLAY_BUCKET.get())
			{
				FluidStack fluidStack = new FluidStack(Fluids.WATER, FluidType.BUCKET_VOLUME);
				return fluidTank.isFluidValid(fluidStack);
			} else if(itemStack.getItem() == Items.POTION && PotionUtils.getMobEffects(itemStack).isEmpty())
			{
				FluidStack fluidStack = new FluidStack(Fluids.WATER, FluidType.BUCKET_VOLUME / 10);
				return fluidTank.isFluidValid(fluidStack);
			} else if(itemStack.getItem() == Items.LAVA_BUCKET)
			{
				FluidStack fluidStack = new FluidStack(Fluids.LAVA, FluidType.BUCKET_VOLUME);
				return fluidTank.isFluidValid(fluidStack);
			}
		}
		
		return false;
	}
	
	public static void handleTransfer(BlockEntity blockEntity, int ownEnergyProduction)
	{
		Level level = blockEntity.getLevel();
		if(level == null || level.isClientSide())
			return;
		
		blockEntity.getCapability(ForgeCapabilities.ENERGY).ifPresent(energyStorage ->
		{
			ModEnergyStorage modEnergyStorage = (ModEnergyStorage) energyStorage;
			modEnergyStorage.addEnergy(ownEnergyProduction);
			
			if(modEnergyStorage.canExtract())
			{
				BlockPos pos = blockEntity.getBlockPos();
				for(Direction direction : Direction.values())
				{
					BlockEntity targetBlockEntity = level.getBlockEntity(pos.relative(direction));
					if(targetBlockEntity != null)
					{
						targetBlockEntity.getCapability(ForgeCapabilities.ENERGY).ifPresent((targetEnergyStorage) ->
						{
							if(targetEnergyStorage.canReceive())
							{
								int acceptedEnergy = targetEnergyStorage.receiveEnergy(modEnergyStorage.getMaxExtract(), false);
								modEnergyStorage.extractEnergy(acceptedEnergy, false);
							}
						});
					}
				}
			}
			
			modEnergyStorage.setEnergyIO();
		});
		
		blockEntity.getCapability(ForgeCapabilities.FLUID_HANDLER).ifPresent(fluidHandler ->
		{
			FluidTank fluidTank = (FluidTank) fluidHandler;
			if(fluidTank.getFluidAmount() > 0)
			{
				for(Direction direction : Direction.values())
				{
					BlockPos targetPos = blockEntity.getBlockPos().relative(direction);
					BlockState state = level.getBlockState(targetPos);
					
					if(!state.hasBlockEntity() || level.getBlockEntity(targetPos) == null || state.getBlock() == ModBlocks.LIQUID_TANK.get())
						continue;
					
					BlockEntity targetBlockEntity = level.getBlockEntity(targetPos);
					targetBlockEntity.getCapability(ForgeCapabilities.FLUID_HANDLER).ifPresent((targetFluidHandler) ->
					{
						FluidTank targetTank = (FluidTank) targetFluidHandler;
						if(targetTank.isFluidValid(new FluidStack(fluidTank.getFluid(), 1)) && targetTank.getSpace() > 0)
						{
							int acceptedFluid = targetTank.fill(new FluidStack(fluidTank.getFluid(), Math.min(FluidType.BUCKET_VOLUME / 10, fluidTank.getFluidAmount())), IFluidHandler.FluidAction.EXECUTE);
							fluidTank.drain(acceptedFluid, IFluidHandler.FluidAction.EXECUTE);
						}
					});
				}
			}
		});
	}
	
	public class Energy
	{
		protected final ModEnergyStorage energyStorage;
		protected LazyOptional<EnergyStorage> lazyEnergyStorage = LazyOptional.empty();
		
		public Energy(int capacity)
		{
			this.energyStorage = new ModEnergyStorage(capacity)
			{
				@Override
				protected void onEnergyChanged()
				{
					setChanged();
					if(level != null)
						level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 2);
				}
			};
		}
		
		public Energy(int capacity, int maxTransfer)
		{
			this.energyStorage = new ModEnergyStorage(capacity, maxTransfer)
			{
				@Override
				protected void onEnergyChanged()
				{
					setChanged();
					if(level != null)
						level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 2);
				}
			};
		}
		
		public Energy(int capacity, int maxReceive, int maxExtract)
		{
			this.energyStorage = new ModEnergyStorage(capacity, maxReceive, maxExtract)
			{
				@Override
				protected void onEnergyChanged()
				{
					setChanged();
					if(level != null)
						level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 2);
				}
			};
		}
		
		public Energy(int capacity, int maxReceive, int maxExtract, int energyAmount)
		{
			this.energyStorage = new ModEnergyStorage(capacity, maxReceive, maxExtract, energyAmount)
			{
				@Override
				protected void onEnergyChanged()
				{
					setChanged();
					if(level != null)
						level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 2);
				}
			};
		}
		
		public void init()
		{
			lazyEnergyStorage = LazyOptional.of(() -> energyStorage);
		}
		
		public void invalidate()
		{
			lazyEnergyStorage.invalidate();
		}
		
		public CompoundTag save(CompoundTag compoundTag)
		{
			compoundTag.put("energy", energyStorage.serializeNBT());
			return compoundTag;
		}
		
		public void load(CompoundTag compoundTag)
		{
			energyStorage.deserializeNBT(compoundTag.getCompound("energy"));
		}
		
		public <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
		{
			if(capability == ForgeCapabilities.ENERGY)
				return lazyEnergyStorage.cast();
			
			return null;
		}
		
		public ModEnergyStorage getEnergyStorage()
		{
			return energyStorage;
		}
	}
	
	public class Fluid
	{
		protected final FluidTank fluidTank;
		protected LazyOptional<FluidTank> lazyFluidTank = LazyOptional.empty();
		
		public Fluid(int capacity)
		{
			this.fluidTank = new FluidTank(capacity)
			{
				@Override
				protected void onContentsChanged()
				{
					setChanged();
					if(level != null)
						level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 2);
				}
			};
		}
		
		public Fluid(int capacity, Predicate<FluidStack> validator)
		{
			this.fluidTank = new FluidTank(capacity, validator)
			{
				@Override
				protected void onContentsChanged()
				{
					setChanged();
					if(level != null)
						level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 2);
				}
			};
		}
		
		public void init()
		{
			lazyFluidTank = LazyOptional.of(() -> fluidTank);
		}
		
		public void invalidate()
		{
			lazyFluidTank.invalidate();
		}
		
		public CompoundTag save(CompoundTag compoundTag)
		{
			compoundTag.put("fluid", fluidTank.writeToNBT(new CompoundTag()));
			return compoundTag;
		}
		
		public void load(CompoundTag compoundTag)
		{
			fluidTank.readFromNBT(compoundTag.getCompound("fluid"));
		}
		
		public <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
		{
			if(capability == ForgeCapabilities.FLUID_HANDLER)
				return lazyFluidTank.cast();
			
			return null;
		}
		
		public FluidTank getFluidTank()
		{
			return fluidTank;
		}
	}
}
