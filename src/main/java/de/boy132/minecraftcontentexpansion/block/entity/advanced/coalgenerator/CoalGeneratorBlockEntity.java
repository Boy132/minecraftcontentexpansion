package de.boy132.minecraftcontentexpansion.block.entity.advanced.coalgenerator;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.coalgenerator.CoalGeneratorMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CoalGeneratorBlockEntity extends AdvancedBlockEntity
{
	public static final int CAPACITY = 100000;
	
	public static final int FUEL_SLOT = 0;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(1)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	protected final ContainerData data;
	
	protected int burnTime;
	protected int neededBurnTime;
	
	public CoalGeneratorBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.COAL_GENERATOR.get(), pos, state);
		energy = new Energy(CAPACITY, 0, 500);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> CoalGeneratorBlockEntity.this.getEnergyStorage().getEnergyIO();
					case 1 -> CoalGeneratorBlockEntity.this.burnTime;
					case 2 -> CoalGeneratorBlockEntity.this.neededBurnTime;
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				switch(index)
				{
					case 0 -> CoalGeneratorBlockEntity.this.getEnergyStorage().setEnergyIO(value);
					case 1 -> CoalGeneratorBlockEntity.this.burnTime = value;
					case 2 -> CoalGeneratorBlockEntity.this.neededBurnTime = value;
				}
			}
			
			@Override
			public int getCount()
			{
				return 3;
			}
		};
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".coal_generator");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new CoalGeneratorMenu(id, inventory, this, data);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
			return lazyItemHandler.cast();
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		compoundTag.putInt("burnTime", burnTime);
		compoundTag.putInt("neededBurnTime", neededBurnTime);
		
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
		
		burnTime = compoundTag.getInt("burnTime");
		neededBurnTime = compoundTag.getInt("neededBurnTime");
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	public int getFuelValue(ItemStack fuel)
	{
		if(fuel.getItem() == Items.COAL)
			return 200;
		else if(fuel.getItem() == Blocks.COAL_BLOCK.asItem())
			return getFuelValue(new ItemStack(Items.COAL)) * 9;
		
		return 0;
	}
	
	public boolean isItemFuel(ItemStack item)
	{
		return getFuelValue(item) > 0;
	}
	
	public boolean isBurning()
	{
		return burnTime > 0;
	}
	
	@Override
	public boolean canDoWork()
	{
		ItemStack fuel = itemHandler.getStackInSlot(FUEL_SLOT);
		return !isBurning() && !fuel.isEmpty() && isItemFuel(fuel) && getEnergyStorage().getEnergyStored() < getEnergyStorage().getMaxEnergyStored();
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, CoalGeneratorBlockEntity coalGeneratorBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		int production = 0;
		
		if(coalGeneratorBlockEntity.isBurning())
		{
			hasChanged = true;
			
			--coalGeneratorBlockEntity.burnTime;
			production = 40;
		}
		
		AdvancedBlockEntity.handleTransfer(coalGeneratorBlockEntity, production);
		
		if(coalGeneratorBlockEntity.canDoWork() && !level.hasNeighborSignal(pos))
		{
			hasChanged = true;
			
			coalGeneratorBlockEntity.burnTime = coalGeneratorBlockEntity.getFuelValue(coalGeneratorBlockEntity.itemHandler.getStackInSlot(FUEL_SLOT));
			coalGeneratorBlockEntity.neededBurnTime = coalGeneratorBlockEntity.burnTime;
			
			coalGeneratorBlockEntity.itemHandler.extractItem(FUEL_SLOT, 1, false);
		}
		
		if(state.getValue(CoalGeneratorBlock.LIT) != coalGeneratorBlockEntity.isBurning())
		{
			hasChanged = true;
			
			state = state.setValue(CoalGeneratorBlock.LIT, coalGeneratorBlockEntity.isBurning());
			level.setBlock(pos, state, 3);
		}
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
