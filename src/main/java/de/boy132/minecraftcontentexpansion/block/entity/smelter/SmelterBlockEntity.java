package de.boy132.minecraftcontentexpansion.block.entity.smelter;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.BaseBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.LimitedItemStackHandler;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.recipe.smelter.SmelterRecipe;
import de.boy132.minecraftcontentexpansion.screen.smelter.SmelterMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.RecipeHolder;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class SmelterBlockEntity extends BaseBlockEntity
{
	public static final int STANDARD_SMELT_TIME = 600;
	public static final float STANDARD_MIN_HEAT = 0.75f;
	
	public static final int MAX_HEAT = 1000;
	
	public static final int FUEL_SLOT = 0;
	public static final int INPUT_SLOT_1 = 1;
	public static final int INPUT_SLOT_2 = 2;
	public static final int INPUT_SLOT_3 = 3;
	public static final int INPUT_SLOT_4 = 4;
	public static final int OUTPUT_SLOT = 5;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(6)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
		
		@Override
		public int getSlotLimit(int slot)
		{
			if(slot == INPUT_SLOT_1 || slot == INPUT_SLOT_2 || slot == INPUT_SLOT_3 || slot == INPUT_SLOT_4)
				return 16;
			
			return super.getSlotLimit(slot);
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Fuel = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == FUEL_SLOT && isItemFuel(stack), (slot, stack) -> slot == FUEL_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Fuel = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Inputs = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == INPUT_SLOT_1 || slot == INPUT_SLOT_2 || slot == INPUT_SLOT_3 || slot == INPUT_SLOT_4, (slot, stack) -> slot == INPUT_SLOT_1 || slot == INPUT_SLOT_2 || slot == INPUT_SLOT_3 || slot == INPUT_SLOT_4);
	private LazyOptional<IItemHandler> lazyItemHandler_Inputs = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Output = new LimitedItemStackHandler(itemHandler, (slot, stack) -> false, (slot, stack) -> slot == OUTPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Output = LazyOptional.empty();
	
	protected final ContainerData data;
	
	protected int heat;
	protected int minHeat;
	
	protected int burnTime;
	protected int neededBurnTime;
	
	protected int smeltTime;
	protected int neededSmeltTime;
	
	
	public SmelterBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.SMELTER.get(), pos, state);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> SmelterBlockEntity.this.heat;
					case 1 -> SmelterBlockEntity.this.minHeat;
					case 2 -> SmelterBlockEntity.this.burnTime;
					case 3 -> SmelterBlockEntity.this.neededBurnTime;
					case 4 -> SmelterBlockEntity.this.smeltTime;
					case 5 -> SmelterBlockEntity.this.neededSmeltTime;
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				switch(index)
				{
					case 0 -> SmelterBlockEntity.this.heat = value;
					case 1 -> SmelterBlockEntity.this.minHeat = value;
					case 2 -> SmelterBlockEntity.this.burnTime = value;
					case 3 -> SmelterBlockEntity.this.neededBurnTime = value;
					case 4 -> SmelterBlockEntity.this.smeltTime = value;
					case 5 -> SmelterBlockEntity.this.neededSmeltTime = value;
				}
			}
			
			@Override
			public int getCount()
			{
				return 6;
			}
		};
	}
	
	@Override
	public boolean canDoWork()
	{
		ItemStack input1 = itemHandler.getStackInSlot(INPUT_SLOT_1);
		ItemStack input2 = itemHandler.getStackInSlot(INPUT_SLOT_2);
		ItemStack input3 = itemHandler.getStackInSlot(INPUT_SLOT_3);
		ItemStack input4 = itemHandler.getStackInSlot(INPUT_SLOT_4);
		if(input1.isEmpty() || input2.isEmpty() || input3.isEmpty() || input4.isEmpty())
			return false;
		
		SmelterRecipe smelterRecipe = getRecipe();
		if(smelterRecipe == null)
			return false;
		
		if(heat < (MAX_HEAT * smelterRecipe.getMinHeat()))
			return false;
		
		ItemStack result = smelterRecipe.assemble(null, level.registryAccess());
		if(result.isEmpty())
			return false;
		
		ItemStack output = itemHandler.getStackInSlot(OUTPUT_SLOT);
		if(output.isEmpty())
			return true;
		
		if(!ItemStack.isSameItem(output, result))
			return false;
		
		int res = output.getCount() + result.getCount();
		return res <= 64 && res <= output.getMaxStackSize();
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".smelter");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new SmelterMenu(id, inventory, this, data);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
		{
			if(side == null)
				return lazyItemHandler.cast();
			
			if(side == Direction.UP)
				return lazyItemHandler_Inputs.cast();
			else if(side == Direction.DOWN)
				return lazyItemHandler_Output.cast();
			else
				return lazyItemHandler_Fuel.cast();
		}
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
		lazyItemHandler_Fuel = LazyOptional.of(() -> itemHandler_Fuel);
		lazyItemHandler_Inputs = LazyOptional.of(() -> itemHandler_Inputs);
		lazyItemHandler_Output = LazyOptional.of(() -> itemHandler_Output);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
		lazyItemHandler_Fuel.invalidate();
		lazyItemHandler_Inputs.invalidate();
		lazyItemHandler_Output.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		compoundTag.putInt("heat", heat);
		
		compoundTag.putInt("burnTime", burnTime);
		compoundTag.putInt("neededBurnTime", neededBurnTime);
		
		compoundTag.putInt("smeltTime", smeltTime);
		compoundTag.putInt("neededSmeltTime", neededSmeltTime);
		
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
		
		heat = compoundTag.getInt("heat");
		
		burnTime = compoundTag.getInt("burnTime");
		neededBurnTime = compoundTag.getInt("neededBurnTime");
		
		smeltTime = compoundTag.getInt("smeltTime");
		neededSmeltTime = compoundTag.getInt("neededSmeltTime");
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	public int getFuelValue(ItemStack fuel)
	{
		if(fuel.getItem() == Items.CHARCOAL)
			return 400;
		else if(fuel.getItem() == Items.COAL)
			return 600;
		else if(fuel.getItem() == Blocks.COAL_BLOCK.asItem())
			return getFuelValue(new ItemStack(Items.COAL)) * 9;
		
		return 0;
	}
	
	public boolean isItemFuel(ItemStack item)
	{
		return getFuelValue(item) > 0;
	}
	
	public boolean isBurning()
	{
		return burnTime > 0;
	}
	
	public boolean isBlockHeatGenerator(BlockState state)
	{
		Block block = state.getBlock();
		
		if((block == Blocks.TORCH || block == Blocks.WALL_TORCH) && heat < MAX_HEAT * 0.3f)
			return true;
		
		if((block == Blocks.FIRE || block == Blocks.CAMPFIRE || block == Blocks.SOUL_CAMPFIRE) && heat < MAX_HEAT * 0.5f)
			return true;
		
		if(block == Blocks.MAGMA_BLOCK && heat < MAX_HEAT * 0.55f)
			return true;
		
		return block == Blocks.LAVA && heat < MAX_HEAT * 0.65f;
	}
	
	public SmelterRecipe getRecipe()
	{
		SimpleContainer container = new SimpleContainer(itemHandler.getSlots());
		for(int i = 0; i < itemHandler.getSlots(); i++)
			container.setItem(i, itemHandler.getStackInSlot(i));
		
		Optional<RecipeHolder<SmelterRecipe>> recipe = level.getRecipeManager().getRecipeFor(ModRecipeTypes.SMELTER.get(), container, level);
		if(recipe.isPresent())
			return recipe.get().value();
		
		return null;
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, SmelterBlockEntity smelterBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		
		if(smelterBlockEntity.isBurning())
		{
			--smelterBlockEntity.burnTime;
			
			if(smelterBlockEntity.heat < MAX_HEAT)
				smelterBlockEntity.heat++;
		}
		
		if(!smelterBlockEntity.isBurning())
		{
			ItemStack fuel = smelterBlockEntity.itemHandler.getStackInSlot(FUEL_SLOT);
			if(!fuel.isEmpty() && smelterBlockEntity.isItemFuel(fuel))
			{
				hasChanged = true;
				
				smelterBlockEntity.burnTime = smelterBlockEntity.getFuelValue(fuel);
				smelterBlockEntity.neededBurnTime = smelterBlockEntity.burnTime;
				
				smelterBlockEntity.itemHandler.extractItem(FUEL_SLOT, 1, false);
			} else
			{
				if(smelterBlockEntity.heat < MAX_HEAT)
				{
					BlockState belowState = level.getBlockState(pos.below());
					if(smelterBlockEntity.isBlockHeatGenerator(belowState))
						smelterBlockEntity.heat++;
					else if(smelterBlockEntity.heat > 0)
						smelterBlockEntity.heat--;
				} else
					smelterBlockEntity.heat--;
			}
		}
		
		SmelterRecipe smelterRecipe = smelterBlockEntity.getRecipe();
		smelterBlockEntity.minHeat = smelterRecipe != null ? (int) (MAX_HEAT * smelterRecipe.getMinHeat()) : 0;
		
		if(smelterBlockEntity.canDoWork())
		{
			hasChanged = true;
			
			smelterBlockEntity.smeltTime++;
			
			if(smelterBlockEntity.heat == MAX_HEAT)
				smelterBlockEntity.smeltTime++;
			
			smelterBlockEntity.neededSmeltTime = smelterRecipe.getSmeltTime();
			if(smelterBlockEntity.smeltTime >= smelterBlockEntity.neededSmeltTime)
			{
				smelterBlockEntity.smeltTime = 0;
				
				ItemStack result = smelterRecipe.assemble(null, smelterBlockEntity.level.registryAccess());
				
				smelterBlockEntity.itemHandler.extractItem(INPUT_SLOT_1, 1, false);
				smelterBlockEntity.itemHandler.extractItem(INPUT_SLOT_2, 1, false);
				smelterBlockEntity.itemHandler.extractItem(INPUT_SLOT_3, 1, false);
				smelterBlockEntity.itemHandler.extractItem(INPUT_SLOT_4, 1, false);
				
				smelterBlockEntity.itemHandler.insertItem(OUTPUT_SLOT, result, false);
			}
		} else
			smelterBlockEntity.smeltTime = 0;
		
		if(state.getValue(SmelterBlock.LIT) != smelterBlockEntity.isBurning())
		{
			hasChanged = true;
			
			state = state.setValue(SmelterBlock.LIT, smelterBlockEntity.isBurning());
			level.setBlock(pos, state, 3);
		}
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
