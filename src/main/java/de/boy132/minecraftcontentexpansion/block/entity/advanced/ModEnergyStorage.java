package de.boy132.minecraftcontentexpansion.block.entity.advanced;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraftforge.energy.EnergyStorage;

public class ModEnergyStorage extends EnergyStorage
{
	protected int energyIO;
	private int lastEnergy;
	
	public ModEnergyStorage(int capacity)
	{
		super(capacity);
	}
	
	public ModEnergyStorage(int capacity, int maxTransfer)
	{
		super(capacity, maxTransfer);
	}
	
	public ModEnergyStorage(int capacity, int maxReceive, int maxExtract)
	{
		super(capacity, maxReceive, maxExtract);
	}
	
	public ModEnergyStorage(int capacity, int maxReceive, int maxExtract, int energy)
	{
		super(capacity, maxReceive, maxExtract, energy);
	}
	
	public int getEnergyIO()
	{
		return energyIO;
	}
	
	public void setEnergyIO()
	{
		this.energyIO = energy - lastEnergy;
		this.lastEnergy = energy;
	}
	
	public void setEnergyIO(int energyIO)
	{
		this.energyIO = energyIO;
	}
	
	public int addEnergy(int value)
	{
		int receivedEnergy = Math.min(capacity - energy, value);
		energy += receivedEnergy;
		
		if(receivedEnergy != 0)
			onEnergyChanged();
		
		return receivedEnergy;
	}
	
	public int getMaxReceive()
	{
		return Math.min(maxReceive, capacity - energy);
	}
	
	public int getMaxExtract()
	{
		return Math.min(energy, maxExtract);
	}
	
	protected void onEnergyChanged()
	{
	
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		int extractedEnergy = super.extractEnergy(maxExtract, simulate);
		if(extractedEnergy != 0)
			onEnergyChanged();
		return extractedEnergy;
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		int receivedEnergy = super.receiveEnergy(maxReceive, simulate);
		if(receivedEnergy != 0)
			onEnergyChanged();
		return receivedEnergy;
	}
	
	@Override
	public boolean canExtract()
	{
		return maxExtract > 0 && energy > 0;
	}
	
	@Override
	public boolean canReceive()
	{
		return maxReceive > 0 && capacity > 0 && energy < capacity;
	}
	
	@Override
	public Tag serializeNBT()
	{
		CompoundTag compoundTag = new CompoundTag();
		
		compoundTag.putInt("energy", energy);
		compoundTag.putInt("capacity", capacity);
		
		compoundTag.putInt("maxReceive", maxReceive);
		compoundTag.putInt("maxExtract", maxExtract);
		
		return compoundTag;
	}
	
	@Override
	public void deserializeNBT(Tag tag)
	{
		if(tag instanceof CompoundTag compoundTag)
		{
			energy = compoundTag.getInt("energy");
			capacity = compoundTag.getInt("capacity");
			
			maxReceive = compoundTag.getInt("maxReceive");
			maxExtract = compoundTag.getInt("maxExtract");
		} else
			throw new IllegalArgumentException("Can not deserialize to an instance that isn't the default implementation");
	}
}
