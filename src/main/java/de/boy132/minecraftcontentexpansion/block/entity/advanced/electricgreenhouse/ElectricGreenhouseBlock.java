package de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse;

import com.mojang.serialization.MapCodec;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedEntityBlock;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import org.jetbrains.annotations.Nullable;

import java.text.NumberFormat;
import java.util.List;

public class ElectricGreenhouseBlock extends AdvancedEntityBlock
{
	public static final MapCodec<ElectricGreenhouseBlock> CODEC = simpleCodec(ElectricGreenhouseBlock::new);
	
	public static final EnumProperty<SoilType> SOIL_TYPE = EnumProperty.create("soil_type", SoilType.class);
	
	public ElectricGreenhouseBlock(Properties properties)
	{
		super(properties);
		registerDefaultState(stateDefinition.any().setValue(SOIL_TYPE, SoilType.NONE));
	}
	
	@Override
	protected MapCodec<ElectricGreenhouseBlock> codec()
	{
		return CODEC;
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(SOIL_TYPE);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter blockGetter, List<Component> tooltip, TooltipFlag flag)
	{
		super.appendHoverText(stack, blockGetter, tooltip, flag);
		
		CompoundTag compound = stack.getTagElement("BlockEntityTag");
		if(compound == null)
		{
			NumberFormat numberFormat = NumberFormat.getIntegerInstance();
			tooltip.add(Component.literal("0 / " + numberFormat.format(ElectricGreenhouseBlockEntity.ENERGY_CAPACITY) + " FE"));
		}
	}
	
	@Override
	public int getFluidTankCapacity()
	{
		return ElectricGreenhouseBlockEntity.FLUID_CAPACITY;
	}
	
	@Nullable
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new ElectricGreenhouseBlockEntity(pos, state);
	}
	
	@Nullable
	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> entityType)
	{
		return createTickerHelper(entityType, ModBlockEntities.ELECTRIC_GREENHOUSE.get(), ElectricGreenhouseBlockEntity::tick);
	}
}
