package de.boy132.minecraftcontentexpansion.block.entity.advanced.electricbrewery;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.LimitedItemStackHandler;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.electricbrewery.ElectricBreweryMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.alchemy.PotionUtils;
import net.minecraft.world.item.alchemy.Potions;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.brewing.BrewingRecipeRegistry;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidType;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ElectricBreweryBlockEntity extends AdvancedBlockEntity
{
	public static final int ENERGY_CAPACITY = 100000;
	public static final int FLUID_CAPACITY = 10 * FluidType.BUCKET_VOLUME;
	
	public static final int STANDARD_BREW_TIME = 300;
	
	public static final int INPUT_SLOT = 0;
	public static final int BOTTLE_SLOT = 1;
	public static final int PROCESS_SLOTS_START = 2;
	
	public static final int PROCESS_SLOTS_COUNT = 6;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(PROCESS_SLOTS_START + PROCESS_SLOTS_COUNT)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Input = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == INPUT_SLOT, (slot, stack) -> slot == INPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Input = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Bottle = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == BOTTLE_SLOT && stack.getItem() == Items.GLASS_BOTTLE, (slot, stack) -> slot == BOTTLE_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Bottle = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_ProcessSlots = new LimitedItemStackHandler(itemHandler, (slot, stack) -> false, (slot, stack) -> slot >= PROCESS_SLOTS_START && !PotionUtils.getMobEffects(stack).isEmpty());
	private LazyOptional<IItemHandler> lazyItemHandler_ProcessSlots = LazyOptional.empty();
	
	protected final ContainerData data;
	
	protected int brewTime;
	protected int neededBrewTime = STANDARD_BREW_TIME;
	
	public ElectricBreweryBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.ELECTRIC_BREWERY.get(), pos, state);
		energy = new Energy(ENERGY_CAPACITY, 200, 0);
		fluid = new Fluid(FLUID_CAPACITY);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> ElectricBreweryBlockEntity.this.getEnergyStorage().getEnergyIO();
					case 1 -> ElectricBreweryBlockEntity.this.brewTime;
					case 2 -> ElectricBreweryBlockEntity.this.neededBrewTime;
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				switch(index)
				{
					case 0 -> ElectricBreweryBlockEntity.this.getEnergyStorage().setEnergyIO(value);
					case 1 -> ElectricBreweryBlockEntity.this.brewTime = value;
					case 2 -> ElectricBreweryBlockEntity.this.neededBrewTime = value;
				}
			}
			
			@Override
			public int getCount()
			{
				return 3;
			}
		};
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".electric_brewery");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new ElectricBreweryMenu(id, inventory, this, data);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
		{
			if(side == null)
				return lazyItemHandler.cast();
			
			if(side == Direction.UP)
				return lazyItemHandler_Input.cast();
			else if(side == Direction.DOWN)
				return lazyItemHandler_ProcessSlots.cast();
			else
				return lazyItemHandler_Bottle.cast();
		}
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
		lazyItemHandler_Input = LazyOptional.of(() -> itemHandler_Input);
		lazyItemHandler_Bottle = LazyOptional.of(() -> itemHandler_Bottle);
		lazyItemHandler_ProcessSlots = LazyOptional.of(() -> itemHandler_ProcessSlots);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
		lazyItemHandler_Input.invalidate();
		lazyItemHandler_Bottle.invalidate();
		lazyItemHandler_ProcessSlots.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		compoundTag.putInt("brewTime", brewTime);
		compoundTag.putInt("neededBrewTime", neededBrewTime);
		
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
		
		brewTime = compoundTag.getInt("brewTime");
		neededBrewTime = compoundTag.getInt("neededBrewTime");
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	@Override
	public boolean canDoWork()
	{
		ItemStack inputStack = itemHandler.getStackInSlot(INPUT_SLOT);
		if(inputStack.isEmpty())
			return false;
		
		if(getEnergyStorage().getEnergyStored() < 20)
			return false;
		
		for(int i = 0; i < PROCESS_SLOTS_COUNT; i++)
		{
			ItemStack processSlotStack = itemHandler.getStackInSlot(PROCESS_SLOTS_START + i);
			if(!processSlotStack.isEmpty() && BrewingRecipeRegistry.hasOutput(processSlotStack, inputStack))
				return true;
		}
		
		return false;
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, ElectricBreweryBlockEntity electricBreweryBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		
		ItemStack bottleStack = electricBreweryBlockEntity.itemHandler.getStackInSlot(BOTTLE_SLOT);
		if(!bottleStack.isEmpty() && bottleStack.getItem() == Items.GLASS_BOTTLE)
		{
			for(int i = 0; i < PROCESS_SLOTS_COUNT; i++)
			{
				bottleStack = electricBreweryBlockEntity.itemHandler.getStackInSlot(BOTTLE_SLOT);
				
				if(bottleStack.isEmpty() || bottleStack.getItem() != Items.GLASS_BOTTLE || electricBreweryBlockEntity.getFluidTank().getFluidAmount() < FluidType.BUCKET_VOLUME / 10)
					break;
				
				ItemStack processSlotStack = electricBreweryBlockEntity.itemHandler.getStackInSlot(PROCESS_SLOTS_START + i);
				if(processSlotStack.isEmpty())
				{
					electricBreweryBlockEntity.itemHandler.setStackInSlot(PROCESS_SLOTS_START + i, PotionUtils.setPotion(new ItemStack(Items.POTION), Potions.WATER));
					electricBreweryBlockEntity.getFluidTank().drain(FluidType.BUCKET_VOLUME / 10, IFluidHandler.FluidAction.EXECUTE);
					
					electricBreweryBlockEntity.itemHandler.extractItem(BOTTLE_SLOT, 1, false);
					
					hasChanged = true;
				}
			}
		}
		
		int consume = 0;
		if(electricBreweryBlockEntity.canDoWork() && !level.hasNeighborSignal(pos))
		{
			electricBreweryBlockEntity.neededBrewTime = STANDARD_BREW_TIME;
			
			electricBreweryBlockEntity.brewTime++;
			if(electricBreweryBlockEntity.brewTime >= electricBreweryBlockEntity.neededBrewTime)
			{
				electricBreweryBlockEntity.brewTime = 0;
				
				ItemStack ingredient = electricBreweryBlockEntity.itemHandler.getStackInSlot(INPUT_SLOT).copy();
				electricBreweryBlockEntity.itemHandler.getStackInSlot(INPUT_SLOT).shrink(1);
				
				for(int i = 0; i < PROCESS_SLOTS_COUNT; i++)
				{
					ItemStack processSlotStack = electricBreweryBlockEntity.itemHandler.getStackInSlot(PROCESS_SLOTS_START + i);
					if(BrewingRecipeRegistry.hasOutput(processSlotStack, ingredient))
					{
						ItemStack potionStack = BrewingRecipeRegistry.getOutput(processSlotStack, ingredient);
						electricBreweryBlockEntity.itemHandler.setStackInSlot(PROCESS_SLOTS_START + i, potionStack);
					}
				}
			}
			
			consume = 20;
			
			hasChanged = true;
		} else
			electricBreweryBlockEntity.brewTime = 0;
		
		AdvancedBlockEntity.handleTransfer(electricBreweryBlockEntity, -consume);
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
