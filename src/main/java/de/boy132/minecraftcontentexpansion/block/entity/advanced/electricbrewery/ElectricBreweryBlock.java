package de.boy132.minecraftcontentexpansion.block.entity.advanced.electricbrewery;

import com.mojang.serialization.MapCodec;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedEntityBlock;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import org.jetbrains.annotations.Nullable;

import java.text.NumberFormat;
import java.util.List;

public class ElectricBreweryBlock extends AdvancedEntityBlock
{
	public static final MapCodec<ElectricBreweryBlock> CODEC = simpleCodec(ElectricBreweryBlock::new);
	
	public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
	
	public ElectricBreweryBlock(Properties properties)
	{
		super(properties);
		registerDefaultState(stateDefinition.any().setValue(FACING, Direction.NORTH));
	}
	
	@Override
	protected MapCodec<ElectricBreweryBlock> codec()
	{
		return CODEC;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter blockGetter, List<Component> tooltip, TooltipFlag flag)
	{
		super.appendHoverText(stack, blockGetter, tooltip, flag);
		
		CompoundTag compound = stack.getTagElement("BlockEntityTag");
		if(compound == null)
		{
			NumberFormat numberFormat = NumberFormat.getIntegerInstance();
			tooltip.add(Component.literal("0 / " + numberFormat.format(ElectricBreweryBlockEntity.ENERGY_CAPACITY) + " FE"));
		}
	}
	
	@Override
	public int getFluidTankCapacity()
	{
		return ElectricBreweryBlockEntity.FLUID_CAPACITY;
	}
	
	@Nullable
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rotation)
	{
		return state.setValue(FACING, rotation.rotate(state.getValue(FACING)));
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mirror)
	{
		return state.rotate(mirror.getRotation(state.getValue(FACING)));
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(FACING);
	}
	
	@Nullable
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new ElectricBreweryBlockEntity(pos, state);
	}
	
	@Nullable
	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> entityType)
	{
		return createTickerHelper(entityType, ModBlockEntities.ELECTRIC_BREWERY.get(), ElectricBreweryBlockEntity::tick);
	}
}
