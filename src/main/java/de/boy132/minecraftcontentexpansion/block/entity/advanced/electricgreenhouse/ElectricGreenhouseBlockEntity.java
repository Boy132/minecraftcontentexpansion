package de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.LimitedItemStackHandler;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.recipe.greenhouse.GreenhouseRecipe;
import de.boy132.minecraftcontentexpansion.screen.electricgreenhouse.ElectricGreenhouseMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.RecipeHolder;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidType;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ElectricGreenhouseBlockEntity extends AdvancedBlockEntity
{
	public static final int ENERGY_CAPACITY = 200000;
	public static final int FLUID_CAPACITY = 10 * FluidType.BUCKET_VOLUME;
	
	public static final int NEEDED_GROW_TIME = 200 * 7;
	
	public static final int SOIL_SLOT = 0;
	public static final int INPUT_SLOTS_START = 1;
	
	public static final int INPUT_SLOTS_COUNT = 4;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(INPUT_SLOTS_START + INPUT_SLOTS_COUNT)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Soil = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == SOIL_SLOT, (slot, stack) -> slot == SOIL_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Soil = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Inputs = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot >= INPUT_SLOTS_START && itemHandler.getStackInSlot(slot).isEmpty() && (stack.is(Tags.Items.SEEDS) || stack.is(Tags.Items.CROPS) || stack.getItem() == Items.SWEET_BERRIES), (slot, stack) -> false);
	private LazyOptional<IItemHandler> lazyItemHandler_Inputs = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Outputs = new LimitedItemStackHandler(itemHandler, (slot, stack) -> false, (slot, stack) -> slot >= INPUT_SLOTS_START && stack.getCount() > 1 && (stack.is(Tags.Items.CROPS) || stack.getItem() == Items.SWEET_BERRIES));
	private LazyOptional<IItemHandler> lazyItemHandler_Outputs = LazyOptional.empty();
	
	protected final ContainerData data;
	
	protected int[] growTimes;
	
	public ElectricGreenhouseBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.ELECTRIC_GREENHOUSE.get(), pos, state);
		energy = new Energy(ENERGY_CAPACITY, 200, 0);
		fluid = new Fluid(FLUID_CAPACITY);
		
		growTimes = new int[4];
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> ElectricGreenhouseBlockEntity.this.getEnergyStorage().getEnergyIO();
					case 1 -> ElectricGreenhouseBlockEntity.this.growTimes[0];
					case 2 -> ElectricGreenhouseBlockEntity.this.growTimes[1];
					case 3 -> ElectricGreenhouseBlockEntity.this.growTimes[2];
					case 4 -> ElectricGreenhouseBlockEntity.this.growTimes[3];
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				switch(index)
				{
					case 0 -> ElectricGreenhouseBlockEntity.this.getEnergyStorage().setEnergyIO(value);
					case 1 -> ElectricGreenhouseBlockEntity.this.growTimes[0] = value;
					case 2 -> ElectricGreenhouseBlockEntity.this.growTimes[1] = value;
					case 3 -> ElectricGreenhouseBlockEntity.this.growTimes[2] = value;
					case 4 -> ElectricGreenhouseBlockEntity.this.growTimes[3] = value;
				}
			}
			
			@Override
			public int getCount()
			{
				return 5;
			}
		};
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".electric_greenhouse");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new ElectricGreenhouseMenu(id, inventory, this, data);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
		{
			if(side == null)
				return lazyItemHandler.cast();
			
			if(side == Direction.UP)
				return lazyItemHandler_Inputs.cast();
			else if(side == Direction.DOWN)
				return lazyItemHandler_Outputs.cast();
			else
				return lazyItemHandler_Soil.cast();
		}
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
		lazyItemHandler_Soil = LazyOptional.of(() -> itemHandler_Soil);
		lazyItemHandler_Inputs = LazyOptional.of(() -> itemHandler_Inputs);
		lazyItemHandler_Outputs = LazyOptional.of(() -> itemHandler_Outputs);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
		lazyItemHandler_Soil.invalidate();
		lazyItemHandler_Inputs.invalidate();
		lazyItemHandler_Outputs.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		compoundTag.putIntArray("growTimes", growTimes);
		
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
		
		growTimes = compoundTag.getIntArray("growTimes");
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	@Override
	public boolean canDoWork()
	{
		int fullInputs = 0;
		for(int i = 0; i < INPUT_SLOTS_COUNT; i++)
		{
			GreenhouseRecipe recipe = getRecipe(i);
			if(recipe != null)
				fullInputs++;
		}
		
		if(fullInputs == 0)
			return false;
		
		return getEnergyStorage().getEnergyStored() >= 20;
	}
	
	private SoilType getSoilType()
	{
		ItemStack soil = itemHandler.getStackInSlot(SOIL_SLOT);
		return SoilType.fromItemStack(soil);
	}
	
	public boolean isSoilValid(int slot)
	{
		ItemStack inputStack = itemHandler.getStackInSlot(INPUT_SLOTS_START + slot);
		if(inputStack.isEmpty())
			return true;
		
		GreenhouseRecipe recipe = getRecipe(slot);
		if(recipe == null)
			return true;
		
		return getSoilType() == recipe.getSoilType();
	}
	
	public GreenhouseRecipe getRecipe(int slot)
	{
		SimpleContainer container = new SimpleContainer(1);
		container.setItem(0, itemHandler.getStackInSlot(INPUT_SLOTS_START + slot));
		
		Optional<RecipeHolder<GreenhouseRecipe>> recipe = level.getRecipeManager().getRecipeFor(ModRecipeTypes.GREENHOUSE.get(), container, level);
		if(recipe.isPresent())
			return recipe.get().value();
		
		return null;
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, ElectricGreenhouseBlockEntity electricGreenhouseBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		int consume = 0;
		if(!level.hasNeighborSignal(pos))
		{
			for(int i = 0; i < INPUT_SLOTS_COUNT; i++)
			{
				if(!electricGreenhouseBlockEntity.canDoWork())
				{
					electricGreenhouseBlockEntity.growTimes[i] = 0;
					continue;
				}
				
				ItemStack inputStack = electricGreenhouseBlockEntity.itemHandler.getStackInSlot(INPUT_SLOTS_START + i);
				if(!inputStack.isEmpty() && inputStack.getCount() == 1)
				{
					GreenhouseRecipe recipe = electricGreenhouseBlockEntity.getRecipe(i);
					if(recipe != null && electricGreenhouseBlockEntity.isSoilValid(i))
					{
						consume += 20;
						
						electricGreenhouseBlockEntity.growTimes[i]++;
						
						if(electricGreenhouseBlockEntity.getFluidTank().getFluidAmount() >= FluidType.BUCKET_VOLUME / 200)
						{
							electricGreenhouseBlockEntity.getFluidTank().drain(FluidType.BUCKET_VOLUME / 200, IFluidHandler.FluidAction.EXECUTE);
							electricGreenhouseBlockEntity.growTimes[i]++;
						}
						
						if(electricGreenhouseBlockEntity.growTimes[i] >= NEEDED_GROW_TIME)
						{
							electricGreenhouseBlockEntity.growTimes[i] = 0;
							
							ItemStack result = recipe.assemble(null, electricGreenhouseBlockEntity.level.registryAccess());
							electricGreenhouseBlockEntity.itemHandler.setStackInSlot(INPUT_SLOTS_START + i, result);
						}
						
						hasChanged = true;
					} else
						electricGreenhouseBlockEntity.growTimes[i] = 0;
				} else
					electricGreenhouseBlockEntity.growTimes[i] = 0;
			}
		}
		
		AdvancedBlockEntity.handleTransfer(electricGreenhouseBlockEntity, -consume);
		
		if(state.getValue(ElectricGreenhouseBlock.SOIL_TYPE) != electricGreenhouseBlockEntity.getSoilType())
		{
			hasChanged = true;
			
			state = state.setValue(ElectricGreenhouseBlock.SOIL_TYPE, electricGreenhouseBlockEntity.getSoilType());
			level.setBlock(pos, state, 3);
		}
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
