package de.boy132.minecraftcontentexpansion.block.entity.kiln;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.BaseBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.LimitedItemStackHandler;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.recipe.kiln.KilnRecipe;
import de.boy132.minecraftcontentexpansion.screen.kiln.KilnMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.RecipeHolder;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class KilnBlockEntity extends BaseBlockEntity
{
	public static final int STANDARD_SMELT_TIME = 200;
	
	public static final int FUEL_SLOT = 0;
	public static final int INPUT_SLOT = 1;
	public static final int OUTPUT_SLOT = 2;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(3)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Fuel = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == FUEL_SLOT && isItemFuel(stack), (slot, stack) -> slot == FUEL_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Fuel = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Input = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == INPUT_SLOT, (slot, stack) -> slot == INPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Input = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Output = new LimitedItemStackHandler(itemHandler, (slot, stack) -> false, (slot, stack) -> slot == OUTPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Output = LazyOptional.empty();
	
	protected final ContainerData data;
	
	protected int burnTime;
	protected int neededBurnTime;
	
	protected int smeltTime;
	protected int neededSmeltTime = STANDARD_SMELT_TIME;
	
	public KilnBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.KILN.get(), pos, state);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> KilnBlockEntity.this.burnTime;
					case 1 -> KilnBlockEntity.this.neededBurnTime;
					case 2 -> KilnBlockEntity.this.smeltTime;
					case 3 -> KilnBlockEntity.this.neededSmeltTime;
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				switch(index)
				{
					case 0 -> KilnBlockEntity.this.burnTime = value;
					case 1 -> KilnBlockEntity.this.neededBurnTime = value;
					case 2 -> KilnBlockEntity.this.smeltTime = value;
					case 3 -> KilnBlockEntity.this.neededSmeltTime = value;
				}
			}
			
			@Override
			public int getCount()
			{
				return 4;
			}
		};
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".kiln");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new KilnMenu(id, inventory, this, data);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
		{
			if(side == null)
				return lazyItemHandler.cast();
			
			if(side == Direction.UP)
				return lazyItemHandler_Input.cast();
			else if(side == Direction.DOWN)
				return lazyItemHandler_Output.cast();
			else
				return lazyItemHandler_Fuel.cast();
		}
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
		lazyItemHandler_Fuel = LazyOptional.of(() -> itemHandler_Fuel);
		lazyItemHandler_Input = LazyOptional.of(() -> itemHandler_Input);
		lazyItemHandler_Output = LazyOptional.of(() -> itemHandler_Output);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
		lazyItemHandler_Fuel.invalidate();
		lazyItemHandler_Input.invalidate();
		lazyItemHandler_Output.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		compoundTag.putInt("burnTime", burnTime);
		compoundTag.putInt("neededBurnTime", neededBurnTime);
		
		compoundTag.putInt("smeltTime", smeltTime);
		
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
		
		burnTime = compoundTag.getInt("burnTime");
		neededBurnTime = compoundTag.getInt("neededBurnTime");
		
		smeltTime = compoundTag.getInt("smeltTime");
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	public int getFuelValue(ItemStack fuel)
	{
		if(fuel.getItem() == Items.CHARCOAL)
			return 800;
		else if(fuel.getItem() == Items.COAL)
			return 1000;
		else if(fuel.getItem() == Blocks.COAL_BLOCK.asItem())
			return getFuelValue(new ItemStack(Items.COAL)) * 9;
		
		return 0;
	}
	
	public boolean isItemFuel(ItemStack item)
	{
		return getFuelValue(item) > 0;
	}
	
	public boolean isBurning()
	{
		return burnTime > 0;
	}
	
	@Override
	public boolean canDoWork()
	{
		ItemStack input = itemHandler.getStackInSlot(INPUT_SLOT);
		if(input.isEmpty())
			return false;
		
		KilnRecipe kilnRecipe = getRecipe();
		if(kilnRecipe == null)
			return false;
		
		ItemStack result = kilnRecipe.assemble(null, level.registryAccess());
		if(result.isEmpty())
			return false;
		
		ItemStack output = itemHandler.getStackInSlot(OUTPUT_SLOT);
		if(output.isEmpty())
			return true;
		
		if(!ItemStack.isSameItem(output, result))
			return false;
		
		int res = output.getCount() + result.getCount();
		return res <= 64 && res <= output.getMaxStackSize();
	}
	
	public KilnRecipe getRecipe()
	{
		SimpleContainer container = new SimpleContainer(itemHandler.getSlots());
		for(int i = 0; i < itemHandler.getSlots(); i++)
			container.setItem(i, itemHandler.getStackInSlot(i));
		
		Optional<RecipeHolder<KilnRecipe>> recipe = level.getRecipeManager().getRecipeFor(ModRecipeTypes.KILN.get(), container, level);
		if(recipe.isPresent())
			return recipe.get().value();
		
		return null;
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, KilnBlockEntity kilnBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		
		if(kilnBlockEntity.isBurning())
			--kilnBlockEntity.burnTime;
		
		if(kilnBlockEntity.canDoWork())
		{
			if(kilnBlockEntity.isBurning())
			{
				hasChanged = true;
				
				kilnBlockEntity.smeltTime++;
				
				KilnRecipe kilnRecipe = kilnBlockEntity.getRecipe();
				kilnBlockEntity.neededSmeltTime = kilnRecipe.getSmeltTime();
				if(kilnBlockEntity.smeltTime >= kilnBlockEntity.neededSmeltTime)
				{
					kilnBlockEntity.smeltTime = 0;
					
					ItemStack result = kilnRecipe.assemble(null, kilnBlockEntity.level.registryAccess());
					
					kilnBlockEntity.itemHandler.extractItem(INPUT_SLOT, 1, false);
					kilnBlockEntity.itemHandler.insertItem(OUTPUT_SLOT, result, false);
				}
			} else if(!kilnBlockEntity.itemHandler.getStackInSlot(FUEL_SLOT).isEmpty() && !kilnBlockEntity.itemHandler.getStackInSlot(INPUT_SLOT).isEmpty())
			{
				hasChanged = true;
				
				kilnBlockEntity.burnTime = kilnBlockEntity.getFuelValue(kilnBlockEntity.itemHandler.getStackInSlot(FUEL_SLOT));
				kilnBlockEntity.neededBurnTime = kilnBlockEntity.burnTime;
				
				kilnBlockEntity.itemHandler.extractItem(FUEL_SLOT, 1, false);
			} else
				kilnBlockEntity.smeltTime = 0;
		} else
			kilnBlockEntity.smeltTime = 0;
		
		if(state.getValue(KilnBlock.LIT) != kilnBlockEntity.isBurning())
		{
			hasChanged = true;
			
			state = state.setValue(KilnBlock.LIT, kilnBlockEntity.isBurning());
			level.setBlock(pos, state, 3);
		}
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
