package de.boy132.minecraftcontentexpansion.block.entity.dryingrack;

import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.recipe.dryingrack.DryingRackRecipe;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeHolder;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class DryingRackBlockEntity extends BlockEntity
{
	public static final int STANDARD_DRYING_TIME = 2000;
	
	public static final int SLOT = 0;
	
	protected final ContainerData data;
	
	protected int dryingTicks;
	protected int neededDryingTicks = STANDARD_DRYING_TIME;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(1)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
			if(level != null)
				level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 2);
		}
		
		@Override
		protected int getStackLimit(int slot, @NotNull ItemStack stack)
		{
			return 1;
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	public DryingRackBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.DRYING_RACK.get(), pos, state);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> DryingRackBlockEntity.this.dryingTicks;
					case 1 -> DryingRackBlockEntity.this.neededDryingTicks;
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				switch(index)
				{
					case 0 -> DryingRackBlockEntity.this.dryingTicks = value;
					case 1 -> DryingRackBlockEntity.this.neededDryingTicks = value;
				}
			}
			
			@Override
			public int getCount()
			{
				return 2;
			}
		};
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
			return lazyItemHandler.cast();
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		compoundTag.putInt("dryingTicks", dryingTicks);
		compoundTag.putInt("neededDryingTicks", neededDryingTicks);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
		
		dryingTicks = compoundTag.getInt("dryingTicks");
		neededDryingTicks = compoundTag.getInt("neededDryingTicks");
	}
	
	@Override
	public CompoundTag getUpdateTag()
	{
		CompoundTag compoundTag = super.getUpdateTag();
		saveAdditional(compoundTag);
		return compoundTag;
	}
	
	@Override
	public void handleUpdateTag(CompoundTag compoundTag)
	{
		super.handleUpdateTag(compoundTag);
		load(compoundTag);
	}
	
	@Nullable
	@Override
	public Packet<ClientGamePacketListener> getUpdatePacket()
	{
		return ClientboundBlockEntityDataPacket.create(this);
	}
	
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	public boolean canDoWork()
	{
		ItemStack input = itemHandler.getStackInSlot(SLOT);
		if(input.isEmpty())
			return false;
		
		DryingRackRecipe dryingRackRecipe = getRecipe();
		if(dryingRackRecipe == null)
			return false;
		
		ItemStack result = dryingRackRecipe.assemble(null, level.registryAccess());
		return !result.isEmpty();
	}
	
	public DryingRackRecipe getRecipe()
	{
		SimpleContainer container = new SimpleContainer(itemHandler.getSlots());
		for(int i = 0; i < itemHandler.getSlots(); i++)
			container.setItem(i, itemHandler.getStackInSlot(i));
		
		Optional<RecipeHolder<DryingRackRecipe>> recipe = level.getRecipeManager().getRecipeFor(ModRecipeTypes.DRYING_RACK.get(), container, level);
		if(recipe.isPresent())
			return recipe.get().value();
		
		return null;
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, DryingRackBlockEntity dryingRackBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		
		if(dryingRackBlockEntity.canDoWork())
		{
			hasChanged = true;
			
			dryingRackBlockEntity.dryingTicks++;
			
			DryingRackRecipe dryingRackRecipe = dryingRackBlockEntity.getRecipe();
			dryingRackBlockEntity.neededDryingTicks = dryingRackRecipe.getDryingTime();
			if(dryingRackBlockEntity.dryingTicks >= dryingRackBlockEntity.neededDryingTicks)
			{
				dryingRackBlockEntity.dryingTicks = 0;
				
				ItemStack result = dryingRackRecipe.assemble(null, dryingRackBlockEntity.level.registryAccess());
				dryingRackBlockEntity.itemHandler.setStackInSlot(SLOT, result);
			}
		} else
			dryingRackBlockEntity.dryingTicks = 0;
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
