package de.boy132.minecraftcontentexpansion.block.entity.millstone;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.BaseBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.LimitedItemStackHandler;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.recipe.millstone.MillstoneRecipe;
import de.boy132.minecraftcontentexpansion.screen.millstone.MillstoneMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeHolder;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class MillstoneBlockEntity extends BaseBlockEntity
{
	public static final int STANDARD_MILL_TIME = 400;
	
	public static final int INPUT_SLOT = 0;
	public static final int OUTPUT_SLOT = 1;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(3)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Input = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == INPUT_SLOT, (slot, stack) -> slot == INPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Input = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Output = new LimitedItemStackHandler(itemHandler, (slot, stack) -> false, (slot, stack) -> slot == OUTPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Output = LazyOptional.empty();
	
	protected final ContainerData data;
	
	protected int millTime;
	protected int neededMillTime = STANDARD_MILL_TIME;
	
	public MillstoneBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.MILLSTONE.get(), pos, state);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> MillstoneBlockEntity.this.millTime;
					case 1 -> MillstoneBlockEntity.this.neededMillTime;
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				switch(index)
				{
					case 0 -> MillstoneBlockEntity.this.millTime = value;
					case 1 -> MillstoneBlockEntity.this.neededMillTime = value;
				}
			}
			
			@Override
			public int getCount()
			{
				return 2;
			}
		};
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".millstone");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new MillstoneMenu(id, inventory, this, data);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
		{
			if(side == null)
				return lazyItemHandler.cast();
			
			if(side == Direction.DOWN)
				return lazyItemHandler_Output.cast();
			else
				return lazyItemHandler_Input.cast();
		}
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
		lazyItemHandler_Input = LazyOptional.of(() -> itemHandler_Input);
		lazyItemHandler_Output = LazyOptional.of(() -> itemHandler_Output);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
		lazyItemHandler_Input.invalidate();
		lazyItemHandler_Output.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		compoundTag.putInt("millTime", millTime);
		compoundTag.putInt("neededMillTime", neededMillTime);
		
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
		
		millTime = compoundTag.getInt("millTime");
		neededMillTime = compoundTag.getInt("neededMillTime");
		
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	@Override
	public boolean canDoWork()
	{
		if(getSurroundingWater() <= 0)
			return false;
		
		ItemStack input = itemHandler.getStackInSlot(INPUT_SLOT);
		if(input.isEmpty())
			return false;
		
		MillstoneRecipe millstoneRecipe = getRecipe();
		if(millstoneRecipe == null)
			return false;
		
		ItemStack result = millstoneRecipe.assemble(null, level.registryAccess());
		if(result.isEmpty())
			return false;
		
		ItemStack output = itemHandler.getStackInSlot(OUTPUT_SLOT);
		if(output.isEmpty())
			return true;
		
		if(!ItemStack.isSameItem(output, result))
			return false;
		
		int res = output.getCount() + result.getCount();
		return res <= 64 && res <= output.getMaxStackSize();
	}
	
	public int getSurroundingWater()
	{
		int waterCount = 0;
		for(Direction direction : Direction.values())
		{
			if(direction == Direction.UP || direction == Direction.DOWN)
				continue;
			
			BlockPos targetPos = worldPosition.relative(direction);
			BlockState targetState = level.getBlockState(targetPos);
			Block targetBlock = targetState.getBlock();
			
			if(targetBlock instanceof LiquidBlock liquidBlock)
			{
				Fluid fluid = liquidBlock.getFluid();
				if(fluid.is(FluidTags.WATER))
					waterCount++;
			}
		}
		return waterCount;
	}
	
	public MillstoneRecipe getRecipe()
	{
		SimpleContainer container = new SimpleContainer(itemHandler.getSlots());
		for(int i = 0; i < itemHandler.getSlots(); i++)
			container.setItem(i, itemHandler.getStackInSlot(i));
		
		Optional<RecipeHolder<MillstoneRecipe>> recipe = level.getRecipeManager().getRecipeFor(ModRecipeTypes.MILLSTONE.get(), container, level);
		if(recipe.isPresent())
			return recipe.get().value();
		
		return null;
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, MillstoneBlockEntity millstoneBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		
		if(millstoneBlockEntity.canDoWork())
		{
			hasChanged = true;
			
			millstoneBlockEntity.millTime++;
			
			MillstoneRecipe millstoneRecipe = millstoneBlockEntity.getRecipe();
			millstoneBlockEntity.neededMillTime = millstoneRecipe.getMillTime();
			if(millstoneBlockEntity.millTime == millstoneBlockEntity.neededMillTime)
			{
				millstoneBlockEntity.millTime = 0;
				
				ItemStack result = millstoneRecipe.assemble(null, millstoneBlockEntity.level.registryAccess());
				
				millstoneBlockEntity.itemHandler.extractItem(INPUT_SLOT, 1, false);
				millstoneBlockEntity.itemHandler.insertItem(OUTPUT_SLOT, result, false);
			}
		} else
			millstoneBlockEntity.millTime = 0;
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
