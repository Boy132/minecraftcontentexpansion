package de.boy132.minecraftcontentexpansion.block.entity.advanced.liquidtank;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.LimitedItemStackHandler;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.liquidtank.LiquidTankMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.BucketItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidType;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class LiquidTankBlockEntity extends AdvancedBlockEntity
{
	public static final int CAPACITY = 10 * FluidType.BUCKET_VOLUME;
	
	public static final int INPUT_SLOT = 0;
	public static final int OUTPUT_SLOT = 1;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(2)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Input = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == INPUT_SLOT, (slot, stack) -> slot == INPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Input = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Output = new LimitedItemStackHandler(itemHandler, (slot, stack) -> false, (slot, stack) -> slot == OUTPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Output = LazyOptional.empty();
	
	public LiquidTankBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.LIQUID_TANK.get(), pos, state);
		fluid = new Fluid(CAPACITY);
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".liquid_tank");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new LiquidTankMenu(id, inventory, this);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
		{
			if(side == null)
				return lazyItemHandler.cast();
			
			if(side == Direction.DOWN)
				return lazyItemHandler_Output.cast();
			else
				return lazyItemHandler_Input.cast();
		}
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
		lazyItemHandler_Input = LazyOptional.of(() -> itemHandler_Input);
		lazyItemHandler_Output = LazyOptional.of(() -> itemHandler_Output);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
		lazyItemHandler_Input.invalidate();
		lazyItemHandler_Output.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	@Override
	public boolean canDoWork()
	{
		return isItemValidForFluid(getFluidTank(), itemHandler.getStackInSlot(INPUT_SLOT)) && itemHandler.getStackInSlot(OUTPUT_SLOT).getCount() + 1 <= itemHandler.getStackInSlot(OUTPUT_SLOT).getMaxStackSize();
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, LiquidTankBlockEntity liquidTankBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		
		AdvancedBlockEntity.handleTransfer(liquidTankBlockEntity, 0);
		
		if(liquidTankBlockEntity.canDoWork())
		{
			BucketItem bucketItem = (BucketItem) liquidTankBlockEntity.itemHandler.getStackInSlot(INPUT_SLOT).getItem();
			if(bucketItem.getFluid() != Fluids.EMPTY)
			{
				liquidTankBlockEntity.getFluidTank().fill(new FluidStack(bucketItem.getFluid(), FluidType.BUCKET_VOLUME), IFluidHandler.FluidAction.EXECUTE);
				
				liquidTankBlockEntity.itemHandler.extractItem(INPUT_SLOT, 1, false);
				liquidTankBlockEntity.itemHandler.insertItem(OUTPUT_SLOT, new ItemStack(Items.BUCKET, 1), false);
				
				hasChanged = true;
			} else
			{
				ItemStack newStack = new ItemStack(liquidTankBlockEntity.getFluidTank().getFluid().getFluid().getBucket());
				if(liquidTankBlockEntity.itemHandler.getStackInSlot(OUTPUT_SLOT).isEmpty() || newStack.getItem() == liquidTankBlockEntity.itemHandler.getStackInSlot(OUTPUT_SLOT).getItem())
				{
					liquidTankBlockEntity.itemHandler.extractItem(INPUT_SLOT, 1, false);
					
					liquidTankBlockEntity.itemHandler.insertItem(OUTPUT_SLOT, newStack, false);
					liquidTankBlockEntity.getFluidTank().drain(FluidType.BUCKET_VOLUME, IFluidHandler.FluidAction.EXECUTE);
					
					hasChanged = true;
				}
			}
		}
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
