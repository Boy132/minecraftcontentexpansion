package de.boy132.minecraftcontentexpansion.block.entity.dryingrack;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraftforge.common.capabilities.ForgeCapabilities;

public class DryingRackBlockEntityRenderer implements BlockEntityRenderer<DryingRackBlockEntity>
{
	private final BlockEntityRendererProvider.Context context;
	
	public DryingRackBlockEntityRenderer(BlockEntityRendererProvider.Context context)
	{
		this.context = context;
	}
	
	@Override
	public void render(DryingRackBlockEntity blockEntity, float partialTick, PoseStack poseStack, MultiBufferSource buffer, int packedLight, int packedOverlay)
	{
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent(itemHandler ->
		{
			ItemStack stack = itemHandler.getStackInSlot(DryingRackBlockEntity.SLOT);
			if(!stack.isEmpty())
			{
				poseStack.pushPose();
				
				switch(blockEntity.getBlockState().getValue(BlockStateProperties.HORIZONTAL_FACING))
				{
					case NORTH ->
					{
						poseStack.translate(0.5f, 0.5f, 0.25f);
						poseStack.mulPose(Axis.YP.rotationDegrees(180f));
					}
					case EAST ->
					{
						poseStack.translate(0.75f, 0.5f, 0.5f);
						poseStack.mulPose(Axis.YP.rotationDegrees(90f));
					}
					case SOUTH ->
					{
						poseStack.translate(0.5f, 0.5f, 0.75f);
					}
					case WEST ->
					{
						poseStack.translate(0.25f, 0.5f, 0.5f);
						poseStack.mulPose(Axis.YP.rotationDegrees(-90f));
					}
				}
				
				ItemRenderer itemRenderer = context.getItemRenderer();
				itemRenderer.renderStatic(stack, ItemDisplayContext.NONE, packedLight, packedOverlay, poseStack, buffer, blockEntity.getLevel(), 0);
				
				poseStack.popPose();
			}
		});
	}
}
