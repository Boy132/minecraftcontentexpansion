package de.boy132.minecraftcontentexpansion.block.entity.advanced.solarpanel;

import com.mojang.serialization.MapCodec;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedEntityBlock;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.jetbrains.annotations.Nullable;

import java.text.NumberFormat;
import java.util.List;

public class SolarPanelBlock extends AdvancedEntityBlock
{
	public static final MapCodec<SolarPanelBlock> CODEC = simpleCodec(SolarPanelBlock::new);
	
	public SolarPanelBlock(Properties properties)
	{
		super(properties);
	}
	
	@Override
	protected MapCodec<SolarPanelBlock> codec()
	{
		return CODEC;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter blockGetter, List<Component> tooltip, TooltipFlag flag)
	{
		super.appendHoverText(stack, blockGetter, tooltip, flag);
		
		CompoundTag compound = stack.getTagElement("BlockEntityTag");
		if(compound == null)
		{
			NumberFormat numberFormat = NumberFormat.getIntegerInstance();
			tooltip.add(Component.literal("0 / " + numberFormat.format(SolarPanelBlockEntity.CAPACITY) + " FE"));
		}
	}
	
	protected static final VoxelShape SHAPE = Block.box(0, 0, 0, 16, 8, 16);
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter getter, BlockPos pos, CollisionContext context)
	{
		return SHAPE;
	}
	
	@Nullable
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new SolarPanelBlockEntity(pos, state);
	}
	
	@Nullable
	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> entityType)
	{
		return createTickerHelper(entityType, ModBlockEntities.SOLAR_PANEL.get(), SolarPanelBlockEntity::tick);
	}
	
	@Override
	public int getFluidTankCapacity()
	{
		return 0;
	}
}
