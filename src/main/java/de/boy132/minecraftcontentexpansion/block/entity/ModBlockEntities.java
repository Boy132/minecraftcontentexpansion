package de.boy132.minecraftcontentexpansion.block.entity;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.battery.BatteryBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.coalgenerator.CoalGeneratorBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricbrewery.ElectricBreweryBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.ElectricGreenhouseBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricsmelter.ElectricSmelterBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.hydraulicpress.HydraulicPressBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.liquidtank.LiquidTankBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.solarpanel.SolarPanelBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.choppingblock.ChoppingBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.dryingrack.DryingRackBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.kiln.KilnBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.millstone.MillstoneBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.smelter.SmelterBlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModBlockEntities
{
	private static final DeferredRegister<BlockEntityType<?>> ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<BlockEntityType<KilnBlockEntity>> KILN = ENTITY_TYPES.register("kiln", () -> BlockEntityType.Builder.of(KilnBlockEntity::new, ModBlocks.KILN.get()).build(null));
	public static final RegistryObject<BlockEntityType<SmelterBlockEntity>> SMELTER = ENTITY_TYPES.register("smelter", () -> BlockEntityType.Builder.of(SmelterBlockEntity::new, ModBlocks.SMELTER.get()).build(null));
	public static final RegistryObject<BlockEntityType<MillstoneBlockEntity>> MILLSTONE = ENTITY_TYPES.register("millstone", () -> BlockEntityType.Builder.of(MillstoneBlockEntity::new, ModBlocks.MILLSTONE.get()).build(null));
	
	public static final RegistryObject<BlockEntityType<ChoppingBlockEntity>> CHOPPING_BLOCK = ENTITY_TYPES.register("chopping_block", () -> BlockEntityType.Builder.of(ChoppingBlockEntity::new, ModBlocks.OAK_CHOPPING_BLOCK.get(), ModBlocks.SPRUCE_CHOPPING_BLOCK.get(), ModBlocks.BIRCH_CHOPPING_BLOCK.get(), ModBlocks.JUNGLE_CHOPPING_BLOCK.get(), ModBlocks.ACACIA_CHOPPING_BLOCK.get(), ModBlocks.DARK_OAK_CHOPPING_BLOCK.get(), ModBlocks.MANGROVE_CHOPPING_BLOCK.get(), ModBlocks.CHERRY_CHOPPING_BLOCK.get(), ModBlocks.CRIMSON_CHOPPING_BLOCK.get(), ModBlocks.WARPED_CHOPPING_BLOCK.get()).build(null));
	
	public static final RegistryObject<BlockEntityType<DryingRackBlockEntity>> DRYING_RACK = ENTITY_TYPES.register("drying_rack", () -> BlockEntityType.Builder.of(DryingRackBlockEntity::new, ModBlocks.OAK_DRYING_RACK.get(), ModBlocks.SPRUCE_DRYING_RACK.get(), ModBlocks.BIRCH_DRYING_RACK.get(), ModBlocks.JUNGLE_DRYING_RACK.get(), ModBlocks.ACACIA_DRYING_RACK.get(), ModBlocks.DARK_OAK_DRYING_RACK.get(), ModBlocks.MANGROVE_DRYING_RACK.get(), ModBlocks.CHERRY_DRYING_RACK.get(), ModBlocks.CRIMSON_DRYING_RACK.get(), ModBlocks.WARPED_DRYING_RACK.get()).build(null));
	
	public static final RegistryObject<BlockEntityType<CoalGeneratorBlockEntity>> COAL_GENERATOR = ENTITY_TYPES.register("coal_generator", () -> BlockEntityType.Builder.of(CoalGeneratorBlockEntity::new, ModBlocks.COAL_GENERATOR.get()).build(null));
	public static final RegistryObject<BlockEntityType<BatteryBlockEntity>> BATTERY = ENTITY_TYPES.register("battery", () -> BlockEntityType.Builder.of(BatteryBlockEntity::new, ModBlocks.BATTERY.get()).build(null));
	public static final RegistryObject<BlockEntityType<SolarPanelBlockEntity>> SOLAR_PANEL = ENTITY_TYPES.register("solar_panel", () -> BlockEntityType.Builder.of(SolarPanelBlockEntity::new, ModBlocks.SOLAR_PANEL.get()).build(null));
	
	public static final RegistryObject<BlockEntityType<LiquidTankBlockEntity>> LIQUID_TANK = ENTITY_TYPES.register("liquid_tank", () -> BlockEntityType.Builder.of(LiquidTankBlockEntity::new, ModBlocks.LIQUID_TANK.get()).build(null));
	
	public static final RegistryObject<BlockEntityType<ElectricBreweryBlockEntity>> ELECTRIC_BREWERY = ENTITY_TYPES.register("electric_brewery", () -> BlockEntityType.Builder.of(ElectricBreweryBlockEntity::new, ModBlocks.ELECTRIC_BREWERY.get()).build(null));
	public static final RegistryObject<BlockEntityType<ElectricSmelterBlockEntity>> ELECTRIC_SMELTER = ENTITY_TYPES.register("electric_smelter", () -> BlockEntityType.Builder.of(ElectricSmelterBlockEntity::new, ModBlocks.ELECTRIC_SMELTER.get()).build(null));
	public static final RegistryObject<BlockEntityType<ElectricGreenhouseBlockEntity>> ELECTRIC_GREENHOUSE = ENTITY_TYPES.register("electric_greenhouse", () -> BlockEntityType.Builder.of(ElectricGreenhouseBlockEntity::new, ModBlocks.ELECTRIC_GREENHOUSE.get()).build(null));
	
	public static final RegistryObject<BlockEntityType<HydraulicPressBlockEntity>> HYDRAULIC_PRESS = ENTITY_TYPES.register("hydraulic_press", () -> BlockEntityType.Builder.of(HydraulicPressBlockEntity::new, ModBlocks.HYDRAULIC_PRESS.get()).build(null));
	
	public static void register(IEventBus eventBus)
	{
		ENTITY_TYPES.register(eventBus);
	}
}
