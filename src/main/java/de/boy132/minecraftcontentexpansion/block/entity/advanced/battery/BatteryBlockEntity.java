package de.boy132.minecraftcontentexpansion.block.entity.advanced.battery;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.LimitedItemStackHandler;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import de.boy132.minecraftcontentexpansion.screen.battery.BatteryMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BatteryBlockEntity extends AdvancedBlockEntity
{
	public static final int CAPACITY = 200000;
	
	public static final int INPUT_SLOT = 0;
	public static final int OUTPUT_SLOT = 1;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(2)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Input = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == INPUT_SLOT, (slot, stack) -> slot == INPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Input = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Output = new LimitedItemStackHandler(itemHandler, (slot, stack) -> false, (slot, stack) -> slot == OUTPUT_SLOT);
	private LazyOptional<IItemHandler> lazyItemHandler_Output = LazyOptional.empty();
	
	protected final ContainerData data;
	
	public BatteryBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.BATTERY.get(), pos, state);
		energy = new Energy(CAPACITY, 1000);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> BatteryBlockEntity.this.getEnergyStorage().getEnergyIO();
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				if(index == 0)
					BatteryBlockEntity.this.getEnergyStorage().setEnergyIO(value);
			}
			
			@Override
			public int getCount()
			{
				return 1;
			}
		};
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".battery");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new BatteryMenu(id, inventory, this, data);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
		{
			if(side == null)
				return lazyItemHandler.cast();
			
			if(side == Direction.DOWN)
				return lazyItemHandler_Output.cast();
			else
				return lazyItemHandler_Input.cast();
		}
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
		lazyItemHandler_Input = LazyOptional.of(() -> itemHandler_Input);
		lazyItemHandler_Output = LazyOptional.of(() -> itemHandler_Output);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
		lazyItemHandler_Input.invalidate();
		lazyItemHandler_Output.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	@Override
	public boolean canDoWork()
	{
		ItemStack input = itemHandler.getStackInSlot(INPUT_SLOT);
		return getEnergyStorage().canExtract() && !input.isEmpty() && input.getCapability(ForgeCapabilities.ENERGY).isPresent() && itemHandler.getStackInSlot(OUTPUT_SLOT).isEmpty();
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, BatteryBlockEntity batteryBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		
		AdvancedBlockEntity.handleTransfer(batteryBlockEntity, 0);
		
		if(batteryBlockEntity.canDoWork())
		{
			hasChanged = true;
			
			ItemStack input = batteryBlockEntity.itemHandler.getStackInSlot(INPUT_SLOT);
			input.getCapability(ForgeCapabilities.ENERGY).ifPresent(energyStorage ->
			{
				int energy = energyStorage.receiveEnergy(Math.min(batteryBlockEntity.getEnergyStorage().getMaxExtract(), batteryBlockEntity.getEnergyStorage().getEnergyStored()), true);
				int extractedEnergy = batteryBlockEntity.getEnergyStorage().extractEnergy(energy, false);
				energyStorage.receiveEnergy(extractedEnergy, false);
				
				if(energyStorage.getEnergyStored() == energyStorage.getMaxEnergyStored())
				{
					batteryBlockEntity.itemHandler.setStackInSlot(OUTPUT_SLOT, input);
					batteryBlockEntity.itemHandler.setStackInSlot(INPUT_SLOT, ItemStack.EMPTY);
				} else
					batteryBlockEntity.itemHandler.setStackInSlot(INPUT_SLOT, input);
			});
		}
		
		int energyState = (int) (((float) batteryBlockEntity.getEnergyStorage().getEnergyStored() / (float) CAPACITY) / 0.2f);
		if(state.getValue(BatteryBlock.ENERGY) != energyState)
		{
			hasChanged = true;
			
			state = state.setValue(BatteryBlock.ENERGY, energyState);
			level.setBlock(pos, state, 3);
		}
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
