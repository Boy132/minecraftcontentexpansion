package de.boy132.minecraftcontentexpansion.block.entity.advanced;

import de.boy132.minecraftcontentexpansion.block.entity.BaseBlockEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraftforge.registries.ForgeRegistries;

import java.text.NumberFormat;
import java.util.List;

public abstract class AdvancedEntityBlock extends BaseEntityBlock
{
	public static final IntegerProperty ENERGY = IntegerProperty.create("energy", 0, 5);
	public static final IntegerProperty FLUID = IntegerProperty.create("fluid", 0, 5);
	
	protected AdvancedEntityBlock(Properties properties)
	{
		super(properties);
	}
	
	@Override
	public RenderShape getRenderShape(BlockState p_49232_)
	{
		return RenderShape.MODEL;
	}
	
	@Override
	public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hitResult)
	{
		if(!level.isClientSide())
		{
			BlockEntity blockEntity = level.getBlockEntity(pos);
			if(blockEntity instanceof BaseBlockEntity baseBlockEntity)
				((ServerPlayer) player).openMenu(baseBlockEntity, pos);
		}
		
		return InteractionResult.sidedSuccess(level.isClientSide());
	}
	
	@Override
	public void onRemove(BlockState state, Level level, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(state.getBlock() != newState.getBlock())
		{
			BlockEntity blockEntity = level.getBlockEntity(pos);
			if(blockEntity instanceof BaseBlockEntity baseBlockEntity)
				baseBlockEntity.dropContents();
		}
		
		super.onRemove(state, level, pos, newState, isMoving);
	}
	
	@Override
	public BlockState playerWillDestroy(Level level, BlockPos blockPos, BlockState state, Player player)
	{
		BlockEntity blockEntity = level.getBlockEntity(blockPos);
		if(blockEntity instanceof AdvancedBlockEntity)
		{
			if(!level.isClientSide && player.isCreative())
			{
				ItemStack itemStack = new ItemStack(this);
				blockEntity.saveToItem(itemStack);
				
				ItemEntity itemEntity = new ItemEntity(level, (double) blockPos.getX() + 0.5D, (double) blockPos.getY() + 0.5D, (double) blockPos.getZ() + 0.5D, itemStack);
				itemEntity.setDefaultPickUpDelay();
				level.addFreshEntity(itemEntity);
			}
		}
		
		return super.playerWillDestroy(level, blockPos, state, player);
	}
	
	@Override
	public void setPlacedBy(Level level, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack)
	{
		super.setPlacedBy(level, pos, state, placer, stack);
		
		CompoundTag compound = stack.getTagElement("BlockEntityTag");
		if(compound != null)
		{
			if(level.getBlockEntity(pos) instanceof AdvancedBlockEntity blockEntity)
				blockEntity.loadCustom(compound);
		}
	}
	
	public abstract int getFluidTankCapacity();
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter blockGetter, List<Component> tooltip, TooltipFlag flag)
	{
		super.appendHoverText(stack, blockGetter, tooltip, flag);
		
		CompoundTag compound = stack.getTagElement("BlockEntityTag");
		if(compound != null)
		{
			CompoundTag energyTag = compound.getCompound("energy");
			if(!energyTag.isEmpty())
			{
				NumberFormat numberFormat = NumberFormat.getIntegerInstance();
				tooltip.add(Component.literal(numberFormat.format(energyTag.getInt("energy")) + " / " + numberFormat.format(energyTag.getInt("capacity")) + " FE"));
			}
			
			CompoundTag fluidTag = compound.getCompound("fluid");
			if(!fluidTag.isEmpty())
			{
				Fluid fluid = ForgeRegistries.FLUIDS.getValue(ResourceLocation.parse(fluidTag.getString("FluidName")));
				if(fluid != null && fluid != Fluids.EMPTY)
				{
					NumberFormat numberFormat = NumberFormat.getIntegerInstance();
					tooltip.add(Component.literal(numberFormat.format(fluidTag.getInt("Amount")) + " / " + numberFormat.format(getFluidTankCapacity()) + " mB ")
							.append(fluid.getFluidType().getDescription()));
				}
			}
		}
	}
	
	@Override
	public ItemStack getCloneItemStack(BlockState state, HitResult target, LevelReader level, BlockPos pos, Player player)
	{
		ItemStack itemstack = super.getCloneItemStack(state, target, level, pos, player);
		
		if(level.getBlockEntity(pos) instanceof AdvancedBlockEntity blockEntity)
		{
			CompoundTag compound = blockEntity.saveCustom(new CompoundTag());
			if(!compound.isEmpty())
				itemstack.addTagElement("BlockEntityTag", compound);
		}
		
		return itemstack;
	}
}
