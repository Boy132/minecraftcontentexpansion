package de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse;

import com.mojang.blaze3d.vertex.PoseStack;
import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.crop.*;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraftforge.common.capabilities.ForgeCapabilities;

public class ElectricGreenhouseBlockEntityRenderer implements BlockEntityRenderer<ElectricGreenhouseBlockEntity>
{
	private final BlockEntityRendererProvider.Context context;
	
	public ElectricGreenhouseBlockEntityRenderer(BlockEntityRendererProvider.Context context)
	{
		this.context = context;
	}
	
	@Override
	public void render(ElectricGreenhouseBlockEntity blockEntity, float partialTick, PoseStack poseStack, MultiBufferSource buffer, int packedLight, int packedOverlay)
	{
		blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER).ifPresent(itemHandler ->
		{
			if(!itemHandler.getStackInSlot(ElectricGreenhouseBlockEntity.SOIL_SLOT).isEmpty())
			{
				for(int i = ElectricGreenhouseBlockEntity.INPUT_SLOTS_START; i < ElectricGreenhouseBlockEntity.INPUT_SLOTS_START + ElectricGreenhouseBlockEntity.INPUT_SLOTS_COUNT; i++)
				{
					ItemStack stack = itemHandler.getStackInSlot(i);
					if(!stack.isEmpty())
					{
						Block block = getBlock(stack);
						if(block != null)
						{
							int maxAge = getMaxAge(stack);
							float growPercentage = (float) blockEntity.data.get(i) / (float) ElectricGreenhouseBlockEntity.NEEDED_GROW_TIME;
							
							if(stack.getItem() == Items.WHEAT || stack.getItem() == Items.BEETROOT || stack.getItem() == ModItems.TOMATO.get() || stack.getItem() == ModItems.GRAPE.get() || stack.getItem() == ModItems.STRAWBERRY.get() || stack.getItem() == ModItems.CORN.get() || stack.getItem() == ModItems.LETTUCE.get() || stack.getItem() == ModItems.ONION.get() || stack.getCount() >= 2)
								growPercentage = 1f;
							
							poseStack.pushPose();
							poseStack.scale(0.45f, 0.45f, 0.45f);
							
							// TODO: find better way to handle this (?)
							if(i == 1)
								poseStack.translate(1.1, 1, 1.1);
							else if(i == 2)
								poseStack.translate(0.1, 1, 1.1);
							else if(i == 3)
								poseStack.translate(1.1, 1, 0.1);
							else if(i == 4)
								poseStack.translate(0.1, 1, 0.1);
							
							context.getBlockRenderDispatcher().renderSingleBlock(block.defaultBlockState().setValue(getAgeProperty(stack), (int) (maxAge * growPercentage)), poseStack, buffer, packedLight, packedOverlay);
							
							poseStack.popPose();
						}
					}
				}
			}
		});
	}
	
	private Block getBlock(ItemStack stack)
	{
		if(stack.getItem() == Items.WHEAT_SEEDS || stack.getItem() == Items.WHEAT)
			return Blocks.WHEAT;
		
		if(stack.getItem() == Items.CARROT)
			return Blocks.CARROTS;
		
		if(stack.getItem() == Items.POTATO)
			return Blocks.POTATOES;
		
		if(stack.getItem() == Items.BEETROOT_SEEDS || stack.getItem() == Items.BEETROOT)
			return Blocks.BEETROOTS;
		
		if(stack.getItem() == Items.SWEET_BERRIES)
			return Blocks.SWEET_BERRY_BUSH;
		
		if(stack.getItem() == Items.NETHER_WART)
			return Blocks.NETHER_WART;
		
		if(stack.getItem() == ModItems.TOMATO_SEEDS.get() || stack.getItem() == ModItems.TOMATO.get())
			return ModBlocks.TOMATOES.get();
		
		if(stack.getItem() == ModItems.GRAPE_SEEDS.get() || stack.getItem() == ModItems.GRAPE.get())
			return ModBlocks.GRAPES.get();
		
		if(stack.getItem() == ModItems.STRAWBERRY_SEEDS.get() || stack.getItem() == ModItems.STRAWBERRY.get())
			return ModBlocks.STRAWBERRIES.get();
		
		if(stack.getItem() == ModItems.CORN_SEEDS.get() || stack.getItem() == ModItems.CORN.get())
			return ModBlocks.CORNS.get();
		
		if(stack.getItem() == ModItems.LETTUCE_SEEDS.get() || stack.getItem() == ModItems.LETTUCE.get())
			return ModBlocks.LETTUCES.get();
		
		if(stack.getItem() == ModItems.ONION_SEEDS.get() || stack.getItem() == ModItems.ONION.get())
			return ModBlocks.ONIONS.get();
		
		if(stack.getItem() == ModItems.RICE.get())
			return ModBlocks.RICE.get();
		
		return null;
	}
	
	private int getMaxAge(ItemStack stack)
	{
		if(stack.getItem() == Items.WHEAT_SEEDS || stack.getItem() == Items.WHEAT)
			return CropBlock.MAX_AGE;
		
		if(stack.getItem() == Items.CARROT)
			return CarrotBlock.MAX_AGE;
		
		if(stack.getItem() == Items.POTATO)
			return PotatoBlock.MAX_AGE;
		
		if(stack.getItem() == Items.BEETROOT_SEEDS || stack.getItem() == Items.BEETROOT)
			return BeetrootBlock.MAX_AGE;
		
		if(stack.getItem() == Items.SWEET_BERRIES)
			return SweetBerryBushBlock.MAX_AGE;
		
		if(stack.getItem() == Items.NETHER_WART)
			return NetherWartBlock.MAX_AGE;
		
		if(stack.getItem() == ModItems.TOMATO_SEEDS.get() || stack.getItem() == ModItems.TOMATO.get())
			return TomatoCropBlock.MAX_AGE;
		
		if(stack.getItem() == ModItems.GRAPE_SEEDS.get() || stack.getItem() == ModItems.GRAPE.get())
			return GrapeCropBlock.MAX_AGE;
		
		if(stack.getItem() == ModItems.STRAWBERRY_SEEDS.get() || stack.getItem() == ModItems.STRAWBERRY.get())
			return StrawberryCropBlock.MAX_AGE;
		
		if(stack.getItem() == ModItems.CORN_SEEDS.get() || stack.getItem() == ModItems.CORN.get())
			return CornDoubleCropBlock.MAX_AGE;
		
		if(stack.getItem() == ModItems.LETTUCE_SEEDS.get() || stack.getItem() == ModItems.LETTUCE.get())
			return LettuceCropBlock.MAX_AGE;
		
		if(stack.getItem() == ModItems.ONION_SEEDS.get() || stack.getItem() == ModItems.ONION.get())
			return OnionCropBlock.MAX_AGE;
		
		if(stack.getItem() == ModItems.RICE.get())
			return RiceCropBlock.MAX_AGE;
		
		return 1;
	}
	
	private IntegerProperty getAgeProperty(ItemStack stack)
	{
		if(stack.getItem() == Items.WHEAT_SEEDS || stack.getItem() == Items.WHEAT)
			return CropBlock.AGE;
		
		if(stack.getItem() == Items.CARROT)
			return CarrotBlock.AGE;
		
		if(stack.getItem() == Items.POTATO)
			return PotatoBlock.AGE;
		
		if(stack.getItem() == Items.BEETROOT_SEEDS || stack.getItem() == Items.BEETROOT)
			return BeetrootBlock.AGE;
		
		if(stack.getItem() == Items.SWEET_BERRIES)
			return SweetBerryBushBlock.AGE;
		
		if(stack.getItem() == Items.NETHER_WART)
			return NetherWartBlock.AGE;
		
		if(stack.getItem() == ModItems.TOMATO_SEEDS.get() || stack.getItem() == ModItems.TOMATO.get())
			return TomatoCropBlock.AGE;
		
		if(stack.getItem() == ModItems.GRAPE_SEEDS.get() || stack.getItem() == ModItems.GRAPE.get())
			return GrapeCropBlock.AGE;
		
		if(stack.getItem() == ModItems.STRAWBERRY_SEEDS.get() || stack.getItem() == ModItems.STRAWBERRY.get())
			return StrawberryCropBlock.AGE;
		
		if(stack.getItem() == ModItems.CORN_SEEDS.get() || stack.getItem() == ModItems.CORN.get())
			return CornDoubleCropBlock.AGE;
		
		if(stack.getItem() == ModItems.LETTUCE_SEEDS.get() || stack.getItem() == ModItems.LETTUCE.get())
			return LettuceCropBlock.AGE;
		
		if(stack.getItem() == ModItems.ONION_SEEDS.get() || stack.getItem() == ModItems.ONION.get())
			return OnionCropBlock.AGE;
		
		if(stack.getItem() == ModItems.RICE.get())
			return RiceCropBlock.AGE;
		
		return CropBlock.AGE;
	}
}

