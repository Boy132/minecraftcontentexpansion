package de.boy132.minecraftcontentexpansion.block.entity;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;

public abstract class BaseBlockEntity extends BlockEntity implements MenuProvider
{
	public BaseBlockEntity(BlockEntityType<?> blockEntityType, BlockPos pos, BlockState state)
	{
		super(blockEntityType, pos, state);
	}
	
	public abstract boolean canDoWork();
	
	public void dropContents()
	{
	
	}
	
	public static void doLitAnimation(BlockState state, Level level, BlockPos pos, RandomSource randomSource, double xOffset, double yOffset, double zOffset)
	{
		if(state.getValue(BlockStateProperties.LIT))
		{
			double x = (double) pos.getX() + xOffset + 0.5D;
			double y = (double) pos.getY() + yOffset;
			double z = (double) pos.getZ() + zOffset + 0.5D;
			
			if(randomSource.nextDouble() < 0.1D)
				level.playLocalSound(x, y, z, SoundEvents.FURNACE_FIRE_CRACKLE, SoundSource.BLOCKS, 1.0F, 1.0F, false);
			
			Direction direction = state.hasProperty(BlockStateProperties.HORIZONTAL_FACING) ? state.getValue(BlockStateProperties.HORIZONTAL_FACING) : Direction.NORTH;
			Direction.Axis axis = direction.getAxis();
			
			double nonAxisX = randomSource.nextDouble() * 0.6D - 0.3D;
			
			double randX = axis == Direction.Axis.X ? (double) direction.getStepX() * 0.52D : nonAxisX;
			double randY = randomSource.nextDouble() * 6.0D / 16.0D;
			double randZ = axis == Direction.Axis.Z ? (double) direction.getStepZ() * 0.52D : nonAxisX;
			
			level.addParticle(ParticleTypes.SMOKE, x + randX, y + randY, z + randZ, 0.0D, 0.0D, 0.0D);
			level.addParticle(ParticleTypes.FLAME, x + randX, y + randY, z + randZ, 0.0D, 0.0D, 0.0D);
		}
	}
}
