package de.boy132.minecraftcontentexpansion.block.entity.kiln;

import com.mojang.serialization.MapCodec;
import de.boy132.minecraftcontentexpansion.block.entity.BaseBlockEntity;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.jetbrains.annotations.Nullable;

public class KilnBlock extends BaseEntityBlock
{
	public static final MapCodec<KilnBlock> CODEC = simpleCodec(KilnBlock::new);
	
	public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
	public static final BooleanProperty LIT = BlockStateProperties.LIT;
	
	public KilnBlock(Properties properties)
	{
		super(properties);
		registerDefaultState(stateDefinition.any().setValue(FACING, Direction.NORTH).setValue(LIT, Boolean.FALSE));
	}
	
	@Override
	protected MapCodec<KilnBlock> codec()
	{
		return CODEC;
	}
	
	@Nullable
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rotation)
	{
		return state.setValue(FACING, rotation.rotate(state.getValue(FACING)));
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mirror)
	{
		return state.rotate(mirror.getRotation(state.getValue(FACING)));
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(FACING, LIT);
	}
	
	private static final VoxelShape SHAPE1 = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 5.0D, 16.0D);
	private static final VoxelShape SHAPE2 = Block.box(1.0D, 5.0D, 1.0D, 15.0D, 9.0D, 15.0D);
	private static final VoxelShape SHAPE3 = Block.box(2.0D, 9.0D, 2.0D, 14.0D, 13.0D, 14.0D);
	private static final VoxelShape SHAPE4 = Block.box(3.0D, 13.0D, 3.0D, 13.0D, 16.0D, 13.0D);
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter getter, BlockPos pos, CollisionContext context)
	{
		return Shapes.or(SHAPE1, SHAPE2, SHAPE3, SHAPE4);
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.MODEL;
	}
	
	@Override
	public void animateTick(BlockState state, Level level, BlockPos pos, RandomSource randomSource)
	{
		BaseBlockEntity.doLitAnimation(state, level, pos, randomSource, 0, 0, 0);
	}
	
	@Override
	public void onRemove(BlockState state, Level level, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(state.getBlock() != newState.getBlock())
		{
			BlockEntity blockEntity = level.getBlockEntity(pos);
			if(blockEntity instanceof KilnBlockEntity kilnBlockEntity)
				kilnBlockEntity.dropContents();
		}
		
		super.onRemove(state, level, pos, newState, isMoving);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hitResult)
	{
		if(!level.isClientSide())
		{
			BlockEntity blockEntity = level.getBlockEntity(pos);
			if(blockEntity instanceof KilnBlockEntity kilnBlockEntity)
				((ServerPlayer) player).openMenu(kilnBlockEntity, pos);
		}
		
		return InteractionResult.sidedSuccess(level.isClientSide());
	}
	
	@Nullable
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new KilnBlockEntity(pos, state);
	}
	
	@Nullable
	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> entityType)
	{
		return createTickerHelper(entityType, ModBlockEntities.KILN.get(), KilnBlockEntity::tick);
	}
}
