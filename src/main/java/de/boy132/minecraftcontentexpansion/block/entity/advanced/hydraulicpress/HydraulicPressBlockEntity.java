package de.boy132.minecraftcontentexpansion.block.entity.advanced.hydraulicpress;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.entity.LimitedItemStackHandler;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedBlockEntity;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.recipe.hydraulicpress.HydraulicPressRecipe;
import de.boy132.minecraftcontentexpansion.screen.hydraulicpress.HydraulicPressMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeHolder;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class HydraulicPressBlockEntity extends AdvancedBlockEntity
{
	public static final int STANDARD_PRESS_TIME = 200;
	
	public static final int CAPACITY = 200000;
	
	public static final int PRIMARY_INPUT_SLOT = 0;
	public static final int SECONDARY_INPUT_SLOT = 1;
	public static final int OUTPUT_SLOT_START = 2;
	public static final int OUTPUT_SLOT_COUNT = 4;
	
	private final ItemStackHandler itemHandler = new ItemStackHandler(6)
	{
		@Override
		protected void onContentsChanged(int slot)
		{
			setChanged();
		}
	};
	private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_PrimaryInput = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == PRIMARY_INPUT_SLOT, (slot, stack) -> false);
	private LazyOptional<IItemHandler> lazyItemHandler_PrimaryInput = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_SecondaryInput = new LimitedItemStackHandler(itemHandler, (slot, stack) -> slot == SECONDARY_INPUT_SLOT, (slot, stack) -> false);
	private LazyOptional<IItemHandler> lazyItemHandler_SecondaryInput = LazyOptional.empty();
	
	private final LimitedItemStackHandler itemHandler_Outputs = new LimitedItemStackHandler(itemHandler, (slot, stack) -> false, (slot, stack) -> slot >= OUTPUT_SLOT_START);
	private LazyOptional<IItemHandler> lazyItemHandler_Outputs = LazyOptional.empty();
	
	protected final ContainerData data;
	
	protected int pressTime;
	protected int neededPressTime;
	
	public HydraulicPressBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.HYDRAULIC_PRESS.get(), pos, state);
		energy = new Energy(CAPACITY, 200, 0);
		
		data = new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return switch(index)
				{
					case 0 -> HydraulicPressBlockEntity.this.getEnergyStorage().getEnergyIO();
					case 1 -> HydraulicPressBlockEntity.this.pressTime;
					case 2 -> HydraulicPressBlockEntity.this.neededPressTime;
					default -> 0;
				};
			}
			
			@Override
			public void set(int index, int value)
			{
				switch(index)
				{
					case 0 -> HydraulicPressBlockEntity.this.getEnergyStorage().setEnergyIO(value);
					case 1 -> HydraulicPressBlockEntity.this.pressTime = value;
					case 2 -> HydraulicPressBlockEntity.this.neededPressTime = value;
				}
			}
			
			@Override
			public int getCount()
			{
				return 3;
			}
		};
	}
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("block." + MinecraftContentExpansion.MODID + ".hydraulic_press");
	}
	
	@Nullable
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inventory, Player player)
	{
		return new HydraulicPressMenu(id, inventory, this, data);
	}
	
	@Override
	public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> capability, @Nullable Direction side)
	{
		if(capability == ForgeCapabilities.ITEM_HANDLER)
		{
			if(side == null)
				return lazyItemHandler.cast();
			
			if(side == Direction.UP)
				return lazyItemHandler_SecondaryInput.cast();
			else if(side == Direction.DOWN)
				return lazyItemHandler_Outputs.cast();
			else
				return lazyItemHandler_PrimaryInput.cast();
		}
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		lazyItemHandler = LazyOptional.of(() -> itemHandler);
		lazyItemHandler_PrimaryInput = LazyOptional.of(() -> itemHandler_PrimaryInput);
		lazyItemHandler_SecondaryInput = LazyOptional.of(() -> itemHandler_SecondaryInput);
		lazyItemHandler_Outputs = LazyOptional.of(() -> itemHandler_Outputs);
	}
	
	@Override
	public void invalidateCaps()
	{
		super.invalidateCaps();
		lazyItemHandler.invalidate();
		lazyItemHandler_PrimaryInput.invalidate();
		lazyItemHandler_SecondaryInput.invalidate();
		lazyItemHandler_Outputs.invalidate();
	}
	
	@Override
	protected void saveAdditional(CompoundTag compoundTag)
	{
		compoundTag.put("inventory", itemHandler.serializeNBT());
		
		compoundTag.putInt("pressTime", pressTime);
		compoundTag.putInt("neededPressTime", neededPressTime);
		
		super.saveAdditional(compoundTag);
	}
	
	@Override
	public void load(CompoundTag compoundTag)
	{
		super.load(compoundTag);
		itemHandler.deserializeNBT(compoundTag.getCompound("inventory"));
		
		pressTime = compoundTag.getInt("pressTime");
		neededPressTime = compoundTag.getInt("neededPressTime");
	}
	
	@Override
	public void dropContents()
	{
		SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
		
		for(int i = 0; i < itemHandler.getSlots(); i++)
			inventory.setItem(i, itemHandler.getStackInSlot(i));
		
		Containers.dropContents(level, worldPosition, inventory);
	}
	
	private boolean isPressing()
	{
		return pressTime > 0;
	}
	
	@Override
	public boolean canDoWork()
	{
		ItemStack input = itemHandler.getStackInSlot(PRIMARY_INPUT_SLOT);
		if(input.isEmpty())
			return false;
		
		if(getEnergyStorage().getEnergyStored() < 20)
			return false;
		
		HydraulicPressRecipe hydraulicPressRecipe = getRecipe();
		if(hydraulicPressRecipe == null)
			return false;
		
		ItemStack result = hydraulicPressRecipe.assemble(null, level.registryAccess());
		for(int i = OUTPUT_SLOT_START; i < OUTPUT_SLOT_START + OUTPUT_SLOT_COUNT; i++)
		{
			if(result.isEmpty())
				break;
			
			result = itemHandler.insertItem(i, result, true);
		}
		
		return result.isEmpty();
	}
	
	public HydraulicPressRecipe getRecipe()
	{
		SimpleContainer container = new SimpleContainer(2);
		container.setItem(0, itemHandler.getStackInSlot(PRIMARY_INPUT_SLOT));
		container.setItem(1, itemHandler.getStackInSlot(SECONDARY_INPUT_SLOT));
		
		Optional<RecipeHolder<HydraulicPressRecipe>> recipe = level.getRecipeManager().getRecipeFor(ModRecipeTypes.HYDRAULIC_PRESS.get(), container, level);
		if(recipe.isPresent())
			return recipe.get().value();
		
		return null;
	}
	
	public static void tick(Level level, BlockPos pos, BlockState state, HydraulicPressBlockEntity hydraulicPressBlockEntity)
	{
		if(level.isClientSide())
			return;
		
		boolean hasChanged = false;
		
		int consume = 0;
		if(hydraulicPressBlockEntity.canDoWork() && !level.hasNeighborSignal(pos))
		{
			consume = 10;
			
			hydraulicPressBlockEntity.pressTime++;
			
			HydraulicPressRecipe hydraulicPressRecipe = hydraulicPressBlockEntity.getRecipe();
			hydraulicPressBlockEntity.neededPressTime = hydraulicPressRecipe.getPressTime();
			if(hydraulicPressBlockEntity.pressTime > hydraulicPressBlockEntity.neededPressTime)
			{
				// set to 1 instead of 0 so that the press animation works smoothly
				hydraulicPressBlockEntity.pressTime = 1;
				
				hydraulicPressBlockEntity.itemHandler.extractItem(PRIMARY_INPUT_SLOT, 1, false);
				hydraulicPressBlockEntity.itemHandler.extractItem(SECONDARY_INPUT_SLOT, 1, false);
				
				ItemStack remainingOutput = hydraulicPressRecipe.assemble(null, hydraulicPressBlockEntity.level.registryAccess());
				for(int i = OUTPUT_SLOT_START; i < OUTPUT_SLOT_START + OUTPUT_SLOT_COUNT; i++)
				{
					if(remainingOutput.isEmpty())
						break;
					
					remainingOutput = hydraulicPressBlockEntity.itemHandler.insertItem(i, remainingOutput, false);
				}
			}
			
			hasChanged = true;
		} else
			hydraulicPressBlockEntity.pressTime = 0;
		
		if(state.getValue(HydraulicPressBlock.PRESSING) != hydraulicPressBlockEntity.isPressing())
		{
			hasChanged = true;
			
			state = state.setValue(HydraulicPressBlock.PRESSING, hydraulicPressBlockEntity.isPressing());
			level.setBlock(pos, state, 3);
		}
		
		AdvancedBlockEntity.handleTransfer(hydraulicPressBlockEntity, -consume);
		
		if(hasChanged)
			setChanged(level, pos, state);
	}
}
