package de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse;

import net.minecraft.util.StringRepresentable;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;

public enum SoilType implements StringRepresentable
{
	NONE("none"),
	DIRT("dirt"),
	SAND("sand"),
	SOULSAND("soulsand");
	
	private final String name;
	
	SoilType(String name)
	{
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		return getSerializedName();
	}
	
	@Override
	public String getSerializedName()
	{
		return name;
	}
	
	public static SoilType fromItemStack(ItemStack itemStack)
	{
		if(itemStack.getItem() == Items.DIRT || itemStack.getItem() == Items.FARMLAND)
			return SoilType.DIRT;
		
		if(itemStack.getItem() == Items.SAND)
			return SoilType.SAND;
		
		if(itemStack.getItem() == Items.SOUL_SAND)
			return SoilType.SOULSAND;
		
		return SoilType.NONE;
	}
}
