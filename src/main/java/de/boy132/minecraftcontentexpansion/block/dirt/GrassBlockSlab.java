package de.boy132.minecraftcontentexpansion.block.dirt;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.SlabType;
import net.minecraft.world.level.lighting.LightEngine;

public class GrassBlockSlab extends DirtSlab
{
	public GrassBlockSlab(Properties properties)
	{
		super(properties);
	}
	
	private static boolean canBeGrass(BlockState state, LevelReader levelReader, BlockPos pos)
	{
		if(state.getValue(TYPE) == SlabType.BOTTOM && state.getValue(WATERLOGGED))
			return false;
		
		BlockPos abovePos = pos.above();
		BlockState aboveState = levelReader.getBlockState(abovePos);
		
		if(aboveState.getFluidState().getAmount() == 8)
			return false;
		
		int i = LightEngine.getLightBlockInto(levelReader, state, pos, aboveState, abovePos, Direction.UP, aboveState.getLightBlock(levelReader, abovePos));
		return i < levelReader.getMaxLightLevel();
	}
	
	private static boolean canPropagate(BlockState state, LevelReader levelReader, BlockPos pos)
	{
		BlockPos abovePos = pos.above();
		return canBeGrass(state, levelReader, pos) && !levelReader.getFluidState(abovePos).is(FluidTags.WATER);
	}
	
	@Override
	public void randomTick(BlockState state, ServerLevel serverLevel, BlockPos pos, RandomSource randomSource)
	{
		if(!canBeGrass(state, serverLevel, pos))
		{
			if(!serverLevel.isAreaLoaded(pos, 1))
				return; // Forge: prevent loading unloaded chunks when checking neighbor's light and spreading
			
			serverLevel.setBlockAndUpdate(pos, ModBlocks.DIRT_SLAB.get().defaultBlockState().setValue(TYPE, state.getValue(TYPE)).setValue(WATERLOGGED, state.getValue(WATERLOGGED)));
		} else
		{
			if(!serverLevel.isAreaLoaded(pos, 3))
				return; // Forge: prevent loading unloaded chunks when checking neighbor's light and spreading
			
			if(serverLevel.getMaxLocalRawBrightness(pos.above()) >= 9)
			{
				BlockState defaultState = defaultBlockState();
				
				for(int i = 0; i < 4; ++i)
				{
					BlockPos posWithOffset = pos.offset(randomSource.nextInt(3) - 1, randomSource.nextInt(5) - 3, randomSource.nextInt(3) - 1);
					BlockState stateWithOffset = serverLevel.getBlockState(posWithOffset);
					if(stateWithOffset.is(ModBlocks.DIRT_SLAB.get()) && canPropagate(defaultState, serverLevel, posWithOffset))
						serverLevel.setBlockAndUpdate(posWithOffset, defaultState.setValue(TYPE, stateWithOffset.getValue(TYPE)).setValue(WATERLOGGED, state.getValue(WATERLOGGED)));
				}
			}
		}
	}
}
