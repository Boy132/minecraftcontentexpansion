package de.boy132.minecraftcontentexpansion.block.dirt;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.ToolAction;
import net.minecraftforge.common.ToolActions;
import org.jetbrains.annotations.Nullable;

public class DirtSlab extends SlabBlock
{
	public DirtSlab(Properties properties)
	{
		super(properties);
	}
	
	@Override
	public @Nullable BlockState getToolModifiedState(BlockState state, UseOnContext context, ToolAction toolAction, boolean simulate)
	{
		ItemStack itemStack = context.getItemInHand();
		if(!itemStack.canPerformAction(toolAction))
			return null;
		
		if(ToolActions.SHOVEL_FLATTEN == toolAction)
			return ModBlocks.DIRT_PATH_SLAB.get().defaultBlockState().setValue(TYPE, state.getValue(TYPE)).setValue(WATERLOGGED, state.getValue(WATERLOGGED));
		
		return super.getToolModifiedState(state, context, toolAction, simulate);
	}
}
