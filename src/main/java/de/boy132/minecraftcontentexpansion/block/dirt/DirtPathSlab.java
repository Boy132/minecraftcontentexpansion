package de.boy132.minecraftcontentexpansion.block.dirt;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.SlabType;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class DirtPathSlab extends SlabBlock
{
	private static final VoxelShape BOTTOM_SHAPE = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 7.0D, 16.0D);
	private static final VoxelShape TOP_SHAPE = Block.box(0.0D, 8.0D, 0.0D, 16.0D, 15.0D, 16.0D);
	private static final VoxelShape DOUBLE_SHAPE = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 15.0D, 16.0D);
	
	public DirtPathSlab(Properties properties)
	{
		super(properties);
	}
	
	@Override
	public boolean useShapeForLightOcclusion(BlockState state)
	{
		return true;
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return !this.defaultBlockState().canSurvive(context.getLevel(), context.getClickedPos()) ? Block.pushEntitiesUp(this.defaultBlockState(), ModBlocks.DIRT_SLAB.get().getStateForPlacement(context), context.getLevel(), context.getClickedPos()) : super.getStateForPlacement(context);
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction direction, BlockState state1, LevelAccessor levelAccessor, BlockPos pos, BlockPos pos1)
	{
		if(direction == Direction.UP && !state.canSurvive(levelAccessor, pos))
			levelAccessor.scheduleTick(pos, this, 1);
		
		return super.updateShape(state, direction, state1, levelAccessor, pos, pos1);
	}
	
	@Override
	public void tick(BlockState state, ServerLevel serverLevel, BlockPos pos, RandomSource randomSource)
	{
		BlockState newState = pushEntitiesUp(state, ModBlocks.DIRT_SLAB.get().defaultBlockState().setValue(TYPE, state.getValue(TYPE)).setValue(WATERLOGGED, state.getValue(WATERLOGGED)), serverLevel, pos);
		serverLevel.setBlockAndUpdate(pos, newState);
		serverLevel.gameEvent(GameEvent.BLOCK_CHANGE, pos, GameEvent.Context.of(null, newState));
	}
	
	@Override
	public boolean canSurvive(BlockState state, LevelReader levelReader, BlockPos pos)
	{
		BlockState blockstate = levelReader.getBlockState(pos.above());
		return !blockstate.isSolid() || blockstate.getBlock() instanceof FenceGateBlock;
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter blockGetter, BlockPos pos, CollisionContext context)
	{
		SlabType slabtype = state.getValue(TYPE);
		switch(slabtype)
		{
			case DOUBLE:
				return DOUBLE_SHAPE;
			case TOP:
				return TOP_SHAPE;
			default:
				return BOTTOM_SHAPE;
		}
	}
	
	@Override
	public boolean isPathfindable(BlockState p_153138_, BlockGetter p_153139_, BlockPos p_153140_, PathComputationType p_153141_)
	{
		return false;
	}
}
