package de.boy132.minecraftcontentexpansion.block.cable;

import net.minecraft.util.StringRepresentable;

public enum CableSide implements StringRepresentable
{
	UP("up"),
	SIDE("side"),
	NONE("none");
	
	private final String name;
	
	CableSide(String name)
	{
		this.name = name;
	}
	
	public String toString()
	{
		return this.getSerializedName();
	}
	
	public String getSerializedName()
	{
		return this.name;
	}
	
	public boolean isConnected()
	{
		return this != NONE;
	}
}