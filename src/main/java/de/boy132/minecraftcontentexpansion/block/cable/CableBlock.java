package de.boy132.minecraftcontentexpansion.block.cable;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public class CableBlock extends Block
{
	public static final EnumProperty<CableSide> NORTH = EnumProperty.create("north", CableSide.class);
	public static final EnumProperty<CableSide> EAST = EnumProperty.create("east", CableSide.class);
	public static final EnumProperty<CableSide> SOUTH = EnumProperty.create("south", CableSide.class);
	public static final EnumProperty<CableSide> WEST = EnumProperty.create("west", CableSide.class);
	
	public static final Map<Direction, EnumProperty<CableSide>> PROPERTY_BY_DIRECTION = Maps.newEnumMap(ImmutableMap.of(Direction.NORTH, NORTH, Direction.EAST, EAST, Direction.SOUTH, SOUTH, Direction.WEST, WEST));
	
	private static final VoxelShape SHAPE_DOT = Block.box(3.0D, 0.0D, 3.0D, 13.0D, 2.0D, 13.0D);
	private static final Map<Direction, VoxelShape> SHAPES_FLOOR = Maps.newEnumMap(ImmutableMap.of(Direction.NORTH, Block.box(3.0D, 0.0D, 0.0D, 13.0D, 2.0D, 13.0D), Direction.SOUTH, Block.box(3.0D, 0.0D, 3.0D, 13.0D, 2.0D, 16.0D), Direction.EAST, Block.box(3.0D, 0.0D, 3.0D, 16.0D, 2.0D, 13.0D), Direction.WEST, Block.box(0.0D, 0.0D, 3.0D, 13.0D, 2.0D, 13.0D)));
	private static final Map<Direction, VoxelShape> SHAPES_UP = Maps.newEnumMap(ImmutableMap.of(Direction.NORTH, Shapes.or(SHAPES_FLOOR.get(Direction.NORTH), Block.box(3.0D, 0.0D, 0.0D, 13.0D, 16.0D, 1.0D)), Direction.SOUTH, Shapes.or(SHAPES_FLOOR.get(Direction.SOUTH), Block.box(3.0D, 0.0D, 15.0D, 13.0D, 16.0D, 16.0D)), Direction.EAST, Shapes.or(SHAPES_FLOOR.get(Direction.EAST), Block.box(15.0D, 0.0D, 3.0D, 16.0D, 16.0D, 13.0D)), Direction.WEST, Shapes.or(SHAPES_FLOOR.get(Direction.WEST), Block.box(0.0D, 0.0D, 3.0D, 1.0D, 16.0D, 13.0D))));
	
	private final BlockState crossState;
	
	public CableBlock(Properties properties)
	{
		super(properties);
		registerDefaultState(stateDefinition.any().setValue(NORTH, CableSide.NONE).setValue(EAST, CableSide.NONE).setValue(SOUTH, CableSide.NONE).setValue(WEST, CableSide.NONE));
		crossState = defaultBlockState().setValue(NORTH, CableSide.SIDE).setValue(EAST, CableSide.SIDE).setValue(SOUTH, CableSide.SIDE).setValue(WEST, CableSide.SIDE);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, @Nullable BlockGetter blockGetter, List<Component> components, TooltipFlag tooltipFlag)
	{
		super.appendHoverText(stack, blockGetter, components, tooltipFlag);
		components.add(Component.translatable("item.nouse"));
	}
	
	private VoxelShape calculateShape(BlockState state)
	{
		VoxelShape voxelshape = SHAPE_DOT;
		
		for(Direction direction : Direction.Plane.HORIZONTAL)
		{
			CableSide cableSide = state.getValue(PROPERTY_BY_DIRECTION.get(direction));
			if(cableSide == CableSide.SIDE)
				voxelshape = Shapes.or(voxelshape, SHAPES_FLOOR.get(direction));
			else if(cableSide == CableSide.UP)
				voxelshape = Shapes.or(voxelshape, SHAPES_UP.get(direction));
		}
		
		return voxelshape;
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter getter, BlockPos pos, CollisionContext context)
	{
		return calculateShape(state);
	}
	
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return getConnectionState(context.getLevel(), crossState, context.getClickedPos());
	}
	
	private static boolean isCross(BlockState state)
	{
		return state.getValue(NORTH).isConnected() && state.getValue(SOUTH).isConnected() && state.getValue(EAST).isConnected() && state.getValue(WEST).isConnected();
	}
	
	private static boolean isDot(BlockState state)
	{
		return !state.getValue(NORTH).isConnected() && !state.getValue(SOUTH).isConnected() && !state.getValue(EAST).isConnected() && !state.getValue(WEST).isConnected();
	}
	
	@Override
	public boolean canSurvive(BlockState state, LevelReader reader, BlockPos pos)
	{
		BlockPos belowPos = pos.below();
		BlockState blockstate = reader.getBlockState(belowPos);
		return canSurviveOn(reader, belowPos, blockstate);
	}
	
	private boolean canSurviveOn(BlockGetter getter, BlockPos pos, BlockState state)
	{
		return state.isFaceSturdy(getter, pos, Direction.UP);
	}
	
	private CableSide getConnectingSide(Level level, BlockPos pos, Direction direction)
	{
		return getConnectingSide(level, pos, direction, !level.getBlockState(pos.above()).isRedstoneConductor(level, pos));
	}
	
	private CableSide getConnectingSide(Level level, BlockPos pos, Direction direction, boolean isRedstoneConductor)
	{
		BlockPos blockPos = pos.relative(direction);
		BlockState blockState = level.getBlockState(blockPos);
		
		if(isRedstoneConductor)
		{
			if(canSurviveOn(level, blockPos, blockState) && shouldConnectTo(level, blockPos.above()))
			{
				if(blockState.isFaceSturdy(level, blockPos, direction.getOpposite()))
					return CableSide.UP;
				
				return CableSide.SIDE;
			}
		}
		
		if(shouldConnectTo(level, blockPos))
			return CableSide.SIDE;
		
		if(blockState.isRedstoneConductor(level, blockPos))
			return CableSide.NONE;
		
		BlockPos blockPosBelow = blockPos.below();
		return shouldConnectTo(level, blockPosBelow) ? CableSide.SIDE : CableSide.NONE;
	}
	
	private BlockState getMissingConnections(Level level, BlockState state, BlockPos pos)
	{
		boolean flag = !level.getBlockState(pos.above()).isRedstoneConductor(level, pos);
		
		for(Direction direction : Direction.Plane.HORIZONTAL)
		{
			if(!state.getValue(PROPERTY_BY_DIRECTION.get(direction)).isConnected())
			{
				CableSide cableSide = getConnectingSide(level, pos, direction, flag);
				state = state.setValue(PROPERTY_BY_DIRECTION.get(direction), cableSide);
			}
		}
		
		return state;
	}
	
	private BlockState getConnectionState(Level level, BlockState state, BlockPos pos)
	{
		boolean flag = isDot(state);
		state = getMissingConnections(level, defaultBlockState(), pos);
		
		if(flag && isDot(state))
			return state;
		
		if(state.getValue(EAST).isConnected())
			state.setValue(WEST, CableSide.SIDE);
		
		if(state.getValue(WEST).isConnected())
			state.setValue(EAST, CableSide.SIDE);
		
		if(state.getValue(SOUTH).isConnected())
			state.setValue(NORTH, CableSide.SIDE);
		
		if(state.getValue(NORTH).isConnected())
			state.setValue(SOUTH, CableSide.SIDE);
		
		return state;
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction direction, BlockState newState, LevelAccessor accessor, BlockPos pos, BlockPos pos2)
	{
		if(direction == Direction.DOWN)
			return state;
		
		if(direction == Direction.UP)
			return getConnectionState((Level) accessor, state, pos);
		
		CableSide cableSide = getConnectingSide((Level) accessor, pos, direction);
		boolean flag = cableSide.isConnected() == state.getValue(PROPERTY_BY_DIRECTION.get(direction)).isConnected() && !isCross(state);
		return flag ? state.setValue(PROPERTY_BY_DIRECTION.get(direction), cableSide) : getConnectionState((Level) accessor, crossState.setValue(PROPERTY_BY_DIRECTION.get(direction), cableSide), pos);
	}
	
	@Override
	public void updateIndirectNeighbourShapes(BlockState state, LevelAccessor accessor, BlockPos pos, int i1, int i2)
	{
		BlockPos.MutableBlockPos mutableBlockPos = new BlockPos.MutableBlockPos();
		
		for(Direction direction : Direction.Plane.HORIZONTAL)
		{
			CableSide cableSide = state.getValue(PROPERTY_BY_DIRECTION.get(direction));
			if(cableSide != CableSide.NONE && !accessor.getBlockState(mutableBlockPos.setWithOffset(pos, direction)).is(this))
			{
				mutableBlockPos.move(Direction.DOWN);
				BlockState blockstate = accessor.getBlockState(mutableBlockPos);
				if(blockstate.is(this))
				{
					BlockPos blockpos = mutableBlockPos.relative(direction.getOpposite());
					accessor.neighborShapeChanged(direction.getOpposite(), accessor.getBlockState(blockpos), mutableBlockPos, blockpos, i1, i2);
				}
				
				mutableBlockPos.setWithOffset(pos, direction).move(Direction.UP);
				BlockState blockState1 = accessor.getBlockState(mutableBlockPos);
				if(blockState1.is(this))
				{
					BlockPos blockPos1 = mutableBlockPos.relative(direction.getOpposite());
					accessor.neighborShapeChanged(direction.getOpposite(), accessor.getBlockState(blockPos1), mutableBlockPos, blockPos1, i1, i2);
				}
			}
		}
	}
	
	private void checkCornerChangeAt(Level level, BlockPos pos)
	{
		if(level.getBlockState(pos).is(this))
		{
			level.updateNeighborsAt(pos, this);
			
			for(Direction direction : Direction.values())
				level.updateNeighborsAt(pos.relative(direction), this);
		}
	}
	
	private void updateNeighborsOfNeighboringWires(Level level, BlockPos pos)
	{
		for(Direction direction : Direction.Plane.HORIZONTAL)
			checkCornerChangeAt(level, pos.relative(direction));
		
		for(Direction direction : Direction.Plane.HORIZONTAL)
		{
			BlockPos blockpos = pos.relative(direction);
			if(level.getBlockState(blockpos).isRedstoneConductor(level, blockpos))
				checkCornerChangeAt(level, blockpos.above());
			else
				checkCornerChangeAt(level, blockpos.below());
		}
	}
	
	@Override
	public void onPlace(BlockState state, Level level, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(!newState.is(state.getBlock()) && !level.isClientSide)
		{
			for(Direction direction : Direction.Plane.VERTICAL)
				level.updateNeighborsAt(pos.relative(direction), this);
			
			updateNeighborsOfNeighboringWires(level, pos);
		}
	}
	
	@Override
	public void onRemove(BlockState state, Level level, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(!isMoving && !state.is(newState.getBlock()))
		{
			super.onRemove(state, level, pos, newState, isMoving);
			if(!level.isClientSide)
			{
				for(Direction direction : Direction.values())
					level.updateNeighborsAt(pos.relative(direction), this);
				
				updateNeighborsOfNeighboringWires(level, pos);
			}
		}
	}
	
	@Override
	public void neighborChanged(BlockState state, Level level, BlockPos pos, Block block, BlockPos pos2, boolean b)
	{
		if(!level.isClientSide)
		{
			if(!state.canSurvive(level, pos))
			{
				dropResources(state, level, pos);
				level.removeBlock(pos, false);
			}
		}
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rotation)
	{
		switch(rotation)
		{
			case CLOCKWISE_180:
				return state.setValue(NORTH, state.getValue(SOUTH)).setValue(EAST, state.getValue(WEST)).setValue(SOUTH, state.getValue(NORTH)).setValue(WEST, state.getValue(EAST));
			case COUNTERCLOCKWISE_90:
				return state.setValue(NORTH, state.getValue(EAST)).setValue(EAST, state.getValue(SOUTH)).setValue(SOUTH, state.getValue(WEST)).setValue(WEST, state.getValue(NORTH));
			case CLOCKWISE_90:
				return state.setValue(NORTH, state.getValue(WEST)).setValue(EAST, state.getValue(NORTH)).setValue(SOUTH, state.getValue(EAST)).setValue(WEST, state.getValue(SOUTH));
			default:
				return state;
		}
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mirror)
	{
		switch(mirror)
		{
			case LEFT_RIGHT:
				return state.setValue(NORTH, state.getValue(SOUTH)).setValue(SOUTH, state.getValue(NORTH));
			case FRONT_BACK:
				return state.setValue(EAST, state.getValue(WEST)).setValue(WEST, state.getValue(EAST));
			default:
				return super.mirror(state, mirror);
		}
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(NORTH, EAST, SOUTH, WEST);
	}
	
	protected static boolean shouldConnectTo(Level level, BlockPos pos)
	{
		BlockEntity blockEntity = level.getBlockEntity(pos);
		return level.getBlockState(pos).getBlock() instanceof CableBlock || (blockEntity != null && blockEntity.getCapability(ForgeCapabilities.ENERGY).isPresent());
	}
}
