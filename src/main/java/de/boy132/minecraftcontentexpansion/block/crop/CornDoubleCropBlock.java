package de.boy132.minecraftcontentexpansion.block.crop;

import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class CornDoubleCropBlock extends DoubleCropBlock
{
	public static final int MAX_AGE = 4;
	public static final IntegerProperty AGE = BlockStateProperties.AGE_4;
	
	private static final VoxelShape[] SHAPES_LOWER = new VoxelShape[] {
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 16.0D, 13.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 16.0D, 14.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 16.0D, 14.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 16.0D, 14.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 16.0D, 14.0D)
	};
	
	private static final VoxelShape[] SHAPES_UPPER = new VoxelShape[] {
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 1.0D, 13.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 5.0D, 14.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 10.0D, 14.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 12.0D, 14.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 12.0D, 14.0D)
	};
	
	public CornDoubleCropBlock(Properties properties)
	{
		super(properties);
	}
	
	@Override
	protected ItemLike getBaseSeedId()
	{
		return ModItems.CORN_SEEDS.get();
	}
	
	@Override
	public int getMaxAge()
	{
		return MAX_AGE;
	}
	
	@Override
	public IntegerProperty getAgeProperty()
	{
		return AGE;
	}
	
	@Override
	protected int getBonemealAgeIncrease(Level level)
	{
		return Mth.nextInt(level.random, 1, 2);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter getter, BlockPos pos, CollisionContext context)
	{
		int age = state.getValue(getAgeProperty());
		
		if(state.getValue(HALF) == DoubleBlockHalf.UPPER)
			return SHAPES_UPPER[age];
		
		return SHAPES_LOWER[age];
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(AGE);
	}
}
