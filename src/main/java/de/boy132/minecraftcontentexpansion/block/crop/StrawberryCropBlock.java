package de.boy132.minecraftcontentexpansion.block.crop;

import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class StrawberryCropBlock extends CropBlock
{
	public static final int MAX_AGE = 4;
	public static final IntegerProperty AGE = BlockStateProperties.AGE_4;
	
	private static final VoxelShape[] SHAPES = new VoxelShape[] {
			Block.box(0.2D * 16, 0.0D, 0.2D * 16, 0.8D * 16, 0.5D * 16, 0.8D * 16),
			Block.box(0.2D * 16, 0.0D, 0.2D * 16, 0.8D * 16, 0.6D * 16, 0.8D * 16),
			Block.box(0.15D * 16, 0.0D, 0.15D * 16, 0.85D * 16, 0.7D * 16, 0.85D * 16),
			Block.box(0.05D * 16, 0.0D, 0.05D * 16, 0.95D * 16, 0.9D * 16, 0.95D * 16),
			Block.box(0.05D * 16, 0.0D, 0.05D * 16, 0.95D * 16, 0.9D * 16, 0.95D * 16)
	};
	
	public StrawberryCropBlock(Properties properties)
	{
		super(properties);
	}
	
	@Override
	protected ItemLike getBaseSeedId()
	{
		return ModItems.STRAWBERRY_SEEDS.get();
	}
	
	@Override
	public int getMaxAge()
	{
		return MAX_AGE;
	}
	
	@Override
	public IntegerProperty getAgeProperty()
	{
		return AGE;
	}
	
	@Override
	protected int getBonemealAgeIncrease(Level level)
	{
		return Mth.nextInt(level.random, 1, 2);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter getter, BlockPos pos, CollisionContext context)
	{
		return SHAPES[state.getValue(getAgeProperty())];
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(AGE);
	}
}
