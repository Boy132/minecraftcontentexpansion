package de.boy132.minecraftcontentexpansion.block.crop;

import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class LettuceCropBlock extends CropBlock
{
	public static final int MAX_AGE = 7;
	public static final IntegerProperty AGE = BlockStateProperties.AGE_7;
	
	private static final VoxelShape[] SHAPES = new VoxelShape[] {
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 3.0D, 13.0D),
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 4.0D, 13.0D),
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 6.0D, 13.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 6.0D, 14.0D),
			Block.box(2.0D, 0.0D, 2.0D, 14.0D, 8.0D, 14.0D),
			Block.box(1.0D, 0.0D, 1.0D, 15.0D, 8.0D, 15.0D),
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 8.0D, 16.0D),
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 8.0D, 16.0D)
	};
	
	public LettuceCropBlock(Properties properties)
	{
		super(properties);
	}
	
	@Override
	protected ItemLike getBaseSeedId()
	{
		return ModItems.LETTUCE_SEEDS.get();
	}
	
	@Override
	public int getMaxAge()
	{
		return MAX_AGE;
	}
	
	@Override
	public IntegerProperty getAgeProperty()
	{
		return AGE;
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter getter, BlockPos pos, CollisionContext context)
	{
		return SHAPES[state.getValue(getAgeProperty())];
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(AGE);
	}
}
