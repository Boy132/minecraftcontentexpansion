package de.boy132.minecraftcontentexpansion.block.crop;

import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class OnionCropBlock extends CropBlock
{
	public static final int MAX_AGE = 6;
	public static final IntegerProperty AGE = IntegerProperty.create("age", 0, 6);
	
	private static final VoxelShape[] SHAPES = new VoxelShape[] {
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 3.0D, 13.0D),
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 5.0D, 13.0D),
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 7.0D, 13.0D),
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 10.0D, 13.0D),
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 13.0D, 13.0D),
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 15.0D, 13.0D),
			Block.box(3.0D, 0.0D, 3.0D, 13.0D, 15.0D, 13.0D)
	};
	
	public OnionCropBlock(Properties properties)
	{
		super(properties);
	}
	
	@Override
	protected ItemLike getBaseSeedId()
	{
		return ModItems.ONION_SEEDS.get();
	}
	
	@Override
	public int getMaxAge()
	{
		return MAX_AGE;
	}
	
	@Override
	public IntegerProperty getAgeProperty()
	{
		return AGE;
	}
	
	@Override
	protected int getBonemealAgeIncrease(Level level)
	{
		return Mth.nextInt(level.random, 2, 4);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter getter, BlockPos pos, CollisionContext context)
	{
		return SHAPES[state.getValue(getAgeProperty())];
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(AGE);
	}
}
