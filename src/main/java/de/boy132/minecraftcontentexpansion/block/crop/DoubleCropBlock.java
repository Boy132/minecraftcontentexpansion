package de.boy132.minecraftcontentexpansion.block.crop;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.BlockHitResult;

import javax.annotation.Nullable;

public abstract class DoubleCropBlock extends CropBlock
{
	public static final EnumProperty<DoubleBlockHalf> HALF = BlockStateProperties.DOUBLE_BLOCK_HALF;
	
	
	public DoubleCropBlock(Properties properties)
	{
		super(properties);
		registerDefaultState(stateDefinition.any().setValue(HALF, DoubleBlockHalf.LOWER));
	}
	
	@Override
	public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hitResult)
	{
		if(state.getValue(HALF) == DoubleBlockHalf.UPPER)
			return InteractionResult.PASS;
		
		return super.use(state, level, pos, player, hand, hitResult);
	}
	
	@Override
	public void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource randomSource)
	{
		if(!level.isAreaLoaded(pos, 1))
			return; // Forge: prevent loading unloaded chunks when checking neighbor's light
		
		if(state.getValue(HALF) == DoubleBlockHalf.UPPER)
			return;
		
		if(level.getRawBrightness(pos, 0) >= 9)
		{
			int i = getAge(state);
			if(i < getMaxAge())
			{
				float f = getGrowthSpeed(this, level, pos);
				if(net.minecraftforge.common.ForgeHooks.onCropsGrowPre(level, pos, state, randomSource.nextInt((int) (25.0F / f) + 1) == 0))
				{
					level.setBlock(pos, getStateForAge(i + 1).setValue(HALF, DoubleBlockHalf.LOWER), 2);
					level.setBlock(pos.above(), getStateForAge(i + 1).setValue(HALF, DoubleBlockHalf.UPPER), 2);
					net.minecraftforge.common.ForgeHooks.onCropsGrowPost(level, pos, state);
				}
			}
		}
	}
	
	@Override
	public void growCrops(Level level, BlockPos pos, BlockState state)
	{
		if(state.getValue(HALF) == DoubleBlockHalf.UPPER)
			return;
		
		int i = getAge(state) + getBonemealAgeIncrease(level);
		int j = getMaxAge();
		if(i > j)
			i = j;
		
		level.setBlock(pos, getStateForAge(i).setValue(HALF, DoubleBlockHalf.LOWER), 2);
		level.setBlock(pos.above(), getStateForAge(i).setValue(HALF, DoubleBlockHalf.UPPER), 2);
	}
	
	@Override
	public boolean isValidBonemealTarget(LevelReader levelReader, BlockPos pos, BlockState state)
	{
		return state.getValue(HALF) == DoubleBlockHalf.LOWER && super.isValidBonemealTarget(levelReader, pos, state);
	}
	
	@Override
	public boolean isBonemealSuccess(Level level, RandomSource randomSource, BlockPos pos, BlockState state)
	{
		return state.getValue(HALF) == DoubleBlockHalf.LOWER && super.isBonemealSuccess(level, randomSource, pos, state);
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction direction, BlockState facingState, LevelAccessor levelAccessor, BlockPos currentPos, BlockPos facingPos)
	{
		DoubleBlockHalf blockHalf = state.getValue(HALF);
		if(direction.getAxis() != Direction.Axis.Y || blockHalf == DoubleBlockHalf.LOWER != (direction == Direction.UP) || facingState.is(this) && facingState.getValue(HALF) != blockHalf)
			return blockHalf == DoubleBlockHalf.LOWER && direction == Direction.DOWN && !state.canSurvive(levelAccessor, currentPos) ? Blocks.AIR.defaultBlockState() : super.updateShape(state, direction, facingState, levelAccessor, currentPos, facingPos);
		
		return Blocks.AIR.defaultBlockState();
	}
	
	@Nullable
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		BlockPos blockpos = context.getClickedPos();
		Level level = context.getLevel();
		return blockpos.getY() < level.getMaxBuildHeight() - 1 && level.getBlockState(blockpos.above()).canBeReplaced(context) ? super.getStateForPlacement(context) : null;
	}
	
	private static BlockState copyWaterloggedFrom(LevelReader levelReader, BlockPos pos, BlockState state)
	{
		return state.hasProperty(BlockStateProperties.WATERLOGGED) ? state.setValue(BlockStateProperties.WATERLOGGED, Boolean.valueOf(levelReader.isWaterAt(pos))) : state;
	}
	
	@Override
	public void setPlacedBy(Level level, BlockPos pos, BlockState state, LivingEntity livingEntity, ItemStack stack)
	{
		BlockPos abovePos = pos.above();
		level.setBlock(abovePos, copyWaterloggedFrom(level, abovePos, defaultBlockState().setValue(HALF, DoubleBlockHalf.UPPER)), 3);
	}
	
	@Override
	public boolean canSurvive(BlockState state, LevelReader levelReader, BlockPos pos)
	{
		if(state.getValue(HALF) == DoubleBlockHalf.LOWER)
			return super.canSurvive(state, levelReader, pos);
		
		BlockState blockstate = levelReader.getBlockState(pos.below());
		if(state.getBlock() != this)
			return super.canSurvive(state, levelReader, pos); //Forge: This function is called during world gen and placement, before this block is set, so if we are not 'here' then assume it's the pre-check.
		
		return blockstate.is(this) && blockstate.getValue(HALF) == DoubleBlockHalf.LOWER;
	}
	
	public static void placeAt(LevelAccessor levelAccessor, BlockState state, BlockPos pos, int flags)
	{
		BlockPos abovePos = pos.above();
		levelAccessor.setBlock(pos, copyWaterloggedFrom(levelAccessor, pos, state.setValue(HALF, DoubleBlockHalf.LOWER)), flags);
		levelAccessor.setBlock(abovePos, copyWaterloggedFrom(levelAccessor, abovePos, state.setValue(HALF, DoubleBlockHalf.UPPER)), flags);
	}
	
	private static void preventCreativeDropFromBottomPart(Level level, BlockPos pos, BlockState state, Player player)
	{
		DoubleBlockHalf blockHalf = state.getValue(HALF);
		if(blockHalf == DoubleBlockHalf.UPPER)
		{
			BlockPos belowPos = pos.below();
			BlockState belowState = level.getBlockState(belowPos);
			if(belowState.is(state.getBlock()) && belowState.getValue(HALF) == DoubleBlockHalf.LOWER)
			{
				BlockState newState = belowState.getFluidState().is(Fluids.WATER) ? Blocks.WATER.defaultBlockState() : Blocks.AIR.defaultBlockState();
				level.setBlock(belowPos, newState, 35);
				level.levelEvent(player, 2001, belowPos, Block.getId(belowState));
			}
		}
	}
	
	@Override
	public BlockState playerWillDestroy(Level level, BlockPos pos, BlockState state, Player player)
	{
		if(!level.isClientSide)
		{
			if(player.isCreative())
				preventCreativeDropFromBottomPart(level, pos, state, player);
			else
				dropResources(state, level, pos, null, player, player.getMainHandItem());
		}
		
		return super.playerWillDestroy(level, pos, state, player);
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(HALF);
	}
	
	@Override
	public long getSeed(BlockState state, BlockPos pos)
	{
		return Mth.getSeed(pos.getX(), pos.below(state.getValue(HALF) == DoubleBlockHalf.LOWER ? 0 : 1).getY(), pos.getZ());
	}
}
