package de.boy132.minecraftcontentexpansion.block;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import de.boy132.minecraftcontentexpansion.block.cable.CableBlock;
import de.boy132.minecraftcontentexpansion.block.crop.*;
import de.boy132.minecraftcontentexpansion.block.dirt.CoarseDirtSlab;
import de.boy132.minecraftcontentexpansion.block.dirt.DirtPathSlab;
import de.boy132.minecraftcontentexpansion.block.dirt.DirtSlab;
import de.boy132.minecraftcontentexpansion.block.dirt.GrassBlockSlab;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.AdvancedEntityBlock;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.battery.BatteryBlock;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.coalgenerator.CoalGeneratorBlock;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricbrewery.ElectricBreweryBlock;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricgreenhouse.ElectricGreenhouseBlock;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.electricsmelter.ElectricSmelterBlock;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.hydraulicpress.HydraulicPressBlock;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.liquidtank.LiquidTankBlock;
import de.boy132.minecraftcontentexpansion.block.entity.advanced.solarpanel.SolarPanelBlock;
import de.boy132.minecraftcontentexpansion.block.entity.choppingblock.ChoppingBlock;
import de.boy132.minecraftcontentexpansion.block.entity.dryingrack.DryingRackBlock;
import de.boy132.minecraftcontentexpansion.block.entity.kiln.KilnBlock;
import de.boy132.minecraftcontentexpansion.block.entity.millstone.MillstoneBlock;
import de.boy132.minecraftcontentexpansion.block.entity.smelter.SmelterBlock;
import de.boy132.minecraftcontentexpansion.fluid.ModFluids;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.properties.NoteBlockInstrument;
import net.minecraft.world.level.material.MapColor;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public class ModBlocks
{
	public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<Block> UNIM_ORE = registerBlock("unim_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_ORE)));
	public static final RegistryObject<Block> DEEPSLATE_UNIM_ORE = registerBlock("deepslate_unim_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.DEEPSLATE_IRON_ORE)));
	public static final RegistryObject<Block> UNIM_BLOCK = registerBlock("unim_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_BLOCK).mapColor(MapColor.TERRACOTTA_LIGHT_GREEN)));
	
	public static final RegistryObject<Block> RUBY_ORE = registerBlock("ruby_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_ORE)));
	public static final RegistryObject<Block> DEEPSLATE_RUBY_ORE = registerBlock("deepslate_ruby_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.DEEPSLATE_IRON_ORE)));
	public static final RegistryObject<Block> RUBY_BLOCK = registerBlock("ruby_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.RAW_IRON_BLOCK).mapColor(MapColor.COLOR_RED)));
	
	public static final RegistryObject<Block> ASCABIT_ORE = registerBlock("ascabit_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_ORE)));
	public static final RegistryObject<Block> DEEPSLATE_ASCABIT_ORE = registerBlock("deepslate_ascabit_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.DEEPSLATE_IRON_ORE)));
	public static final RegistryObject<Block> ASCABIT_BLOCK = registerBlock("ascabit_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_BLOCK).mapColor(MapColor.COLOR_BLUE)));
	public static final RegistryObject<Block> RAW_ASCABIT_BLOCK = registerBlock("raw_ascabit_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.RAW_IRON_BLOCK).mapColor(MapColor.COLOR_BLUE)));
	
	public static final RegistryObject<Block> TIN_ORE = registerBlock("tin_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.COPPER_ORE)));
	public static final RegistryObject<Block> DEEPSLATE_TIN_ORE = registerBlock("deepslate_tin_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.DEEPSLATE_COPPER_ORE)));
	public static final RegistryObject<Block> TIN_BLOCK = registerBlock("tin_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.COPPER_BLOCK).mapColor(MapColor.COLOR_LIGHT_GRAY)));
	public static final RegistryObject<Block> RAW_TIN_BLOCK = registerBlock("raw_tin_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.RAW_COPPER_BLOCK).mapColor(MapColor.COLOR_LIGHT_GRAY)));
	
	public static final RegistryObject<Block> TITANIUM_ORE = registerBlock("titanium_ore", () -> new DropExperienceBlock(UniformInt.of(3, 7), BlockBehaviour.Properties.ofFullCopy(Blocks.DIAMOND_ORE)));
	public static final RegistryObject<Block> DEEPSLATE_TITANIUM_ORE = registerBlock("deepslate_titanium_ore", () -> new DropExperienceBlock(UniformInt.of(3, 7), BlockBehaviour.Properties.ofFullCopy(Blocks.DEEPSLATE_DIAMOND_ORE)));
	public static final RegistryObject<Block> TITANIUM_BLOCK = registerBlock("titanium_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.DIAMOND_BLOCK).mapColor(MapColor.COLOR_CYAN)));
	public static final RegistryObject<Block> RAW_TITANIUM_BLOCK = registerBlock("raw_titanium_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.RAW_IRON_BLOCK).mapColor(MapColor.COLOR_CYAN)));
	
	public static final RegistryObject<Block> ZINC_ORE = registerBlock("zinc_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.COPPER_ORE)));
	public static final RegistryObject<Block> DEEPSLATE_ZINC_ORE = registerBlock("deepslate_zinc_ore", () -> new DropExperienceBlock(ConstantInt.of(0), BlockBehaviour.Properties.ofFullCopy(Blocks.DEEPSLATE_COPPER_ORE)));
	public static final RegistryObject<Block> ZINC_BLOCK = registerBlock("zinc_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.COPPER_BLOCK).mapColor(MapColor.COLOR_LIGHT_BLUE)));
	public static final RegistryObject<Block> RAW_ZINC_BLOCK = registerBlock("raw_zinc_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.RAW_COPPER_BLOCK).mapColor(MapColor.COLOR_LIGHT_BLUE)));
	
	public static final RegistryObject<Block> STEEL_BLOCK = registerBlock("steel_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_BLOCK).mapColor(MapColor.COLOR_GRAY)));
	
	public static final RegistryObject<Block> BRONZE_BLOCK = registerBlock("bronze_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_BLOCK).mapColor(MapColor.TERRACOTTA_BROWN)));
	
	public static final RegistryObject<Block> BRASS_BLOCK = registerBlock("brass_block", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_BLOCK).mapColor(MapColor.TERRACOTTA_ORANGE)));
	
	public static final RegistryObject<Block> CEMENT = registerBlock("cement", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.GRAY_CONCRETE).mapColor(MapColor.COLOR_GRAY)));
	public static final RegistryObject<Block> CEMENT_STAIRS = registerBlock("cement_stairs", () -> new StairBlock(CEMENT.get().defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(CEMENT.get())));
	public static final RegistryObject<Block> CEMENT_SLAB = registerBlock("cement_slab", () -> new SlabBlock(BlockBehaviour.Properties.ofFullCopy(CEMENT.get())));
	public static final RegistryObject<Block> CEMENT_WALL = registerBlock("cement_wall", () -> new WallBlock(BlockBehaviour.Properties.ofFullCopy(CEMENT.get())));
	
	public static final RegistryObject<Block> CEMENT_PLATE = registerBlock("cement_plate", () -> new Block(BlockBehaviour.Properties.ofFullCopy(ModBlocks.CEMENT.get())));
	public static final RegistryObject<Block> CEMENT_PLATE_STAIRS = registerBlock("cement_plate_stairs", () -> new StairBlock(CEMENT_PLATE.get().defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(CEMENT_PLATE.get())));
	public static final RegistryObject<Block> CEMENT_PLATE_SLAB = registerBlock("cement_plate_slab", () -> new SlabBlock(BlockBehaviour.Properties.ofFullCopy(CEMENT_PLATE.get())));
	public static final RegistryObject<Block> CEMENT_PLATE_WALL = registerBlock("cement_plate_wall", () -> new WallBlock(BlockBehaviour.Properties.ofFullCopy(CEMENT_PLATE.get())));
	
	public static final RegistryObject<Block> LIMESTONE = registerBlock("limestone", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.TUFF).mapColor(MapColor.COLOR_LIGHT_GRAY)));
	public static final RegistryObject<Block> LIMESTONE_STAIRS = registerBlock("limestone_stairs", () -> new StairBlock(LIMESTONE.get().defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(LIMESTONE.get())));
	public static final RegistryObject<Block> LIMESTONE_SLAB = registerBlock("limestone_slab", () -> new SlabBlock(BlockBehaviour.Properties.ofFullCopy(LIMESTONE.get())));
	public static final RegistryObject<Block> LIMESTONE_WALL = registerBlock("limestone_wall", () -> new WallBlock(BlockBehaviour.Properties.ofFullCopy(LIMESTONE.get())));
	
	public static final RegistryObject<Block> LIMESTONE_BRICKS = registerBlock("limestone_bricks", () -> new Block(BlockBehaviour.Properties.ofFullCopy(ModBlocks.LIMESTONE.get())));
	public static final RegistryObject<Block> LIMESTONE_BRICK_STAIRS = registerBlock("limestone_brick_stairs", () -> new StairBlock(LIMESTONE_BRICKS.get().defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(LIMESTONE_BRICKS.get())));
	public static final RegistryObject<Block> LIMESTONE_BRICK_SLAB = registerBlock("limestone_brick_slab", () -> new SlabBlock(BlockBehaviour.Properties.ofFullCopy(LIMESTONE_BRICKS.get())));
	public static final RegistryObject<Block> LIMESTONE_BRICK_WALL = registerBlock("limestone_brick_wall", () -> new WallBlock(BlockBehaviour.Properties.ofFullCopy(LIMESTONE_BRICKS.get())));
	
	public static final RegistryObject<Block> MARBLE = registerBlock("marble", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.TUFF).mapColor(MapColor.COLOR_LIGHT_GRAY)));
	public static final RegistryObject<Block> MARBLE_STAIRS = registerBlock("marble_stairs", () -> new StairBlock(MARBLE.get().defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(MARBLE.get())));
	public static final RegistryObject<Block> MARBLE_SLAB = registerBlock("marble_slab", () -> new SlabBlock(BlockBehaviour.Properties.ofFullCopy(MARBLE.get())));
	public static final RegistryObject<Block> MARBLE_WALL = registerBlock("marble_wall", () -> new WallBlock(BlockBehaviour.Properties.ofFullCopy(MARBLE.get())));
	
	public static final RegistryObject<Block> MARBLE_COBBLE = registerBlock("marble_cobble", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.TUFF).mapColor(MapColor.COLOR_LIGHT_GRAY)));
	public static final RegistryObject<Block> MARBLE_COBBLE_STAIRS = registerBlock("marble_cobble_stairs", () -> new StairBlock(MARBLE_COBBLE.get().defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(MARBLE_COBBLE.get())));
	public static final RegistryObject<Block> MARBLE_COBBLE_SLAB = registerBlock("marble_cobble_slab", () -> new SlabBlock(BlockBehaviour.Properties.ofFullCopy(MARBLE_COBBLE.get())));
	public static final RegistryObject<Block> MARBLE_COBBLE_WALL = registerBlock("marble_cobble_wall", () -> new WallBlock(BlockBehaviour.Properties.ofFullCopy(MARBLE_COBBLE.get())));
	
	public static final RegistryObject<Block> MARBLE_BRICKS = registerBlock("marble_bricks", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.CALCITE).mapColor(MapColor.TERRACOTTA_WHITE)));
	public static final RegistryObject<Block> MARBLE_BRICK_STAIRS = registerBlock("marble_brick_stairs", () -> new StairBlock(MARBLE_BRICKS.get().defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(MARBLE_BRICKS.get())));
	public static final RegistryObject<Block> MARBLE_BRICK_SLAB = registerBlock("marble_brick_slab", () -> new SlabBlock(BlockBehaviour.Properties.ofFullCopy(MARBLE_BRICKS.get())));
	public static final RegistryObject<Block> MARBLE_BRICK_WALL = registerBlock("marble_brick_wall", () -> new WallBlock(BlockBehaviour.Properties.ofFullCopy(MARBLE_BRICKS.get())));
	
	public static final RegistryObject<Block> MARBLE_LAMP = registerBlock("marble_lamp", () -> new Block(BlockBehaviour.Properties.ofFullCopy(MARBLE_BRICKS.get()).lightLevel(state -> 13)));
	
	public static final RegistryObject<Block> DIRT_SLAB = registerBlock("dirt_slab", () -> new DirtSlab(BlockBehaviour.Properties.ofFullCopy(Blocks.DIRT)));
	public static final RegistryObject<Block> COARSE_DIRT_SLAB = registerBlock("coarse_dirt_slab", () -> new CoarseDirtSlab(BlockBehaviour.Properties.ofFullCopy(Blocks.COARSE_DIRT)));
	public static final RegistryObject<Block> DIRT_PATH_SLAB = registerBlock("dirt_path_slab", () -> new DirtPathSlab(BlockBehaviour.Properties.ofFullCopy(Blocks.DIRT_PATH)));
	public static final RegistryObject<Block> GRASS_BLOCK_SLAB = registerBlock("grass_block_slab", () -> new GrassBlockSlab(BlockBehaviour.Properties.ofFullCopy(Blocks.GRASS_BLOCK)));
	
	public static final RegistryObject<Block> GLASS_SLAB = registerBlock("glass_slab", () -> new SlabBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.GLASS).noOcclusion()));
	
	public static final RegistryObject<Block> STONE_DOOR = BLOCKS.register("stone_door", () -> new DoorBlock(ModBlockSetTypes.STONE, BlockBehaviour.Properties.ofFullCopy(Blocks.STONE).strength(3.0F).noOcclusion()));
	public static final RegistryObject<Block> STONE_TRAPDOOR = registerBlock("stone_trapdoor", () -> new TrapDoorBlock(ModBlockSetTypes.STONE, BlockBehaviour.Properties.ofFullCopy(Blocks.STONE).strength(3.0F).noOcclusion().isValidSpawn((state, getter, pos, entityType) -> false)));
	
	public static final RegistryObject<Block> SANDSTONE_DOOR = BLOCKS.register("sandstone_door", () -> new DoorBlock(ModBlockSetTypes.SANDSTONE, BlockBehaviour.Properties.ofFullCopy(Blocks.SANDSTONE).strength(3.0F).noOcclusion()));
	public static final RegistryObject<Block> SANDSTONE_TRAPDOOR = registerBlock("sandstone_trapdoor", () -> new TrapDoorBlock(ModBlockSetTypes.SANDSTONE, BlockBehaviour.Properties.ofFullCopy(Blocks.SANDSTONE).strength(3.0F).noOcclusion().isValidSpawn((state, getter, pos, entityType) -> false)));
	
	public static final RegistryObject<Block> GLASS_DOOR = BLOCKS.register("glass_door", () -> new DoorBlock(ModBlockSetTypes.GLASS, BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_DOOR)));
	public static final RegistryObject<Block> GLASS_TRAPDOOR = registerBlock("glass_trapdoor", () -> new TrapDoorBlock(ModBlockSetTypes.GLASS, BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_TRAPDOOR)));
	
	public static final RegistryObject<Block> WOODEN_GLASS_FRAME = registerBlock("wooden_glass_frame", () -> new IronBarsBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.GLASS_PANE)));
	
	public static final RegistryObject<Block> KILN = registerBlock("kiln", () -> new KilnBlock(BlockBehaviour.Properties.of().mapColor(MapColor.COLOR_RED).sound(SoundType.STONE).requiresCorrectToolForDrops().noOcclusion().strength(3F).lightLevel(state -> state.getValue(KilnBlock.LIT) ? 13 : 0)));
	public static final RegistryObject<Block> SMELTER = registerBlock("smelter", () -> new SmelterBlock(BlockBehaviour.Properties.of().mapColor(MapColor.STONE).sound(SoundType.STONE).requiresCorrectToolForDrops().strength(4F).lightLevel(state -> state.getValue(SmelterBlock.LIT) ? 13 : 0)));
	public static final RegistryObject<Block> MILLSTONE = registerBlock("millstone", () -> new MillstoneBlock(BlockBehaviour.Properties.of().mapColor(MapColor.STONE).sound(SoundType.STONE).requiresCorrectToolForDrops().noOcclusion().strength(3.5F)));
	
	public static final RegistryObject<Block> OAK_CHOPPING_BLOCK = registerBlock("oak_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.STRIPPED_OAK_WOOD).noOcclusion()));
	public static final RegistryObject<Block> SPRUCE_CHOPPING_BLOCK = registerBlock("spruce_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.STRIPPED_SPRUCE_WOOD).noOcclusion()));
	public static final RegistryObject<Block> BIRCH_CHOPPING_BLOCK = registerBlock("birch_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.STRIPPED_BIRCH_WOOD).noOcclusion()));
	public static final RegistryObject<Block> JUNGLE_CHOPPING_BLOCK = registerBlock("jungle_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.STRIPPED_JUNGLE_WOOD).noOcclusion()));
	public static final RegistryObject<Block> ACACIA_CHOPPING_BLOCK = registerBlock("acacia_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.STRIPPED_ACACIA_WOOD).noOcclusion()));
	public static final RegistryObject<Block> DARK_OAK_CHOPPING_BLOCK = registerBlock("dark_oak_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.STRIPPED_DARK_OAK_WOOD).noOcclusion()));
	public static final RegistryObject<Block> MANGROVE_CHOPPING_BLOCK = registerBlock("mangrove_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.of().mapColor(MapColor.COLOR_RED).instrument(NoteBlockInstrument.BASS).strength(2.0F).sound(SoundType.WOOD).ignitedByLava().noOcclusion()));
	public static final RegistryObject<Block> CHERRY_CHOPPING_BLOCK = registerBlock("cherry_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.STRIPPED_CHERRY_WOOD).noOcclusion()));
	public static final RegistryObject<Block> CRIMSON_CHOPPING_BLOCK = registerBlock("crimson_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.STRIPPED_CRIMSON_HYPHAE).noOcclusion()));
	public static final RegistryObject<Block> WARPED_CHOPPING_BLOCK = registerBlock("warped_chopping_block", () -> new ChoppingBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.STRIPPED_WARPED_HYPHAE).noOcclusion()));
	
	public static final RegistryObject<Block> OAK_DRYING_RACK = registerBlock("oak_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_PLANKS).noOcclusion()));
	public static final RegistryObject<Block> SPRUCE_DRYING_RACK = registerBlock("spruce_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.SPRUCE_PLANKS).noOcclusion()));
	public static final RegistryObject<Block> BIRCH_DRYING_RACK = registerBlock("birch_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.BIRCH_PLANKS).noOcclusion()));
	public static final RegistryObject<Block> JUNGLE_DRYING_RACK = registerBlock("jungle_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.JUNGLE_PLANKS).noOcclusion()));
	public static final RegistryObject<Block> ACACIA_DRYING_RACK = registerBlock("acacia_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.ACACIA_PLANKS).noOcclusion()));
	public static final RegistryObject<Block> DARK_OAK_DRYING_RACK = registerBlock("dark_oak_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.DARK_OAK_PLANKS).noOcclusion()));
	public static final RegistryObject<Block> MANGROVE_DRYING_RACK = registerBlock("mangrove_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.MANGROVE_PLANKS).noOcclusion()));
	public static final RegistryObject<Block> CHERRY_DRYING_RACK = registerBlock("cherry_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.CHERRY_PLANKS).noOcclusion()));
	public static final RegistryObject<Block> CRIMSON_DRYING_RACK = registerBlock("crimson_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.CRIMSON_PLANKS).noOcclusion()));
	public static final RegistryObject<Block> WARPED_DRYING_RACK = registerBlock("warped_drying_rack", () -> new DryingRackBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.WARPED_PLANKS).noOcclusion()));
	
	public static final RegistryObject<Block> MACHINE_FRAME = registerBlock("machine_frame", () -> new Block(BlockBehaviour.Properties.of().mapColor(MapColor.METAL).sound(SoundType.METAL).requiresCorrectToolForDrops().strength(5F)));
	
	public static final RegistryObject<Block> COAL_GENERATOR = registerBlock("coal_generator", () -> new CoalGeneratorBlock(BlockBehaviour.Properties.of().mapColor(MapColor.METAL).sound(SoundType.METAL).requiresCorrectToolForDrops().strength(5F).lightLevel(state -> state.getValue(CoalGeneratorBlock.LIT) ? 13 : 0)));
	public static final RegistryObject<Block> BATTERY = registerBlock("battery", () -> new BatteryBlock(BlockBehaviour.Properties.of().mapColor(MapColor.METAL).sound(SoundType.METAL).requiresCorrectToolForDrops().strength(5F)));
	public static final RegistryObject<Block> SOLAR_PANEL = registerBlock("solar_panel", () -> new SolarPanelBlock(BlockBehaviour.Properties.of().mapColor(MapColor.METAL).sound(SoundType.METAL).requiresCorrectToolForDrops().strength(3F)));
	
	public static final RegistryObject<Block> LIQUID_TANK = registerBlock("liquid_tank", () -> new LiquidTankBlock(BlockBehaviour.Properties.of().mapColor(MapColor.METAL).sound(SoundType.METAL).requiresCorrectToolForDrops().strength(5F).noOcclusion())); // TODO: change light level when filled with lava
	
	public static final RegistryObject<Block> ELECTRIC_BREWERY = registerBlock("electric_brewery", () -> new ElectricBreweryBlock(BlockBehaviour.Properties.of().mapColor(MapColor.METAL).sound(SoundType.METAL).requiresCorrectToolForDrops().strength(5F)));
	public static final RegistryObject<Block> ELECTRIC_SMELTER = registerBlock("electric_smelter", () -> new ElectricSmelterBlock(BlockBehaviour.Properties.of().mapColor(MapColor.METAL).sound(SoundType.METAL).requiresCorrectToolForDrops().strength(5F).lightLevel(state -> state.getValue(ElectricSmelterBlock.LIT) ? 13 : 0)));
	public static final RegistryObject<Block> ELECTRIC_GREENHOUSE = registerBlock("electric_greenhouse", () -> new ElectricGreenhouseBlock(BlockBehaviour.Properties.of().mapColor(MapColor.METAL).sound(SoundType.METAL).requiresCorrectToolForDrops().strength(5F).noOcclusion()));
	
	public static final RegistryObject<Block> HYDRAULIC_PRESS = registerBlock("hydraulic_press", () -> new HydraulicPressBlock(BlockBehaviour.Properties.of().mapColor(MapColor.METAL).sound(SoundType.METAL).requiresCorrectToolForDrops().strength(5F)));
	
	public static final RegistryObject<Block> COPPER_CABLE = registerBlock("copper_cable", () -> new CableBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.REDSTONE_WIRE).strength(0.3F).sound(SoundType.COPPER)));
	
	public static final RegistryObject<CropBlock> TOMATOES = BLOCKS.register("tomatoes", () -> new TomatoCropBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.WHEAT)));
	public static final RegistryObject<CropBlock> GRAPES = BLOCKS.register("grapes", () -> new GrapeCropBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.WHEAT)));
	public static final RegistryObject<CropBlock> STRAWBERRIES = BLOCKS.register("strawberries", () -> new StrawberryCropBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.WHEAT)));
	public static final RegistryObject<CropBlock> CORNS = BLOCKS.register("corns", () -> new CornDoubleCropBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.WHEAT)));
	public static final RegistryObject<CropBlock> LETTUCES = BLOCKS.register("lettuces", () -> new LettuceCropBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.WHEAT)));
	public static final RegistryObject<CropBlock> ONIONS = BLOCKS.register("onions", () -> new OnionCropBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.WHEAT)));
	public static final RegistryObject<CropBlock> RICE = BLOCKS.register("rice", () -> new RiceCropBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.WHEAT)));
	
	public static final RegistryObject<Block> CHOCOLATE_CAKE = registerBlock("chocolate_cake", () -> new CakeWithoutCandlesBlock(BlockBehaviour.Properties.ofFullCopy(Blocks.CAKE).noLootTable()));
	
	public static final RegistryObject<LiquidBlock> OIL = BLOCKS.register("oil", () -> new LiquidBlock(ModFluids.OIL, BlockBehaviour.Properties.ofFullCopy(Blocks.WATER).mapColor(MapColor.COLOR_BLACK).noLootTable()));
	
	private static <T extends Block> RegistryObject<T> registerBlock(String name, Supplier<T> block)
	{
		RegistryObject<T> registeredBlock = BLOCKS.register(name, block);
		registerBlockItem(name, registeredBlock);
		return registeredBlock;
	}
	
	private static <T extends Block> RegistryObject<Item> registerBlockItem(String name, RegistryObject<T> block)
	{
		return ModItems.ITEMS.register(name, () -> new BlockItem(block.get(), new Item.Properties().rarity(block.get() instanceof AdvancedEntityBlock ? Rarity.RARE : block.get() == MACHINE_FRAME.get() ? Rarity.UNCOMMON : Rarity.COMMON).stacksTo(block.get() instanceof CakeBlock ? 1 : 64)));
	}
	
	public static void register(IEventBus eventBus)
	{
		BLOCKS.register(eventBus);
	}
}