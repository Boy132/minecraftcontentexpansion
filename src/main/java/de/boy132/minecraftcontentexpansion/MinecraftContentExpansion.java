package de.boy132.minecraftcontentexpansion;

import de.boy132.minecraftcontentexpansion.block.ModBlocks;
import de.boy132.minecraftcontentexpansion.block.entity.ModBlockEntities;
import de.boy132.minecraftcontentexpansion.config.ModConfigValues;
import de.boy132.minecraftcontentexpansion.datagen.recipe.condition.ModConditions;
import de.boy132.minecraftcontentexpansion.entchantments.ModEnchantments;
import de.boy132.minecraftcontentexpansion.entity.ModEntityTypes;
import de.boy132.minecraftcontentexpansion.fluid.ModFluidTypes;
import de.boy132.minecraftcontentexpansion.fluid.ModFluids;
import de.boy132.minecraftcontentexpansion.item.ModCreativeModeTabs;
import de.boy132.minecraftcontentexpansion.item.ModItems;
import de.boy132.minecraftcontentexpansion.item.bucket.ModBucketItem;
import de.boy132.minecraftcontentexpansion.loot.ModGlobalLootModifierSerializers;
import de.boy132.minecraftcontentexpansion.loot.condition.ModLootItemConditionTypes;
import de.boy132.minecraftcontentexpansion.networking.ModNetworkMessages;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeSerializers;
import de.boy132.minecraftcontentexpansion.recipe.ModRecipeTypes;
import de.boy132.minecraftcontentexpansion.screen.ModMenuTypes;
import de.boy132.minecraftcontentexpansion.stats.ModStats;
import net.minecraft.world.level.block.ComposterBlock;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModContainer;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(MinecraftContentExpansion.MODID)
public class MinecraftContentExpansion
{
	public static final String MODID = "minecraftcontentexpansion";
	
	public static ModContainer CONTAINER;
	
	public MinecraftContentExpansion(FMLJavaModLoadingContext context)
	{
		CONTAINER = context.getContainer();
		
		IEventBus modEventBus = context.getModEventBus();
		
		/*===================== Registries =====================*/
		ModItems.register(modEventBus);
		ModBlocks.register(modEventBus);
		
		ModFluidTypes.register(modEventBus);
		ModFluids.register(modEventBus);
		
		ModBlockEntities.register(modEventBus);
		ModMenuTypes.register(modEventBus);
		
		ModEntityTypes.register(modEventBus);
		
		ModConditions.register(modEventBus);
		
		ModRecipeSerializers.register(modEventBus);
		ModRecipeTypes.register(modEventBus);
		
		ModLootItemConditionTypes.register(modEventBus);
		ModGlobalLootModifierSerializers.register(modEventBus);
		
		ModEnchantments.register(modEventBus);
		
		ModCreativeModeTabs.register(modEventBus);
		
		ModStats.register(modEventBus);
		
		/*===================== Register Event Bus =====================*/
		modEventBus.addListener(this::commonSetup);
		modEventBus.addListener(this::enqueueIMC);
		
		/*===================== Register Config =====================*/
		context.registerConfig(ModConfig.Type.CLIENT, ModConfigValues.clientConfig);
		context.registerConfig(ModConfig.Type.COMMON, ModConfigValues.commonConfig);
	}
	
	private void commonSetup(FMLCommonSetupEvent event)
	{
		event.enqueueWork(() ->
		{
			ModNetworkMessages.register();
			
			ModBucketItem.registerDispenserBehaviorForEmpty((ModBucketItem) ModItems.CLAY_BUCKET.get());
			ModBucketItem.registerDispenserBehaviorForFilled((ModBucketItem) ModItems.WATER_CLAY_BUCKET.get());
			
			ComposterBlock.COMPOSTABLES.put(ModItems.TOMATO_SEEDS.get(), 0.3F);
			ComposterBlock.COMPOSTABLES.put(ModItems.TOMATO.get(), 0.65F);
			
			ComposterBlock.COMPOSTABLES.put(ModItems.GRAPE_SEEDS.get(), 0.3F);
			ComposterBlock.COMPOSTABLES.put(ModItems.GRAPE.get(), 0.65F);
			
			ComposterBlock.COMPOSTABLES.put(ModItems.STRAWBERRY_SEEDS.get(), 0.3F);
			ComposterBlock.COMPOSTABLES.put(ModItems.STRAWBERRY.get(), 0.65F);
			
			ComposterBlock.COMPOSTABLES.put(ModItems.CORN_SEEDS.get(), 0.3F);
			ComposterBlock.COMPOSTABLES.put(ModItems.CORN.get(), 0.65F);
			
			ComposterBlock.COMPOSTABLES.put(ModItems.LETTUCE_SEEDS.get(), 0.3F);
			ComposterBlock.COMPOSTABLES.put(ModItems.LETTUCE.get(), 0.65F);
			
			ComposterBlock.COMPOSTABLES.put(ModItems.ONION_SEEDS.get(), 0.3F);
			ComposterBlock.COMPOSTABLES.put(ModItems.ONION.get(), 0.65F);
			
			ComposterBlock.COMPOSTABLES.put(ModItems.RICE.get(), 0.65F);
			
			ComposterBlock.COMPOSTABLES.put(ModBlocks.CHOCOLATE_CAKE.get().asItem(), 1F);
			
			ComposterBlock.COMPOSTABLES.put(ModItems.APPLE_PIE.get(), 1F);
			ComposterBlock.COMPOSTABLES.put(ModItems.STRAWBERRY_PIE.get(), 1F);
			
			ModStats.get();
		});
	}
	
	private void enqueueIMC(InterModEnqueueEvent event)
	{
	
	}
}
