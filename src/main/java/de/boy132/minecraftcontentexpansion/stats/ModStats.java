package de.boy132.minecraftcontentexpansion.stats;

import de.boy132.minecraftcontentexpansion.MinecraftContentExpansion;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.stats.StatFormatter;
import net.minecraft.stats.Stats;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

import java.util.HashMap;
import java.util.Map;

public class ModStats
{
	private static final Map<ResourceLocation, StatFormatter> FORMATTERS = new HashMap<>();
	
	private static final DeferredRegister<ResourceLocation> CUSTOM_STATS = DeferredRegister.create(Registries.CUSTOM_STAT, MinecraftContentExpansion.MODID);
	
	public static final RegistryObject<ResourceLocation> CHOPPED_WOOD = registerStat("chopped_wood", StatFormatter.DEFAULT);
	
	private static RegistryObject<ResourceLocation> registerStat(String name, StatFormatter statFormatter)
	{
		RegistryObject<ResourceLocation> registryObject = CUSTOM_STATS.register(name, () -> ResourceLocation.fromNamespaceAndPath(MinecraftContentExpansion.MODID, name));
		FORMATTERS.put(registryObject.getId(), statFormatter);
		return registryObject;
	}
	
	public static void get()
	{
		CUSTOM_STATS.getEntries().forEach((registryObject) -> Stats.CUSTOM.get(registryObject.get(), FORMATTERS.get(registryObject.getId())));
	}
	
	public static void register(IEventBus eventBus)
	{
		CUSTOM_STATS.register(eventBus);
	}
}
