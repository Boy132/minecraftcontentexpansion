![logo](/src/main/resources/logo.png)

# Welcome to MinecraftContentExpansion (or MCE)

Visit our wiki for more information!

If you want to report a bug or a glitch go to our Issue page and write us your problem.

## Dependencies

Required:

* Minecraft 1.20.4
* [Forge 49.0.38](https://files.minecraftforge.net/net/minecraftforge/forge/)

Optional:

* [Just Enough Items (JEI)](https://modrinth.com/mod/jei)
* [Jade](https://modrinth.com/mod/jade)
* [Curios](https://modrinth.com/mod/curios)
* [Enchantment Descriptions](https://modrinth.com/mod/enchantment-descriptions)
* [Configured](https://www.curseforge.com/minecraft/mc-mods/configured)

## Translations

![english](https://img.shields.io/poeditor/progress/249295/en?token=24bd47545443e57248129895196d54f0)
![german](https://img.shields.io/poeditor/progress/249295/de?token=24bd47545443e57248129895196d54f0)

[Help us translate the mod to other languages](https://poeditor.com/join/project/tY2034qKfH), like:  
![french](https://img.shields.io/poeditor/progress/249295/fr?token=24bd47545443e57248129895196d54f0)
![polish](https://img.shields.io/poeditor/progress/249295/pl?token=24bd47545443e57248129895196d54f0)
![portuguese_br](https://img.shields.io/poeditor/progress/249295/pt-br?token=24bd47545443e57248129895196d54f0)
![spanish_mx](https://img.shields.io/poeditor/progress/249295/es-mx?token=24bd47545443e57248129895196d54f0)
![chinese_simple](https://img.shields.io/poeditor/progress/249295/zh-Hans?token=24bd47545443e57248129895196d54f0)
